# Flutter优化

- https://tech.meituan.com/2020/09/18/flutter-in-meituan.html

# 打包：

- 进入example目录
- （推荐）执行(只打armeabi-v7a):`flutter build apk --release --target-platform android-arm --split-per-abi`
- 执行(打包所有):`flutter build apk --release`
- 执行(只打x86_64):`flutter build apk --release --target-platform android-x64 --split-per-abi`
- apk位置：`xxx\uni-anchor\example\build\app\outputs\apk\release`
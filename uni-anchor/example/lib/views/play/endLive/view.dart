import 'dart:convert';
import 'dart:math';

import 'package:agora_rtc_rawdata_example/views/processImg/ProcessImg.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/views/login/controller.dart';

import 'package:agora_rtc_rawdata_example/views/mine/controller.dart';
import 'package:agora_rtc_rawdata_example/views/play/endLive/controller.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class EndLiveScreen extends GetView<EndLiveController> {
  const EndLiveScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: Colors.white,
      body: WillPopScope(
          onWillPop: () async {
            // Get.offAllNamed("/layout");
            return false;
          },
        child:GetX<EndLiveController>(
          init: controller,
          builder: (_) => SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: width(30)).copyWith(top: width(212)),
                alignment: Alignment.centerLeft,
                child: Column(
                    children: [
                      Container(
                        width: width(160),
                        height: width(160),
                        clipBehavior: Clip.hardEdge,
                        decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(60))
                        ),
                        margin: EdgeInsets.only(right: width(30)),
                        child: MyImage(url: controller.userInfo.value.avatar.toString(),id: 'end'),
                        // ProcessImgView(url: controller.userInfo.value.avatar.toString(),id: 'c2',),
                      ),
                      SizedBox(height: width(15),),
                      Container(
                        alignment: Alignment.center,
                        child: Text(
                          "${controller.userInfo.value.nickName??''}",
                          style: TextStyle(
                            fontSize: sp(28),
                            fontWeight: FontWeight.w400,
                            color: const Color(0xff000000),
                          ),
                        ),
                      ),
                      SizedBox(height: width(46),),
                      Container(
                        alignment: Alignment.center,
                        child: Text(
                          "直播结束",
                          style: TextStyle(
                            fontSize: sp(40),
                            fontWeight: FontWeight.w400,
                            color: const Color(0xff000000),
                          ),
                        ),
                      ),
                      SizedBox(height: width(90),),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Column(
                              children: [
                                Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    "${controller.liveCount.value}",
                                    style: TextStyle(
                                      fontSize: sp(28),
                                      fontWeight: FontWeight.w400,
                                      color: const Color(0xff00BF4D),
                                    ),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    "直播场次",
                                    style: TextStyle(
                                      fontSize: sp(28),
                                      fontWeight: FontWeight.w400,
                                      color: const Color(0xff000000),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Column(
                              children: [
                                Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    "${controller.userCount.value}",
                                    style: TextStyle(
                                      fontSize: sp(28),
                                      fontWeight: FontWeight.w400,
                                      color: const Color(0xffFF0000),
                                    ),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    "总观看人数",
                                    style: TextStyle(
                                      fontSize: sp(28),
                                      fontWeight: FontWeight.w400,
                                      color: const Color(0xff000000),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Column(
                              children: [
                                Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    "${controller.moneyNumber.value}",

                                    style: TextStyle(
                                      fontSize: sp(28),
                                      fontWeight: FontWeight.w400,
                                      color: const Color(0xff5302FF),
                                    ),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    "礼物价值",
                                    style: TextStyle(
                                      fontSize: sp(28),
                                      fontWeight: FontWeight.w400,
                                      color: const Color(0xff000000),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Column(
                              children: [
                                Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    "${controller.fansCount.value}",
                                    style: TextStyle(
                                      fontSize: sp(28),
                                      fontWeight: FontWeight.w400,
                                      color: const Color(0xffFF8A00),
                                    ),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    "订阅人数",
                                    style: TextStyle(
                                      fontSize: sp(28),
                                      fontWeight: FontWeight.w400,
                                      color: const Color(0xff000000),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: width(102),),
                      InkWell(
                        onTap: () {
                          Get.offAllNamed("/layout");
                        },
                        child: Container(
                          margin: EdgeInsets.symmetric(horizontal: width(20)),
                          height: width(80),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(40)),
                            color: Color(0xff9F44FF)
                          ),
                          child: Text(
                            "返回首页",
                            style: TextStyle(
                              fontSize: sp(40),
                              fontWeight: FontWeight.w400,
                              color: const Color(0xffFFFFFF),
                            ),
                          ),
                        ),
                      )
                    ],
                ),
              )))),
    );
  }
}

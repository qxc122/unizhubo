import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/models/user_model.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';

class EndLiveController extends GetxController {
  RxString titleString = "out".obs;
  RxString avatar = "out".obs;
  RxString fansCount = '0'.obs;
  RxString liveCount = '0'.obs;
  RxString liveTime = '0'.obs;
  RxString moneyNumber = '0'.obs;
  RxString userCount = '0'.obs;
  var userInfo = Rx<UserModel>(UserModel());

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
    getInit();
  }

  getInit() async {
    var res = await API.getUserInfo();
    print(res.toString());
    if (res['code'] == 200) {
      var user = UserModel.fromJson(res['data']);
      userInfo.value = user;
      update();
    }
  }
}

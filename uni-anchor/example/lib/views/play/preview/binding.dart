import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/play/preview/controller.dart';
class Previewbinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PreviewController>(()=> PreviewController());
  }
}

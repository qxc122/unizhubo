import 'dart:developer';

import 'package:agora_rtc_rawdata/agora_rtc_rawdata.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';
import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;
import 'package:agora_rtc_rawdata/agora_rtc_rawdata.dart';
import 'package:permission_handler/permission_handler.dart';

class PreviewController extends GetxController {
  RxString titleString = "out".obs;
  RxInt remoteUidM = 0.obs;
  RxBool localUserJoined = false.obs;
  RxBool startPreview = false.obs;
  RxBool isJoined = false.obs;
  RxList remoteUid = [].obs;
  late RtcEngine _engine;
  get engine => this._engine;

  RxString appId = "22b193762fc34ee59bcca01a1abdb790".obs; /// b646785207b84110be190b025b1f4b3c
  RxString token = "007eJxTYFB4rd1TpLqNm83+KTebtxFf8ozrBW6mQoFlPIXLG3N0dRUYjIySDC2Nzc2M0pKNTVJTTS2TkpMTDQwTDROTUpLMLQ3OTd2c3BDIyJD3YAcrIwMEgvgsDLmJRcUMDADtgRwU".obs;
  RxString channel = "2022".obs; /// 2021

  @override
  void onInit() {
    super.onInit();
    initAgora();
  }

  @override
  void onReady() {
    // 调用
  }

  @override
  void onClose() {
    deinitEngine();
  }

  deinitEngine() async {
    await AgoraRtcRawdata.unregisterAudioFrameObserver();
    await AgoraRtcRawdata.unregisterVideoFrameObserver();
    await _engine.leaveChannel();
    await _engine.stopPreview();
    await _engine.destroy();
  }

  initAgora() async {
    // await [Permission.microphone, Permission.camera].request();
    if (defaultTargetPlatform == TargetPlatform.android) {
      await [Permission.microphone, Permission.camera].request();
    }
    _engine = await RtcEngine.create(appId.value);
    _engine.setEventHandler(
        RtcEngineEventHandler(joinChannelSuccess: (channel, uid, elapsed) {
      log('joinChannelSuccess $channel $uid $elapsed');
      isJoined.value = true;
    }, userJoined: (uid, elapsed) {
      log('userJoined  $uid $elapsed');
      remoteUid.add(uid);
    }, userOffline: (uid, reason) {
      log('userJoined  $uid $reason');
      remoteUid.removeWhere((element) => element == uid);
    }));
    await _engine.enableVideo();
    await _engine.startPreview();
    startPreview.value = true;
    var handle = await _engine.getNativeHandle();
    if (handle != null) {
      await AgoraRtcRawdata.registerAudioFrameObserver(handle);
      await AgoraRtcRawdata.registerVideoFrameObserver(handle);
    }
    await _engine.joinChannel(null, channel.value, null, 0);
    //关闭本地声音
    await _engine.muteLocalAudioStream(true);
    //关闭远程声音
    await _engine.muteRemoteAudioStream(0, true);
    // 声网相关视频设置
    VideoEncoderConfiguration videoConfig = VideoEncoderConfiguration(
      frameRate: VideoFrameRate.Fps30,
      bitrate: 3240,
      dimensions: VideoDimensions(width: 1280, height: 720),
    );
    await _engine.setVideoEncoderConfiguration(videoConfig);
  }
}

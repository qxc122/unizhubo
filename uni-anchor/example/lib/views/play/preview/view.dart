import 'dart:convert';
import 'dart:math';
import 'dart:developer';

import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;
import 'package:agora_rtc_rawdata/agora_rtc_rawdata.dart';
import 'package:agora_rtc_rawdata_example/config/agora.config.dart' as config;
import 'package:faceunity_ui/Faceunity_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/views/login/controller.dart';

import 'package:agora_rtc_rawdata_example/views/mine/controller.dart';
import 'package:agora_rtc_rawdata_example/views/play/preview/controller.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class PreviewScreen extends GetView<PreviewController> {
  const PreviewScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(()=>Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            // if (controller.startPreview.value)
            controller.startPreview.value ? RtcLocalView.SurfaceView(): Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              // child:  Image.asset(
              //   "images/live_bg.png",
              //   fit: BoxFit.cover,
              // ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: List.of(controller.remoteUid.map(
                        (e) => Container(
                      width: 120,
                      height: 120,
                      child: RtcRemoteView.SurfaceView(
                          uid: e,
                          channelId: controller.channel.value
                      ),
                    ),
                  )),
                ),
              ),
            ),
            //传camera 回调显示 UI，不传不显示
            FaceunityUI(
              cameraCallback: () => controller.engine.switchCamera(),
            )
            // Positioned(
            //   right: width(26),
            //   bottom: width(370),
            //   child: Column(
            //     children: [
            //       Container(
            //         width: width(200),
            //         padding: EdgeInsets.symmetric(horizontal: width(12)).copyWith(bottom: width(10),top: width(30)),
            //         decoration:  const BoxDecoration(
            //             borderRadius: BorderRadius.all(Radius.circular(5)),
            //             color: Color.fromRGBO(255, 255, 255, 0.5)
            //         ),
            //         child: Column(
            //           children: [
            //             InkWell(
            //               onTap: () {
            //                 Get.toNamed("/liveRoom");
            //               },
            //               child: Container(
            //                 margin:EdgeInsets.only(top: width(14)),
            //                 height: width(60),
            //                 alignment: Alignment.center,
            //                 decoration:  BoxDecoration(
            //                     borderRadius: BorderRadius.all(Radius.circular(6)),
            //                     color: Color.fromRGBO(255, 255, 255, 0.63),
            //                     border: Border.all(color: Color(0xff9F44FF),width: width(1))
            //                 ),
            //                 child: Text(
            //                   "绿播",
            //                   style: TextStyle(
            //                     fontSize: sp(32),
            //                     fontWeight: FontWeight.w400,
            //                     color: const Color(0xff9F44FF),
            //                   ),
            //                 ),
            //               ),
            //             ),
            //             InkWell(
            //               onTap: () {
            //                 Get.toNamed("/liveRoom");
            //               },
            //               child: Container(
            //                 margin:EdgeInsets.only(top: width(14)),
            //                 height: width(60),
            //                 alignment: Alignment.center,
            //                 decoration:  BoxDecoration(
            //                     borderRadius: BorderRadius.all(Radius.circular(6)),
            //                     color: Color.fromRGBO(255, 255, 255, 0.63),
            //                     border: Border.all(color: Color(0xff9F44FF),width: width(1))
            //                 ),
            //                 child: Text(
            //                   "黄播",
            //                   style: TextStyle(
            //                     fontSize: sp(32),
            //                     fontWeight: FontWeight.w400,
            //                     color: const Color(0xff9F44FF),
            //                   ),
            //                 ),
            //               ),
            //             ),
            //             InkWell(
            //               onTap: () {
            //                 Get.toNamed("/liveRoom");
            //               },
            //               child: Container(
            //                 margin:EdgeInsets.only(top: width(14)),
            //                 height: width(60),
            //                 alignment: Alignment.center,
            //                 decoration:  BoxDecoration(
            //                     borderRadius: BorderRadius.all(Radius.circular(6)),
            //                     color: Color.fromRGBO(255, 255, 255, 0.63),
            //                     border: Border.all(color: Color(0xff9F44FF),width: width(1))
            //                 ),
            //                 child: Text(
            //                   "无人直播",
            //                   style: TextStyle(
            //                     fontSize: sp(32),
            //                     fontWeight: FontWeight.w400,
            //                     color: const Color(0xff9F44FF),
            //                   ),
            //                 ),
            //               ),
            //             ),
            //           ],
            //         ),
            //       )
            //     ],
            //   ),
            // ),
            // Positioned(
            //   right: width(26),
            //   bottom: width(600),
            //   child: Column(
            //     children: [
            //       Container(
            //         width: width(200),
            //         height: width(60),
            //         alignment: Alignment.center,
            //         decoration: const BoxDecoration(
            //           borderRadius: BorderRadius.all(Radius.circular(30)),
            //           color: Color(0xff9F44FF)
            //         ),
            //         child: Text(
            //           "开始直播",
            //           style: TextStyle(
            //             fontSize: sp(32),
            //             fontWeight: FontWeight.w400,
            //             color: const Color(0xffFFFFFF),
            //           ),
            //         ),
            //       ),
            //     ],
            //   ),
            // ),
          ],
        )));
  }
}

import 'dart:convert';
import 'dart:math';

import 'package:agora_rtc_rawdata_example/views/processImg/ProcessImg.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/play/controller.dart';

import '../../utils/screen.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class PlayScreen extends GetView<PlayController> {
  const PlayScreen({Key? key}) : super(key: key);

  _buildHome(String title) {
    return SingleChildScrollView(
        child: Container(
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(
                        color: const Color(0xffEEEEEE), width: width(1)))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: width(32)),
                  alignment: Alignment.centerLeft,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            controller.changelottery();
                          },
                          child: Column(
                            children: [
                              controller.lotteryId.value != 0 ?Container(
                                width: width(80),
                                height: width(80),
                                child: MyImage(url: controller.icon.value.toString(),id: controller.icon.value.toString(),),
                                // Image.network(
                                //   "${controller.icon.value}",
                                // ),
                              ): Container(
                                width: width(64),
                                height: width(80),
                                child: Image.asset(
                                  "images/icon_setting_game.webp",
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: width(22)),
                                child: Text(
                                  "${controller.name.value}",
                                  style: TextStyle(
                                    fontSize: sp(28),
                                    fontWeight: FontWeight.w400,
                                    height: 1.0,
                                    color: const Color(0xff000000),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            Get.toNamed("/preview");
                          },
                          child: Column(
                            children: [
                              Container(
                                width: width(64),
                                height: width(80),
                                child: Image.asset(
                                  "images/my.png",
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: width(22)),
                                child: Text(
                                  "美颜",
                                  style: TextStyle(
                                    fontSize: sp(28),
                                    fontWeight: FontWeight.w400,
                                    height: 1.0,
                                    color: const Color(0xff000000),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                          child: InkWell(
                        onTap: () {
                          controller.setPicLog();
                        },
                        child: Column(
                          children: [
                            controller.studioThumbImage.value == null ||  controller.studioThumbImage.value == ""
                                ? Container(
                                    width: width(64),
                                    height: width(80),
                                    child: Image.asset(
                                      "images/icon_setting_thumb.png",
                                    ),
                                  )
                                : Container(
                                    width: width(64),
                                    height: width(80),
                                    child: MyImage(url: controller.studioThumbImage.value.toString(),id: controller.studioThumbImage.value.toString(),),
                                    // Image.network(
                                    //   "${controller.studioThumbImage.value}",
                                    // ),
                                  ),
                            Container(
                              margin: EdgeInsets.only(top: width(22)),
                              child: Text(
                                "切换封面",
                                style: TextStyle(
                                  fontSize: sp(28),
                                  fontWeight: FontWeight.w400,
                                  height: 1.0,
                                  color: const Color(0xff000000),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ))
                    ],
                  ),
                ),
                Container(
                  height: width(20),
                  color: const Color(0xffF9F9F9),
                ),
                // list 列表
                Container(
                  alignment: Alignment.centerLeft,
                  decoration: BoxDecoration(
                      border: Border(
                          top: BorderSide(
                              color: const Color(0xffEEEEEE),
                              width: width(1)))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          controller.setTitleLog();
                        },
                        child: Container(
                          height: width(100),
                          padding: EdgeInsets.symmetric(horizontal: width(25)),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      color: const Color(0xffEEEEEE),
                                      width: width(1)))),
                          child: Row(
                            children: [
                              Container(
                                child: Text(
                                  "直播标题",
                                  style: TextStyle(
                                    fontSize: sp(28),
                                    fontWeight: FontWeight.w400,
                                    height: 1.0,
                                    color: const Color(0xff000000),
                                  ),
                                ),
                              ),
                              Expanded(
                                  child: Container(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  "${controller.title.value}",
                                  style: TextStyle(
                                    fontSize: sp(28),
                                    fontWeight: FontWeight.w400,
                                    color: controller.title.value !='输入标题' ? const Color(0xff000000) : const Color(0xffCBCBCB),
                                  ),
                                ),
                              ))
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          controller.setQualityLog();
                        },
                        child: Container(
                          height: width(100),
                          padding: EdgeInsets.symmetric(horizontal: width(25)),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      color: const Color(0xffEEEEEE),
                                      width: width(1)))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Text(
                                  "直播质量",
                                  style: TextStyle(
                                    fontSize: sp(28),
                                    fontWeight: FontWeight.w400,
                                    height: 1.0,
                                    color: const Color(0xff000000),
                                  ),
                                ),
                              ),
                              Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(right: width(14)),
                                    child: Text(
                                      "${controller.qualityList.value[controller.currentQuality.value]}",
                                      style: TextStyle(
                                        fontSize: sp(28),
                                        fontWeight: FontWeight.w400,
                                        height: 1.0,
                                        color: const Color(0xff000000),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width: width(32),
                                    height: width(32),
                                    child: Image.asset(
                                      "images/right_arr.webp",
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          controller.setRoomTypeLog();
                        },
                        child: Container(
                          height: width(100),
                          padding: EdgeInsets.symmetric(horizontal: width(25)),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      color: const Color(0xffEEEEEE),
                                      width: width(1)))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Text(
                                  "房间类型",
                                  style: TextStyle(
                                    fontSize: sp(28),
                                    fontWeight: FontWeight.w400,
                                    height: 1.0,
                                    color: const Color(0xff000000),
                                  ),
                                ),
                              ),
                              Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(right: width(14)),
                                    child: Text(
                                      "${controller.roomTypeText.value}",
                                      style: TextStyle(
                                        fontSize: sp(28),
                                        fontWeight: FontWeight.w400,
                                        height: 1.0,
                                        color: const Color(0xff000000),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width: width(32),
                                    height: width(32),
                                    child: Image.asset(
                                      "images/right_arr.webp",
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      controller.roomTypeText.value  != "免费" ? InkWell(
                        onTap: () {
                          controller.setTrySeeTimeLog();
                        },
                        child: Container(
                          height: width(100),
                          padding: EdgeInsets.symmetric(horizontal: width(25)),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      color: const Color(0xffEEEEEE),
                                      width: width(1)))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Text(
                                  "试看时间",
                                  style: TextStyle(
                                    fontSize: sp(28),
                                    fontWeight: FontWeight.w400,
                                    height: 1.0,
                                    color: const Color(0xff000000),
                                  ),
                                ),
                              ),
                              Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(right: width(14)),
                                    child: Text(
                                      "${controller.trySeeTime.value}秒",
                                      style: TextStyle(
                                        fontSize: sp(28),
                                        fontWeight: FontWeight.w400,
                                        height: 1.0,
                                        color: const Color(0xff000000),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width: width(32),
                                    height: width(32),
                                    child: Image.asset(
                                      "images/right_arr.webp",
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ):Container()
                    ],
                  ),
                ),

                // 直播类型
                SizedBox(
                  height: width(28),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ...controller.tabs.value.asMap().entries.map((element) {
                      return InkWell(
                        onTap: () {
                          controller.changeIndex(element.key);
                        },
                        child: Container(
                          width: width(200),
                          height: width(60),
                          alignment: Alignment.center,
                          margin: EdgeInsets.symmetric(horizontal: width(12)),
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: const Color(0xff9F44FF),
                                  width: width(2)),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(30)),
                              color:
                                  element.key == controller.currentIndex.value
                                      ? Color(0xff9F44FF)
                                      : Colors.white),
                          child: Text(
                            "${element.value}",
                            style: TextStyle(
                              fontSize: sp(28),
                              fontWeight: FontWeight.w400,
                              height: 1.2,
                              color:
                                  element.key == controller.currentIndex.value
                                      ? Colors.white
                                      : Color(0xff9F44FF),
                            ),
                          ),
                        ),
                      );
                    }),
                  ],
                ),
                SizedBox(
                  height: width(76),
                ),
                // 按钮
                InkWell(
                  onTap: () {
                    Get.toNamed("/preview");
                  },
                  child: Container(
                    height: width(80),
                    alignment: Alignment.center,
                    margin: EdgeInsets.symmetric(horizontal: width(45)),
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: const Color(0xff9F44FF), width: width(1)),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(30)),
                        color: const Color(0xff9F44FF)),
                    child: Text(
                      "预览",
                      style: TextStyle(
                        fontSize: sp(36),
                        fontWeight: FontWeight.w400,
                        height: 1.2,
                        color: Colors.white,
                      ),
                    ),
                  ),
                )
              ],
            )));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          title: Text('开播设置',
              style: TextStyle(
                  color: Color(0xff000000),
                  fontSize: sp(34),
                  fontWeight: FontWeight.bold)),
          backgroundColor: Colors.white,
        ),
        body: GetX<PlayController>(
            init: controller,
            builder: (_) => SingleChildScrollView(
                child: _buildHome(controller.titleString.value))));
  }
}

import 'dart:convert';

import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/views/play/liveRoom/controller.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

class MedalView extends GetView<LiveRoomController> {
  final String level;
  const MedalView({
    Key? key,
    required this.level,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width(110),
      height: width(30),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          image: int.parse("${level}") < 10 ? DecorationImage(
            image:  AssetImage("images/vip1.png"),
                fit: BoxFit.cover,
          ): int.parse("${level}") >= 10 && int.parse("${level}") < 20 ? DecorationImage(
              image:  AssetImage("images/vip2.png"),
            fit: BoxFit.cover,
          ): int.parse("${level}") >= 20 && int.parse("${level}") < 40 ? DecorationImage(
              image:  AssetImage("images/vip3.png"),
            fit: BoxFit.cover,
          ): int.parse("${level}") >= 40 && int.parse("${level}") < 60 ? DecorationImage(
              image:  AssetImage("images/vip4.png"),
            fit: BoxFit.cover,
          ): int.parse("${level}") >= 60 && int.parse("${level}") < 90 ? DecorationImage(
              image:  AssetImage("images/vip5.png"),
            fit: BoxFit.cover,
          ): int.parse("${level}") >= 90 && int.parse("${level}") < 110 ? DecorationImage(
              image:  AssetImage("images/vip6.png"),
            fit: BoxFit.cover,
          ): int.parse("${level}") >= 110 && int.parse("${level}") < 130 ? DecorationImage(
              image:  AssetImage("images/vip7.png"),
            fit: BoxFit.cover,
          ): int.parse("${level}") >= 130 && int.parse("${level}") < 160 ? DecorationImage(
              image:  AssetImage("images/vip8.png"),
            fit: BoxFit.cover,
          ): int.parse("${level}") >= 160 && int.parse("${level}") < 190 ? DecorationImage(
              image:  AssetImage("images/vip9.png"),
            fit: BoxFit.cover,
          ): int.parse("${level}") >= 190 && int.parse("${level}") < 220 ? DecorationImage(
              image:  AssetImage("images/vip10.png"),
            fit: BoxFit.cover,
          ):  int.parse("${level}") >= 220 && int.parse("${level}") < 250 ? DecorationImage(
              image:  AssetImage("images/vip11.png"),
            fit: BoxFit.cover,
          ): int.parse("${level}") >= 250 && int.parse("${level}") < 280 ? DecorationImage(
              image:  AssetImage("images/vip12.png"),
            fit: BoxFit.cover,
          ):DecorationImage(
              image:  AssetImage("images/vip13.png"),
            fit: BoxFit.cover,
          )
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(left: width(32)),
            child: Text(
              "VIP ${level == '0' || level == '-1' ? '1': level}",
              style: TextStyle(
                fontSize: sp(20),
                fontWeight: FontWeight.w400,
                color: const Color(0xffFFFFFF),
              ),
            ),
          )
        ],
      ),
    );
  }
}

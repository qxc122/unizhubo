import 'package:agora_rtc_rawdata_example/models/barrage_model.dart';
import 'package:agora_rtc_rawdata_example/models/game_model.dart';
import 'package:agora_rtc_rawdata_example/models/gift_model.dart';
import 'package:agora_rtc_rawdata_example/views/play/liveRoom/medal.dart';
import 'package:agora_rtc_rawdata_example/views/processImg/ProcessImg.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/play/controller.dart';
import 'package:agora_rtc_rawdata_example/views/play/liveRoom/controller.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../utils/screen.dart';

class GiftLog extends StatelessWidget {
  var controller = Get.find<LiveRoomController>();

  @override
  Widget build(BuildContext context) {
    return Obx(() => Container(
          alignment: Alignment.bottomCenter,
          height: width(1000),
          padding: EdgeInsets.symmetric(horizontal: width(10)),
          decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15), topRight: Radius.circular(15)),
              color: Color.fromRGBO(0, 0, 0, 0.5)),
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(top: width(28), bottom: width(20)),
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            color: const Color(0xff484848), width: width(1)))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    ...controller.tabs.asMap().entries.map((e) {
                      return InkWell(
                        onTap: () {
                          controller.changeIndex(e.key);
                        },
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(bottom: width(4)),
                              child: Text(
                                "${e.value}",
                                style: TextStyle(
                                  fontSize: sp(24),
                                  fontWeight: FontWeight.w400,
                                  color: const Color(0xffffffff),
                                ),
                              ),
                            ),
                            e.key == controller.currentIndex.value
                                ? Container(
                                    width: width(40),
                                    height: width(4),
                                    decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(4)),
                                        gradient: LinearGradient(
                                            begin: Alignment.centerLeft,
                                            end: Alignment.centerRight,
                                            colors: [
                                              Color(0xff6129FF),
                                              Color(0xffD96CFF)
                                            ])),
                                  )
                                : Container(
                                    width: width(40),
                                    height: width(4),
                                  )
                          ],
                        ),
                      );
                    }),
                  ],
                ),
              ),
              controller.currentIndex.value == 0
                  ? GameView()
                  : controller.currentIndex.value == 1
                      ? BarrageView()
                      : GiftView(),
              Container(
                margin: EdgeInsets.only(top: width(30), bottom: width(30)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: width(400),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                            onTap: () {
                              controller.switchFlash();
                            },
                            child: Container(
                              width: width(45),
                              height: width(45),
                              margin: EdgeInsets.only(
                                  right: width(22), top: width(40)),
                              child: controller.torchOn.value
                                  ? Image.asset(
                                      "images/ic_live_flash_open.png",
                                    )
                                  : Image.asset(
                                      "images/ic_live_flash_close.png",
                                    ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              controller.switchCamera();
                            },
                            child: Container(
                              width: width(45),
                              height: width(45),
                              margin: EdgeInsets.only(
                                  right: width(22), top: width(40)),
                              child: Image.asset(
                                "images/live_switch_camera.webp",
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              controller.switchSpeaker();
                            },
                            child: Container(
                              width: width(45),
                              height: width(45),
                              margin: EdgeInsets.only(
                                  right: width(22), top: width(40)),
                              child: controller.speakerFlag.value
                                  ? Image.asset(
                                      "images/ic_live_speaker_close.png",
                                    )
                                  : Image.asset(
                                      "images/ic_live_speaker_open.png",
                                    ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }

  /// 游戏
  Widget GameView() {
    return Expanded(
        child: Container(
      margin: EdgeInsets.only(top: width(30)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: width(40),
            alignment: Alignment.center,
            margin: EdgeInsets.only(bottom: width(20)),
            child:
            ListView.builder(
              scrollDirection: Axis.horizontal, // 横向滚动
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () {
                    controller.changeGame(controller.lotteryList[index]);
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: width(20)),
                    height: width(40),
                    alignment: Alignment.center,
                    decoration: int.parse(controller.tid.value.toString()) == int.parse(controller.lotteryList[index].lotteryId.toString()) ? BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(width(30))),
                        gradient: LinearGradient(
                          stops: [0.03, 0.97],
                            begin:
                            Alignment.centerLeft,
                            end:
                            Alignment.centerRight,
                            colors: [
                              Color(0xff6129FF),
                              Color(0xffD96CFF)
                            ]),
                    ):BoxDecoration(),
                    child: Text(
                      "${controller.lotteryList[index].name}",
                      style: TextStyle(
                        fontSize: sp(28),
                        fontWeight: FontWeight.w400,
                        color: const Color(0xffFFFFFF),
                      ),
                    ),
                  ),
                );
              },
              itemCount: controller.lotteryList.length,
            ),
          ),
          Container(
            alignment: Alignment.topLeft,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(width(60))),
                color: Colors.black),
            height: width(60),
            padding: EdgeInsets.symmetric(horizontal: width(30)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(),
                Container(
                  child: Text(
                    "封盘",
                    style: TextStyle(
                      fontSize: sp(28),
                      fontWeight: FontWeight.w400,
                      color: const Color(0xffFFFFFF),
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
              child: Container(
                child: SmartRefresher(
                  enablePullDown: true,
                  enablePullUp: true,
                  header: WaterDropHeader(
                    refresh: CircularProgressIndicator(
                        strokeWidth: 2,
                        valueColor: AlwaysStoppedAnimation(Color(0xff999999))),
                    complete: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.done,
                          color: Colors.grey,
                          size: sp(30),
                        ),
                        Container(width: 30.0),
                        Text("加载完成",
                            style: TextStyle(
                                fontSize: sp(28), color: Color(0xff999999)))
                      ],
                    ),
                  ),
                  footer: CustomFooter(
                    builder: (BuildContext context, LoadStatus? mode) {
                      Widget body;
                      if (mode == LoadStatus.idle) {
                        body = Text(
                          "下拉加载更多",
                          style:
                          TextStyle(color: Color(0xff999999), fontSize: sp(28)),
                        );
                      } else if (mode == LoadStatus.loading) {
                        body = CupertinoActivityIndicator();
                      } else if (mode == LoadStatus.failed) {
                        body = Text(
                          "加载失败，点击重试",
                          style:
                          TextStyle(color: Color(0xff999999), fontSize: sp(28)),
                        );
                      } else if (mode == LoadStatus.canLoading) {
                        body = Text("释放加载更多",
                            style: TextStyle(
                                color: Color(0xff999999), fontSize: sp(28)));
                      } else {
                        body = Text(
                          "暂无更多数据",
                          style:
                          TextStyle(color: Color(0xff999999), fontSize: sp(28)),
                        );
                      }
                      return Container(
                        height: 55.0,
                        child: Center(child: body),
                      );
                    },
                  ),
                  controller: controller.refreshController,
                  onRefresh: () {
                    controller.getLogList(1);
                  },
                  onLoading: () {
                    controller.getLogList(controller.pageNum.value + 1);
                  },
                  child: controller.gameList.isEmpty
                      ? GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      controller.getLogList(1);
                    },
                    child: controller.isLoading.value
                        ? Center(
                      child: CupertinoActivityIndicator(),
                    )
                        : empty(),
                  )
                      : ListView.separated(
                    physics: ScrollPhysics(),
                    itemBuilder: (c, i) => GameItem(controller.gameList[i]),
                    itemCount: controller.gameList.length,
                    separatorBuilder: (context, i) => Container(),
                  ),
                ),
              ))
        ],
      ),
    ));
  }

  Widget GameItem(GameModel e) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: width(30)).copyWith(top: width(20)),
      alignment: Alignment.centerLeft,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Row(
              children: [
                Container(
                  width: width(36),
                  height: width(36),
                  child:
                  // MyImage(url: e.avatar.toString(),id: 'online' + element.userId.toString())
                  Image.asset(
                    "images/live_game_item_icon.webp",
                  )
                ),
                Container(
                  margin: EdgeInsets.only(left: width(10)),
                  width: width(200),
                  child: Text(
                    " ${e.userName}",
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: sp(28),
                      fontWeight: FontWeight.w400,
                      color: const Color(0xffFFFFFF),
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    " ${e.betTime}",
                    style: TextStyle(
                      fontSize: sp(28),
                      fontWeight: FontWeight.w400,
                      color: const Color(0xffFFFFFF),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Text(
              "投注：¥ ${e.betAmount}",
              style: TextStyle(
                fontSize: sp(28),
                fontWeight: FontWeight.w400,
                color: const Color(0xffFFFFFF),
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// 弹幕
  Widget BarrageView() {
    return Expanded(
        child: Container(
          margin: EdgeInsets.only(top: width(30)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                  child: Container(
                    child: SmartRefresher(
                      enablePullDown: true,
                      enablePullUp: true,
                      header: WaterDropHeader(
                        refresh: CircularProgressIndicator(
                            strokeWidth: 2,
                            valueColor: AlwaysStoppedAnimation(Color(0xff999999))),
                        complete: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.done,
                              color: Colors.grey,
                              size: sp(30),
                            ),
                            Container(width: 30.0),
                            Text("加载完成",
                                style: TextStyle(
                                    fontSize: sp(28), color: Color(0xff999999)))
                          ],
                        ),
                      ),
                      footer: CustomFooter(
                        builder: (BuildContext context, LoadStatus? mode) {
                          Widget body;
                          if (mode == LoadStatus.idle) {
                            body = Text(
                              "下拉加载更多",
                              style:
                              TextStyle(color: Color(0xff999999), fontSize: sp(28)),
                            );
                          } else if (mode == LoadStatus.loading) {
                            body = CupertinoActivityIndicator();
                          } else if (mode == LoadStatus.failed) {
                            body = Text(
                              "加载失败，点击重试",
                              style:
                              TextStyle(color: Color(0xff999999), fontSize: sp(28)),
                            );
                          } else if (mode == LoadStatus.canLoading) {
                            body = Text("释放加载更多",
                                style: TextStyle(
                                    color: Color(0xff999999), fontSize: sp(28)));
                          } else {
                            body = Text(
                              "暂无更多数据",
                              style:
                              TextStyle(color: Color(0xff999999), fontSize: sp(28)),
                            );
                          }
                          return Container(
                            height: 55.0,
                            child: Center(child: body),
                          );
                        },
                      ),
                      controller: controller.refreshController,
                      onRefresh: () {
                        controller.getLogList(1);
                      },
                      onLoading: () {
                        controller.getLogList(controller.pageNum.value + 1);
                      },
                      child: controller.barrageList.isEmpty
                          ? GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onTap: () {
                          controller.getLogList(1);
                        },
                        child: controller.isLoading.value
                            ? Center(
                          child: CupertinoActivityIndicator(),
                        )
                            : empty(),
                      )
                          : ListView.separated(
                        physics: ScrollPhysics(),
                        itemBuilder: (c, i) => BarrageItem(controller.barrageList[i],i),
                        itemCount: controller.barrageList.length,
                        separatorBuilder: (context, i) => Container(),
                      ),
                    ),
                  ))
            ],
          ),
        ));
  }

  Widget BarrageItem(BarrageModel e, int i) {
    return Container(
      margin: EdgeInsets.only(bottom: width(20)),
      padding: EdgeInsets.symmetric(horizontal: width(30)),
      child: Row(
        children: [
          Container(
            width: width(100),
            height: width(100),
            margin: EdgeInsets.only(right: width(10)),
            alignment: Alignment.center,
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(
                borderRadius:
                BorderRadius.circular(50)),
            child: MyImage(url: e.avatar.toString(),id: 'barrage' + i.toString())
            // Image.network(
            //     "${e.avatar}",
            //     errorBuilder: (ctx, err, stackTrace) =>
            //         Image.asset(
            //           'images/icon_contribute_def_head.webp',
            //           width: width(160),
            //           height: width(160),
            //         )
            // ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  MedalView(level: e.userLevel.toString()),
                  Container(
                    margin: EdgeInsets.only(left: width(10)),
                    child: Text(
                      " ${e.userName}",
                      style: TextStyle(
                        fontSize: sp(28),
                        fontWeight: FontWeight.w400,
                        color: const Color(0xffFFFFFF),
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: width(10)),
                child: Text(
                  " ${e.barrageContent}",
                  style: TextStyle(
                    fontSize: sp(26),
                    fontWeight: FontWeight.w400,
                    color: const Color(0xffFFFFFF),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  /// 礼物
  Widget GiftView() {
    return Expanded(
        child: Container(
      margin: EdgeInsets.only(top: width(30)),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                    "时间",
                    style: TextStyle(
                      fontSize: sp(28),
                      fontWeight: FontWeight.w400,
                      color: const Color(0xffFFFFFF),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                    "昵称",
                    style: TextStyle(
                      fontSize: sp(28),
                      fontWeight: FontWeight.w400,
                      color: const Color(0xffFFFFFF),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                    "礼物",
                    style: TextStyle(
                      fontSize: sp(28),
                      fontWeight: FontWeight.w400,
                      color: const Color(0xffFFFFFF),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                    "数量",
                    style: TextStyle(
                      fontSize: sp(28),
                      fontWeight: FontWeight.w400,
                      color: const Color(0xffFFFFFF),
                    ),
                  ),
                ),
              ),
              Expanded(
                  child: Container(
                alignment: Alignment.center,
                child: Text(
                  "价值(钻石)",
                  style: TextStyle(
                    fontSize: sp(28),
                    fontWeight: FontWeight.w400,
                    color: const Color(0xffFFFFFF),
                  ),
                ),
              )),
            ],
          ),
          Expanded(
              child: Container(
            child: SmartRefresher(
              enablePullDown: true,
              enablePullUp: true,
              header: WaterDropHeader(
                refresh: CircularProgressIndicator(
                    strokeWidth: 2,
                    valueColor: AlwaysStoppedAnimation(Color(0xff999999))),
                complete: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.done,
                      color: Colors.grey,
                      size: sp(30),
                    ),
                    Container(width: 30.0),
                    Text("加载完成",
                        style: TextStyle(
                            fontSize: sp(28), color: Color(0xff999999)))
                  ],
                ),
              ),
              footer: CustomFooter(
                builder: (BuildContext context, LoadStatus? mode) {
                  Widget body;
                  if (mode == LoadStatus.idle) {
                    body = Text(
                      "下拉加载更多",
                      style:
                          TextStyle(color: Color(0xff999999), fontSize: sp(28)),
                    );
                  } else if (mode == LoadStatus.loading) {
                    body = CupertinoActivityIndicator();
                  } else if (mode == LoadStatus.failed) {
                    body = Text(
                      "加载失败，点击重试",
                      style:
                          TextStyle(color: Color(0xff999999), fontSize: sp(28)),
                    );
                  } else if (mode == LoadStatus.canLoading) {
                    body = Text("释放加载更多",
                        style: TextStyle(
                            color: Color(0xff999999), fontSize: sp(28)));
                  } else {
                    body = Text(
                      "暂无更多数据",
                      style:
                          TextStyle(color: Color(0xff999999), fontSize: sp(28)),
                    );
                  }
                  return Container(
                    height: 55.0,
                    child: Center(child: body),
                  );
                },
              ),
              controller: controller.refreshController,
              onRefresh: () {
                controller.getLogList(1);
              },
              onLoading: () {
                controller.getLogList(controller.pageNum.value + 1);
              },
              child: controller.giftList.isEmpty
                  ? GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () {
                        controller.getLogList(1);
                      },
                      child: controller.isLoading.value
                          ? Center(
                              child: CupertinoActivityIndicator(),
                            )
                          : empty(),
                    )
                  : ListView.separated(
                      physics: ScrollPhysics(),
                      itemBuilder: (c, i) => GiftItem(controller.giftList[i]),
                      itemCount: controller.giftList.length,
                      separatorBuilder: (context, i) => Container(),
                    ),
            ),
          ))
        ],
      ),
    ));
  }

  Widget GiftItem(GiftModel e) {
    return Container(
      margin: EdgeInsets.only(top: width(20)),
      height: width(72),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(width(36))),
        color: Colors.black
      ),
      child: Row(
        children: [
          Expanded(
            child: Container(
              alignment: Alignment.center,
              child: Text(
                "${e.givingTime}",
                style: TextStyle(
                  fontSize: sp(28),
                  fontWeight: FontWeight.w400,
                  color: const Color(0xffFFFFFF),
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.center,
              child: Text(
                "${e.userName}",
                style: TextStyle(
                  fontSize: sp(24),
                  fontWeight: FontWeight.w400,
                  color: const Color(0xffFFFFFF),
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.center,
              child: Text(
                "${e.giftName}",
                style: TextStyle(
                  fontSize: sp(28),
                  fontWeight: FontWeight.w400,
                  color: const Color(0xffFFFFFF),
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.center,
              child: Text(
                "${e.giftNumber}",
                style: TextStyle(
                  fontSize: sp(28),
                  fontWeight: FontWeight.w400,
                  color: const Color(0xffFFFFFF),
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.center,
              child: Text(
                "${e.price}",
                style: TextStyle(
                  fontSize: sp(28),
                  fontWeight: FontWeight.w400,
                  color: const Color(0xffFFFFFF),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  ///默认空页面
  empty() {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: width(220),
          height: width(226),
          margin: EdgeInsets.only(bottom: width(10)),
          child: Image.asset(
            "images/no_data.png",
            width: width(220),
            height: width(226),
          ),
        ),
        Text("暂无更多数据",
            style: TextStyle(
                fontSize: sp(28),
                color: Color(0xff768A90),
                fontWeight: FontWeight.w400,
                decoration: TextDecoration.none))
      ],
    ));
  }
}

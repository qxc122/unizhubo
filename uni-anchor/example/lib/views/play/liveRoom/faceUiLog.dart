import 'package:faceunity_ui/Faceunity_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/play/controller.dart';
import 'package:agora_rtc_rawdata_example/views/play/liveRoom/controller.dart';

import '../../../utils/screen.dart';

class FaceUiLog extends StatelessWidget {
  var controller = Get.find<LiveRoomController>();

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomCenter,
      height: width(500),
      // color: Colors.white,
      child: FaceunityUI(
        cameraCallback: () => controller.engine.switchCamera(),
      )
    );
  }
}

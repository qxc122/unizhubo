import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:math' as math;
import 'dart:convert' as convert;
import 'dart:math';
import 'dart:typed_data';
import 'package:agora_rtc_rawdata_example/models/admin_model.dart';
import 'package:agora_rtc_rawdata_example/models/barrage_model.dart';
import 'package:agora_rtc_rawdata_example/models/chat_message_model.dart';
import 'package:agora_rtc_rawdata_example/models/game_model.dart';
import 'package:agora_rtc_rawdata_example/models/gift_model.dart';
import 'package:agora_rtc_rawdata_example/models/lottery_model.dart';
import 'package:agora_rtc_rawdata_example/models/room_member_model.dart';
import 'package:agora_rtc_rawdata_example/models/room_product_model.dart';
import 'package:agora_rtc_rawdata_example/models/winning_model.dart';
import 'package:agora_rtc_rawdata_example/utils/aes.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/views/play/liveRoom/NoteLog.dart';
import 'package:agora_rtc_rawdata_example/views/play/liveRoom/adminListLog.dart';
import 'package:agora_rtc_rawdata_example/views/play/liveRoom/onLineLog.dart';
import 'package:agora_rtc_rawdata_example/views/play/liveRoom/roomFTypeLog.dart';
import 'package:agora_rtc_rawdata_example/views/play/liveRoom/roomTTypeLog.dart';
import 'package:agora_rtc_rawdata_example/views/processImg/ProcessImg.dart';
import 'package:flutter/material.dart';

import 'package:agora_rtc_rawdata/agora_rtc_rawdata.dart';
import 'package:agora_rtc_rawdata_example/models/user_model.dart';
import 'package:agora_rtm/agora_rtm.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter_barrage/flutter_barrage.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/views/play/endLive/controller.dart';
import 'package:agora_rtc_rawdata_example/views/play/liveRoom/giftLog.dart';
import 'package:agora_rtc_rawdata_example/views/play/liveRoom/manageListLog.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:svgaplayer_flutter/parser.dart';
import 'package:svgaplayer_flutter/player.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:wakelock/wakelock.dart';

class LiveRoomController extends GetxController
    with SingleGetTickerProviderMixin {
  var controller2 = Get.put(EndLiveController());
  RxString titleString = "out".obs;

  RxList<String> manageList = ["管理员列表", "踢人列表", "禁言列表", "切换收费直播间"].obs;
  RxInt currentManage = 0.obs;

  RxString chargeType = '0'.obs;

  List<String> tabs = ['游戏', '弹幕', '礼物'];
  RxInt currentIndex = 0.obs;

  Timer? timer;
  Timer? giftTimer;
  RxInt seconds = 0.obs;
  RxBool timeState = true.obs;
  RxString content = '00:00:00'.obs;

  final titleCtr = TextEditingController();
  final FocusNode titleFocus = FocusNode();
  var userInfo = Rx<UserModel>(UserModel());
  var msgInfo = Rx<ChatMessageModel>(ChatMessageModel());
  var msgBody = Rx<ChatMessageModel>(ChatMessageModel());

  RxString noticeContent = "".obs;
  RxString studioStatus = "1".obs;

  /// 直播间状态 0：未开播，1：开播，2：网络状态不好
  RxString studioNum = "".obs;

  /// 房间号
  /// 声网直播模块
  /// uat：1dd9c1416e754ea9b7651d47eac10590
  /// dev： c30b10fa91e949b0b47800dd87def391
  /// pro：22f4eb5c247a462d9df410743125f0db
  /// pre：246f2c236bf7457bac630b3fa2aeb8a4
  /// IM 消息解密 c1kgVioySoUVimtw
  ///  图片解密 qs0fIlSFMMZxOuzD
  RxString appId = "1dd9c1416e754ea9b7651d47eac10590".obs;
  RxString studioLivePushFlow = "".obs;

  /// 直播推流地址
  RxString token = "".obs;
  RxString agoraKey = "".obs;

  /// 房间直播流密钥key
  RxString agoraKeyAsciiCode = "".obs;

  /// 房间直播流密钥key ascii码
  RxList<int> agoraKeyAsciiCodeList = [1].obs;
  RxString agoraSalt = "".obs;

  /// 房间直播流密钥盐
  RxString channel = "".obs;
  RxString giveFirepower = "0.0".obs;
  RxString fansNum = "0".obs;
  RxString focusNum = "0".obs;
  RxInt remoteUidM = 0.obs;
  RxBool localUserJoined = false.obs;
  RxBool startPreview = false.obs;
  RxBool isJoined = false.obs;
  RxList remoteUid = [].obs;
  late RtcEngine _engine;
  get engine => this._engine;
  RxString sendMsgValue = "".obs;

  /// 声网实时聊天模块
  RxList infoStrings = [].obs;
  AgoraRtmClient? _client;
  AgoraRtmChannel? _rtmchannel;
  AgoraRtmChannel? _rtmGamechannel;
  RxBool isLogin = false.obs;
  RxBool isInChannel = false.obs;
  RxBool isInGameChannel = false.obs;
  RxString channelId = "".obs;
  RxString channelGameId = "".obs;
  RxString rtmToken = "".obs;
  RxString studioThumbImage = "".obs;

  RxBool isFaceFlag = false.obs;

  /// 美颜功能
  RxBool currentCameraFlag = false.obs;

  /// 当前是前置还是后置摄像
  RxBool torchOn = false.obs;

  /// 闪光灯是否打开
  RxBool speakerFlag = false.obs;

  /// 是否静音
  RxDouble bottomValue = 500.0.obs;

  RxString firepower = "0".obs;

  /// 火力值,计算方式：主播在直播间收到的总金币数 * 100, 也就是钻石 * 10
  RxBool isCharge = false.obs;

  /// 是否开通收费直播
  RxString onlineNum = "0".obs;

  /// 在线人数
  RxString startTime = "0".obs;

  /// 开播时间 秒
  RxString productId = "".obs;

  /// 商品id
  RxList<RoomMemberModel> onlineList = RxList<RoomMemberModel>();
  /// 在线人数详情 默认50条
  RxList<ChatMessageModel> chatMessageList = RxList<ChatMessageModel>();
  /// 文字聊天列表
  var preGiftMsg = Rx<ChatMessageModel>(ChatMessageModel());
  RxInt giftNum = 1.obs;
  RxList<ChatMessageModel> chatGiftList = RxList<ChatMessageModel>();
  /// 连送礼物列表
  RxList<AdminModel> adminList = RxList<AdminModel>();

  /// 礼物列表
  RxList<GiftModel> giftList = RxList<GiftModel>();

  /// 弹幕列表
  RxList<BarrageModel> barrageList = RxList<BarrageModel>();

  /// 游戏列表
  RxList<GameModel> gameList = RxList<GameModel>();

  RxList<String> onlineListId = RxList<String>();

  /// 管理员/禁言列表/踢人列表等
  RxInt pageNum = 1.obs;
  RefreshController refreshController =
      RefreshController(initialRefresh: false);
  RxBool isLoading = false.obs;

  ///
  ScrollController scrollController = ScrollController();

  RxString currentValue = "管理员列表".obs;
  RxInt currentIndexValue = 0.obs;

  late SVGAAnimationController _animationController;
  get animationController => this._animationController;
  RxString svga = "".obs;

  late ValueNotifier<BarrageValue> timelineNotifier;
  late BarrageWallController _barrageWallController;
  get barrageWallController => this._barrageWallController;
  List<Bullet> _bullets = [];
  get bullets => this._bullets;

  RxList<roomProductModel> roomProductList = RxList<roomProductModel>();
  RxList<String> roomFTypeList = ["按时收费商品", "按场收费商品"].obs;
  RxInt currentFRoomType = 6.obs;
  RxInt giftId = 0.obs;
  RxInt currentFType = 0.obs;
  RxInt tid = 0.obs;
  RxList<LotteryModel> lotteryList = RxList<LotteryModel>();
  RxString currentTicketName = ''.obs;

  late AnimationController _giftController;
  late AnimationController _giftNumController;
  late Animation<double> _animationScale;
  late Animation<Offset> _animationSlideUp;
  get animationSlideUp => this._animationSlideUp;
  get animationScale => this._animationScale;
  get giftController => this._giftController;
  get giftNumController => this._giftNumController;
  RxBool isFlag = true.obs;
  RxBool showWidget = false.obs;


  RxString sharpness = '0'.obs;

  @override
  void onInit() {
    _animationController = SVGAAnimationController(vsync: this);
    _barrageWallController = BarrageWallController();
    _giftController = AnimationController(vsync: this,duration: new Duration(milliseconds: 600),);
    _giftNumController = AnimationController(vsync: this,duration: new Duration(milliseconds: 600),);
    _animationScale = Tween(begin: 0.0, end: 1.0).animate(_giftNumController);
    _animationSlideUp = new Tween(
      begin: Offset(0.0, 5.0),
      end: Offset(0.0, 0.0),
    ).animate(CurvedAnimation(parent: _giftController, curve: Curves.ease));
    super.onInit();
  }

  @override
  void onReady() {
    var map = Get.arguments;
    channelId.value = map['studioNum'].toString();
    channelGameId.value = map['channelGameId'].toString();
    sharpness.value = map['sharpness'].toString();
    print(channelGameId.value);
    print("909090");
    chargeType.value = map['chargeType'].toString();
    channel.value = map['studioNum'].toString();
    studioNum.value = map['studioNum'].toString();
    studioLivePushFlow.value = map['studioLivePushFlow'].toString();
    studioThumbImage.value = map['studioThumbImage'].toString();
    getInit();
    getByType();
    getLottery();
    if (channelId.value.isNotEmpty) {
      getImToken();
      getFlowToken();
      getRoomDetail();
    }
  }

  @override
  void onClose() {
    _barrageWallController.dispose();
    _animationController.dispose();
    _giftController.dispose();
    _giftNumController.dispose();
    Wakelock.disable();
    cancelTimer();
    deinitEngine();
    if (giftTimer != null) {
      giftTimer?.cancel();
    }
  }

  setRoomProduct(roomProductModel element) async {
    giftId.value = element.giftId!;
    Get.back();

    /// 切换收费模式
    var res2 = await API.switchCharge({"productId": giftId.value});
    if (res2['code'] == 200) {
      BotToast.showText(
          text: "切换成功",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
      chargeType.value = '1';
    } else {
      BotToast.showText(
          text: res2["msg"],
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    }
  }

  setRoomFType(int index) async {
    // Get.bottomSheet(RoomFTypeLog());
    currentFType.value = index;
    var res = await API.roomProducts({"type": index == 0 ? 6 : 7});
    if (res['code'] == 200) {
      var list = (res['data'] as List)
          .map((e) => roomProductModel.fromJson(e))
          .toList();
      roomProductList.clear();
      roomProductList.addAll(list);
      Get.back();
      Get.bottomSheet(RoomTTypeLog());
    }
  }

  loadAnimation(String svg) async {
    // print("98989898");
    final videoItem = await SVGAParser.shared.decodeFromURL(svg);
    _animationController.videoItem = videoItem;
    _animationController.reset();
    _animationController.forward();
    // _animationController.reset().whenComplete(() => _animationController.videoItem = null);
    _animationController.addStatusListener((status) {
      // print('---status---$status');
      if (status.toString() == 'AnimationStatus.completed') {
        _animationController.videoItem = null;
        svga.value = "";
      }
    });
    update();
  }

  deinitEngine() async {
    await AgoraRtcRawdata.unregisterAudioFrameObserver();
    await AgoraRtcRawdata.unregisterVideoFrameObserver();
    await _engine.destroy();
    await _client?.logout();
  }

  void countDownTest() {
    timeState.value = !timeState.value;
    if (!timeState.value) {
      //获取当期时间
      var now = DateTime.now();
      String timestamp =
          "${now.hour.toString().padLeft(2, '0')}:${now.minute.toString().padLeft(2, '0')}:${now.second.toString().padLeft(2, '0')}";
      // print(now);
      print(timestamp);
      var twoHours = now.add(Duration()).difference(now);
      // print(twoHours);
      //获取总秒数
      seconds.value = twoHours.inSeconds;
      // print(seconds);
      startTimer();
    } else {
      seconds.value = 0;
      cancelTimer();
    }
  }

  String constructTime(int seconds) {
    int hour = seconds ~/ 3600;
    int minute = seconds % 3600 ~/ 60;
    int second = seconds % 60;
    return formatTime(hour) +
        ":" +
        formatTime(minute) +
        ":" +
        formatTime(second);
  }

  String formatTime(int timeNum) {
    return timeNum < 10 ? "0" + timeNum.toString() : timeNum.toString();
  }

  void startTimer() {
    //设置 1 秒回调一次
    const period = const Duration(seconds: 1);
    timer = Timer.periodic(period, (timer) {
      seconds.value = seconds.value + 1;
      content.value = constructTime(int.parse(seconds.value.toString()));
    });
  }

  void cancelTimer() {
    if (timer != null) {
      timer?.cancel();
    }
  }

  getRoomDetail() async {
    var res = await API.onlineUsers({"studioNum": studioNum.value});
    print(res.toString());
    print("123123123");
    if (res['code'] == 200) {
      // firepower.value = res['data']['firepower'].toString();
      // isCharge.value = res['data']['isCharge'];
      // onlineNum.value = res['data']['onlineNum'].toString() == ''
      //     ? '0'
      //     : res['data']['onlineNum'].toString();
      // productId.value = res['data']['productId'].toString();
      // startTime.value = res['data']['startTime'].toString();

      /// 直播间在线人数列表，默认值显示前50条 RoomMemberModel
      var list = (res['data'] as List)
          .map((e) => RoomMemberModel.fromJson(e))
          .toList();
      onlineListId.clear();
      onlineList.clear();
      list.forEach((element) {
        onlineListId.add(element.userId.toString());
      });
      onlineList.addAll(list);
      update();
    }
  }

  /// 获取主播当前直播间信息
  getInit() async {
    var res = await API.getUserInfo();
    // print(res.toString());
    if (res['code'] == 200) {
      var user = UserModel.fromJson(res['data']);
      userInfo.value = user;
      update();
    }
  }

  changeGame(LotteryModel element) {
    tid.value = int.parse(element.lotteryId.toString());
    getLogList(1);
  }

  getLottery() async {
    var res = await API.getLottery();
    // print(res.toString());
    lotteryList.clear();
    if (res['code'] == 200) {
      var list =
          (res['data'] as List).map((e) => LotteryModel.fromJson(e)).toList();
      lotteryList.insert(
          0,
          LotteryModel(
              icon: "images/game_clear.png", lotteryId: 0, name: '全部'));
      lotteryList.addAll(list);
      lotteryList.refresh();
    }
  }

  /// 获取openssl生成直播密钥
  // getOpenSSLKey() async {
  //   var res = await API.getOpenSSLKey();
  //   print(res.toString());
  //   if (res['code'] == 200) {
  //     // agoraKey.value = res['data']['agoraKey'];
  //     // agoraSalt.value = res['data']['agoraSalt'];
  //     update();
  //   }
  // }

  setFaceUi() {
    isFaceFlag.value = true;
    bottomValue.value = 0.0;
    update();
  }

  getOnLine() {
    getRoomDetail();
    Get.bottomSheet(OnLineListLog());
  }

  /// 获取IM的token
  getImToken() async {
    var res = await API.getImToken();
    // print(res.toString());
    if (res['code'] == 200) {
      rtmToken.value = res['data']['token'];
      createClient();
    }
  }

  /// 获取推流和拉流的token
  getFlowToken() async {
    var res = await API.getFlowToken({"channelName": channelId.value});
    // print(res.toString());
    if (res['code'] == 200) {
      token.value = res['data']['token'];
      agoraSalt.value = res['data']['agoraSalt'];
      agoraKey.value = res['data']['agoraKey'];
      agoraKeyAsciiCode.value = res['data']['agoraKeyAsciiCode'];
      // var list = agoraKeyAsciiCode.value.split(",");
      // agoraKeyAsciiCodeList.value = [];
      // list.forEach((element) {
      //   agoraKeyAsciiCodeList.add(int.parse(element));
      // });
      initAgora();
    }
  }

  /// 获取公告
  getByType() async {
    var res = await API.getByType({"type": 1});
    // print(res.toString());
    if (res['code'] == 200) {
      noticeContent.value = res['data']["noticeContent"];
      update();
    }
  }

  /// 获取主播当前直播间信息
  getStudioInfo() async {
    var res = await API.getStudioInfo();
    // print(res.toString());
    if (res['code'] == 200) {
      var user = UserModel.fromJson(res['data']);
      userInfo.value = user;
      update();
    }
  }

  /// 获取主播当前开播状态
  getStudioStatus() async {
    var res = await API.getStudioStatus();
    // print(res.toString());
    if (res['code'] == 200) {}
  }

  /// 直播间贡献榜
  getContributionList() async {
    var res = await API.getContributionList({"dateType": 1});
    // print(res.toString());
    if (res['code'] == 200) {}
  }

  /// 直播间用户扩展信息
  getUserExpandInfo() async {
    var res = await API.getUserExpandInfo();
    // print(res.toString());
    if (res['code'] == 200) {
      giveFirepower.value = res['data']['giveFirepower'].toString();
      fansNum.value = res['data']['fansNum'].toString();
      focusNum.value = res['data']['focusNum'].toString();
      update();
    }
  }

  // 初始化声网实时聊天模块
  createClient() async {
    _client = await AgoraRtmClient.createInstance(appId.value);
    _client?.onMessageReceived = (AgoraRtmMessage message, String peerId) {
      _log("Peer msg: $peerId, msg: ${message.text}");
    };
    _client?.onConnectionStateChanged = (int state, int reason) {
      _log('Connection state changed: $state, reason: $reason');
      if (state == 5) {
        _client?.logout();
        _log('Logout.');
        isLogin.value = false;
      }
    };
    _client?.onLocalInvitationReceivedByPeer =
        (AgoraRtmLocalInvitation invite) {
      _log(
          'Local invitation received by peer: ${invite.calleeId}, content: ${invite.content}');
    };
    _client?.onRemoteInvitationReceivedByPeer =
        (AgoraRtmRemoteInvitation invite) {
      _log(
          'Remote invitation received by peer: ${invite.callerId}, content: ${invite.content}');
    };

    _client?.onConnectionStateChanged = (int state, int reason) {
      print("网络状态变化");
      print(state);
      print(reason);
      /// https://docs.agora.io/cn/live-streaming-premium-legacy/API%20Reference/java/classio_1_1agora_1_1rtc_1_1_i_rtc_channel_event_handler.html?platform=Android#a6dfbfe241ac004b792a499c29033ddff
      if (reason == 13) {
        BotToast.showText(
            text: "您的网络连接不稳定，建议切换网络。",
            textStyle: const TextStyle(fontSize: 14, color: Colors.white),
            align: const Alignment(0, 0));
      }
      // if (state == 1 || state == 5 || reason == 5) {
      //   BotToast.showText(
      //       text: "网络连接失败",
      //       textStyle: const TextStyle(fontSize: 14, color: Colors.white),
      //       align: const Alignment(0, 0));
      //   logoutRoom();
      // }
    };

    toggleLogin();
  }

  void toggleLogin() async {
    if (isLogin.value) {
      try {
        await _client?.logout();
        // _log('Logout success.');
        isLogin.value = false;
        isInChannel.value = false;
      } catch (errorCode) {
        _log('Logout error: $errorCode');
      }
    } else {
      // print(rtmToken.value);
      // print(channelId.value);
      try {
        await _client?.login(rtmToken.value, userInfo.value.userId.toString());
        // _log('Login success: 0');
        isLogin.value = true;
        toggleJoinChannel();
        // print("游戏id${int.parse(channelGameId.value)}");
        // if (int.parse(channelGameId.value) > 0){
        //   toggleJoinGameChannel();
        // }
        toggleJoinGameChannel();
      } catch (errorCode) {
        _log('Login error: $errorCode');
      }
    }
  }

  void toggleJoinChannel() async {
    if (isInChannel.value) {
      try {
        await _rtmchannel?.leave();
        // _log('Leave channel success.');
        if (_rtmchannel != null) {
          _client?.releaseChannel(_rtmchannel!.channelId!);
        }
        isInChannel.value = false;
      } catch (errorCode) {
        _log('Leave channel error: $errorCode');
      }
    } else {
      try {
        _rtmchannel = await _createChannel(channelId.value);
        await _rtmchannel?.join();
        // _log('Join channel success.');
        isInChannel.value = true;
      } catch (errorCode) {
        _log('Join channel error: $errorCode');
      }
    }
  }

  Future<AgoraRtmChannel?> _createChannel(String name) async {
    AgoraRtmChannel? channel = await _client?.createChannel(name);
    if (channel != null) {
      channel.onMemberJoined = (AgoraRtmMember member) {
        _log('Member joined: ${member.userId}, channel: ${member.channelId}');
        onlineListId.add(member.userId);
      };
      channel.onMemberLeft = (AgoraRtmMember member) {
        _log('Member left: ${member.userId}, channel: ${member.channelId}');
        onlineListId.remove(member.userId);
      };
      channel.onMessageReceived =
          (AgoraRtmMessage message, AgoraRtmMember member) {
        print("marssss1");
        // 收到的消息文字需要解密
        var data = EncryptUtil.aesCommonDecode(message.text);
        var ind = data.toString().lastIndexOf("}");
        var jo = data.toString().substring(0, ind) + "}";
        print(jo);
        print(jo.toString().indexOf("zjMoney"));
        print('huo');
        var msg = ChatMessageModel.fromJson(json.decode(jo));
        msgInfo.value = msg;
        //有人进入或离开房间
        if (msgInfo.value.body?.operatorType == 23) {}
        //刷新热度信息
        if (msgInfo.value.body?.operatorType == 24) {
          var nu = msgInfo.value.body?.content?.hotnessNum.toString();
          giveFirepower.value = nu.toString() == 'null' ? '0' : nu.toString();
          update();
        }
        // 有人进入或离开房间
        // if (msgInfo.value.body?.operatorType == 1) {
        //   print('进入');
        //   var online = msgInfo.value.body?.content?.onlineUsersCount.toString();
        //   onlineNum.value = online.toString() == 'null' || online.toString() == ''
        //       ? onlineNum.value
        //       : online.toString();
        // }
        if (jo.indexOf("zjMoney") > -1) {
        } else {
          chatMessageList.add(msgInfo.value);
        }

        //如果是送礼物 显示送礼物动画
        if (msgInfo.value.body?.operatorType == 6) {
          preGiftMsg.value = msgInfo.value;
          var gif = msgInfo.value.body?.content?.liveGift?.dynamicImage.toString();
          svga.value = gif.toString();
          loadAnimation(svga.value);
          // 这里判断是否是同一个人连续送的礼物
          if (preGiftMsg.value.code == 200 && preGiftMsg.value.body?.content?.userId.toString() == msgInfo.value.body?.content?.userId.toString()) {
            if (showWidget.value) {
              _giftNumController.forward();
              giftNum.value = giftNum.value + 1;
            } else {
              _giftController.forward();
              showWidget.value = true;
            }
            if (giftTimer != null) {
              giftTimer?.cancel();
            }
            const period = const Duration(seconds: 10);
            giftTimer = Timer.periodic(period, (timer) {
              _giftController.reverse();
              showWidget.value = false;
              giftNum.value = 1;
            });
          } else {
            _giftController.reverse();
            showWidget.value = false;
            giftNum.value = 1;
          }

          // const timeout = const Duration(seconds: 10);
          // Timer(timeout, () {
          //   showWidget.value = false;
          //   giftNum.value = 1;
          //   _giftController.reverse();
          // });


          // const timeout = const Duration(seconds: 10);
          // Timer(timeout, () { //callback function
          //   svga.value = "";
          //   _animationController.videoItem = null;
          //   // _animationController.dispose();
          // });
        } else if (msgInfo.value.body?.operatorType == 7) {
          //弹幕7
          String? ava = msgInfo.value.body?.content?.avatar.toString();
          String? uid = msgInfo.value.body?.content?.userId.toString();
          barrageWallController.send([
            new Bullet(
                child: Container(
              width: width(400),
              height: width(110),
              child: Row(
                children: [
                  Container(
                    width: width(80),
                    height: width(80),
                    margin: EdgeInsets.only(right: width(10)),
                    child: MyImage(
                      url: ava.toString(),
                      id: 'danmu' + uid.toString(),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: Text(
                          "${msgInfo.value.body?.content?.nickName}",
                          style: TextStyle(
                            fontSize: sp(24),
                            fontWeight: FontWeight.w400,
                            color: const Color(0xffFFFFFF),
                          ),
                        ),
                      ),
                      Container(
                        child: Text(
                          "${msgInfo.value.body?.content?.barrage}",
                          style: TextStyle(
                            fontSize: sp(24),
                            fontWeight: FontWeight.w400,
                            color: const Color(0xffFFFFFF),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ))
          ]);
        }
        scrollController.jumpTo(scrollController.position.maxScrollExtent);
      };
    }
    return channel;
  }

  void toggleJoinGameChannel() async {
    if (isInGameChannel.value) {
      try {
        await _rtmGamechannel?.leave();
        // _log('Leave channel success.');
        if (_rtmGamechannel != null) {
          _client?.releaseChannel(_rtmGamechannel!.channelId!);
        }
        isInGameChannel.value = false;
      } catch (errorCode) {
        _log('Leave channel error: $errorCode');
      }
    } else {
      try {
        // _rtmchannel = await _createChannel(channelId.value);
        _rtmGamechannel = await _createGameChannel(channelGameId.value);
        await _rtmGamechannel?.join();
        // _log('Join channel success.');
        isInGameChannel.value = true;
      } catch (errorCode) {
        _log('Join channel error: $errorCode');
      }
    }
  }

  Future<AgoraRtmChannel?> _createGameChannel(String name) async {
    AgoraRtmChannel? channel = await _client?.createChannel(name);
    if (channel != null) {
      channel.onMemberJoined = (AgoraRtmMember member) {
        _log('Member joined: ${member.userId}, channel: ${member.channelId}');
      };
      channel.onMemberLeft = (AgoraRtmMember member) {
        _log('Member left: ${member.userId}, channel: ${member.channelId}');
      };
      channel.onMessageReceived =
          (AgoraRtmMessage message, AgoraRtmMember member) {
        print("marssss2");
        // 收到的消息文字需要解密
        var data = EncryptUtil.aesCommonDecode(message.text);
        var ind = data.toString().lastIndexOf("}");
        var jo = data.toString().substring(0, ind) + "}";
        print(jo);
        var msg;
        // var msg = ChatMessageModel.fromJson(json.decode(jo));
        if (jo.toString().indexOf("zjMoney") > -1) {
          var msg2 = WinningLotteryModel.fromJson(json.decode(jo));
          print("mars234");
          print(msg2.body?.content?.length.toString());
          var len = msg2.body?.content?.length.toString();
          print(int.parse(len.toString()) > 0);
          // chatMessageList.add(msgInfo.value);
          if (int.parse(len.toString()) > 0) {
            msg2.body?.content?.forEach((element) {
              print(element.nickName);
              var conte = {
                "nickName": element.nickName,
                "studioNum": element.studioNum,
                "uid": element.uid,
                "zjMoney": element.zjMoney,
                "ticketName": currentTicketName.value
              };
              var jsonInfo = {
                "code": 200,
                "body": {
                  "operatorType": 21,
                  "chatType": 6,
                  "sendTime": "",
                  "content": convert.jsonEncode(conte)
                },
                "isReceive": false,
                "sendTime": ""
              };
              print("进来了");
              print(jsonInfo);
              chatMessageList.add(ChatMessageModel.fromJson(jsonInfo));
            });
          }
        } else {
          msg = ChatMessageModel.fromJson(json.decode(jo));
          msgInfo.value = msg;
          if (msgInfo.value.body?.operatorType == 20) {
            var name = msgInfo.value.body?.content?.ticketName.toString();
            currentTicketName.value = name.toString();
          }
          chatMessageList.add(msgInfo.value);
        }
        scrollController.jumpTo(scrollController.position.maxScrollExtent);
      };
    }
    return channel;
  }

  void toggleSendChannelMessage(String text) async {
    print("火星黑洞");
    print(text);
    try {
      await _rtmchannel?.sendMessage(AgoraRtmMessage.fromText(text));
      titleCtr.text = "";
      chatMessageList.add(msgBody.value);
      // _log(text);
    } catch (errorCode) {
      _log('Send channel message error: $errorCode');
    }
  }

  void toggleGetMembers() async {
    try {
      List<AgoraRtmMember>? members = await _rtmchannel?.getMembers();
      _log('Members: $members');
    } catch (errorCode) {
      _log('GetMembers failed: $errorCode');
    }
  }

  _log(String info) {
    print(info);
    infoStrings.insert(0, info);
  }

  //  初始化声网视频模块
  initAgora() async {
    // retrieve permissions
    await [Permission.microphone, Permission.camera].request();
    //create the engine
    _engine = await RtcEngine.create(appId.value);
    _engine.setEventHandler(
        RtcEngineEventHandler(joinChannelSuccess: (channel, uid, elapsed) {
      // log('joinChannelSuccess $channel $uid $elapsed');
      isJoined.value = true;
    }, userJoined: (uid, elapsed) {
      // log('userJoined  $uid $elapsed');
      remoteUid.add(uid);
    }, userOffline: (uid, reason) {
      // log('userJoined  $uid $reason');
      remoteUid.removeWhere((element) => element == uid);
    }));

    /// 声网相关视频设置
    VideoEncoderConfiguration configuration = VideoEncoderConfiguration();
    if (sharpness.value == '0') { /// 1080p
      configuration.dimensions = VideoDimensions(width: 1920, height: 1080);
    } else if (sharpness.value == '1') { /// 720p
      configuration.dimensions = VideoDimensions(width: 1280, height: 720);
    } else if (sharpness.value == '2') { /// 480p
      configuration.dimensions = VideoDimensions(width: 840, height: 480);
    } else if (sharpness.value == '3') { /// 360p
      configuration.dimensions = VideoDimensions(width: 640, height: 360);
    } else {
      configuration.dimensions = VideoDimensions(width: 1920, height: 1080);
    }
    await _engine.setVideoEncoderConfiguration(configuration);

    /// 设置美颜效果选项。
    // setBeautyEffectOptions
    /// 开始视频流。
    await _engine.enableVideo();
    await _engine.setChannelProfile(ChannelProfile.LiveBroadcasting);

    /// 设置主播角色
    await _engine.setClientRole(ClientRole.Broadcaster);

    /// 开始预览
    await _engine.startPreview();
    startPreview.value = true;
    var handle = await _engine.getNativeHandle();
    if (handle != null) {
      await AgoraRtcRawdata.registerAudioFrameObserver(handle);
      await AgoraRtcRawdata.registerVideoFrameObserver(handle);
    }

    /// 加密
    EncryptionConfig config = new EncryptionConfig();
    config.encryptionMode = EncryptionMode.AES128GCM2;
    config.encryptionKey = agoraKey.value;
    Uint8List uint8List = base64Decode(agoraSalt.value);
    List<int> encryptionKdfSalt = uint8List.toList();
    config.encryptionKdfSalt = encryptionKdfSalt;
    await _engine.enableEncryption(true, config);

    /// 加入频道
    await _engine.joinChannel(token.value, channel.value, null,
        int.parse(userInfo.value.userId.toString()));

    /// 切换前置/后置摄像头。 /// 暂时没有这样的API的。如果想要了解当前用户的摄像头状态，可在每次调用switchCamera()方法时记录下，如初始为0（即前置），调用一次变为1（即后置），再次调用则为0，从而实现状态记录。
    ///  await _engine.switchCamera();

    /// 检测设备是否支持闪光灯常开。
    /// 请在启动摄像头之后调用该方法，如 joinChannel [2/2]、enableVideo 或者 enableLocalVideo 之后。
    /// 一般情况下，app 默认开启前置摄像头，因此如果你的前置摄像头不支持闪光灯常开，直接使用该方法会返回 false。如果需要检查后置摄像头是否支持闪光灯常开，需要先使用 switchCamera 切换摄像头，再使用该方法。
    /// await _engine.isCameraTorchSupported();
    /// 设置是否打开闪光灯  请在启动摄像头之前调用该方法，如 joinChannel [2/2]、enableVideo 或者 enableLocalVideo 之前。
    /// setCameraTorchOn(isOn) /// true: 打开闪光灯 /// false:（默认）关闭闪光灯。

    /// 调节本地播放的所有远端用户信号音量。静音  该方法在加入频道前后都能调用。
    /// await _engine.adjustPlaybackSignalVolume(0);
    /// muteLocalAudioStream(false) 是否取消发布本地音频流 true：取消发布。 false：发布。

    /// 推流
    // log(studioLivePushFlow.value);
    // await _engine.addPublishStreamUrl(studioLivePushFlow.value, false);

    //关闭本地声音
    await _engine.muteLocalAudioStream(true);
    //关闭远程声音
    await _engine.muteRemoteAudioStream(0, true);
    // 声网相关视频设置
    // VideoEncoderConfiguration videoConfig = VideoEncoderConfiguration(
    //   frameRate: VideoFrameRate.Fps30,
    //   bitrate: 3240,
    //   dimensions: VideoDimensions(width: 1280, height: 720),
    // );
    // await _engine.setVideoEncoderConfiguration(videoConfig);
    /// 如果在直播间禁止屏幕熄屏
    Wakelock.enable();

    /// 直播开始计时
    countDownTest();
  }

  switchCamera() async {
    currentCameraFlag.value = !currentCameraFlag.value;
    await _engine.switchCamera();
  }

  switchFlash() async {
    if (await _engine.isCameraTorchSupported() == false) {
      BotToast.showText(
          text: "您手机前置摄像不支持闪光灯，请切换摄像头后试一下～", align: Alignment(0, 0));
    } else {
      _engine.setCameraTorchOn(!torchOn.value);
      torchOn.value = !torchOn.value;
    }
  }

  switchSpeaker() async {
    // muteLocalAudioStream(false) 是否取消发布本地音频流 true：取消发布。 false：发布。
    await _engine.muteLocalAudioStream(!speakerFlag.value);
    speakerFlag.value = !speakerFlag.value;
  }

  sendMsg() {
    if (sendMsgValue.value.isEmpty) {
      BotToast.showText(text: "请先输入您想说的话～", align: Alignment(0, 0));
    } else {
      var mb = {
        "code": 200,
        "isReceive": false,
        "msgId": math.Random().nextInt(32).toString(),
        "receiptTime": DateTime.now().millisecondsSinceEpoch.toString(),
        "sendTime": DateTime.now().millisecondsSinceEpoch.toString(),
        "body": {
          "chatType": 2,
          "id": math.Random().nextInt(64).toString(),
          "operatorType": 0,
          "sendTime": DateTime.now().millisecondsSinceEpoch.toString(),
          "senderType": "",
          "content": {
            "avatar": userInfo.value.avatar,
            "level": "0",
            "msgContent": sendMsgValue.value,
            "nickName": "主播",
            "userId": userInfo.value.userId
          }
        }
      };
      var user = ChatMessageModel.fromJson(mb);
      msgBody.value = user;
      var text = EncryptUtil.aesDecode(convert.jsonEncode(mb));
      toggleSendChannelMessage(text);
    }
  }

  changeIndex(int index) {
    print(index);
    currentIndex.value = index;
    tid.value = 0;
    giftList.clear();
    barrageList.clear();
    gameList.clear();
    getLogList(1);
    update();
  }

  getLogList(int page) async {
    var res;

    /// 游戏 弹幕 礼物
    if (currentIndex.value == 0) {
      if (page == 1) {
        gameList.clear();
      }
      res = await API.getRoomGameList({
        "pageNum": page,
        "pageSize": 20,
        "tid": tid.value == 0 ? '' : tid.value
      });
      print(res.toString());
      var list = (res['data']['list'] as List)
          .map((e) => GameModel.fromJson(e))
          .toList();
      gameList.addAll(list);
      refreshController.refreshCompleted();
      refreshController.loadComplete();
      if (list.length < 20) {
        refreshController.loadNoData();
      }
      isLoading.value = false;
    } else if (currentIndex.value == 1) {
      res = await API.getRoomBarrageList({
        "pageNum": page,
        "pageSize": 20,
      });
      if (page == 1) {
        barrageList.clear();
      }
      // print(res.toString());
      var list1 = (res['data']['list'] as List)
          .map((e) => BarrageModel.fromJson(e))
          .toList();
      barrageList.addAll(list1);
      refreshController.refreshCompleted();
      refreshController.loadComplete();
      if (list1.length < 20) {
        refreshController.loadNoData();
      }
      isLoading.value = false;
    } else if (currentIndex.value == 2) {
      res = await API.getRoomGiftList({
        "pageNum": page,
        "pageSize": 20,
      });
      if (page == 1) {
        giftList.clear();
      }
      // print(res.toString());
      var list2 = (res['data']['list'] as List)
          .map((e) => GiftModel.fromJson(e))
          .toList();
      giftList.addAll(list2);
      refreshController.refreshCompleted();
      refreshController.loadComplete();
      if (list2.length < 20) {
        refreshController.loadNoData();
      }
      isLoading.value = false;
    }
  }

  setTitle(String text) {
    sendMsgValue.value = text;
  }

  setGiftLog() {
    getLogList(1);
    Get.bottomSheet(GiftLog());
  }

  setManageLog() {
    print('123123');
    print(chargeType.value);
    print('123123');
    if (chargeType.value == '0') {
      manageList.value = ["管理员列表", "踢人列表", "禁言列表", "切换收费直播间"];
    } else {
      manageList.value = ["管理员列表", "踢人列表", "禁言列表", "切换免费直播间"];
    }
    Get.bottomSheet(ManageListLog());
  }

  setManage(int index) async {
    Get.back();
    // "管理员列表", "踢人列表", "禁言列表", "切换收费直播间"
    currentValue.value = manageList[index];
    currentIndexValue.value = index;
    pageNum.value = 1;
    getAdminList(1);
    if (currentIndexValue.value < 3) {
      Get.bottomSheet(AdminListLog());
    }
  }

  goOn() {
    Get.back();
    Get.bottomSheet(RoomFTypeLog());
  }

  getAdminList(int page) async {
    var res;
    print(currentIndexValue.value);
    if (currentIndexValue.value == 0) {
      res = await API.getAdminList({
        "userId": userInfo.value.userId,
        "pageNum": page,
        "pageSize": 20,
      });
    } else if (currentIndexValue.value == 1) {
      res = await API.getKickingList({
        "userId": userInfo.value.userId,
        "pageNum": page,
        "pageSize": 20,
      });
    } else if (currentIndexValue.value == 2) {
      res = await API.getBannedList({
        "userId": userInfo.value.userId,
        "pageNum": page,
        "pageSize": 20,
      });
    } else if (currentIndexValue.value == 3) {
      if (chargeType.value == '0') {
        Get.dialog(NoteLog());
      } else {
        var res2 = await API.switchCharge({"productId": 0});
        if (res2['code'] == 200) {
          BotToast.showText(
              text: "切换成功",
              textStyle: const TextStyle(fontSize: 14, color: Colors.white),
              align: const Alignment(0, 0));
          chargeType.value = '0';
        } else {
          BotToast.showText(
              text: res2["msg"],
              textStyle: const TextStyle(fontSize: 14, color: Colors.white),
              align: const Alignment(0, 0));
        }
      }

      // var res3 = await API.roomProducts({"type": "6"});
      // if (res3['code'] == 200) {
      //   /// 切换收费模式
      //   var res2 = await API.switchCharge({"productId": chargeType.value == '0' ? res3['data'][0]['giftId']:''});
      //   // print(res2.toString());
      //   if (res2['code'] == 200) {
      //     BotToast.showText(
      //         text: "切换成功",
      //         textStyle: const TextStyle(fontSize: 14, color: Colors.white),
      //         align: const Alignment(0, 0));
      //     if (chargeType.value == '0') {
      //       chargeType.value = '2';
      //     } else {
      //       chargeType.value = '0';
      //     }
      //   } else {
      //     BotToast.showText(
      //         text: res2["msg"],
      //         textStyle: const TextStyle(fontSize: 14, color: Colors.white),
      //         align: const Alignment(0, 0));
      //   }
      // }

    }
    // print(res.toString());
    if (currentIndexValue.value < 3 && res['code'] == 200) {
      var list = (res['data']['list'] as List)
          .map((e) => AdminModel.fromJson(e))
          .toList();
      adminList.addAll(list);
      refreshController.refreshCompleted();
      refreshController.loadComplete();
      if (list.length < 20) {
        refreshController.loadNoData();
      }
      isLoading.value = false;
    }
  }

  logoutRoom() async {
    var res = await API.liveClose();
    print(res.toString());
    if (res['code'] == 200) {
      // cancelTimer();
      // deinitEngine();
      // Wakelock.disable();
      // _barrageWallController.dispose();
      // _animationController.dispose();
      controller2.fansCount.value = res['data']['fansCount'].toString() == 'null' ? '0': res['data']['fansCount'].toString();
      controller2.liveCount.value = res['data']['liveCount'].toString() == 'null' ? '0': res['data']['liveCount'].toString();
      controller2.liveTime.value = res['data']['liveTime'].toString() == 'null' ? '0': res['data']['liveTime'].toString();
      controller2.moneyNumber.value = res['data']['moneyNumber'].toString() == 'null' ? '0': res['data']['moneyNumber'].toString();
      controller2.userCount.value = res['data']['userCount'].toString() == 'null' ? '0': res['data']['userCount'].toString();
      Get.offAllNamed("/endLive");
    }
  }

  getOnlineService() async {
    var res = await API.getOnlineService();
    print(res.toString());
    if (res['code'] == 200) {
      var url = Uri.parse(res['data']['context']);
      Get.toNamed("/webView", arguments: {"title": "客服中心", "url": url});
      // if (!await launchUrl(url, mode: LaunchMode.externalApplication)) {
      //   throw 'Could not launch $url';
      // }
    }
  }
}

import 'package:agora_rtc_rawdata_example/models/admin_model.dart';
import 'package:agora_rtc_rawdata_example/views/play/liveRoom/medal.dart';
import 'package:agora_rtc_rawdata_example/views/processImg/ProcessImg.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/play/controller.dart';
import 'package:agora_rtc_rawdata_example/views/play/liveRoom/controller.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../utils/screen.dart';

class AdminListLog extends StatelessWidget {
  var controller = Get.find<LiveRoomController>();

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomCenter,
      padding: EdgeInsets.symmetric(horizontal: width(25)),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(topRight: Radius.circular(width(20)), topLeft: Radius.circular(width(20)))
      ),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: width(42)),
            alignment:Alignment.center,
            child: Text(
              "${controller.currentValue.value}",
              style: TextStyle(
                fontSize: sp(32),
                fontWeight: FontWeight.w500,
                height: 1.0,
                color: const Color(0xff333333),
              ),
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.centerLeft,
              child: SmartRefresher(
                enablePullDown: true,
                enablePullUp: true,
                header: WaterDropHeader(
                  refresh: CircularProgressIndicator(
                      strokeWidth: 2,
                      valueColor:
                      AlwaysStoppedAnimation(Color(0xff999999))),
                  complete: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.done,
                        color: Colors.grey,
                        size: sp(30),
                      ),
                      Container(width: 30.0),
                      Text("加载完成",
                          style: TextStyle(
                              fontSize: sp(28),
                              color: Color(0xff999999)))
                    ],
                  ),
                ),
                footer: CustomFooter(
                  builder: (BuildContext context, LoadStatus? mode) {
                    Widget body;
                    if (mode == LoadStatus.idle) {
                      body = Text(
                        "下拉加载更多",
                        style: TextStyle(
                            color: Color(0xff999999),
                            fontSize: sp(28)),
                      );
                    } else if (mode == LoadStatus.loading) {
                      body = CupertinoActivityIndicator();
                    } else if (mode == LoadStatus.failed) {
                      body = Text(
                        "加载失败，点击重试",
                        style: TextStyle(
                            color: Color(0xff999999),
                            fontSize: sp(28)),
                      );
                    } else if (mode == LoadStatus.canLoading) {
                      body = Text("释放加载更多",
                          style: TextStyle(
                              color: Color(0xff999999),
                              fontSize: sp(28)));
                    } else {
                      body = Text(
                        "暂无更多数据",
                        style: TextStyle(
                            color: Color(0xff999999),
                            fontSize: sp(28)),
                      );
                    }
                    return Container(
                      height: 55.0,
                      child: Center(child: body),
                    );
                  },
                ),
                controller: controller.refreshController,
                onRefresh: () {
                  controller.getAdminList(1);
                },
                onLoading: () {
                  controller.getAdminList(controller.pageNum.value + 1);
                },
                child: controller.adminList.isEmpty
                    ? GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    controller.getAdminList(1);
                  },
                  child: controller.isLoading.value
                      ? Center(
                    child: CupertinoActivityIndicator(),
                  )
                      : empty(),
                )
                    : ListView.separated(
                  physics: ScrollPhysics(),
                  itemBuilder: (c, i) => AdminItem(
                      controller.adminList[i]),
                  itemCount: controller.adminList.length,
                  separatorBuilder: (context, i) => Container(),
                ),
              ),
            ),)
        ],
      ),
    );
  }


  Widget AdminItem(AdminModel element) {
    return Container(
      margin: EdgeInsets.only(bottom: width(20)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Container(
                width: width(100),
                height: width(100),
                clipBehavior: Clip.hardEdge,
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50))
                ),
                margin: EdgeInsets.only(right: width(20)),
                child:MyImage(url: element.avatar.toString(),id: 'admin' + element.id.toString())
                // Image.network(
                //     "${element.avatar}",
                //     width: width(120),
                //     height: width(120),
                //     errorBuilder: (ctx, err, stackTrace) =>
                //         Image.asset(
                //           'images/avatar.png',
                //           width: width(120),
                //           height: width(120),
                //         )
                // ),
              ),
              Column(
                crossAxisAlignment:CrossAxisAlignment.start,
                children: [
                  Container(
                    margin:EdgeInsets.only(bottom: width(10)),
                    child: Text(
                      "${element.nickName}",
                      style: TextStyle(
                        fontSize: sp(28),
                        fontWeight: FontWeight.w400,
                        height: 1.0,
                        color: const Color(0xfff9f9f9),
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      Container(
                        margin:EdgeInsets.only(right: width(10)),
                        width: width(32),
                        height: width(32),
                        child: element.sex == 2 ? Image.asset("images/sex_woman.webp"):Image.asset("images/sex_man.webp"),
                      ),
                      MedalView(level: element.userLevel.toString()),
                    ],
                  )
                ],
              )
            ],
          )
        ],
      ),
    );
  }


  ///默认空页面
  empty() {
    return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: width(220),
              height: width(226),
              margin: EdgeInsets.only(bottom: width(10)),
              child: Image.asset(
                "images/no_data.png",
                width: width(220),
                height: width(226),
              ),
            ),
            Text("暂无更多数据",
                style: TextStyle(
                    fontSize: sp(28),
                    color: Color(0xff768A90),
                    fontWeight: FontWeight.w400,
                    decoration: TextDecoration.none))
          ],
        ));
  }
}

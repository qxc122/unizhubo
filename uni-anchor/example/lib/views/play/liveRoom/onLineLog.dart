import 'package:agora_rtc_rawdata_example/views/play/liveRoom/medal.dart';
import 'package:agora_rtc_rawdata_example/views/processImg/ProcessImg.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/play/controller.dart';
import 'package:agora_rtc_rawdata_example/views/play/liveRoom/controller.dart';

import '../../../utils/screen.dart';

class OnLineListLog extends StatelessWidget {
  var controller = Get.find<LiveRoomController>();

  @override
  Widget build(BuildContext context) {
    return Obx(()=>Container(
      alignment: Alignment.bottomCenter,
      height: width(600),
      padding: EdgeInsets.symmetric(horizontal: width(25)),
      decoration: BoxDecoration(
          color: Color.fromRGBO(0, 0, 0, 0.6),
          borderRadius: BorderRadius.only(topRight: Radius.circular(width(20)), topLeft: Radius.circular(width(20)))
      ),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: width(32)),
            alignment:Alignment.center,
            child: Text(
              "在线人数",
              style: TextStyle(
                fontSize: sp(28),
                fontWeight: FontWeight.w400,
                height: 1.0,
                color: const Color(0xfff9f9f9),
              ),
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ...controller.onlineList.map((element){
                    return Container(
                      margin: EdgeInsets.only(bottom: width(20)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Container(
                                  width: width(100),
                                  height: width(100),
                                  clipBehavior: Clip.hardEdge,
                                  decoration: const BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(50))
                                  ),
                                  margin: EdgeInsets.only(right: width(20)),
                                  child:MyImage(url: element.avatar.toString(),id: 'online' + element.userId.toString())
                                // Image.network(
                                //     "${element.avatar}",
                                //     width: width(120),
                                //     height: width(120),
                                //     errorBuilder: (ctx, err, stackTrace) =>
                                //         Image.asset(
                                //           'images/icon_contribute_def_head.webp',
                                //           width: width(120),
                                //           height: width(120),
                                //         )
                                // ),
                              ),
                              Column(
                                crossAxisAlignment:CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin:EdgeInsets.only(bottom: width(10)),
                                    child: Text(
                                      "${element.nickName}",
                                      style: TextStyle(
                                        fontSize: sp(28),
                                        fontWeight: FontWeight.w400,
                                        height: 1.0,
                                        color: const Color(0xfff9f9f9),
                                      ),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        margin:EdgeInsets.only(right: width(10)),
                                        width: width(32),
                                        height: width(32),
                                        child: element.sex == 2 ? Image.asset("images/sex_woman.png"): element.sex == 1 ?Image.asset("images/sex_man.png"):Image.asset("images/sex_sectet.png"),
                                      ),
                                      MedalView(level: element.level.toString()),
                                    ],
                                  )
                                ],
                              )
                            ],
                          )
                        ],
                      ),
                    );
                  })
                ],
              ),
            ),
          )
        ],
      ),
    ));
  }
}

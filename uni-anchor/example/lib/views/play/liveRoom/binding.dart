import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/play/liveRoom/controller.dart';
class LiveRoombinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LiveRoomController>(()=> LiveRoomController());
  }
}

import 'package:agora_rtc_rawdata_example/views/play/liveRoom/medal.dart';
import 'package:agora_rtc_rawdata_example/views/processImg/ProcessImg.dart';
import 'package:faceunity_ui/Faceunity_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barrage/flutter_barrage.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/views/play/liveRoom/controller.dart';
import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;
import 'package:svgaplayer_flutter/player.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class LiveRoomScreen extends GetView<LiveRoomController> {
  const LiveRoomScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: Colors.white,
        body: WillPopScope(
            onWillPop: () async {
              return false;
            },
            child: Stack(
              children: [
                controller.startPreview.value
                    ? InkWell(
                        onTap: () {
                          if (controller.isFaceFlag.value) {
                            controller.bottomValue.value = 500.0;
                          }
                        },
                        child: Container(
                          child: RtcLocalView.SurfaceView(),
                        ),
                      )
                    : controller.studioThumbImage.value != ''
                        ? MyImage(
                            url: controller.studioThumbImage.value.toString(),
                            id: 'playb')
                        : Container(
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height,
                            // child: Image.asset(
                            //   "images/live_bg.png",
                            //   fit: BoxFit.cover,
                            // ),
                          ),
                Align(
                  alignment: Alignment.topLeft,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: List.of(controller.remoteUid.map(
                        (e) => Container(
                          width: 120,
                          height: 120,
                          child: RtcRemoteView.SurfaceView(
                            uid: e,
                            channelId: controller.channel.value,
                          ),
                        ),
                      )),
                    ),
                  ),
                ),
                // 连送礼物动画
                Positioned(
                  bottom: width(700),
                  left: width(20),
                  child: SlideTransition(
                  position: controller.animationSlideUp,
                  child: FadeTransition(
                      opacity: controller.giftController,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: width(500),
                            child: Row(
                              mainAxisAlignment:
                              MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Container(
                                        width: width(80),
                                        height: width(80),
                                        clipBehavior: Clip.hardEdge,
                                        decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(40))),
                                        margin:
                                        EdgeInsets.only(right: width(20)),
                                        child: MyImage(
                                            url: (controller.preGiftMsg.value
                                                .body?.content?.avatar)
                                                .toString(),
                                            id: (controller.preGiftMsg.value
                                                .body?.content?.userId)
                                                .toString())),
                                    Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(
                                              bottom: width(10)),
                                          child: Text(
                                            "${controller.preGiftMsg.value.body?.content?.nickName}",
                                            style: TextStyle(
                                              fontSize: sp(28),
                                              fontWeight: FontWeight.w400,
                                              height: 1.0,
                                              color: const Color(0xfff9f9f9),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(
                                              bottom: width(10)),
                                          child: Text(
                                            "送出${controller.preGiftMsg.value.body?.content?.liveGift?.giftName}",
                                            style: TextStyle(
                                              fontSize: sp(28),
                                              fontWeight: FontWeight.w400,
                                              height: 1.0,
                                              color: const Color(0xfff9f9f9),
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                                Row(
                                  children: [
                                    Container(
                                        width: width(80),
                                        height: width(80),
                                        clipBehavior: Clip.hardEdge,
                                        decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(40))),
                                        margin:
                                        EdgeInsets.only(right: width(20)),
                                        child: MyImage(
                                            url: (controller
                                                .preGiftMsg
                                                .value
                                                .body
                                                ?.content
                                                ?.liveGift
                                                ?.imageUrl)
                                                .toString(),
                                            id: "preGiftMsgId")),
                                    ScaleTransition(
                                      scale: controller.animationScale,
                                      child: Container(
                                        child: Text(
                                          "X${controller.giftNum.value.toString()}",
                                          style: TextStyle(
                                            fontSize: sp(28),
                                            fontWeight: FontWeight.w400,
                                            height: 1.0,
                                            color: const Color(0xfffffffff),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                          )
                        ],
                      )
                  ),
                ),),
                // Positioned(
                //     bottom: width(700),
                //     left: width(20),
                //     child: Column(
                //       crossAxisAlignment: CrossAxisAlignment.start,
                //       children: [
                //         Container(
                //           width: width(500),
                //           child: Row(
                //             mainAxisAlignment:
                //             MainAxisAlignment.spaceBetween,
                //             children: [
                //               Row(
                //                 children: [
                //                   Container(
                //                       width: width(80),
                //                       height: width(80),
                //                       clipBehavior: Clip.hardEdge,
                //                       decoration: const BoxDecoration(
                //                           borderRadius: BorderRadius.all(
                //                               Radius.circular(40))),
                //                       margin:
                //                       EdgeInsets.only(right: width(20)),
                //                       child: MyImage(
                //                           url: (controller.preGiftMsg.value
                //                               .body?.content?.avatar)
                //                               .toString(),
                //                           id: (controller.preGiftMsg.value
                //                               .body?.content?.userId)
                //                               .toString())),
                //                   Column(
                //                     crossAxisAlignment:
                //                     CrossAxisAlignment.start,
                //                     children: [
                //                       Container(
                //                         margin: EdgeInsets.only(
                //                             bottom: width(10)),
                //                         child: Text(
                //                           "${controller.preGiftMsg.value.body?.content?.nickName}",
                //                           style: TextStyle(
                //                             fontSize: sp(28),
                //                             fontWeight: FontWeight.w400,
                //                             height: 1.0,
                //                             color: const Color(0xfff9f9f9),
                //                           ),
                //                         ),
                //                       ),
                //                       Container(
                //                         margin: EdgeInsets.only(
                //                             bottom: width(10)),
                //                         child: Text(
                //                           "送出${controller.preGiftMsg.value.body?.content?.liveGift?.giftName}",
                //                           style: TextStyle(
                //                             fontSize: sp(28),
                //                             fontWeight: FontWeight.w400,
                //                             height: 1.0,
                //                             color: const Color(0xfff9f9f9),
                //                           ),
                //                         ),
                //                       ),
                //                     ],
                //                   )
                //                 ],
                //               ),
                //               Row(
                //                 children: [
                //                   Container(
                //                       width: width(80),
                //                       height: width(80),
                //                       clipBehavior: Clip.hardEdge,
                //                       decoration: const BoxDecoration(
                //                           borderRadius: BorderRadius.all(
                //                               Radius.circular(40))),
                //                       margin:
                //                       EdgeInsets.only(right: width(20)),
                //                       child: MyImage(
                //                           url: (controller
                //                               .preGiftMsg
                //                               .value
                //                               .body
                //                               ?.content
                //                               ?.liveGift
                //                               ?.imageUrl)
                //                               .toString(),
                //                           id: "preGiftMsgId")),
                //                   // ScaleTransition(
                //                   //   scale: controller.animation,
                //                   //   child: Container(
                //                   //     child: Text(
                //                   //       "X${controller.giftNum.value.toString()}",
                //                   //       style: TextStyle(
                //                   //         fontSize: sp(28),
                //                   //         fontWeight: FontWeight.w400,
                //                   //         height: 1.0,
                //                   //         color: const Color(0xfff9f9f9),
                //                   //       ),
                //                   //     ),
                //                   //   ),
                //                   // )
                //                 ],
                //               )
                //             ],
                //           ),
                //         )
                //       ],
                //     )),
                // 弹幕
                Positioned(
                  top: 20,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.width *
                          MediaQuery.of(context).size.aspectRatio +
                      100,
                  child: BarrageWall(
                    debug: false, // show debug panel
                    speed: 6, // speed of bullet show in screen (seconds)
                    /*
              speed: 8,
              speedCorrectionInMilliseconds: 3000,*/
                    /*
                timelineNotifier: timelineNotifier, // send a BarrageValue notifier let bullet fires using your own timeline*/
                    bullets: controller.bullets,
                    child: new Container(),
                    controller: controller.barrageWallController,
                  ),
                ),
                // 左上角
                Positioned(
                  top: width(80),
                  left: width(12),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: width(140),
                        height: width(60),
                        alignment: Alignment.center,
                        decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            color: Color.fromRGBO(0, 0, 0, 0.2)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              width: width(12),
                              height: width(12),
                              decoration: const BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(12)),
                                  color: Color(0xffFF0000)),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: width(10)),
                              child: Text(
                                "${controller.content.value}",
                                style: TextStyle(
                                  fontSize: sp(20),
                                  fontWeight: FontWeight.w400,
                                  color: const Color(0xffFFFFFF),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      // SizedBox(
                      //   height: width(14),
                      // ),
                      // InkWell(
                      //     onTap: () {
                      //       controller.setGiftLog();
                      //     },
                      //     child: Container(
                      //       width: width(150),
                      //       height: width(40),
                      //       alignment: Alignment.centerLeft,
                      //       decoration: const BoxDecoration(
                      //           borderRadius: BorderRadius.all(Radius.circular(20)),
                      //           color: Color.fromRGBO(0, 0, 0, 0.2)),
                      //       child: Row(
                      //         children: [
                      //           Container(
                      //             width: width(32),
                      //             height: width(32),
                      //             child: Image.asset(
                      //               "images/l_gift.png",
                      //             ),
                      //           ),
                      //           Container(
                      //             child: Text(
                      //               "${controller.giveFirepower.value}",
                      //               style: TextStyle(
                      //                 fontSize: sp(20),
                      //                 fontWeight: FontWeight.w400,
                      //                 color: const Color(0xffFFFFFF),
                      //               ),
                      //             ),
                      //           ),
                      //         ],
                      //       ),
                      //     ))
                    ],
                  ),
                ),
                // 右上角
                Positioned(
                  top: width(80),
                  right: 0,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: width(20)),
                        child: Row(
                          children: [
                            // Stack(
                            //   clipBehavior: Clip.none,
                            //   children: [
                            //     Container(
                            //       width: width(60),
                            //       height: width(60),
                            //       margin: EdgeInsets.only(left: width(10)),
                            //       decoration: BoxDecoration(
                            //           borderRadius: const BorderRadius.all(
                            //               Radius.circular(30)),
                            //           color: const Color.fromRGBO(85, 85, 85, 0.5),
                            //           border: Border.all(
                            //               color: Colors.white, width: width(1))),
                            //       alignment: Alignment.center,
                            //       child: Image.asset(
                            //         "images/avatar.png",
                            //       ),
                            //     ),
                            //     Positioned(
                            //       left: -width(30),
                            //       child: Container(
                            //         width: width(60),
                            //         height: width(60),
                            //         margin: EdgeInsets.only(left: width(10)),
                            //         decoration: BoxDecoration(
                            //             borderRadius: const BorderRadius.all(
                            //                 Radius.circular(30)),
                            //             color:
                            //             const Color.fromRGBO(85, 85, 85, 0.5),
                            //             border: Border.all(
                            //                 color: Colors.white, width: width(1))),
                            //         alignment: Alignment.center,
                            //         child: Image.asset(
                            //           "images/avatar.png",
                            //         ),
                            //       ),
                            //     ),
                            //     Positioned(
                            //       left: -width(60),
                            //       child: Container(
                            //         width: width(60),
                            //         height: width(60),
                            //         margin: EdgeInsets.only(left: width(10)),
                            //         decoration: BoxDecoration(
                            //             borderRadius: const BorderRadius.all(
                            //                 Radius.circular(30)),
                            //             color:
                            //             const Color.fromRGBO(85, 85, 85, 0.5),
                            //             border: Border.all(
                            //                 color: Colors.white, width: width(1))),
                            //         alignment: Alignment.center,
                            //         child: Image.asset(
                            //           "images/avatar.png",
                            //         ),
                            //       ),
                            //     )
                            //   ],
                            // ),
                            InkWell(
                                onTap: () {
                                  controller.getOnLine();
                                },
                                child: Container(
                                  width: width(60),
                                  height: width(60),
                                  margin: EdgeInsets.only(left: width(10)),
                                  decoration: BoxDecoration(
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(30)),
                                      color:
                                          const Color.fromRGBO(85, 85, 85, 0.5),
                                      border: Border.all(
                                          color: Colors.white,
                                          width: width(1))),
                                  alignment: Alignment.center,
                                  child: Text(
                                    "${controller.onlineListId.length}+",
                                    style: TextStyle(
                                      fontSize: sp(16),
                                      fontWeight: FontWeight.w400,
                                      color: const Color(0xffFFFFFF),
                                    ),
                                  ),
                                ))
                          ],
                        ),
                      ),
                      SizedBox(
                        height: width(44),
                      ),
                      InkWell(
                          onTap: () {
                            controller.getOnlineService();
                          },
                          child: Container(
                            width: width(60),
                            height: width(60),
                            margin: EdgeInsets.only(right: width(22)),
                            child: Image.asset(
                              "images/kf.png",
                              width: width(60),
                              height: width(60),
                            ),
                          )),

                      // Container(
                      //   width: width(206),
                      //   height: width(154),
                      //   margin: EdgeInsets.only(right: width(8)),
                      //   child: Image.asset(
                      //     "images/mp.png",
                      //   ),
                      // ),
                    ],
                  ),
                ),

                /// 底部动画
                controller.svga.value != ""
                    ? Positioned(
                        bottom: 0,
                        left: 0,
                        width: MediaQuery.of(context).size.width,
                        child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height,
                            child: SVGAImage(controller.animationController)))
                    : Container(),
                //底部
                Positioned(
                  bottom: width(20),
                  left: 0,
                  width: MediaQuery.of(context).size.width,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: width(20)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: width(520),
                          margin: EdgeInsets.only(bottom: width(10)),
                          child: Text(
                            "${controller.noticeContent.value}",
                            style: TextStyle(
                              fontSize: sp(24),
                              fontWeight: FontWeight.w400,
                              color: const Color(0xffFFF500),
                            ),
                          ),
                        ),
                        Container(
                          width: width(520),
                          height: width(338),
                          child: SingleChildScrollView(
                            controller: controller.scrollController,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ...controller.chatMessageList.value.map((e) {
                                  return e.body?.operatorType == 0 ||
                                          e.body?.operatorType == 7
                                      ? Container(
                                          margin: EdgeInsets.only(
                                              bottom: width(10)),
                                          padding: EdgeInsets.symmetric(
                                              horizontal: width(10),
                                              vertical: width(10)),
                                          decoration: const BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10)),
                                              color:
                                                  Color.fromRGBO(0, 0, 0, 0.2)),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              MedalView(
                                                  level: e.body?.content?.level
                                                          .toString() ??
                                                      '1'),
                                              Container(
                                                padding: EdgeInsets.only(
                                                    left: width(16)),
                                                child: Text(
                                                  "${e.body?.content?.nickName}: ",
                                                  style: TextStyle(
                                                    fontSize: sp(24),
                                                    height: 1.0,
                                                    fontWeight: FontWeight.w400,
                                                    color:
                                                        const Color(0xffFFF500),
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                child: Container(
                                                  child: Text(
                                                    "${e.body?.operatorType == 0 ? e.body?.content?.msgContent : e.body?.content?.barrage}",
                                                    style: TextStyle(
                                                      fontSize: sp(24),
                                                      height: 1.0,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: const Color(
                                                          0xffFFFFFF),
                                                    ),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        )
                                      : e.body?.operatorType == 1
                                          ? Container(
                                              margin: EdgeInsets.only(
                                                  bottom: width(10)),
                                              decoration: const BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(30)),
                                                  gradient: LinearGradient(
                                                      begin:
                                                          Alignment.centerLeft,
                                                      end:
                                                          Alignment.centerRight,
                                                      colors: [
                                                        Color.fromRGBO(
                                                            125, 93, 255, 0.8),
                                                        Color.fromRGBO(
                                                            176, 96, 255, 0)
                                                      ])),
                                              width: width(600),
                                              height: width(50),
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: width(10)),
                                              child: Row(
                                                children: [
                                                  MedalView(
                                                      level: e.body?.content
                                                              ?.level
                                                              .toString() ??
                                                          '1'),
                                                  Container(
                                                    padding: EdgeInsets.only(
                                                        left: width(16)),
                                                    child: Text(
                                                      "${e.body?.content?.nickName}",
                                                      style: TextStyle(
                                                        fontSize: sp(24),
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        color: const Color(
                                                            0xffFFF500),
                                                      ),
                                                    ),
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      "进入房间！",
                                                      style: TextStyle(
                                                        fontSize: sp(24),
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        color: const Color(
                                                            0xffFFFFFF),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                          : e.body?.operatorType == 6
                                              ? Container(
                                                  margin: EdgeInsets.only(
                                                      bottom: width(10)),
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: width(10),
                                                      vertical: width(10)),
                                                  decoration:
                                                      const BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius
                                                                      .circular(
                                                                          20)),
                                                          color: Color.fromRGBO(
                                                              0, 0, 0, 0.2)),
                                                  child: Wrap(
                                                    children: [
                                                      MedalView(
                                                          level: e.body?.content
                                                                  ?.level
                                                                  .toString() ??
                                                              '1'),
                                                      Container(
                                                        padding:
                                                            EdgeInsets.only(
                                                                left:
                                                                    width(16)),
                                                        child: Text(
                                                          "${e.body?.content?.nickName}",
                                                          style: TextStyle(
                                                            fontSize: sp(24),
                                                            fontWeight:
                                                                FontWeight.w400,
                                                            color: const Color(
                                                                0xffFFF500),
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Text(
                                                          "送出【${e.body?.content?.liveGift?.giftName}】X${e.body?.content?.liveGift?.giftNumber ?? '1'}",
                                                          style: TextStyle(
                                                            fontSize: sp(24),
                                                            fontWeight:
                                                                FontWeight.w400,
                                                            color: const Color(
                                                                0xffFFFFFF),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ))
                                              : e.body?.operatorType == 20
                                                  ? Container(
                                                      margin: EdgeInsets.only(
                                                          bottom: width(10)),
                                                      decoration: const BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius
                                                                      .circular(
                                                                          10)),
                                                          color: Color.fromRGBO(
                                                              0, 0, 0, 0.2)),
                                                      padding:
                                                          const EdgeInsets.only(
                                                              left: 10,
                                                              right: 10,
                                                              bottom: 5),
                                                      child: Text.rich(
                                                          TextSpan(children: [
                                                        WidgetSpan(
                                                            // alignment: .PlaceholderAlignment.middle,
                                                            child: Container(
                                                          margin:
                                                              const EdgeInsets
                                                                      .only(
                                                                  right: 4),
                                                          padding: EdgeInsets
                                                              .symmetric(
                                                                  horizontal:
                                                                      width(
                                                                          10)),
                                                          decoration: BoxDecoration(
                                                              color: Color(
                                                                  0xff0500ff),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          4)),
                                                          child: Text(
                                                            "系统",
                                                            style: const TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                height: 1.2,
                                                                fontSize: 12),
                                                          ),
                                                        )),
                                                        TextSpan(
                                                            text: '用户',
                                                            style: const TextStyle(
                                                                color: Color(
                                                                    0xffffffff),
                                                                fontSize: 12)),
                                                        TextSpan(
                                                            text:
                                                                '${e.body?.content?.nickName}',
                                                            style: const TextStyle(
                                                                color: Color(
                                                                    0xffFFF500),
                                                                fontSize: 12)),
                                                        TextSpan(
                                                            text: '在',
                                                            style: const TextStyle(
                                                                color: Color(
                                                                    0xffffffff),
                                                                fontSize: 12)),
                                                        TextSpan(
                                                            text:
                                                                '${e.body?.content?.ticketName}',
                                                            style: const TextStyle(
                                                                color: Color(
                                                                    0xffFFF500),
                                                                fontSize: 12)),
                                                        TextSpan(
                                                            text: '玩法中，已经下注了',
                                                            style: const TextStyle(
                                                                color: Color(
                                                                    0xffffffff),
                                                                fontSize: 12)),
                                                        TextSpan(
                                                            text:
                                                                '${e.body?.content?.amount}',
                                                            style: const TextStyle(
                                                                color: Color(
                                                                    0xffFFF500),
                                                                fontSize: 12)),
                                                        TextSpan(
                                                            text: '元。',
                                                            style: const TextStyle(
                                                                color: Color(
                                                                    0xffffffff),
                                                                fontSize: 12)),
                                                      ])),
                                                    )
                                                  : e.body?.operatorType == 21
                                                      ? Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  bottom: width(
                                                                      10)),
                                                          decoration: const BoxDecoration(
                                                              borderRadius: BorderRadius
                                                                  .all(Radius
                                                                      .circular(
                                                                          10)),
                                                              color: Color
                                                                  .fromRGBO(
                                                                      0,
                                                                      0,
                                                                      0,
                                                                      0.2)),
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  left: 10,
                                                                  right: 10,
                                                                  bottom: 5),
                                                          child: Text.rich(
                                                              TextSpan(
                                                                  children: [
                                                                WidgetSpan(
                                                                    // alignment: .PlaceholderAlignment.middle,
                                                                    child:
                                                                        Container(
                                                                  margin: const EdgeInsets
                                                                          .only(
                                                                      right: 4),
                                                                  padding: EdgeInsets.symmetric(
                                                                      horizontal:
                                                                          width(
                                                                              10)),
                                                                  decoration: BoxDecoration(
                                                                      color: Colors
                                                                              .blue[
                                                                          900],
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              4)),
                                                                  child: Text(
                                                                    "中奖",
                                                                    style: const TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        height:
                                                                            1.2,
                                                                        fontSize:
                                                                            12),
                                                                  ),
                                                                )),
                                                                TextSpan(
                                                                    text: '用户',
                                                                    style: const TextStyle(
                                                                        color: Color(
                                                                            0xffffffff),
                                                                        fontSize:
                                                                            12)),
                                                                TextSpan(
                                                                    text:
                                                                        '${e.body?.content?.nickName}',
                                                                    style: const TextStyle(
                                                                        color: Color(
                                                                            0xffFFF500),
                                                                        fontSize:
                                                                            12)),
                                                                TextSpan(
                                                                    text: '在',
                                                                    style: const TextStyle(
                                                                        color: Color(
                                                                            0xffffffff),
                                                                        fontSize:
                                                                            12)),
                                                                TextSpan(
                                                                    text:
                                                                        '${e.body?.content?.ticketName}',
                                                                    style: const TextStyle(
                                                                        color: Color(
                                                                            0xffFFF500),
                                                                        fontSize:
                                                                            12)),
                                                                TextSpan(
                                                                    text:
                                                                        '玩法中了',
                                                                    style: const TextStyle(
                                                                        color: Color(
                                                                            0xffffffff),
                                                                        fontSize:
                                                                            12)),
                                                                TextSpan(
                                                                    text:
                                                                        '${e.body?.content?.zjMoney}',
                                                                    style: const TextStyle(
                                                                        color: Color(
                                                                            0xffFFF500),
                                                                        fontSize:
                                                                            12)),
                                                                TextSpan(
                                                                    text: '元。',
                                                                    style: const TextStyle(
                                                                        color: Color(
                                                                            0xffffffff),
                                                                        fontSize:
                                                                            12)),
                                                              ])),
                                                        )
                                                      : Container();
                                })
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: width(68),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Container(
                                  width: width(360),
                                  height: width(60),
                                  decoration: const BoxDecoration(
                                      color: Color.fromRGBO(0, 0, 0, 0.2),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(30))),
                                  padding: EdgeInsets.symmetric(
                                      horizontal: width(16)),
                                  alignment: Alignment.centerLeft,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(
                                        child: TextField(
                                          cursorColor: const Color(0xffffffff),
                                          style: TextStyle(
                                              color: const Color(0xffffffff),
                                              fontSize: sp(28)),
                                          decoration: const InputDecoration(
                                            border: InputBorder.none,
                                            hintText: "说点什么叭~~~~~~",
                                            hintStyle: TextStyle(
                                                color: Color.fromRGBO(
                                                    235, 236, 240, 0.6),
                                                fontSize: 14.0),
                                            floatingLabelBehavior:
                                                FloatingLabelBehavior.never,
                                            contentPadding: EdgeInsets.only(
                                                top: 0, bottom: 15),
                                          ),
                                          focusNode: controller.titleFocus,
                                          controller: controller.titleCtr,
                                          onChanged: (String text) {
                                            controller.setTitle(text);
                                          },
                                        ),
                                      ),
                                      InkWell(
                                        onTap: () {
                                          controller.sendMsg();
                                        },
                                        child: Container(
                                          width: width(30),
                                          height: width(30),
                                          child: Image.asset(
                                            "images/l_send.png",
                                            width: width(30),
                                            height: width(30),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                InkWell(
                                  onTap: () {
                                    controller.setManageLog();
                                  },
                                  child: Container(
                                      width: width(60),
                                      height: width(60),
                                      margin: EdgeInsets.only(left: width(12)),
                                      decoration: const BoxDecoration(
                                          color: Color.fromRGBO(0, 0, 0, 0.2),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(30))),
                                      alignment: Alignment.center,
                                      child: Icon(
                                        Icons.more_horiz,
                                        color: Colors.white,
                                      )),
                                ),
                                InkWell(
                                  onTap: () {
                                    controller.setGiftLog();
                                  },
                                  child: Container(
                                    width: width(60),
                                    height: width(60),
                                    margin: EdgeInsets.only(left: width(12)),
                                    decoration: const BoxDecoration(
                                        color: Color.fromRGBO(0, 0, 0, 0.2),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(30))),
                                    alignment: Alignment.center,
                                    child: Image.asset(
                                      "images/l_lw.png",
                                      width: width(40),
                                      height: width(42),
                                    ),
                                  ),
                                ),
                                InkWell(
                                  onTap: () {
                                    controller.setFaceUi();
                                  },
                                  child: Container(
                                    width: width(60),
                                    height: width(60),
                                    margin: EdgeInsets.only(left: width(12)),
                                    decoration: const BoxDecoration(
                                        color: Color.fromRGBO(0, 0, 0, 0.2),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(30))),
                                    alignment: Alignment.center,
                                    child: Image.asset(
                                      "images/l_emoj.png",
                                      width: width(44),
                                      height: width(40),
                                    ),
                                  ),
                                ),
                                // InkWell(
                                //   child: Container(
                                //     width: width(60),
                                //     height: width(60),
                                //     margin: EdgeInsets.only(left: width(12)),
                                //     decoration: const BoxDecoration(
                                //         color: Color.fromRGBO(0, 0, 0, 0.2),
                                //         borderRadius:
                                //             BorderRadius.all(Radius.circular(30))),
                                //     alignment: Alignment.center,
                                //     child: Image.asset(
                                //       "images/l_share.png",
                                //       width: width(36),
                                //       height: width(40),
                                //     ),
                                //   ),
                                // )
                              ],
                            ),
                            InkWell(
                              onTap: () {
                                showDialog(
                                    context: context,
                                    builder: (context) {
                                      return CupertinoAlertDialog(
                                        title: Text('确认退出直播？',
                                            style: TextStyle(fontSize: sp(32))),
                                        actions: <Widget>[
                                          CupertinoDialogAction(
                                            child: Text('离开',
                                                style: TextStyle(
                                                    color: Color(0xff909090))),
                                            onPressed: () {
                                              controller.logoutRoom();
                                            },
                                          ),
                                          CupertinoDialogAction(
                                              child: Text('继续',
                                                  style: TextStyle(
                                                      color:
                                                          Color(0xff9F44FF))),
                                              onPressed: () async {
                                                Get.back();
                                              }),
                                        ],
                                      );
                                    });
                              },
                              child: Container(
                                width: width(60),
                                height: width(60),
                                child: Image.asset(
                                  "images/l_close.png",
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                // 底部美颜
                Positioned(
                    bottom: -width(controller.bottomValue.value),
                    left: 0,
                    width: MediaQuery.of(context).size.width,
                    child: FaceunityUI(
                      cameraCallback: () => controller.engine.switchCamera(),
                    )),
              ],
            ))));
  }
}

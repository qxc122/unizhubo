import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/views/play/liveRoom/controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RoomTTypeLog extends StatelessWidget {
  var controller = Get.find<LiveRoomController>();

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomCenter,
      height: width(340),
      padding: EdgeInsets.symmetric(horizontal: width(25)),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: width(8)),
            decoration: const BoxDecoration(
                color: Color(0xffFFFFFF),
                borderRadius: BorderRadius.all(Radius.circular(15))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ...controller.roomProductList.value.asMap().entries.map((element) {
                  return InkWell(
                    onTap: () {
                      controller.setRoomProduct(element.value);
                    },
                    child: Container(
                      height: width(80),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          border: element.key == (controller.roomProductList.length -1) ? const Border():Border(
                              bottom: BorderSide(
                                  color: Color(0xffD6D6D6), width: width(1)))),
                      child: Text(
                        "${element.value.price}钻石/${controller.currentFType.value == 0 ? '分钟':'场' }",
                        style: TextStyle(
                          fontSize: sp(32),
                          fontWeight: FontWeight.w400,
                          color: const Color(0xff9F44FF),
                        ),
                      ),
                    ),
                  );
                })
              ],
            ),
          ),
        ],
      ),
    );
  }
}

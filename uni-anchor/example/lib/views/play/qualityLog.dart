import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/play/controller.dart';

import '../../utils/screen.dart';

class QualityLog extends StatelessWidget {
  var controller = Get.find<PlayController>();

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomCenter,
      height: width(500),
      padding: EdgeInsets.symmetric(horizontal: width(25)),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: width(8)),
            decoration: const BoxDecoration(
                color: Color(0xffFFFFFF),
                borderRadius: BorderRadius.all(Radius.circular(15))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ...controller.qualityList.value.asMap().entries.map((element) {
                  return InkWell(
                    onTap: () {
                      controller.setQuality(element.key);
                    },
                    child: Container(
                      height: width(80),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          border: element.key == (controller.qualityList.length -1) ? const Border():Border(
                              bottom: BorderSide(
                                  color: Color(0xffD6D6D6), width: width(1)))),
                      child: Text(
                        "${element.value}",
                        style: TextStyle(
                          fontSize: sp(32),
                          fontWeight: FontWeight.w400,
                          color: const Color(0xff9F44FF),
                        ),
                      ),
                    ),
                  );
                })
              ],
            ),
          ),
          SizedBox(
            height: width(24),
          ),
          InkWell(
            onTap: () {
              Get.back();
            },
            child: Container(
              height: width(60),
              alignment: Alignment.center,
              decoration: const BoxDecoration(
                  color: Color(0xffFFFFFF),
                  borderRadius: BorderRadius.all(Radius.circular(15))),
              child: Text(
                "取消",
                style: TextStyle(
                  fontSize: sp(32),
                  fontWeight: FontWeight.w400,
                  color: const Color(0xff000000),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

import 'package:agora_rtc_rawdata_example/models/lottery_model.dart';
import 'package:agora_rtc_rawdata_example/models/room_product_model.dart';
import 'package:agora_rtc_rawdata_example/utils/aes2.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/views/play/lotteryLog.dart';
import 'package:agora_rtc_rawdata_example/views/play/roomFTypeLog.dart';
import 'package:agora_rtc_rawdata_example/views/play/roomTTypeLog.dart';
import 'package:agora_rtc_rawdata_example/views/play/trySeeTimeLog.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart' as getx;
import 'package:dio/dio.dart';
import 'package:image_picker/image_picker.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/services/local_storage_repository.dart';
import 'package:agora_rtc_rawdata_example/views/play/photographLog.dart';
import 'package:agora_rtc_rawdata_example/views/play/qualityLog.dart';
import 'package:agora_rtc_rawdata_example/views/play/roomTypeLog.dart';
import 'package:agora_rtc_rawdata_example/views/play/setTitleLog.dart';

class PlayController extends getx.GetxController {
  getx.RxString titleString = "withdraw".obs;
  getx.RxList<String> tabs = ["绿播", "黄播", "无人直播"].obs;
  getx.RxInt currentIndex = 3.obs;
  getx.RxList<String> qualityList =
      ["超清", "高清(720p)", "标清(480p)", "流畅(360p)"].obs;
  getx.RxInt currentQuality = 3.obs;

  getx.RxList<String> roomFTypeList = ["按时收费商品", "按场收费商品"].obs;
  getx.RxInt currentFRoomType = 6.obs;

  getx.RxList<String> roomTypeList = ["免费", "收费"].obs;
  getx.RxInt currentRoomType = 0.obs;

  getx.RxList<String> picList = ["拍照", "从相册选择"].obs;
  getx.RxInt currentPic = 0.obs;
  getx.RxString title = "输入标题".obs;
  getx.RxString urlPath = "".obs;
  getx.RxString studioThumbImage = "".obs;
  getx.RxString studioThumbImageBaseUrl = "".obs;

  /// 选择游戏相关
  getx.RxString name = "游戏".obs;
  getx.RxString roomTypeText = "免费".obs;
  getx.RxString icon = "".obs;
  getx.RxInt lotteryId = 0.obs;
  getx.RxInt currentFType = 0.obs;
  getx.RxInt giftId = 0.obs;
  getx.RxString trySeeTime = '10'.obs;

  /// 试看时间
  final titleCtr = TextEditingController();
  final FocusNode titleFocus = FocusNode();

  final timeCtr = TextEditingController();
  final FocusNode timeFocus = FocusNode();

  getx.RxList<roomProductModel> roomProductList =
      getx.RxList<roomProductModel>();

  getx.RxList<LotteryModel> lotteryList = getx.RxList<LotteryModel>();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
    getInit();
    getLottery();
  }

  getInit() async {
    var res = await API.getStudioInfo();
    title.value = await SpUtils.getTitle() ?? '输入标题';
    // print(res.toString());
    if (res['code'] == 200) {
      studioThumbImage.value = res['data']['studioThumbImage']!= null
          ? res['data']['studioThumbImage']
          : "";
      // if(studioThumbImage.value.toString().contains(".w.")) {
      //   var response = await Dio().get(studioThumbImage.value.toString(),options: Options(responseType: ResponseType.bytes)); /// options: Options(responseType: ResponseType.bytes
      //   final bytes = response.data;
      //   studioThumbImageBaseUrl.value = EncryptUtil2.addProcessImg(bytes);
      // }
      currentQuality.value = int.parse(res['data']['sharpness']);
      icon.value = res['data']['icon'] != null ? res['data']['icon'] : "";
      name.value = res['data']['name'] != null ? res['data']['name'] : '游戏';
      lotteryId.value =
          res['data']['lotteryId'] != null ? res['data']['lotteryId'] : 0;
    }
  }

  getLottery() async {
    var res = await API.getLottery();
    // print(res.toString());
    lotteryList.clear();
    if (res['code'] == 200) {
      var list =
          (res['data'] as List).map((e) => LotteryModel.fromJson(e)).toList();
      lotteryList.addAll(list);
      // lotteryList.forEach((element) async {
      //   if(element.icon.toString().contains(".w.")) {
      //     var response = await Dio().get(element.icon.toString(),options: Options(responseType: ResponseType.bytes)); /// options: Options(responseType: ResponseType.bytes
      //     final bytes = response.data;
      //     element.baseUrl = EncryptUtil2.addProcessImg(bytes);
      //     print(element.baseUrl.toString());
      //   }
      // });
      lotteryList.insert(
          0,
          LotteryModel(
              icon: "images/game_clear.png", lotteryId: 0, name: '游戏'));
      lotteryList.refresh();
      // getx.Get.bottomSheet(LotteryLog());
    }
  }
  changelottery() {
    getx.Get.bottomSheet(LotteryLog());
  }

  changeLottery(LotteryModel element) {
    icon.value = element.icon!;
    name.value = element.name!;
    lotteryId.value = element.lotteryId!;
    getx.Get.back();
  }

  liveBegin() async {
    if (studioThumbImage.value.isEmpty) {
      BotToast.showText(text: "请上传封面", align: Alignment(0, 0));
    } else if (title.value.isEmpty || title.value == "输入标题") {
      BotToast.showText(text: "请设置直播间标题", align: Alignment(0, 0));
    }
    // else if (lotteryId.value == 0) {
    //   BotToast.showText(text: "请先选择关联的游戏", align: Alignment(0, 0));
    // }
    else {
      var res = await API.liveBegin({
        "colour": currentIndex.value,
        "studioThumbImage": studioThumbImage.value,
        "studioTitle": title.value,
        "chargeType": currentRoomType.value,
        "sharpness": currentQuality.value,
        "gameId": lotteryId.value == 0 ?'': lotteryId.value,
        "trySeeTime": trySeeTime.value,
        "productId": giftId.value
      });
      print(res.toString());
      if (res['code'] == 200) {
        SpUtils.setStudioNum(res['data']['studioNum']);
        getx.Get.toNamed("/liveRoom", arguments: {
          "studioNum": res['data']['studioNum'],
          "channelGameId": "t_${lotteryId.value}",
          "chargeType": res['data']['chargeType'].toString(),
          "sharpness": res['data']['sharpness'],
          "studioLivePushFlow": res['data']['studioLivePushFlow']
        });
      }
    }
  }

  //  切换播放类型
  changeIndex(int index) {
    currentIndex.value = index;
    liveBegin();
  }

  setTitleLog() {
    titleCtr.text = title.value == "输入标题" ? '' : title.value;
    getx.Get.dialog(SetTitleLog(), barrierDismissible: true);
  }

  setLotteryLog() {
    // getx.Get.bottomSheet(LotteryLog());
  }

  setQualityLog() {
    getx.Get.bottomSheet(QualityLog());
  }

  setQuality(int index) {
    currentQuality.value = index;
    getx.Get.back();
  }

  setRoomTypeLog() {
    getx.Get.bottomSheet(RoomTypeLog());
  }

  setRoomFType(int index) async {
    currentFType.value = index;
    currentRoomType.value = index + 1;
    var res = await API.roomProducts({"type": index == 0 ? 6 : 7});
    // print(res.toString());
    if (res['code'] == 200) {
      var list = (res['data'] as List)
          .map((e) => roomProductModel.fromJson(e))
          .toList();
      roomProductList.clear();
      roomProductList.addAll(list);
      getx.Get.back();
      getx.Get.bottomSheet(RoomTTypeLog());
    }
  }

  setRoomProduct(roomProductModel element) {
    giftId.value = element.giftId!;
    roomTypeText.value =
        "${element.price}钻石/${currentFType.value == 0 ? '分钟' : '场'}";
    getx.Get.back();
  }

  setRoomType(int index) {
    currentRoomType.value = index;
    getx.Get.back();
    if (index == 1) {
      getx.Get.bottomSheet(RoomFTypeLog());
    } else {
      roomTypeText.value = "免费";
    }
  }

  setTrySeeTimeLog() {
    timeCtr.text = trySeeTime.value;
    getx.Get.dialog(TrySeeTimeLog(), barrierDismissible: true);
  }

  setPicLog() {
    getx.Get.bottomSheet(PhotographLog());
  }

  setPic(int index) {
    currentPic.value = index;
    if (index == 0) {
      getCamera();
    } else {
      getImage();
    }
  }

  setTitle(String text) {
    if(text == '') {
      title.value = "输入标题";
    } else {
      title.value = text;
    }
  }

  setTime(String text) {
    trySeeTime.value = text;
  }

  // 上传相册图片
  getImage() async {
    final ImagePicker _picker = ImagePicker();
    final pickedFile = await _picker.pickImage(source: ImageSource.gallery, imageQuality: 60);
    print(pickedFile);
    print(pickedFile?.path);
    urlPath.value = pickedFile != null ? pickedFile.path.toString() : "";
    print(88888);
    print(urlPath);
    update();
    if (pickedFile != null) {
      upload(pickedFile);
      EasyLoading.show(status: '上传中...');
    }
  }

  // 拍照上传
  getCamera() async {
    final ImagePicker _picker = ImagePicker();
    final pickedFile = await _picker.pickImage(source: ImageSource.camera, imageQuality: 60);
    print(pickedFile);
    print(pickedFile?.path);
    urlPath.value = pickedFile != null ? pickedFile.path.toString() : "";
    print(88888);
    print(urlPath);
    update();
    if (pickedFile != null) {
      upload(pickedFile);
      EasyLoading.show(status: '上传中...');
    }
  }

  upload(image) async {
    getx.Get.back();
    String path = image.path;
    var name = path.substring(path.lastIndexOf("/") + 1, path.length);
    print(name);
    print(111111);
    FormData formdata = FormData.fromMap(
        {"file": await MultipartFile.fromFile(path, filename: name)});
    print(formdata);
    var res = await API.uploadSingleImg(formdata);
    // print(res);
    if (res["code"] == 200) {
      studioThumbImage.value = res['data']['filekeyurl'];
      EasyLoading.dismiss();
      // BotToast.showText(text: "上传成功", align: Alignment(0, 0));
      var res2 = await API
          .updateStudioThumbImage({"thumbImage": res['data']['filekeyurl']});
      EasyLoading.dismiss();
      if (res2['code'] == 200) {
        BotToast.showText(text: "上传成功", align: Alignment(0, 0));
        getInit();
      } else {
        BotToast.showText(text: res['msg'], align: Alignment(0, 0));
      }
    } else {
      EasyLoading.dismiss();
      BotToast.showText(text: res['msg'], align: Alignment(0, 0));
    }
  }
}

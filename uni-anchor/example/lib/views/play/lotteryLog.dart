import 'dart:convert';

import 'package:agora_rtc_rawdata_example/binding/main_binding.dart';
import 'package:agora_rtc_rawdata_example/views/processImg/ProcessImg.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/play/controller.dart';

import '../../utils/screen.dart';

class LotteryLog extends GetView<PlayController> {
  var controller = Get.find<PlayController>();

  @override
  Widget build(BuildContext context) {
    return Obx(()=>Container(
      alignment: Alignment.bottomCenter,
      height: width(540),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10)),
        color: Colors.white,
      ),
      child: Column(
        children: [
          Container(
            height: width(100),
            margin: EdgeInsets.only(bottom: width(20)),
            padding: EdgeInsets.symmetric(horizontal: width(30)),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10)),
              color: Color(0xffF5f5f5),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Text(
                    "选择游戏",
                    style: TextStyle(
                      fontSize: sp(28),
                      fontWeight: FontWeight.w500,
                      color: const Color(0xff333333),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    Get.back();
                  },
                  child: Container(
                    width: width(36),
                    height: width(36),
                    child: Image.asset(
                      "images/icon_close.png",
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Wrap(
                children: [
                  ...controller.lotteryList.map((element){
                    return InkWell(
                      onTap: () {
                        controller.changeLottery(element);
                      },
                      child: Column(
                        children: [
                          element.lotteryId == 0 ? Container(
                            width: width(175),
                            height: width(100),
                            child: Image.asset(
                              'images/game_clear.png',
                            ),
                          ): Container(
                              width: width(175),
                              height: width(100),
                              // child: Image.network(
                              //     "${element.icon}",
                              //     errorBuilder: (ctx, err, stackTrace) =>
                              //         Image.asset(
                              //           'images/avatar.png',
                              //           width: width(100),
                              //           height: width(100),
                              //         )
                              // ),
                              alignment: Alignment.center,
                              child: Container(
                                  width: width(100),
                                  height: width(100),
                                  key: UniqueKey(),
                                  child:MyImage(url: element.icon.toString(),id: 'ls${element.lotteryId}'),
                              )
                          ),
                          Container(
                            margin: EdgeInsets.only(top: width(10),bottom: width(20)),
                            width: width(160),
                            alignment: Alignment.center,
                            child: Text(
                              "${element.name}",
                              style: TextStyle(
                                fontSize: sp(24),
                                fontWeight: FontWeight.w400,
                                color: const Color(0xff333333),
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  })
                ],
              ),
            ),
          )
        ],
      ),
    ));
  }
}

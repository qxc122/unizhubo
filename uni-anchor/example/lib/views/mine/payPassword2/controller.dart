import 'dart:async';

import 'package:agora_rtc_rawdata_example/models/user_model.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';

class PayPassword2Controller extends GetxController {
  RxString titleString = "修改支付密码".obs;
  RxString oldP ="".obs;
  RxString pass ="".obs;
  RxString newPass ="".obs;
  RxBool flag = false.obs;

  RxBool isShow = true.obs;
  RxBool isShow1 = true.obs;
  RxBool isShow2 = true.obs;
  RxBool isPayPassword = true.obs;

  final titleCtr = TextEditingController();
  final FocusNode titleFocus = FocusNode();

  final passwordCtr = TextEditingController();
  final FocusNode passwordFocus = FocusNode();

  final newPCtr = TextEditingController();
  final FocusNode newPFocus = FocusNode();

  RxInt count = 60.obs;
  RxBool canGetVCode = true.obs; //是否可以获取验证码，默认为`false`
  RxString smsStr = '获取验证码'.obs;
  var userInfo = Rx<UserModel>(UserModel());

  /// 倒计时的计时器。
  Timer? timer;



  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
    getUserInfo();
  }

  @override
  void close() {
    timer?.cancel();
  }

  getUserInfo() async {
    var res = await API.getUserInfo();
    print(res.toString());
    if (res['code'] == 200) {
      var user = UserModel.fromJson(res['data']);
      userInfo.value = user;
      titleCtr.text = userInfo.value.mobilePhone.toString();
      oldP.value = userInfo.value.mobilePhone.toString();
      update();
    }
  }

  sendMsg() async {
    var res = await API.sendSms({
      "areaCode": "86",
      "captchaKey": "",
      "imgCode": '',
      "phone": userInfo.value.mobilePhone,
      "sendType": 4
    });
    print(res);
    if (res['code'] == 200) {
      BotToast.showText(text: "发送成功", align: Alignment(0, 0));
      startTimer();
    } else {
      BotToast.showText(text: res['msg'], align: Alignment(0, 0));
    }
  }

  startTimer() {
    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      count.value--;
      canGetVCode.value = false;
      print(count);
      smsStr.value = '已发送${count.value}s';
      if (count.value == 0) {
        smsStr.value = '重新获取';
        canGetVCode.value = true;
        count.value = 60;
        timer.cancel();
      }
      update();
    });
  }

  setTitle(String text) {
    oldP.value = text;
    verify();
  }

  setTitle1(String text) {
    pass.value = text;
    verify();
  }

  setTitle2(String text) {
    newPass.value = text;
    verify();
  }
  clear() {
    isShow.value = !isShow.value;
    verify();
  }

  clear1() {isShow1.value = !isShow1.value;
    verify();
  }

  clear2() {
    isShow2.value = !isShow2.value;
    verify();
  }

  verify() {
    if (isPayPassword.value) {
      if (oldP.isNotEmpty && pass.isNotEmpty && newPass.isNotEmpty) {
        flag.value = true;
      } else {
        flag.value = false;
      }
    } else {
      if (pass.isNotEmpty && newPass.isNotEmpty) {
        flag.value = true;
      } else {
        flag.value = false;
      }
    }

    update();
  }

  save() async {
    if (isPayPassword.value) {
      if (oldP.isEmpty) {
        BotToast.showText(
            text: "请输入手机号",
            textStyle: const TextStyle(fontSize: 14, color: Colors.white),
            align: const Alignment(0, 0));
      } else if (pass.isEmpty) {
        BotToast.showText(
            text: "请输入验证码",
            textStyle: const TextStyle(fontSize: 14, color: Colors.white),
            align: const Alignment(0, 0));
      } else if (newPass.isEmpty) {
        BotToast.showText(
            text: "请设置支付密码",
            textStyle: const TextStyle(fontSize: 14, color: Colors.white),
            align: const Alignment(0, 0));
      }else {
        var res = await API.setPayPasswordByMessage({
          "password":newPass.value,
          "smsCode":pass.value,
        });
        print(res.toString());
        if (res['code'] == 200) {
          // titleCtr.text = "";
          // oldP.value = "";
          passwordCtr.text = "";
          pass.value = '';
          newPCtr.text = '';
          newPass.value = '';
          BotToast.showText(
              text: "修改成功",
              textStyle: const TextStyle(fontSize: 14, color: Colors.white),
              align: const Alignment(0, 0));
          Get.back(result: true);
        } else {
          BotToast.showText(
              text: res["msg"],
              textStyle: const TextStyle(fontSize: 14, color: Colors.white),
              align: const Alignment(0, 0));
        }
      }
    } else {
      if (pass.isEmpty) {
        BotToast.showText(
            text: "请设置新的密码",
            textStyle: const TextStyle(fontSize: 14, color: Colors.white),
            align: const Alignment(0, 0));
      } else if (newPass.isEmpty) {
        BotToast.showText(
            text: "请确认密码",
            textStyle: const TextStyle(fontSize: 14, color: Colors.white),
            align: const Alignment(0, 0));
      } else if (newPass.value != pass.value) {
        BotToast.showText(
            text: "两次密码不一致",
            textStyle: const TextStyle(fontSize: 14, color: Colors.white),
            align: const Alignment(0, 0));
      } else {
        var res = await API.modifyPayPassword({
          "confirmPassword":newPass.value,
          "password":pass.value
        });
        print(res.toString());
        if (res['code'] == 200) {
          titleCtr.text = "";
          oldP.value = "";
          passwordCtr.text = "";
          pass.value = '';
          newPCtr.text = '';
          newPass.value = '';
          BotToast.showText(
              text: "设置成功",
              textStyle: const TextStyle(fontSize: 14, color: Colors.white),
              align: const Alignment(0, 0));
          Get.back(result: true);
        } else {
          BotToast.showText(
              text: res["msg"],
              textStyle: const TextStyle(fontSize: 14, color: Colors.white),
              align: const Alignment(0, 0));
        }
      }
    }

  }

}

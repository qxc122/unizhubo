import 'package:agora_rtc_rawdata_example/views/mine/payPassword2/controller.dart';
import 'package:get/get.dart';
class PayPassword2binding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PayPassword2Controller>(()=> PayPassword2Controller());
  }
}

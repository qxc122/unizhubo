import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/models/user_model.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';
import 'package:url_launcher/url_launcher.dart';

class MineController extends GetxController
{
  RxString titleString = "out".obs;
  var userInfo = Rx<UserModel>(UserModel());


  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
    getInit();
  }
  getOnlineService() async {
    var res = await API.getOnlineService();
    print(res.toString());
    if (res['code'] == 200) {
      var url = Uri.parse(res['data']['context']);
      Get.toNamed("/webView", arguments: {
        "url":url,
        "title": "客服中心"
      });
      // if (!await launchUrl(url, mode: LaunchMode.externalApplication)) {
      //   throw 'Could not launch $url';
      // }
    }
  }

  toInformation() {
    Get.toNamed("/information")?.then((value) {
      if (value != null && value) {
        getInit();
      }
    });
  }

  getInit() async {
    var res = await API.getUserInfo();
    print(res.toString());
    if (res['code'] == 200) {
      var user = UserModel.fromJson(res['data']);
      userInfo.value = user;
      // String avatar = await SpUtils.getAvatar() ?? '';
      update();
    }
  }

  logout() async {
    var res = await API.logout();
    print(res.toString());
    if (res['code'] == 200) {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      await preferences.remove("token");
      Get.offAllNamed("/login");
    } else {
      BotToast.showText(
          text: res["msg"],
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    }
    Get.offAllNamed("/login");
  }

}

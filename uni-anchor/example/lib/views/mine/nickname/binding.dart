import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/mine/nickname/controller.dart';
class Nicknamebinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<NicknameController>(()=> NicknameController());
  }
}

import 'dart:convert';
import 'dart:math';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/views/mine/nickname/controller.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class NicknameScreen extends GetView<NicknameController> {
  const NicknameScreen({Key? key}) : super(key: key);

  _buildHome(String title, context) {
    return SingleChildScrollView(
      child: Container(
        decoration: BoxDecoration(
            border: Border(
                top: BorderSide(
                    color: const Color(0xffEEEEEE), width: width(1)))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: width(24)),
              color: Colors.white,
              alignment: Alignment.centerLeft,
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      // inputFormatters: [
                      //   LengthLimitingTextInputFormatter(11)
                      // ],
                      keyboardType: TextInputType.text,
                      cursorColor: const Color(0xff333333),
                      style: TextStyle(
                          color: const Color(0xff333333), fontSize: sp(28)),
                      decoration: const InputDecoration(
                        border: InputBorder.none,
                        hintText: "爱太痛",
                        hintStyle:
                        TextStyle(color: Color(0xffC7C7CC), fontSize: 14.0),
                        floatingLabelBehavior: FloatingLabelBehavior.never,
                      ),
                      focusNode: controller.titleFocus,
                      controller: controller.titleCtr,
                      onChanged: (String text) {
                        controller.setTitle(text);
                      },
                    ),
                  ),
                ],
              )
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        backgroundColor: const Color(0xffF5F5F5),
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(width(20)),
            icon: Image.asset(
              "images/back.png",
              width: width(16),
            ),
          ),
          title: Text('修改昵称',
              style: TextStyle(
                  color: const Color(0xff000000),
                  fontSize: sp(34),
                  fontWeight: FontWeight.bold)),
          backgroundColor: Colors.white,
          actions: [
            InkWell(
              onTap: () {
                controller.save();
              },
              child: Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(right: width(30)),
                child: Text('保存',
                    style: TextStyle(
                        color: const Color(0xff9F44FF),
                        fontSize: sp(32),
                        fontWeight: FontWeight.w400)),
              ),
            )
          ],
        ),
        body: GetX<NicknameController>(
            init: controller,
            builder: (_) => SingleChildScrollView(
                child: _buildHome(controller.titleString.value, context))));
  }
}

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';

class NicknameController extends GetxController
{
  RxString titleString = "out".obs;

  RxString nickname ="".obs;

  final titleCtr = TextEditingController();
  final FocusNode titleFocus = FocusNode();



  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
  }

  setTitle(String text) {
    nickname.value = text;
    update();
  }

  save() async {
    if (nickname.value.isEmpty) {
      BotToast.showText(
          text: "请输入昵称",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    } else {
      var res = await API.modifNickName({
        "nickName": nickname.value
      });
      print(res.toString());
      if (res['code'] == 200) {
        Get.back(result: true);
      }
    }
  }

}
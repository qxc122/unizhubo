import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/mine/information/controller.dart';
class Informationbinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<InformationController>(()=> InformationController());
  }
}

import 'dart:convert';
import 'dart:math';
import 'package:agora_rtc_rawdata_example/utils/aes2.dart';
import 'package:agora_rtc_rawdata_example/views/processImg/ProcessImg.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/views/mine/information/controller.dart';
import 'package:svgaplayer_flutter/player.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class InformationScreen extends GetView<InformationController> {
  const InformationScreen({Key? key}) : super(key: key);

  _buildHome(String title, context) {
    return SingleChildScrollView(
      child: Container(
        decoration: BoxDecoration(
            border: Border(
                top: BorderSide(
                    color: const Color(0xffEEEEEE), width: width(1)))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InkWell(
              onTap: () {
                controller.setPicLog();
              },
              child: Container(
                height: width(128),
                padding: EdgeInsets.symmetric(horizontal: width(20)),
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(
                        bottom: BorderSide(
                            color: const Color(0xffEEEEEE), width: width(1)))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        "头像",
                        style: TextStyle(
                          fontSize: sp(24),
                          fontWeight: FontWeight.w400,
                          color: const Color(0xff000000),
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(right: width(12)),
                          width: width(100),
                          height: width(100),
                          clipBehavior: Clip.hardEdge,
                          decoration: const BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(60))),
                          child: MyImage(
                              url: controller.userInfo.value.avatar.toString(),
                              id: "a4"),
                          // ProcessImgView(url: controller.userInfo.value.avatar.toString(),id: "a4",),
                          // ProcessImgView MyImgWidget
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () {
                controller.toNick();
              },
              child: Container(
                height: width(90),
                padding: EdgeInsets.symmetric(horizontal: width(20)),
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(
                        bottom: BorderSide(
                            color: const Color(0xffEEEEEE), width: width(1)))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        "昵称",
                        style: TextStyle(
                          fontSize: sp(24),
                          fontWeight: FontWeight.w400,
                          color: const Color(0xff000000),
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Container(
                          child: Text(
                            "${controller.userInfo.value.nickName ?? ''}",
                            style: TextStyle(
                              fontSize: sp(24),
                              fontWeight: FontWeight.w400,
                              color: const Color(0xff7D7C7C),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: width(10)),
                          width: width(32),
                          height: width(32),
                          child: Image.asset(
                            "images/right_arr.webp",
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            InkWell(
              child: Container(
                height: width(90),
                padding: EdgeInsets.symmetric(horizontal: width(20)),
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(
                        bottom: BorderSide(
                            color: const Color(0xffEEEEEE), width: width(1)))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        "账号",
                        style: TextStyle(
                          fontSize: sp(24),
                          fontWeight: FontWeight.w400,
                          color: const Color(0xff000000),
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Container(
                          child: Text(
                            "${controller.userInfo.value.userAccount ?? ''}",
                            style: TextStyle(
                              fontSize: sp(24),
                              fontWeight: FontWeight.w400,
                              color: const Color(0xff7D7C7C),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: width(30),
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    child: Container(
                      height: width(90),
                      padding: EdgeInsets.symmetric(horizontal: width(20)),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(
                              bottom: BorderSide(
                                  color: const Color(0xffEEEEEE),
                                  width: width(1)))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Text(
                              "上次登录时间",
                              style: TextStyle(
                                fontSize: sp(24),
                                fontWeight: FontWeight.w400,
                                color: const Color(0xff000000),
                              ),
                            ),
                          ),
                          Row(
                            children: [
                              Container(
                                child: Text(
                                  "${controller.userInfo.value.lastLoginTime.toString().split("+")[0].replaceAll(new RegExp(r"T"), " ")}",
                                  style: TextStyle(
                                    fontSize: sp(24),
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xff7D7C7C),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    child: Container(
                      height: width(90),
                      padding: EdgeInsets.symmetric(horizontal: width(20)),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(
                              bottom: BorderSide(
                                  color: const Color(0xffEEEEEE),
                                  width: width(1)))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Text(
                              "注册时间",
                              style: TextStyle(
                                fontSize: sp(24),
                                fontWeight: FontWeight.w400,
                                color: const Color(0xff000000),
                              ),
                            ),
                          ),
                          Row(
                            children: [
                              Container(
                                child: Text(
                                  "${controller.userInfo.value.registerTime.toString().split("+")[0].replaceAll(new RegExp(r"T"), " ")}",
                                  style: TextStyle(
                                    fontSize: sp(24),
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xff7D7C7C),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: width(92),
            ),
            Container(
              alignment: Alignment.center,
              child: Text(
                "*以上内容修改后将不能再次修改",
                style: TextStyle(
                  fontSize: sp(24),
                  fontWeight: FontWeight.w400,
                  color: const Color(0xffFF0000),
                ),
              ),
            ),

            // Container(
            //   color: Colors.red,
            //   width: width(750),
            //   height: width(400),
            //   child: SVGASimpleImage(
            //       resUrl:
            //       "https://img.91momo50.vip/picture/2022/11/23/1762256766635909120.svga?auth_key=1673435272320-642d02de5e97473a8f3a84a0df129d5c-0-55c9c4f3a326e39843f81f03da0e7dc2"),
            // )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        backgroundColor: const Color(0xffF5F5F5),
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              Get.back(result: true);
            },
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(width(20)),
            icon: Image.asset(
              "images/back.png",
              width: width(16),
            ),
          ),
          title: Text('个人资料',
              style: TextStyle(
                  color: const Color(0xff000000),
                  fontSize: sp(34),
                  fontWeight: FontWeight.bold)),
          backgroundColor: Colors.white,
        ),
        body: WillPopScope(
            onWillPop: () async {
              Get.back(result: true);
              return true;
            },
            child: GetX<InformationController>(
                init: controller,
                builder: (_) => SingleChildScrollView(
                    child:
                        _buildHome(controller.titleString.value, context)))));
  }
}

import 'dart:convert';
import 'dart:developer';
import 'dart:typed_data';
import 'package:agora_rtc_rawdata_example/utils/aes.dart';
import 'package:agora_rtc_rawdata_example/utils/aes2.dart';
import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:flutter/services.dart';

import 'package:bot_toast/bot_toast.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:image_picker/image_picker.dart';
import 'package:get/get.dart' as getx;
import 'package:dio/dio.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/models/user_model.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';
import 'package:agora_rtc_rawdata_example/views/mine/information/photographLog.dart';
import 'package:agora_rtc_rawdata_example/views/mine/nickname/controller.dart';

class InformationController extends getx.GetxController {
  var controller2 = getx.Get.put(NicknameController());

  getx.RxString titleString = "out".obs;

  getx.RxList<String> picList = ["拍照", "从相册选择"].obs;
  getx.RxInt currentPic = 0.obs;
  getx.RxString urlPath = "".obs;
  getx.RxBool nickNameStatus = true.obs;

  getx.RxString avatar = "".obs;

  late Uint8List _imgUrl; /// 需要解码地址
  get imgUrl => this._imgUrl;

  var userInfo = getx.Rx<UserModel>(UserModel());

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
    getInit();
  }

  getInit() async {
  var res = await API.getUserInfo();
    print(res.toString());
    if (res['code'] == 200) {
      var user = UserModel.fromJson(res['data']);
      userInfo.value = user;
      userInfo.refresh();
      update();
    }
  }



toNick() {
    if (userInfo.value.nickNameStatus == true) {
      controller2.nickname.value = userInfo.value.nickName.toString();
      controller2.titleCtr.text = userInfo.value.nickName.toString();
      getx.Get.toNamed(AppRoutes.Nickname)?.then((value) {
        if (value != null && value) {
          getInit();
        }
      });
    }

  }

  setPicLog() {
    getx.Get.bottomSheet(PhotographLog());
  }

  setPic(int index) {
    currentPic.value = index;
    if (index == 0) {
      getCamera();
    } else {
      getImage();
    }
  }

  // 上传相册图片
  getImage() async {
    final ImagePicker _picker = ImagePicker();
    final pickedFile = await _picker.pickImage(source: ImageSource.gallery, imageQuality: 80);
    print(pickedFile);
    print(pickedFile?.path);
    urlPath.value = pickedFile != null ? pickedFile.path.toString() : "";
    print(88888);
    print(urlPath);
    update();
    if (pickedFile != null) {
      upload(pickedFile);
      EasyLoading.show(status: '上传中...');
    }
  }

  // 拍照上传
  getCamera() async {
    final ImagePicker _picker = ImagePicker();
    final pickedFile = await _picker.pickImage(source: ImageSource.camera, imageQuality: 80);
    print(pickedFile);
    print(pickedFile?.path);
    urlPath.value = pickedFile != null ? pickedFile.path.toString() : "";
    print(88888);
    print(urlPath);
    update();
    if (pickedFile != null) {
      upload(pickedFile);
      EasyLoading.show(status: '上传中...');
    }
  }

  upload(image) async {
    getx.Get.back();
    String path = image.path;
    var name = path.substring(path.lastIndexOf("/") + 1, path.length);
    print(name);
    print(111111);
    FormData formdata = FormData.fromMap(
        {"file": await MultipartFile.fromFile(path, filename: name)});
    print(formdata);
    var res = await API.uploadSingleImg(formdata);
    print(res);
    if (res["code"] == 200) {
      var res2 = await API.updateAvatar({
        "avatar": res['data']['filekeyurl']
      });
      EasyLoading.dismiss();
      if (res2['code'] == 200) {
        BotToast.showText(text: "上传成功", align: Alignment(0, 0));
        getInit();
      } else {
        BotToast.showText(text: res['msg'], align: Alignment(0, 0));
      }
    } else {
      EasyLoading.dismiss();
      BotToast.showText(text: res['msg'], align: Alignment(0, 0));
    }
  }
}

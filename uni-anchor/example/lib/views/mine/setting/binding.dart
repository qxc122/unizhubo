import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/mine/setting/controller.dart';
class Settingbinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SettingController>(()=> SettingController());
  }
}

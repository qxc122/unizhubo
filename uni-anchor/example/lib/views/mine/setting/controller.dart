import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ota_update/ota_update.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/models/user_model.dart';
import 'package:agora_rtc_rawdata_example/services/config.dart';
import 'package:agora_rtc_rawdata_example/utils/cache.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';
import 'package:agora_rtc_rawdata_example/views/mine/payPassword/controller.dart';
import 'package:agora_rtc_rawdata_example/views/mine/setting/updateLog.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingController extends GetxController
{

  var controller2 = Get.put(PayPasswordController());
  RxString titleString = "out".obs;
  var userInfo = Rx<UserModel>(UserModel());
  RxString cacheSize = '0'.obs;
  RxString showVersion = "".obs;
  RxString downUrl = "".obs;
  RxString content = "".obs;
  RxString isForced = "".obs;
  RxBool isPayPassword = false.obs;
  RxBool showPayFlag = false.obs;
  RxDouble currentProcess = 0.0.obs;



  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
    getInit();
    getAppCache();


  }

  checkVersion() async {
    var res = await API.queryAppVersion();
    print(res.toString());
    if (res['code'] == 200) {
      showVersion.value = res['data']['showVersion'].toString();
      downUrl.value = res['data']['downUrl'].toString();
      content.value = res['data']['content'].toString();
      isForced.value = res['data']['isForced'].toString();
      currentProcess.value = 0.0;
      Get.dialog(UpdateLog(),
          barrierDismissible: isForced.value == '1' ? false : true);
    }
  }

  updateLink() async {
    if (currentProcess == 0.0) {
      print(GlobalConfig.deviceType);
      if (GlobalConfig.deviceType == 'ios') {
        // 如果是ios升级直接跳转app store地址升级
        // const url =
        //     "https://itunes.apple.com/cn/app/id1380512641"; // id 后面的数字换成自己的应用 id 就行了
        if (await canLaunch(downUrl.value)) {
          await launch(downUrl.value, forceSafariVC: false);
        } else {
          throw 'Could not launch $downUrl';
        }
      } else {
        // 如果是android 直接下载apk升级
        try {
          OtaUpdate()
              .execute(
            downUrl.value,
            destinationFilename: 'bball.apk',
          )
              .listen(
                (OtaEvent event) {
              print(event.value);
              if (int.parse(event.value.toString()) == 100) {
                Get.back();
              } else {
                currentProcess.value = (int.parse(event.value.toString()) / 100 ).toDouble();
              }
              update();
            },
          );
        } catch (e) {
          print('Failed to make OTA update. Details: $e');
        }
      }
    }

  }

  getInit() async {
    var res = await API.getUserInfo();
    var res2 = await API.getUserInfo2();
    SharedPreferences preferences = await SharedPreferences.getInstance();
    showPayFlag.value = preferences.getBool('isFamily')?? false;
    print(res.toString());
    if (res['code'] == 200) {
      var user = UserModel.fromJson(res['data']);
      userInfo.value = user;
      update();
    }
    if (res2['code'] == 200) {
      isPayPassword.value = res2['data']["isPayPassword"];
      update();
    }
  }

  toPayPassword() {
    controller2.isPayPassword.value = isPayPassword.value;
    if (isPayPassword.value) {
      controller2.titleString.value = '修改支付密码';
    } else {
      controller2.titleString.value = '设置支付密码';
    }
    Get.toNamed(AppRoutes.PayPassword)?.then((value) {
      if (value != null && value) {
        getInit();
      }
    });
  }

  // 获取缓存的大小
  void getAppCache() {
    CacheUtil.loadApplicationCache().then((value) {
      print(value);
      // 获取到缓存大小
      String cacheSting = CacheUtil.formatSize(value);
      cacheSize.value = cacheSting;
      print(cacheSting);
    });
  }

  clearCache() async {
    // 获取缓存
    Directory directory = await getApplicationDocumentsDirectory();
    //清楚缓存并重新获取缓存大小
    CacheUtil.delDir(directory).then((value) {
      BotToast.showText(text: "清除成功", align: Alignment.center);
      Get.back();
      getAppCache();
    });
  }

  logout() async {
    var res = await API.logout();
    print(res.toString());
    if (res['code'] == 200) {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      await preferences.remove("token");
      Get.offAllNamed("/login");
    } else {
      BotToast.showText(
          text: res["msg"],
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    }
    Get.offAllNamed("/login");
  }

}

import 'dart:convert';
import 'dart:math';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/services/config.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/views/mine/setting/controller.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class SettingScreen extends GetView<SettingController> {
  const SettingScreen({Key? key}) : super(key: key);

  _buildHome(String title, context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: width(12),),
            Container(
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {
                      Get.toNamed("/loginPassword");
                    },
                    child: Container(
                      height: width(90),
                      padding: EdgeInsets.symmetric(horizontal: width(20)),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: const Color(0xffEEEEEE), width: width(1)))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Text(
                              "修改登录密码",
                              style: TextStyle(
                                fontSize: sp(24),
                                fontWeight: FontWeight.w400,
                                color: const Color(0xff000000),
                              ),
                            ),
                          ),
                          Container(
                            width: width(32),
                            height: width(32),
                            child: Image.asset(
                              "images/right_arr.webp",
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    child: Container(
                      height: width(90),
                      padding: EdgeInsets.symmetric(horizontal: width(20)),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: const Color(0xffEEEEEE), width: width(1)))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Text(
                              "地区选择",
                              style: TextStyle(
                                fontSize: sp(24),
                                fontWeight: FontWeight.w400,
                                color: const Color(0xff000000),
                              ),
                            ),
                          ),
                          Container(
                            child: Text(
                              "${controller.userInfo.value.countryName??''}",
                              style: TextStyle(
                                fontSize: sp(24),
                                fontWeight: FontWeight.w400,
                                color: const Color(0xff7D7C7C),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      controller.checkVersion();
                    },
                    child: Container(
                      height: width(90),
                      padding: EdgeInsets.symmetric(horizontal: width(20)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Text(
                              "当前版本",
                              style: TextStyle(
                                fontSize: sp(24),
                                fontWeight: FontWeight.w400,
                                color: const Color(0xff000000),
                              ),
                            ),
                          ),
                          Container(
                            child: Text(
                              "V ${GlobalConfig.deviceType=='ios'? GlobalConfig.iosVersionName:GlobalConfig.andVersionName}.uat",
                              style: TextStyle(
                                fontSize: sp(24),
                                fontWeight: FontWeight.w400,
                                color: const Color(0xff7D7C7C),
                              ),
                            ),
                          ),
                          // Container(
                          //   alignment: Alignment.center,
                          //   width: width(80),
                          //   height: width(36),
                          //   decoration: const BoxDecoration(
                          //     borderRadius: BorderRadius.all(Radius.circular(18)),
                          //     color: Color(0xff9F44FF)
                          //   ),
                          //   child: Text(
                          //     "更新",
                          //     style: TextStyle(
                          //       fontSize: sp(24),
                          //       fontWeight: FontWeight.w400,
                          //       color: const Color(0xffFFFFFF),
                          //     ),
                          //   ),
                          // ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: width(14),),
            Container(
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return CupertinoAlertDialog(
                              title:
                              Text('是否清空全部缓存？', style: TextStyle(fontSize: sp(32))),
                              actions: <Widget>[
                                CupertinoDialogAction(
                                  child: Text('取消',style: TextStyle(color: Color(0xff000000))),
                                  onPressed: () {
                                    Get.back();
                                  },
                                ),
                                CupertinoDialogAction(
                                    child: Text('确定',
                                        style: TextStyle(color: Color(0xff3300FF))),
                                    onPressed: () async {
                                      controller.clearCache();
                                    }),
                              ],
                            );
                          });

                    },
                    child: Container(
                      height: width(90),
                      padding: EdgeInsets.symmetric(horizontal: width(20)),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: const Color(0xffEEEEEE), width: width(1)))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Text(
                              "清理缓存",
                              style: TextStyle(
                                fontSize: sp(24),
                                fontWeight: FontWeight.w400,
                                color: const Color(0xff000000),
                              ),
                            ),
                          ),
                          Row(
                            children: [
                              Container(
                                child: Text(
                                  "${controller.cacheSize.value}",
                                  style: TextStyle(
                                    fontSize: sp(24),
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xff7D7C7C),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: width(14)),
                                width: width(32),
                                height: width(32),
                                child: Image.asset(
                                  "images/right_arr.webp",
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  controller.showPayFlag.value ? InkWell(
                    onTap: () {
                      if (controller.isPayPassword.value) {

                      } else {

                      }
                      controller.toPayPassword();
                    },
                    child: Container(
                      height: width(90),
                      padding: EdgeInsets.symmetric(horizontal: width(20)),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: const Color(0xffEEEEEE), width: width(1)))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Text(
                              "支付密码",
                              style: TextStyle(
                                fontSize: sp(24),
                                fontWeight: FontWeight.w400,
                                color: const Color(0xff000000),
                              ),
                            ),
                          ),
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: width(14)),
                                width: width(32),
                                height: width(32),
                                child: Image.asset(
                                  "images/right_arr.webp",
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ):Container(),
                  InkWell(
                    child: Container(
                      height: width(90),
                      padding: EdgeInsets.symmetric(horizontal: width(20)),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: const Color(0xffEEEEEE), width: width(1)))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Text(
                              "语言设置",
                              style: TextStyle(
                                fontSize: sp(24),
                                fontWeight: FontWeight.w400,
                                color: const Color(0xff000000),
                              ),
                            ),
                          ),
                          Row(
                            children: [
                              Container(
                                child: Text(
                                  "简体中文",
                                  style: TextStyle(
                                    fontSize: sp(24),
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xff7D7C7C),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: width(250),),
            InkWell(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return CupertinoAlertDialog(
                        title:
                        Text('是否退出此账号？', style: TextStyle(fontSize: sp(32))),
                        actions: <Widget>[
                          CupertinoDialogAction(
                            child: Text('取消'),
                            onPressed: () {
                              Get.back();
                            },
                          ),
                          CupertinoDialogAction(
                              child: Text('确定',
                                  style: TextStyle(color: Color(0xff909090))),
                              onPressed: () async {
                                controller.logout();
                              }),
                        ],
                      );
                    });
              },
              child: Container(
                height: width(80),
                margin: EdgeInsets.symmetric(horizontal: width(148)),
                alignment: Alignment.center,
                decoration:  BoxDecoration(
                   image: DecorationImage(
                       image: new AssetImage('images/jzz/abg.png'),
                     fit: BoxFit.fitWidth
                   )
                ),
                child: Text(
                  "切换账号",
                  style: TextStyle(
                    fontSize: sp(28),
                    fontWeight: FontWeight.w400,
                    color: const Color(0xffFFFFFF),
                  ),
                ),
              ),
              // Container(
              //   height: width(60),
              //   margin: EdgeInsets.symmetric(horizontal: width(148)),
              //   alignment: Alignment.center,
              //   decoration: const BoxDecoration(
              //     borderRadius: BorderRadius.all(Radius.circular(30)),
              //     color: Color(0xff9F44FF)
              //   ),
              //   child: Text(
              //     "切换账号",
              //     style: TextStyle(
              //       fontSize: sp(28),
              //       fontWeight: FontWeight.w400,
              //       color: const Color(0xffFFFFFF),
              //     ),
              //   ),
              // ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        backgroundColor: const Color(0xffF5F5F5),
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(width(20)),
            icon: Image.asset("images/back.png",width: width(16),),
          ),
          title: Text('设置',
              style: TextStyle(
                  color: const Color(0xff000000),
                  fontSize: sp(34),
                  fontWeight: FontWeight.bold)),
          backgroundColor: Colors.white,
        ),
        body: GetX<SettingController>(
            init: controller,
            builder: (_) => SingleChildScrollView(
//                header: PhoenixHeader(),
//                onRefresh: () async {
//                  //下拉请求新数据
//                  // controller.onrefresh();
//                },
                child: _buildHome(controller.titleString.value, context))));
  }
}

import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/mine/loginPassword/controller.dart';
class LoginPasswordbinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LoginPasswordController>(()=> LoginPasswordController());
  }
}

import 'dart:convert';
import 'dart:typed_data';

import 'package:bot_toast/bot_toast.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';

class LoginPasswordController extends GetxController {
  RxString titleString = "out".obs;
  RxString oldP ="".obs;
  RxString pass ="".obs;
  RxString newPass ="".obs;
  RxBool flag = false.obs;

  final titleCtr = TextEditingController();
  final FocusNode titleFocus = FocusNode();

  final passwordCtr = TextEditingController();
  final FocusNode passwordFocus = FocusNode();

  final newPCtr = TextEditingController();
  final FocusNode newPFocus = FocusNode();





  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
  }

  setTitle(String text) {
    oldP.value = text;
    verify();
  }

  setTitle1(String text) {
    pass.value = text;
    verify();
  }

  setTitle2(String text) {
    newPass.value = text;
    verify();
  }
  clear() {
    titleCtr.text ="";
    oldP.value = "";
    verify();
  }

  clear1() {
    passwordCtr.text ="";
    pass.value = "";
    verify();
  }

  clear2() {
    newPCtr.text ="";
    newPass.value = "";
    verify();
  }

  verify() {
    if (oldP.isNotEmpty && pass.isNotEmpty && newPass.isNotEmpty && newPass.value == pass.value) {
      flag.value = true;
    } else {
      flag.value = false;
    }
    update();
  }

  String generateMD5(String data) {
    Uint8List content = new Utf8Encoder().convert(data);
    Digest digest = md5.convert(content);
    return digest.toString();
  }

  save() async {
    if (oldP.isEmpty) {
      BotToast.showText(
          text: "请输入旧密码",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    } else if (pass.isEmpty) {
      BotToast.showText(
          text: "请设置新的密码",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    } else if (newPass.isEmpty) {
      BotToast.showText(
          text: "请确认密码",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    } else if (newPass.value != pass.value) {
      BotToast.showText(
          text: "两次密码不一致",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    } else {
      var res = await API.resetPassword({
        "oldPassword":oldP.value,
        "password":pass.value,
        "type":1
      });
      print(res.toString());
      if (res['code'] == 200) {
        titleCtr.text = "";
        oldP.value = "";
        passwordCtr.text = "";
        pass.value = '';
        newPCtr.text = '';
        newPass.value = '';
        BotToast.showText(
            text: "修改成功",
            textStyle: const TextStyle(fontSize: 14, color: Colors.white),
            align: const Alignment(0, 0));
        Get.offAllNamed("/login");
      } else {
        BotToast.showText(
            text: res["msg"],
            textStyle: const TextStyle(fontSize: 14, color: Colors.white),
            align: const Alignment(0, 0));
      }
    }
  }

}

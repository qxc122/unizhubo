import 'dart:convert';
import 'dart:math';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/views/mine/loginPassword/controller.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class LoginPasswordScreen extends GetView<LoginPasswordController> {
  const LoginPasswordScreen({Key? key}) : super(key: key);

  _buildHome(String title, context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: width(10),
              color: const Color(0xffF6F6F6),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: width(20)),
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: width(100),
                    padding: EdgeInsets.symmetric(horizontal: width(24)),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: const Color(0xffEEEEEE),
                                width: width(1)))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: width(40)),
                                width: width(24),
                                height: width(28),
                                child: Image.asset(
                                  "images/aq.png",
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: TextField(
                                  obscureText: true,
                                  keyboardType: TextInputType.visiblePassword,
                                  cursorColor: const Color(0xff333333),
                                  style: TextStyle(
                                      color: const Color(0xff333333),
                                      fontSize: sp(28)),
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "请输入旧密码",
                                    hintStyle: TextStyle(
                                        color: Color(0xffC7C7CC),
                                        fontSize: 14.0),
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.never,
                                  ),
                                  focusNode: controller.titleFocus,
                                  controller: controller.titleCtr,
                                  onChanged: (String text) {
                                    controller.setTitle(text);
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            controller.clear();
                          },
                          child: Container(
                            width: width(30),
                            height: width(30),
                            child: Image.asset(
                              "images/c2.png",
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: width(100),
                    padding: EdgeInsets.symmetric(horizontal: width(24)),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: const Color(0xffEEEEEE),
                                width: width(1)))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: width(40)),
                                width: width(24),
                                height: width(28),
                                child: Image.asset(
                                  "images/password.png",
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: TextField(
                                  obscureText: true,
                                  keyboardType: TextInputType.visiblePassword,
                                  cursorColor: const Color(0xff333333),
                                  style: TextStyle(
                                      color: const Color(0xff333333),
                                      fontSize: sp(28)),
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "请设置新密码（6-18位字符）",
                                    hintStyle: TextStyle(
                                        color: Color(0xffC7C7CC),
                                        fontSize: 14.0),
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.never,
                                  ),
                                  focusNode: controller.passwordFocus,
                                  controller: controller.passwordCtr,
                                  onChanged: (String text) {
                                    controller.setTitle1(text);
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            controller.clear1();
                          },
                          child: Container(
                            width: width(30),
                            height: width(30),
                            child: Image.asset(
                              "images/c2.png",
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: width(100),
                    padding: EdgeInsets.symmetric(horizontal: width(24)),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: const Color(0xffEEEEEE),
                                width: width(1)))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: width(40)),
                                width: width(24),
                                height: width(28),
                                child: Image.asset(
                                  "images/password.png",
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: TextField(
                                  obscureText: true,
                                  keyboardType: TextInputType.visiblePassword,
                                  cursorColor: const Color(0xff333333),
                                  style: TextStyle(
                                      color: const Color(0xff333333),
                                      fontSize: sp(28)),
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "请确认新密码",
                                    hintStyle: TextStyle(
                                        color: Color(0xffC7C7CC),
                                        fontSize: 14.0),
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.never,
                                  ),
                                  focusNode: controller.newPFocus,
                                  controller: controller.newPCtr,
                                  onChanged: (String text) {
                                    controller.setTitle2(text);
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            controller.clear2();
                          },
                          child: Container(
                            width: width(30),
                            height: width(30),
                            child: Image.asset(
                              "images/c2.png",
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: width(186),
            ),
            InkWell(
              onTap: () {
                controller.save();
              },
              child: Container(
                height: width(70),
                margin: EdgeInsets.symmetric(horizontal: width(75)),
                alignment: Alignment.center,
                decoration: controller.flag.value
                    ? const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(35)),
                        color: Color(0xff9F44FF))
                    : const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(35)),
                        color: Color(0xffCCCCCC)),
                child: Text(
                  "确定",
                  style: TextStyle(
                    fontSize: sp(28),
                    fontWeight: FontWeight.w400,
                    color: const Color(0xffFFFFFF),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(width(30)),
            icon: Image.asset(
              "images/back.png",
              width: width(16),
            ),
          ),
          title: Text('修改登录密码',
              style: TextStyle(
                  color: const Color(0xff000000),
                  fontSize: sp(34),
                  fontWeight: FontWeight.bold)),
          backgroundColor: Colors.white,
        ),
        body: GetX<LoginPasswordController>(
            init: controller,
            builder: (_) => SingleChildScrollView(
                child: _buildHome(controller.titleString.value, context))));
  }
}

import 'dart:convert';
import 'dart:math';

import 'package:agora_rtc_rawdata_example/views/processImg/ProcessImg.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';

import 'package:agora_rtc_rawdata_example/views/mine/controller.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class MineScreen extends GetView<MineController> {
  const MineScreen({Key? key}) : super(key: key);

  _buildHome(String title, context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: width(100),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: width(20)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        width: width(120),
                        height: width(120),
                        clipBehavior: Clip.hardEdge,
                        decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(60))
                        ),
                        margin: EdgeInsets.only(right: width(30)),
                        child: MyImage(url: controller.userInfo.value.avatar.toString(),id: 'a3',),
                        // ProcessImgView(url: controller.userInfo.value.avatar.toString(),id: 'a3',),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            child: Text(
                              "${controller.userInfo.value.nickName}",
                              style: TextStyle(
                                fontSize: sp(28),
                                fontWeight: FontWeight.w400,
                                color: const Color(0xff000000),
                              ),
                            ),
                          ),
                          Container(
                            child: Text(
                              "账号：${controller.userInfo.value.userAccount??''}",
                              style: TextStyle(
                                fontSize: sp(28),
                                fontWeight: FontWeight.w400,
                                color: const Color(0xff000000),
                              ),
                            ),
                          ),
                          Container(
                            child: Text(
                              "家族：${controller.userInfo.value.familyName??''}",
                              style: TextStyle(
                                fontSize: sp(28),
                                fontWeight: FontWeight.w400,
                                color: const Color(0xff000000),
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                  InkWell(
                    onTap: () {
                      controller.getOnlineService();
                    },
                    child: Container(
                      width: width(60),
                      height: width(60),
                      child: Image.asset(
                        "images/kf.png",
                        width: width(60),
                        height: width(60),
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: width(52),
            ),
            Container(
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {
                     controller.toInformation();
                    },
                    child: Container(
                      height: width(100),
                      padding: EdgeInsets.symmetric(horizontal: width(20)),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: const Color(0xffEEEEEE),
                                  width: width(1)))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Container(
                                width: width(40),
                                height: width(40),
                                margin: EdgeInsets.only(right: width(28)),
                                child: Image.asset(
                                  "images/grzx.png",
                                ),
                              ),
                              Container(
                                child: Text(
                                  "个人资料",
                                  style: TextStyle(
                                    fontSize: sp(24),
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xff000000),
                                  ),
                                ),
                              )
                            ],
                          ),
                          Container(
                            width: width(32),
                            height: width(32),
                            child: Image.asset(
                              "images/right_arr.webp",
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Get.toNamed("/history");
                    },
                    child: Container(
                      height: width(100),
                      padding: EdgeInsets.symmetric(horizontal: width(20)),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: const Color(0xffEEEEEE),
                                  width: width(1)))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Container(
                                width: width(40),
                                height: width(40),
                                margin: EdgeInsets.only(right: width(28)),
                                child: Image.asset(
                                  "images/history.png",
                                ),
                              ),
                              Container(
                                child: Text(
                                  "开播历史",
                                  style: TextStyle(
                                    fontSize: sp(24),
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xff000000),
                                  ),
                                ),
                              )
                            ],
                          ),
                          Container(
                            width: width(32),
                            height: width(32),
                            child: Image.asset(
                              "images/right_arr.webp",
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Get.toNamed("/setting");
                    },
                    child: Container(
                      height: width(100),
                      padding: EdgeInsets.symmetric(horizontal: width(20)),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: const Color(0xffEEEEEE),
                                  width: width(1)))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Container(
                                width: width(40),
                                height: width(40),
                                margin: EdgeInsets.only(right: width(28)),
                                child: Image.asset(
                                  "images/setting.png",
                                ),
                              ),
                              Container(
                                child: Text(
                                  "设置",
                                  style: TextStyle(
                                    fontSize: sp(24),
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xff000000),
                                  ),
                                ),
                              )
                            ],
                          ),
                          Container(
                            width: width(32),
                            height: width(32),
                            child: Image.asset(
                              "images/right_arr.webp",
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return CupertinoAlertDialog(
                              title:
                              Text('是否确定退出？', style: TextStyle(fontSize: sp(32))),
                              actions: <Widget>[
                                CupertinoDialogAction(
                                  child: Text('取消'),
                                  onPressed: () {
                                    Get.back();
                                  },
                                ),
                                CupertinoDialogAction(
                                    child: Text('确定',
                                        style: TextStyle(color: Color(0xff909090))),
                                    onPressed: () async {
                                      controller.logout();
                                    }),
                              ],
                            );
                          });
                    },
                    child: Container(
                      height: width(100),
                      padding: EdgeInsets.symmetric(horizontal: width(20)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Container(
                                width: width(40),
                                height: width(40),
                                margin: EdgeInsets.only(right: width(28)),
                                child: Image.asset(
                                  "images/logout.png",
                                ),
                              ),
                              Container(
                                child: Text(
                                  "退出",
                                  style: TextStyle(
                                    fontSize: sp(24),
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xff000000),
                                  ),
                                ),
                              )
                            ],
                          ),
                          Container(
                            width: width(32),
                            height: width(32),
                            child: Image.asset(
                              "images/right_arr.webp",
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        backgroundColor: const Color(0xffF5F5F5),
        body: GetX<MineController>(
            init: controller,
            builder: (_) => SingleChildScrollView(
                child: _buildHome(controller.titleString.value, context))));
  }
}

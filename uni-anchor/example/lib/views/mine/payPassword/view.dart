import 'dart:convert';
import 'dart:math';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/views/mine/payPassword/controller.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class PayPasswordScreen extends GetView<PayPasswordController> {
  const PayPasswordScreen({Key? key}) : super(key: key);

  _buildHome(String title, context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: width(10),
              color: const Color(0xffF6F6F6),
            ),
            controller.isPayPassword.value? Container(
              padding: EdgeInsets.symmetric(horizontal: width(20)),
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: width(100),
                    padding: EdgeInsets.symmetric(horizontal: width(24)),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: const Color(0xffEEEEEE),
                                width: width(1)))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: width(40)),
                                width: width(24),
                                height: width(28),
                                child: Image.asset(
                                  "images/aq.png",
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: TextField(
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(6)
                                  ],
                                  obscureText: controller.isShow1.value,
                                  keyboardType: controller.isShow1.value ? TextInputType.number : TextInputType.number,
                                  cursorColor: const Color(0xff333333),
                                  style: TextStyle(
                                      color: const Color(0xff333333),
                                      fontSize: sp(28)),
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "请输入旧密码",
                                    hintStyle: TextStyle(
                                        color: Color(0xffC7C7CC),
                                        fontSize: 14.0),
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.never,
                                  ),
                                  focusNode: controller.passwordFocus,
                                  controller: controller.passwordCtr,
                                  onChanged: (String text) {
                                    controller.setTitle1(text);
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: width(100),
                    padding: EdgeInsets.symmetric(horizontal: width(24)),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: const Color(0xffEEEEEE),
                                width: width(1)))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: width(40)),
                                width: width(24),
                                height: width(28),
                                child: Image.asset(
                                  "images/password.png",
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: TextField(
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(6)
                                  ],
                                  obscureText: controller.isShow2.value,
                                  keyboardType: controller.isShow2.value ? TextInputType.number : TextInputType.number,
                                  cursorColor: const Color(0xff333333),
                                  style: TextStyle(
                                      color: const Color(0xff333333),
                                      fontSize: sp(28)),
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "请设置支付密码（6位纯数字）",
                                    hintStyle: TextStyle(
                                        color: Color(0xffC7C7CC),
                                        fontSize: 14.0),
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.never,
                                  ),
                                  focusNode: controller.newPFocus,
                                  controller: controller.newPCtr,
                                  onChanged: (String text) {
                                    controller.setTitle2(text);
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                        // InkWell(
                        //   onTap: () {
                        //     controller.clear2();
                        //   },
                        //   child: Container(
                        //     width: width(32),
                        //     height: width(20),
                        //     child: Image.asset(
                        //       controller.isShow2.value ?"images/e1.png" : "images/e2.png",
                        //     ),
                        //   ),
                        // )
                      ],
                    ),
                  )
                ],
              ),
            ): Container(
              padding: EdgeInsets.symmetric(horizontal: width(20)),
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: width(100),
                    padding: EdgeInsets.symmetric(horizontal: width(24)),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: const Color(0xffEEEEEE),
                                width: width(1)))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Row(
                            children: [
                              Container(
                                  margin: EdgeInsets.only(right: width(20)),
                                  width: width(24),
                                  height: width(28),
                                  child: Icon(Icons.phone_android,size: sp(32),)
                              ),
                              Container(
                                margin: EdgeInsets.only(right: width(20)),
                                width: width(66),
                                child: Text(
                                  "+86",
                                  style: TextStyle(
                                    fontSize: sp(28),
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xff000000),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: TextField(
                                  enabled: false,
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(6)
                                  ],
                                  // obscureText: controller.isShow.value,
                                  // keyboardType: controller.isShow.value ? TextInputType.number : TextInputType.number,
                                  cursorColor: const Color(0xff333333),
                                  style: TextStyle(
                                      color: const Color(0xff333333),
                                      fontSize: sp(28)),
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "请输入手机号",
                                    hintStyle: TextStyle(
                                        color: Color(0xffC7C7CC),
                                        fontSize: 14.0),
                                    floatingLabelBehavior:
                                    FloatingLabelBehavior.never,
                                  ),
                                  focusNode: controller.titleFocus,
                                  controller: controller.titleCtr,
                                  onChanged: (String text) {
                                    controller.setTitle(text);
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                        // InkWell(
                        //   onTap: () {
                        //     controller.clear();
                        //   },
                        //   child: Container(
                        //     width: width(32),
                        //     height: width(20),
                        //     child: Image.asset(
                        //       controller.isShow.value ?"images/e1.png" : "images/e2.png",
                        //     ),
                        //   ),
                        // )
                      ],
                    ),
                  ),
                  Container(
                    height: width(100),
                    padding: EdgeInsets.symmetric(horizontal: width(24)),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: const Color(0xffEEEEEE),
                                width: width(1)))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: width(40)),
                                width: width(24),
                                height: width(28),
                                child: Image.asset(
                                  "images/aq.png",
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: TextField(
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(6)
                                  ],
                                  obscureText: controller.isShow1.value,
                                  keyboardType: controller.isShow1.value ? TextInputType.number : TextInputType.number,
                                  cursorColor: const Color(0xff333333),
                                  style: TextStyle(
                                      color: const Color(0xff333333),
                                      fontSize: sp(28)),
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "请输入验证码",
                                    hintStyle: TextStyle(
                                        color: Color(0xffC7C7CC),
                                        fontSize: 14.0),
                                    floatingLabelBehavior:
                                    FloatingLabelBehavior.never,
                                  ),
                                  focusNode: controller.passwordFocus,
                                  controller: controller.passwordCtr,
                                  onChanged: (String text) {
                                    controller.setTitle1(text);
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            controller.sendMsg();
                          },
                          child: Container(
                              width: width(156),
                              height: width(50),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(width(40))),
                                  color: Color(0xffD7BFFF)
                              ),
                              alignment: Alignment.center,
                              child: Obx(()=>Text(
                                "${controller.smsStr.value}",
                                style: TextStyle(
                                  fontSize: sp(24),
                                  fontWeight: FontWeight.w400,
                                  color: const Color(0xff3300FF),
                                ),
                              ),)
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: width(100),
                    padding: EdgeInsets.symmetric(horizontal: width(24)),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: const Color(0xffEEEEEE),
                                width: width(1)))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: width(40)),
                                width: width(24),
                                height: width(28),
                                child: Image.asset(
                                  "images/password.png",
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: TextField(
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(6)
                                  ],
                                  obscureText: controller.isShow2.value,
                                  keyboardType: controller.isShow2.value ? TextInputType.number : TextInputType.number,
                                  cursorColor: const Color(0xff333333),
                                  style: TextStyle(
                                      color: const Color(0xff333333),
                                      fontSize: sp(28)),
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "请设置支付密码（6位纯数字）",
                                    hintStyle: TextStyle(
                                        color: Color(0xffC7C7CC),
                                        fontSize: 14.0),
                                    floatingLabelBehavior:
                                    FloatingLabelBehavior.never,
                                  ),
                                  focusNode: controller.newPFocus,
                                  controller: controller.newPCtr,
                                  onChanged: (String text) {
                                    controller.setTitle2(text);
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                        // InkWell(
                        //   onTap: () {
                        //     controller.clear2();
                        //   },
                        //   child: Container(
                        //     width: width(32),
                        //     height: width(20),
                        //     child: Image.asset(
                        //       controller.isShow2.value ?"images/e1.png" : "images/e2.png",
                        //     ),
                        //   ),
                        // )
                      ],
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: width(26),
            ),
            controller.isPayPassword.value? InkWell(
              onTap: () {
                Get.toNamed("/forgetPassword");
              },
              child: Container(
                alignment: Alignment.centerRight,
                child:  Text(
                  "忘记密码？",
                  style: TextStyle(
                    fontSize: sp(28),
                    fontWeight: FontWeight.w400,
                    color: const Color(0xffF333333),
                  ),
                ),
              ),
            ):Container(),
            SizedBox(
              height: width(166),
            ),
            InkWell(
              onTap: () {
                controller.save();
              },
              child: Container(
                height: width(70),
                margin: EdgeInsets.symmetric(horizontal: width(75)),
                alignment: Alignment.center,
                decoration: controller.flag.value
                    ? const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(35)),
                        color: Color(0xff9F44FF))
                    : const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(35)),
                        color: Color(0xffCCCCCC)),
                child: Text(
                  "确认提交",
                  style: TextStyle(
                    fontSize: sp(28),
                    fontWeight: FontWeight.w400,
                    color: const Color(0xffFFFFFF),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(width(30)),
            icon: Image.asset(
              "images/back.png",
              width: width(16),
            ),
          ),
          title: Text('${controller.titleString.value}',
              style: TextStyle(
                  color: const Color(0xff000000),
                  fontSize: sp(34),
                  fontWeight: FontWeight.bold)),
          backgroundColor: Colors.white,
        ),
        body: GetX<PayPasswordController>(
            init: controller,
            builder: (_) => SingleChildScrollView(
                child: _buildHome(controller.titleString.value, context))));
  }
}

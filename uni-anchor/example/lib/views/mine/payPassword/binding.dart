import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/mine/payPassword/controller.dart';
class PayPasswordbinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PayPasswordController>(()=> PayPasswordController());
  }
}

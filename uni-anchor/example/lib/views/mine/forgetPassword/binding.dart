import 'package:agora_rtc_rawdata_example/views/mine/forgetPassword/controller.dart';
import 'package:get/get.dart';
class ForgetPasswordbinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ForgetPasswordController>(()=> ForgetPasswordController());
  }
}

import 'dart:convert';
import 'dart:math';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/views/mine/history/controller.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class HistoryScreen extends GetView<HistoryController> {
  const HistoryScreen({Key? key}) : super(key: key);

  _buildHome(String title, context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: width(64),
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: width(20)),
              child: Text(
                "开播历史，只展示近30天的数据",
                style: TextStyle(
                  fontSize: sp(24),
                  fontWeight: FontWeight.w400,
                  color: const Color(0xff000000),
                ),
              ),
            ),
            controller.historyList.value.length > 0 ? Container(
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ...controller.historyList.value.map((e){
                    return InkWell(
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: width(24),vertical: width(24)),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      color: const Color(0xffEEEEEE),
                                      width: width(1)))),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              InkWell(
                                onTap:() {
                            controller.changeFlag(e);
                        },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      padding:EdgeInsets.symmetric(vertical: width(12)),
                                      child: Text(
                                        "${e.liveDate??''}",
                                        style: TextStyle(
                                          fontSize: sp(26),
                                          fontWeight: FontWeight.w400,
                                          color: const Color(0xff000000),
                                        ),
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(right: width(8)),
                                          child: Text(
                                            "${e.dayHour}H ${e.dayMinute}M",
                                            style: TextStyle(
                                              fontSize: sp(24),
                                              fontWeight: FontWeight.w400,
                                              color: const Color(0xff000000),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: width(12),
                                          height: width(24),
                                          child: e.flag == true ? Image.asset(
                                            "images/btt.png",
                                          ): Image.asset(
                                            "images/bpt.png",
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              e.flag == true ?
                              Column(
                                children: [
                                  ...?e.historyList?.map((element){
                                    return Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          padding:EdgeInsets.only(bottom: width(4)),
                                          child: Text(
                                            "${element.startTime}-${element.endTime}",
                                            style: TextStyle(
                                              fontSize: sp(24),
                                              fontWeight: FontWeight.w400,
                                              color: const Color(0xff7D7C7C),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          child: Text(
                                            "${element.hour}H ${element.minute}M",
                                            style: TextStyle(
                                              fontSize: sp(24),
                                              fontWeight: FontWeight.w400,
                                              color: const Color(0xff7D7C7C),
                                            ),
                                          ),
                                        ),
                                      ],
                                    );
                                  })
                                ],
                              ):Container()

                            ],
                          ),
                        ));
                  })

                ],
              ),
            ):empty()
          ],
        ),
      ),
    );
  }


  ///默认空页面
  empty() {
    return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: width(300),),
            Container(
              width: width(220),
              height: width(226),
              margin: EdgeInsets.only(bottom: width(10)),
              child: Image.asset(
                "images/no_data.png",
                width: width(220),
                height: width(226),
              ),
            ),
            Text("暂无数据",
                style: TextStyle(
                    fontSize: sp(28),
                    color: Color(0xff768A90),
                    fontWeight: FontWeight.w400,
                    decoration: TextDecoration.none))
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        backgroundColor: const Color(0xffF5F5F5),
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(width(20)),
            icon: Image.asset(
              "images/back.png",
              width: width(16),
            ),
          ),
          title: Text('开播历史',
              style: TextStyle(
                  color: const Color(0xff000000),
                  fontSize: sp(34),
                  fontWeight: FontWeight.bold)),
          backgroundColor: Colors.white,
        ),
        body: GetX<HistoryController>(
            init: controller,
            builder: (_) => SingleChildScrollView(
                child: _buildHome(controller.titleString.value, context))));
  }
}

import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/mine/history/controller.dart';
class Historybinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HistoryController>(()=> HistoryController());
  }
}

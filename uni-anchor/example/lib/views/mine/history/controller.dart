import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/models/history_model.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';

class HistoryController extends GetxController
{
  RxString titleString = "out".obs;

  RxList<HistoryModel> historyList = RxList<HistoryModel>();



  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
    getInit();
  }

  changeFlag(HistoryModel item) {
    if (item.flag == true) {
      item.flag = false;
    } else {
      item.flag = true;
    }
    historyList.refresh();
    update();
  }

  getInit() async {
    var res = await API.liveHistory();
    print(res.toString());
    if (res['code'] == 200) {
      var list = (res['data'] as List).map((e) => HistoryModel.fromJson(e)).toList();
      list.forEach((element) {
        element.flag = false;
      });
      historyList.addAll(list);
      update();
    }
  }

}
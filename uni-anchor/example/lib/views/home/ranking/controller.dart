import 'dart:developer';

import 'package:agora_rtc_rawdata_example/utils/aes2.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/models/rank_model.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';

class RankingController extends GetxController
{
  RxString titleString = "out".obs;

  RxList<String> tabs = ["日榜","周榜","月榜"].obs;
  RxInt currentIndex = 0.obs;
  RxBool isPrevious = true.obs; //是否上期：是：查询上一天，上周，月的数据 [必填]
  RxInt type = 1.obs; /// 查询类型 1：日 2:周 3：月 4：总 [必填]
  RxList<RankModel> rankList = RxList<RankModel>();
  var champion = Rx<RankModel>(RankModel());
  var secondPlace = Rx<RankModel>(RankModel());
  var thirdPlace = Rx<RankModel>(RankModel());



  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
    getInit();
  }

  getInit() async {
    rankList.clear();
    champion = Rx<RankModel>(RankModel());
    secondPlace = Rx<RankModel>(RankModel());
    thirdPlace = Rx<RankModel>(RankModel());
    var res = await API.rankingAnchor({
      "isPrevious": isPrevious.value,
      "type": type.value,
    });
    log(res.toString());
    if (res['code'] == 200) {
      var list =
      (res['data'] as List).map((e) => RankModel.fromJson(e)).toList();
      rankList.addAll(list);
      if (rankList.length>=3) {
        champion.value = rankList[0];
        secondPlace.value = rankList[1];
        thirdPlace.value = rankList[2];
      } else if (rankList.length == 2) {
        champion.value = rankList[0];
        secondPlace.value = rankList[1];
      } else if (rankList.length == 1) {
        champion.value = rankList[0];
      }
      update();
    }
  }

//  切换tab
  changeIndex(int index) {
    currentIndex.value = index;
    type.value = index + 1;
    getInit();
    update();
  }

}

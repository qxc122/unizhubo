import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/home/ranking/controller.dart';
class Rankingbinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RankingController>(()=> RankingController());
  }
}

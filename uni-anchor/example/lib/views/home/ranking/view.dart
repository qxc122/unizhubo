import 'dart:convert';
import 'dart:math';

import 'package:agora_rtc_rawdata_example/views/processImg/ProcessImg.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/views/home/ranking/controller.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class RankingScreen extends GetView<RankingController> {
  const RankingScreen({Key? key}) : super(key: key);

  _buildHome(String title, context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            Container(
              alignment: Alignment.center,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)),
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0xff9D7AFF),
                    Color(0xffF3CFFF),
                  ],
                ),
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: width(25),
                  ),
                  Container(
                    alignment: Alignment.center,
                    width: width(360),
                    height: width(40),
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        color: Color(0xffFFFFFF)),
                    child: Row(
                      children: [
                        ...controller.tabs.value.asMap().entries.map((element) {
                          return InkWell(
                            onTap: () {
                              controller.changeIndex(element.key);
                            },
                            child: Container(
                              width: width(120),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20)),
                                  color: element.key ==
                                          controller.currentIndex.value
                                      ? Color(0xff6732FF)
                                      : Color(0xffFFFFFF)),
                              child: Text(
                                "${element.value}",
                                style: TextStyle(
                                  fontSize: sp(28),
                                  fontWeight: FontWeight.w400,
                                  color: element.key ==
                                          controller.currentIndex.value
                                      ? Color(0xffFFFFFF)
                                      : Color(0xff6732FF),
                                ),
                              ),
                            ),
                          );
                        }),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: width(23),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: width(74)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: width(94)),
                          child: Column(
                            children: [
                              Stack(
                                clipBehavior: Clip.none,
                                children: [
                                  Container(
                                    width: width(160),
                                    height: width(160),
                                    clipBehavior: Clip.hardEdge,
                                    decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(85))
                                    ),
                                    child: MyImage(url: controller.secondPlace.value.avatar.toString(),id: 'ranks2'),
                                    // Image.network(
                                    //   "${controller.secondPlace.value.avatar}",
                                    //     errorBuilder: (ctx, err, stackTrace) =>
                                    //         Image.asset(
                                    //           'images/icon_contribute_def_head.webp',
                                    //           width: width(160),
                                    //           height: width(160),
                                    //         )
                                    // ),
                                  ),
                                  Positioned(
                                    top: -width(30),
                                    child: Container(
                                      width: width(170),
                                      height: width(210),
                                      child: Image.asset(
                                        "images/avatar1.png",
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              Container(
                                margin: EdgeInsets.only(top: width(10)),
                                width: width(160),
                                alignment: Alignment.center,
                                child: Text(
                                  "${controller.secondPlace.value.nickName??''}",
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontSize: sp(24),
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xffFFFFFF),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(bottom: width(10)),
                                child: Text(
                                  "粉丝 ${controller.secondPlace.value.fansCount == 'null'?'0':controller.secondPlace.value.fansCount} 个",
                                  style: TextStyle(
                                    fontSize: sp(20),
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xffFFFFFF),
                                  ),
                                ),
                              ),
                              Container(
                                constraints: BoxConstraints(
                                  minWidth: width(140),
                                ),
                                height: width(40),
                                alignment: Alignment.center,
                                decoration: const BoxDecoration(
                                    color: Colors.white,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20))),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: width(30),
                                      height: width(30),
                                      child: Image.asset(
                                        "images/gift.png",
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: width(8)),
                                      child: Text(
                                        "${controller.secondPlace.value.firepower??'0'}",
                                        style: TextStyle(
                                          fontSize: sp(20),
                                          fontWeight: FontWeight.w400,
                                          color: const Color(0xffFF7D05),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          child: Column(
                            children: [
                              Stack(
                                clipBehavior: Clip.none,
                                children: [
                                  Container(
                                    width: width(170),
                                    height: width(170),
                                    clipBehavior: Clip.hardEdge,
                                    decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(85))
                                    ),
                                    child: MyImage(url: controller.champion.value.avatar.toString(),id: 'ranks1'),
                                    // Image.network(
                                    //     "${controller.champion.value.avatar}",
                                    //     errorBuilder: (ctx, err, stackTrace) =>
                                    //         Image.asset(
                                    //           'images/icon_contribute_def_head.webp',
                                    //           width: width(170),
                                    //           height: width(170),
                                    //         )
                                    // ),
                                  ),
                                  Positioned(
                                    top: -width(30),
                                    child: Container(
                                      width: width(170),
                                      height: width(220),
                                      child: Image.asset(
                                        "images/avatar2.png",
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              Container(
                                margin: EdgeInsets.only(top: width(10)),
                                width: width(160),
                                alignment: Alignment.center,
                                child: Text(
                                  "${controller.champion.value.nickName??''}",
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontSize: sp(24),
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xffFFFFFF),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(bottom: width(10)),
                                child: Text(
                                  "粉丝 ${controller.champion.value.fansCount == 'null'?'0':controller.champion.value.fansCount} 个",
                                  style: TextStyle(
                                    fontSize: sp(20),
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xffFFFFFF),
                                  ),
                                ),
                              ),
                              Container(
                                constraints: BoxConstraints(
                                  minWidth: width(140),
                                ),
                                padding: EdgeInsets.symmetric(horizontal: width(10)),
                                height: width(40),
                                alignment: Alignment.center,
                                decoration: const BoxDecoration(
                                    color: Colors.white,
                                    borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: width(30),
                                      height: width(30),
                                      child: Image.asset(
                                        "images/gift.png",
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: width(8)),
                                      child: Text(
                                        "${controller.champion.value.firepower??'0'}",
                                        style: TextStyle(
                                          fontSize: sp(20),
                                          fontWeight: FontWeight.w400,
                                          color: const Color(0xffFF7D05),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: width(94)),
                          child: Column(
                            children: [
                              Stack(
                                clipBehavior: Clip.none,
                                children: [
                                  Container(
                                    width: width(160),
                                    height: width(160),
                                    alignment: Alignment.center,
                                    clipBehavior: Clip.hardEdge,
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(85)),
                                    child: MyImage(url: controller.thirdPlace.value.avatar.toString(),id: 'ranks3'),
                                    // Image.network(
                                    //   "${controller.thirdPlace.value.avatar}",
                                    //     errorBuilder: (ctx, err, stackTrace) =>
                                    //         Image.asset(
                                    //           'images/icon_contribute_def_head.webp',
                                    //           width: width(160),
                                    //           height: width(160),
                                    //         )
                                    // ),
                                  ),
                                  Positioned(
                                    top: -width(30),
                                    child: Container(
                                      width: width(170),
                                      height: width(220),
                                      child: Image.asset(
                                        "images/avatar3.png",
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              Container(
                                margin: EdgeInsets.only(top: width(10)),
                                width: width(160),
                                alignment: Alignment.center,
                                child: Text(
                                  "${controller.thirdPlace.value.nickName??''}",
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontSize: sp(24),
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xffFFFFFF),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(bottom: width(10)),
                                child: Text(
                                  "粉丝 ${controller.thirdPlace.value.fansCount == 'null'?'0':controller.thirdPlace.value.fansCount} 个",
                                  style: TextStyle(
                                    fontSize: sp(20),
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xffFFFFFF),
                                  ),
                                ),
                              ),
                              Container(
                                constraints: BoxConstraints(
                                  minWidth: width(140),
                                ),
                                height: width(40),
                                alignment: Alignment.center,
                                decoration: const BoxDecoration(
                                    color: Colors.white,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20))),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: width(30),
                                      height: width(30),
                                      child: Image.asset(
                                        "images/gift.png",
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: width(8)),
                                      child: Text(
                                        "${controller.thirdPlace.value.firepower??'0'}",
                                        style: TextStyle(
                                          fontSize: sp(20),
                                          fontWeight: FontWeight.w400,
                                          color: const Color(0xffFF7D05),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: width(34),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: width(25)),
              alignment: Alignment.centerLeft,
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ...controller.rankList.asMap().entries.map((e) {
                    return e.key > 2 ?Container(
                      height: width(180),
                      alignment: Alignment.centerLeft,
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: const Color(0xffD8D8D8),
                                  width: width(1)))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(left: width(20)),
                                child: Text(
                                  "${e.value.position ?? ''}",
                                  style: TextStyle(
                                    fontSize: sp(28),
                                    fontWeight: FontWeight.w400,
                                    height: 1.0,
                                    color: const Color(0xff9F9F9F),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    left: width(20), right: width(9)),
                                width: width(110),
                                height: width(110),
                                clipBehavior: Clip.hardEdge,
                                decoration: const BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(55)),),
                                child: MyImage(url: e.value.avatar.toString(),id: 'rank' + e.value.userId.toString(),),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    width:width(300),
                                    child: Text(
                                      "${e.value.nickName ?? ''}",
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontSize: sp(24),
                                        fontWeight: FontWeight.w400,
                                        height: 1.0,
                                        color: const Color(0xff000000),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: width(12),
                                  ),
                                  Container(
                                    child: Text(
                                      "粉丝${e.value.fansCount  == 'null'?'0':e.value.fansCount}个",
                                      style: TextStyle(
                                        fontSize: sp(24),
                                        fontWeight: FontWeight.w400,
                                        height: 1.0,
                                        color: const Color(0xff9F9F9F),
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                          Container(
                            padding:EdgeInsets.symmetric(horizontal: width(12)),
                            constraints: BoxConstraints(
                              minWidth: width(160),
                            ),
                            height: width(50),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(
                                    color: const Color(0xffA041FF),
                                    width: width(2)),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  margin:EdgeInsets.only(right: width(8)),
                                  width: width(30),
                                  height: width(30),
                                  child: Image.asset(
                                    "images/gift.png",
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: width(2)),
                                  child: Text(
                                    "${e.value.firepower}",
                                    style: TextStyle(
                                      fontSize: sp(20),
                                      fontWeight: FontWeight.w400,
                                      color: const Color(0xffFF7D05),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ):SizedBox();
                  })
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        backgroundColor: const Color(0xffF5F5F5),
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(width(20)),
            icon: Image.asset(
              "images/back.png",
              width: width(16),
            ),
          ),
          title: Text('排行榜',
              style: TextStyle(
                  color: const Color(0xff000000),
                  fontSize: sp(34),
                  fontWeight: FontWeight.bold)),
          backgroundColor: Colors.white,
        ),
        body: GetX<RankingController>(
            init: controller,
            builder: (_) => SingleChildScrollView(
                child: _buildHome(controller.titleString.value, context))));
  }
}

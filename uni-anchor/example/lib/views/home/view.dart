import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:agora_rtc_rawdata_example/utils/screen.dart';

import 'package:agora_rtc_rawdata_example/views/home/controller.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class HomeScreen extends GetView<HomeController> {
  const HomeScreen({Key? key}) : super(key: key);

  _buildHome(String title) {
    return Container(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // 数据卡片
        Container(
          height: width(350),
          alignment: Alignment.center,
          margin:
              EdgeInsets.symmetric(horizontal: width(25), vertical: width(20)),
          padding: EdgeInsets.only(top: width(44), bottom: width(96)),
          decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              image: DecorationImage(
                  image: AssetImage("images/home_bg.png"),
                  fit: BoxFit.cover)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              InkWell(
                onTap: () {
                  controller.getInit();
                },
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                    "今日 （地区)",
                    style: TextStyle(
                      fontSize: sp(32),
                      fontWeight: FontWeight.w500,
                      height: 1.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                child: Text(
                  "${controller.liveTime.value}",
                  style: TextStyle(
                    fontSize: sp(56),
                    fontWeight: FontWeight.w400,
                    height: 1.0,
                    color: Colors.white,
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                child: Text(
                  "直播时长",
                  style: TextStyle(
                    fontSize: sp(32),
                    fontWeight: FontWeight.w400,
                    height: 1.0,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
        // list 列表
        Container(
          decoration: BoxDecoration(
              border: Border(
                  top: BorderSide(
                      color: const Color(0xffEEEEEE), width: width(1)))),
          child: Column(
            children: [
              InkWell(
                onTap: () {
                  Get.toNamed("/wallet");
                },
                child: Container(
                  padding: EdgeInsets.only(right: width(25), left: width(46)),
                  height: width(100),
                  alignment: Alignment.centerLeft,
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              color: const Color(0xffEEEEEE), width: width(1)))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Container(
                            width: width(40),
                            height: height(40),
                            child: Image.asset(
                              "images/wallet.png",
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: width(20)),
                            child: Text(
                              "我的钱包",
                              style: TextStyle(
                                fontSize: sp(28),
                                fontWeight: FontWeight.w400,
                                height: 1.0,
                                color: const Color(0xff000000),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        width: width(32),
                        height: width(32),
                        child: Image.asset(
                          "images/right_arr.webp",
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  Get.toNamed("/income");
                },
                child: Container(
                  padding: EdgeInsets.only(right: width(25), left: width(46)),
                  height: width(100),
                  alignment: Alignment.centerLeft,
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              color: const Color(0xffEEEEEE), width: width(1)))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Container(
                            width: width(40),
                            height: width(40),
                            child: Image.asset(
                              "images/income.png",
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: width(20)),
                            child: Text(
                              "收入详情",
                              style: TextStyle(
                                fontSize: sp(28),
                                fontWeight: FontWeight.w400,
                                height: 1.0,
                                color: const Color(0xff000000),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        width: width(32),
                        height: width(32),
                        child: Image.asset(
                          "images/right_arr.webp",
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  Get.toNamed("/ranking");
                },
                child: Container(
                  padding: EdgeInsets.only(right: width(25), left: width(46)),
                  height: width(100),
                  alignment: Alignment.centerLeft,
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              color: const Color(0xffEEEEEE), width: width(1)))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Container(
                            width: width(40),
                            height: width(40),
                            child: Image.asset(
                              "images/ranking.png",
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: width(20)),
                            child: Text(
                              "排行榜",
                              style: TextStyle(
                                fontSize: sp(28),
                                fontWeight: FontWeight.w400,
                                height: 1.0,
                                color: const Color(0xff000000),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        width: width(32),
                        height: width(32),
                        child: Image.asset(
                          "images/right_arr.webp",
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        extendBodyBehindAppBar: false,
        appBar: AppBar(
          toolbarHeight: 0,
          centerTitle: true,
          backgroundColor: Colors.transparent,
        ),
        body: GetX<HomeController>(
            init: controller,
            builder: (_) => SingleChildScrollView(
                child: _buildHome(controller.titleString.value))));
  }
}

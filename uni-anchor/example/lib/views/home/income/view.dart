import 'dart:convert';
import 'dart:math';

import 'package:agora_rtc_rawdata_example/models/incom_model.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pickers/pickers.dart';
import 'package:flutter_pickers/time_picker/model/date_mode.dart';
import 'package:flutter_pickers/time_picker/model/pduration.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:agora_rtc_rawdata_example/components/indicator.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/views/home/income/controller.dart';

UnderlineInputBorder border = const UnderlineInputBorder(borderSide: BorderSide.none);

class IncomeScreen extends StatelessWidget {
  var controller = Get.find<IncomeController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        backgroundColor: const Color(0xffF5F5F5),
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(width(20)),
            icon: Image.asset("images/back.png",width: width(16),),
          ),
          title: Text('收入详情',
              style: TextStyle(
                  color: const Color(0xff000000),
                  fontSize: sp(34),
                  fontWeight: FontWeight.bold)),
          backgroundColor: Colors.white,
        ),
        body: GetBuilder<IncomeController>(
            init: controller,
            builder: (_) => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        color:Colors.white,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            ...controller.tabMap.entries.map((e){
                              return Expanded(child: InkWell(
                                onTap: () {
                                  controller.changeIndex(e.key);
                                },
                                child: Column(
                                  children: [
                                    Container(
                                      margin:EdgeInsets.only(bottom: width(20)),
                                      child: Text(
                                        "${e.value??''}",
                                        style: TextStyle(
                                          fontSize: sp(24),
                                          fontWeight: FontWeight.w400,
                                          color: const Color(0xff000000),
                                        ),
                                      ),
                                    ),
                                    e.key == controller.currentIndex.value ? Container(
                                      width:width(20),
                                      height:width(4),
                                      decoration: const BoxDecoration(
                                          borderRadius:BorderRadius.all(Radius.circular(4)),
                                          gradient: LinearGradient(
                                              begin: Alignment.centerLeft,
                                              end: Alignment.centerRight,
                                              colors:[Color(0xff6129FF), Color(0xffD96CFF
                                              )]
                                          )
                                      ),
                                    ):Container(width:width(20),
                                      height:width(4),)
                                  ],
                                ),
                              ));
                            })
                          ],
                        ) ,
                      ),
                      Container(
                        padding:EdgeInsets.symmetric(horizontal: width(30)),
                        color: Colors.white,
                        height: width(100),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            InkWell(
                              onTap:() {
                                Pickers.showDatePicker(
                                  context,
                                  // 模式，详见下方
                                  mode: DateMode.YM,
                                  // 样式  详见下方样式
                                  // pickerStyle: pickerStyle,
                                  // 默认选中
                                  selectDate: PDuration(year:int.parse(controller.yearMonth.value.split("-")[0]),month: int.parse(controller.yearMonth.value.split("-")[1])),
                                  minDate: PDuration(year: 2010),
                                  maxDate: PDuration(year: 2040),
                                  onConfirm: (p) {
                                    print('longer >>> 返回数据：$p');
                                    controller.setTime(p.year.toString(),p.month.toString());
                                  },
                                  // onChanged: (p) => print(p),
                                );
                              },
                              child: Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(right: width(12)),
                                    child: Text(
                                      "${controller.yearMonth.value}",
                                      style: TextStyle(
                                        fontSize: sp(24),
                                        fontWeight: FontWeight.w400,
                                        color: const Color(0xff000000),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width: width(10),
                                    height: width(16),
                                    child: Image.asset(
                                      "images/bp.png",
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  child: Text(
                                    "支出：${controller.expend.value}",
                                    style: TextStyle(
                                      fontSize: sp(20),
                                      fontWeight: FontWeight.w400,
                                      color: const Color(0xff808080),
                                    ),
                                  ),
                                ),
                                Container(
                                  child: Text(
                                    "收入：${controller.income.value}",
                                    style: TextStyle(
                                      fontSize: sp(20),
                                      fontWeight: FontWeight.w400,
                                      color: const Color(0xff808080),
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  height: width(30),
                  color: Color(0xffF5F5F5),
                ),
                Expanded(
                  child: Container(
                  alignment: Alignment.centerLeft,
                  child: SmartRefresher(
                    enablePullDown: true,
                    enablePullUp: true,
                    header: WaterDropHeader(
                      refresh: CircularProgressIndicator(
                          strokeWidth: 2,
                          valueColor:
                          AlwaysStoppedAnimation(Color(0xff999999))),
                      complete: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.done,
                            color: Colors.grey,
                            size: sp(30),
                          ),
                          Container(width: 30.0),
                          Text("加载完成",
                              style: TextStyle(
                                  fontSize: sp(28),
                                  color: Color(0xff999999)))
                        ],
                      ),
                    ),
                    footer: CustomFooter(
                      builder: (BuildContext context, LoadStatus? mode) {
                        Widget body;
                        if (mode == LoadStatus.idle) {
                          body = Text(
                            "下拉加载更多",
                            style: TextStyle(
                                color: Color(0xff999999),
                                fontSize: sp(28)),
                          );
                        } else if (mode == LoadStatus.loading) {
                          body = CupertinoActivityIndicator();
                        } else if (mode == LoadStatus.failed) {
                          body = Text(
                            "加载失败，点击重试",
                            style: TextStyle(
                                color: Color(0xff999999),
                                fontSize: sp(28)),
                          );
                        } else if (mode == LoadStatus.canLoading) {
                          body = Text("释放加载更多",
                              style: TextStyle(
                                  color: Color(0xff999999),
                                  fontSize: sp(28)));
                        } else {
                          body = Text(
                            "暂无更多数据",
                            style: TextStyle(
                                color: Color(0xff999999),
                                fontSize: sp(28)),
                          );
                        }
                        return Container(
                          height: 55.0,
                          child: Center(child: body),
                        );
                      },
                    ),
                    controller: controller.refreshController,
                    onRefresh: () {
                      controller.getInit(1);
                    },
                    onLoading: () {
                      controller.getInit(controller.pageNum.value + 1);
                    },
                    child: controller.incomeList.isEmpty
                        ? GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () {
                        controller.getInit(1);
                      },
                      child: controller.isLoading.value
                          ? Center(
                        child: CupertinoActivityIndicator(),
                      )
                          : empty(),
                    )
                        : ListView.separated(
                      physics: ScrollPhysics(),
                      itemBuilder: (c, i) => IncomeItem(
                          controller.incomeList[i]),
                      itemCount: controller.incomeList.length,
                      separatorBuilder: (context, i) => Container(),
                    ),
                  ),
                ),)
              ],
            )));
  }

  Widget IncomeItem(IncomeModel e) {
    return Container(
      height: width(132),
      padding: EdgeInsets.symmetric(horizontal: width(30)),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color:const Color(0xffF2F2F2),width: width(1))),
          color: Colors.white
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment:CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                children: [
                  Container(
                    width: width(30),
                    height: width(30),
                    child: Image.asset(
                      "images/${e.changeType}.png",
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: width(4)),
                    child: Text(
                      "${e.changeName??''}",
                      style: TextStyle(
                        fontSize: sp(28),
                        fontWeight: FontWeight.w400,
                        color: const Color(0xff000000),
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(left: width(20)),
                child: Text(
                  "【${controller.tabMap.value[e.changeType]??'其他'}】${e.createTime??''}",
                  style: TextStyle(
                    fontSize: sp(24),
                    fontWeight: FontWeight.w400,
                    color: const Color(0xffCBCBCB),
                  ),
                ),
              ),
            ],
          ),
          Container(
            child: Text(
              "${e.changeMoney??''}",
              style: TextStyle(
                fontSize: sp(28),
                fontWeight: FontWeight.w500,
                color: const Color(0xff5A5A5A),
              ),
            ),
          ),
        ],
      ),
    );
  }


  ///默认空页面
  empty() {
    return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: width(220),
              height: width(226),
              margin: EdgeInsets.only(bottom: width(10)),
              child: Image.asset(
                "images/no_data.png",
                width: width(220),
                height: width(226),
              ),
            ),
            Text("暂无数据",
                style: TextStyle(
                    fontSize: sp(28),
                    color: Color(0xff768A90),
                    fontWeight: FontWeight.w400,
                    decoration: TextDecoration.none))
          ],
        ));
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/models/incom_model.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';

class IncomeController extends GetxController
{
  RxString titleString = "out".obs;
  List<String> tabs = ['全部', '订阅', '礼物', '佣金', '提现', '其他'];
  /// 27订阅, 32礼物, 14佣金, 34提现, 26其他
  RxMap tabMap = {0:'全部',27: "订阅", 32: "礼物", 14: "佣金", 34:"提现", 26:"其他"}.obs;
  RxInt currentIndex = 0.obs;
  RxString startTime = "".obs;
  RxString endTime = "".obs;
  RxString yearMonth = "".obs;
  RxString expend = "".obs; /// 支出金额
  RxString income = "".obs;  /// 收入金额，根据收入类型统计
  RxInt pageNum = 0.obs;
  RxInt totalNum = 0.obs;
  RxInt changeType = 0.obs;


  RxList<IncomeModel> incomeList = RxList<IncomeModel>();
  RefreshController refreshController =
  RefreshController(initialRefresh: false);
  RxBool isLoading = false.obs;



  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
    DateTime today = new DateTime.now();
    String dateSlug ="${today.year.toString()}-${today.month.toString().padLeft(2,'0')}";
    print(dateSlug);
    yearMonth.value = dateSlug.toString();
    getInit(1);
    getMoney();
  }
  getMoney() async {
    var res = await API.anchorIncomeMonth({
      "changeType": changeType.value,
      "yearMonth": yearMonth.value,
    });
    print(res.toString());
    if (res['code'] == 200) {
      income.value = res['data']['income'].toString();
      expend.value = res['data']['expend'].toString();
      update();
    }
  }

  setTime (String year, String month) {
    if (month.toString().length==1){
      yearMonth.value = year + "-0" + month;
      startTime.value = year + "-0" + month;
    } else {
      yearMonth.value = year + "-" + month;
      startTime.value =year + "-" + month;
    }
    getInit(1);
    getMoney();
    update();
  }

  getInit(int page) async {
    pageNum.value = page;
    if (pageNum.value == 1) {
      incomeList.clear();
    }
    var res = await API.anchorIncomeDetails({
      "changeType": changeType.value,
      "startTime": yearMonth.value,
      "endTime": endTime.value,
      "pageNum": pageNum.value,
      "pageSize":20
    });
    print(res.toString());
    if (res['code'] == 200) {
      totalNum.value = res['data']["total"];
      var list =
      (res['data']['list'] as List).map((e) => IncomeModel.fromJson(e)).toList();
      incomeList.addAll(list);
      refreshController.refreshCompleted();
      refreshController.loadComplete();
      if (list.length < 20) {
        refreshController.loadNoData();
      }
      isLoading.value = false;
      update();
    }
  }

  changeIndex(int index){
    currentIndex.value = index;
    changeType.value = index;
    getInit(1);
    getMoney();
    update();
  }

}

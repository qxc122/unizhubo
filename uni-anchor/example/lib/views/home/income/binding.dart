import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/home/income/controller.dart';
class Incomebinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<IncomeController>(()=> IncomeController());
  }
}

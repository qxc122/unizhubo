import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/home/controller.dart';

import '../../../utils/screen.dart';



class UpdateLog extends Dialog {
  @override
  Widget build(BuildContext context) {
    var controller = Get.find<HomeController>();

    //自定义弹框内容
    return WillPopScope(
        onWillPop: () async {
          return Future.value(controller.isForced.value=='1'?false:true);
        },
        child:Material(
            type: MaterialType.transparency,
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Stack(
                    clipBehavior: Clip.none,
                    children: [
                      Container(
                        width: width(560),
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(5))
                        ),
                        child: Column(
                          children: [
                            Container(
                              alignment:Alignment.center,
                              width: width(560),
                              height: width(240),
                              child: Image.asset(
                                "images/update_bg.png",
                                fit: BoxFit.cover,
                              ),
                            ),
                            SizedBox(height: width(18),),
                            Container(
                              child: Text(
                                "B 球邀请您体验最新版本",
                                style: TextStyle(
                                  fontSize: sp(32),
                                  fontWeight: FontWeight.w400,
                                  height: 1.0,
                                  color: const Color(0xff000000),
                                ),
                              ),
                            ),
                            SizedBox(height: width(18),),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: width(35)),
                              child: SingleChildScrollView(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        "${controller.content.value}",
                                        style: TextStyle(
                                          fontSize: sp(32),
                                          fontWeight: FontWeight.w400,
                                          height: 1.0,
                                          color: const Color(0xff979797),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(height: width(72),),
                            controller.currentProcess.value != 0.0 ? Container(
                              margin: EdgeInsets.symmetric(horizontal: width(30)),
                              child: LinearProgressIndicator(
                                value: controller.currentProcess.value,
                                minHeight: width(8),
                                backgroundColor: Color(0xffE1E1E1),
                                valueColor: AlwaysStoppedAnimation(Color(0xff9F44FF)),
                              ),
                            ):Container(),
                            controller.currentProcess.value == 0.0 ? controller.isForced.value == '1'? Container(
                              width: width(420),
                              height: width(60),
                              child: Row(
                                children: [
                                  Expanded(child: InkWell(
                                    onTap: () {
                                      controller.updateLink();
                                    },
                                    child: Container(
                                      height: width(60),
                                      decoration: const BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(30)),
                                          color: Color(0xff7032FF)
                                      ),
                                      alignment: Alignment.center,
                                      child: Text(
                                        "立即更新",
                                        style: TextStyle(
                                          fontSize: sp(36),
                                          fontWeight: FontWeight.w400,
                                          color: const Color(0xffFFFFFF),
                                        ),
                                      ),
                                    ),
                                  ),),
                                ],
                              ),
                            ): Container(
                              width: width(420),
                              height: width(60),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(30)),
                                  border: Border.all(color: Color(0xff7032FF),width: width(2))
                              ),
                              child: Row(
                                children: [
                                  Expanded(child: InkWell(
                                    onTap: () {
                                      controller.updateLink();
                                    },
                                    child: Container(
                                      width: width(210),
                                      height: width(60),
                                      decoration: const BoxDecoration(
                                          borderRadius: BorderRadius.only(topLeft:Radius.circular(30),bottomLeft:Radius.circular(30)),
                                          color: Color(0xff7032FF)
                                      ),
                                      alignment: Alignment.center,
                                      child: Text(
                                        "立即更新",
                                        style: TextStyle(
                                          fontSize: sp(36),
                                          fontWeight: FontWeight.w400,
                                          color: const Color(0xffFFFFFF),
                                        ),
                                      ),
                                    ),
                                  ),),
                                  Expanded(child: InkWell(
                                    onTap: () {
                                      Get.back();
                                    },
                                    child: Container(
                                      height: width(60),
                                      alignment: Alignment.center,
                                      child: Text(
                                        "下次再说",
                                        style: TextStyle(
                                          fontSize: sp(36),
                                          fontWeight: FontWeight.w400,
                                          color: const Color(0xff7032FF),
                                        ),
                                      ),
                                    ),
                                  ))
                                ],
                              ),
                            ):Container(),
                            SizedBox(height: width(42),),
                          ],
                        ),
                      ),
                      controller.isForced.value == '1'?Container():Positioned(
                          right: -width(20),
                          top: -width(20),
                          child: InkWell(
                            onTap: () {
                              Get.back();
                            },
                            child: Container(
                              width: width(40),
                              height: width(40),
                              child: Image.asset(
                                "images/close.png",
                              ),
                            ),
                          )
                      )
                    ],
                  )
                ],
              ),
            )));
  }
}

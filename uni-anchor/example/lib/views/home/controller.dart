// import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ota_update/ota_update.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/services/config.dart';
import 'package:agora_rtc_rawdata_example/services/local_storage_repository.dart';
import 'package:agora_rtc_rawdata_example/views/home/updateLog.dart';
import 'package:url_launcher/url_launcher.dart';

class HomeController extends GetxController {
  RxString titleString = "home".obs;

  RxString showVersion = "".obs;
  RxString downUrl = "".obs;
  RxString content = "".obs;
  RxString isForced = "".obs;
  RxBool isPayPassword = false.obs;
  RxDouble currentProcess = 0.0.obs;
  RxString liveTime = "00:00:00".obs;

  /// 直播时间，单位秒

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
    getInit();
  }

  getInit() async {

    var res = await API.liveTime();
    if (res['code'] == 200) {
      var d = Duration(seconds: res['data']['liveTime']);
      List<String> parts = d.toString().split(".")[0].split(":");
      liveTime.value = int.parse(parts[0].toString()) < 1
          ? "0${parts[0]}" + ":" + parts[1] + ":" + parts[2]
        : parts[0] + ":" + parts[1] + ":" + parts[2] ;
      update();
    }
  }

  checkVersion() async {
    var res = await API.queryAppVersion();
    if (res['code'] == 200) {
      showVersion.value = res['data']['showVersion'].toString();
      downUrl.value = res['data']['downUrl'].toString();
      content.value = res['data']['content'].toString();
      isForced.value = res['data']['isForced'].toString();
      currentProcess.value = 0.0;
      Get.dialog(UpdateLog(),
          barrierDismissible: isForced.value == '1' ? false : true);
    }
  }

  updateLink() async {
    if (currentProcess == 0.0) {
      if (GlobalConfig.deviceType == 'ios') {
        // 如果是ios升级直接跳转app store地址升级
        if (await canLaunch(downUrl.value)) {
          await launch(downUrl.value, forceSafariVC: false);
        } else {
          throw 'Could not launch $downUrl';
        }
      } else {
        // 如果是android 直接下载apk升级
        try {
          OtaUpdate()
              .execute(
            downUrl.value,
            destinationFilename: 'bball.apk',
          )
              .listen(
                (OtaEvent event) {
              if (int.parse(event.value.toString()) == 100) {
                Get.back();
              } else {
                currentProcess.value = (int.parse(event.value.toString()) / 100 ).toDouble();
              }
              update();
            },
          );
        } catch (e) {
          print('Failed to make OTA update. Details: $e');
        }
      }
    }

  }
}

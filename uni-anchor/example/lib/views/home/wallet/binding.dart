import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/home/wallet/controller.dart';
class Walletbinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<WalletController>(()=> WalletController());
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';

class WalletController extends GetxController
{
  RxString titleString = "out".obs;
  RxString otherMoney = "0.0".obs; /// 其他收入(弹幕)
  RxString rebatesMoney = "0.0".obs; /// 佣金收入收入
  RxString todayIncome = "0.0".obs; /// 当日收入
  RxString totalAssets = "0.0".obs; /// 总资产
  RxString giftMoney = "0.0".obs; /// 礼物收入
  RxString monthIncome = "0.0".obs; /// 当月收入
  RxString focusMoney = "0.0".obs; /// 关注收入
  RxString shortcutOptionsUnit = "0.0".obs; /// 金币单位



  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
    getInit();
  }

  getInit() async {
    var res = await API.assets();
    print(res.toString());
    if (res['code'] == 200) {
      focusMoney.value = res['data']['focusMoney'].toString();
      giftMoney.value = res['data']['giftMoney'].toString();
      monthIncome.value = res['data']['monthIncome'].toString();
      otherMoney.value = res['data']['otherMoney'].toString();
      rebatesMoney.value = res['data']['rebatesMoney'].toString();
      shortcutOptionsUnit.value = res['data']['shortcutOptionsUnit'].toString();
      todayIncome.value = res['data']['todayIncome'].toString();
      totalAssets.value = res['data']['totalAssets'].toString();
      update();
    }
  }

}
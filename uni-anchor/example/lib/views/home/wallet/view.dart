import 'dart:convert';
import 'dart:math';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/views/home/wallet/controller.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class WalletScreen extends GetView<WalletController> {
  const WalletScreen({Key? key}) : super(key: key);

  _buildHome(String title, context) {
    return SingleChildScrollView(
      child: Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.symmetric(horizontal: width(15)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.symmetric(horizontal: width(20)).copyWith(top: width(40),bottom: width(50)),
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  image: DecorationImage(
                      image: AssetImage("images/wallet_bg.png"),
                      fit: BoxFit.cover)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.only(left: width(30)),
                    child: Text(
                      "总资产（${controller.shortcutOptionsUnit.value}）",
                      style: TextStyle(
                        fontSize: sp(28),
                        fontWeight: FontWeight.w400,
                        height: 1.2,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.only(left: width(30)),
                    child: Text(
                      "${controller.totalAssets.value}",
                      style: TextStyle(
                        fontSize: sp(28),
                        fontWeight: FontWeight.w400,
                        height: 1.4,
                        color: const Color(0xffFAFF00),
                      ),
                    ),
                  ),
                  SizedBox(height: width(24),),
                  const Divider(color: Colors.white),
                  SizedBox(height: width(44),),
                  Row(
                    children: [
                      Expanded(
                        child: Column(
                          children: [
                            Container(
                              child: Text(
                                "今日收入（${controller.shortcutOptionsUnit.value}）",
                                style: TextStyle(
                                  fontSize: sp(28),
                                  fontWeight: FontWeight.w400,
                                  height: 1.2,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            Container(
                              child: Text(
                                "${controller.todayIncome.value}",
                                style: TextStyle(
                                  fontSize: sp(28),
                                  fontWeight: FontWeight.w400,
                                  height: 1.4,
                                  color: const Color(0xffFAFF00),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: width(1),
                        height: width(60),
                        color: Colors.white,
                      ),
                      Expanded(
                        child: Column(
                          children: [
                            Container(
                              child: Text(
                                "本月收入（${controller.shortcutOptionsUnit.value}）",
                                style: TextStyle(
                                  fontSize: sp(28),
                                  fontWeight: FontWeight.w400,
                                  height: 1.2,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            Container(
                              child: Text(
                                "${controller.monthIncome.value}",
                                style: TextStyle(
                                  fontSize: sp(28),
                                  fontWeight: FontWeight.w400,
                                  height: 1.4,
                                  color: const Color(0xffFAFF00),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
            SizedBox(height: width(56),),
            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "资产来源",
                style: TextStyle(
                  fontSize: sp(28),
                  fontWeight: FontWeight.w400,
                  height: 1.0,
                  color: const Color(0xff000000),
                ),
              ),
            ),
            SizedBox(height: width(20),),
            Container(
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: width(80),
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(horizontal: width(20)),
                    decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Color(0xffF9F9F9),width: width(1)))
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Container(
                              width: width(30),
                              height: width(30),
                              child: Image.asset(
                                "images/dy.png",
                                width: width(30),
                                height: width(30),
                              ),
                            ),
                            Container(
                              padding:EdgeInsets.only(left: width(14)),
                              child: Text(
                                "订阅",
                                style: TextStyle(
                                  fontSize: sp(24),
                                  fontWeight: FontWeight.w400,
                                  color: const Color(0xff000000),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          child: Text(
                            "${controller.focusMoney.value}",
                            style: TextStyle(
                              fontSize: sp(24),
                              fontWeight: FontWeight.w400,
                              color: const Color(0xff000000),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: width(80),
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(horizontal: width(20)),
                    decoration: BoxDecoration(
                        border: Border(bottom: BorderSide(color: Color(0xffF9F9F9),width: width(1)))
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Container(
                              width: width(30),
                              height: width(30),
                              child: Image.asset(
                                "images/lw.png",
                                width: width(30),
                                height: width(30),
                              ),
                            ),
                            Container(
                              padding:EdgeInsets.only(left: width(14)),
                              child: Text(
                                "礼物",
                                style: TextStyle(
                                  fontSize: sp(24),
                                  fontWeight: FontWeight.w400,
                                  color: const Color(0xff000000),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          child: Text(
                            "${controller.giftMoney.value}",
                            style: TextStyle(
                              fontSize: sp(24),
                              fontWeight: FontWeight.w400,
                              color: const Color(0xff000000),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: width(80),
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(horizontal: width(20)),
                    decoration: BoxDecoration(
                        border: Border(bottom: BorderSide(color: Color(0xffF9F9F9),width: width(1)))
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Container(
                              width: width(30),
                              height: width(30),
                              child: Image.asset(
                                "images/yj.png",
                                width: width(30),
                                height: width(30),
                              ),
                            ),
                            Container(
                              padding:EdgeInsets.only(left: width(14)),
                              child: Text(
                                "佣金",
                                style: TextStyle(
                                  fontSize: sp(24),
                                  fontWeight: FontWeight.w400,
                                  color: const Color(0xff000000),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          child: Text(
                            "${controller.rebatesMoney.value}",
                            style: TextStyle(
                              fontSize: sp(24),
                              fontWeight: FontWeight.w400,
                              color: const Color(0xff000000),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: width(80),
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(horizontal: width(20)),
                    decoration: BoxDecoration(
                        border: Border(bottom: BorderSide(color: Color(0xffF9F9F9),width: width(1)))
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Container(
                              width: width(30),
                              height: width(30),
                              child: Image.asset(
                                "images/qt.png",
                                width: width(30),
                                height: width(30),
                              ),
                            ),
                            Container(
                              padding:EdgeInsets.only(left: width(14)),
                              child: Text(
                                "其他",
                                style: TextStyle(
                                  fontSize: sp(24),
                                  fontWeight: FontWeight.w400,
                                  color: const Color(0xff000000),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          child: Text(
                            "${controller.otherMoney.value}",
                            style: TextStyle(
                              fontSize: sp(24),
                              fontWeight: FontWeight.w400,
                              color: const Color(0xff000000),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        backgroundColor: const Color(0xffF5F5F5),
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(width(20)),
            icon: Image.asset("images/back.png",width: width(16),),
          ),
          title: Text('我的钱包',
              style: TextStyle(
                  color: Color(0xff000000),
                  fontSize: sp(34),
                  fontWeight: FontWeight.bold)),
          backgroundColor: Colors.white,
        ),
        body: GetX<WalletController>(
            init: controller,
            builder: (_) => SingleChildScrollView(
                child: _buildHome(controller.titleString.value, context))));
  }
}

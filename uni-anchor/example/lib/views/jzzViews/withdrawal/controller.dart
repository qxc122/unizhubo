import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';

class WithdrawalController extends GetxController {
  RxString titleString = "out".obs;
  RxString withdrawAccount = "0.0".obs;

  /// 账户可提现（金币/USD）
  RxString withdrawAnchor = "0.0".obs;

  /// 主播账户未提现（金币/USD）

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
    getInit();
  }

  getInit() async {
    var res = await API.canWithdraw();
    print(res.toString());
    if (res['code'] == 200) {
      withdrawAccount.value = res['data']['withdrawAccount'].toString();
      withdrawAnchor.value = res['data']['withdrawAnchor'].toString();
      update();
    }
  }

  getOnlineService() async {
    var res = await API.getOnlineService();
    print(res.toString());
    if (res['code'] == 200) {
      var url = Uri.parse(res['data']['context']);
      Get.toNamed("/webView", arguments: {"url": url, "title": "客服中心"});
    }
  }

  toDetail() {
    Get.toNamed("/anchorWithdrawal")?.then((value) {
      if (value != null && value) {
        getInit();
      }
    });
  }

  toDetail2() {
    Get.toNamed("/withdrawalDetail")?.then((value) {
      if (value != null && value) {
        getInit();
      }
    });
  }
}

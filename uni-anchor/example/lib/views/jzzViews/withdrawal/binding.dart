
import 'package:agora_rtc_rawdata_example/views/jzzViews/withdrawal/controller.dart';
import 'package:get/get.dart';
class Withdrawalbinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<WithdrawalController>(()=> WithdrawalController());
  }
}

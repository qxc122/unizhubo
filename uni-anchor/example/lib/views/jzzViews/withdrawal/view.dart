import 'dart:convert';
import 'dart:math';

import 'package:agora_rtc_rawdata_example/views/jzzViews/withdrawal/controller.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class WithdrawalScreen extends GetView<WithdrawalController> {
  const WithdrawalScreen({Key? key}) : super(key: key);

  _buildHome(String title, context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: width(30)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: width(248),
            padding: EdgeInsets.symmetric(horizontal: width(30)).copyWith(top: width(40),bottom: width(24)),
            alignment: Alignment.centerLeft,
            decoration:  BoxDecoration(
                image: DecorationImage(
                    image: new AssetImage('images/jzz/cbg1.png'),
                    fit: BoxFit.fitWidth
                )
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Text(
                    "账户可提现（金币）",
                    style: TextStyle(
                      fontSize: sp(28),
                      fontWeight: FontWeight.w400,
                      color: const Color(0xffFFFFFF),
                    ),
                  ),
                ),
                SizedBox(height: width(42),),
                Container(
                  child: Text(
                    "${controller.withdrawAccount.value}",
                    style: TextStyle(
                      fontSize: sp(40),
                      fontWeight: FontWeight.w400,
                      color: const Color(0xffFFF500),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    InkWell(
                      onTap: () {
                        controller.toDetail2();
                      },
                      child: Container(
                        alignment: Alignment.center,
                        width: width(200),
                        height: width(50),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(width(40))),
                            border: Border.all(color: Color(0xffffffff),width: width(2))
                        ),
                        child: Text(
                          "提现",
                          style: TextStyle(
                            fontSize: sp(28),
                            fontWeight: FontWeight.w400,
                            color: const Color(0xffFFFFFF),
                          ),
                        ),
                      ),
                    )
                  ],
                )
              ],
            )
          ),
          SizedBox(height: width(32),),
          Container(
              height: width(248),
              padding: EdgeInsets.symmetric(horizontal: width(30)).copyWith(top: width(40),bottom: width(24)),
              alignment: Alignment.centerLeft,
              decoration:  BoxDecoration(
                  image: DecorationImage(
                      image: new AssetImage('images/jzz/cbg2.png'),
                      fit: BoxFit.fitWidth
                  )
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      "主播可提现（金币）",
                      style: TextStyle(
                        fontSize: sp(28),
                        fontWeight: FontWeight.w400,
                        color: const Color(0xffFFFFFF),
                      ),
                    ),
                  ),
                  SizedBox(height: width(42),),
                  Container(
                    child: Text(
                      "${controller.withdrawAnchor.value}",
                      style: TextStyle(
                        fontSize: sp(40),
                        fontWeight: FontWeight.w400,
                        color: const Color(0xffFFF500),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      InkWell(
                        onTap: () {
                          controller.toDetail();
                        },
                        child: Container(
                          alignment: Alignment.center,
                          width: width(200),
                          height: width(50),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(width(40))),
                              border: Border.all(color: Color(0xffffffff),width: width(2))
                          ),
                          child: Text(
                            "提现",
                            style: TextStyle(
                              fontSize: sp(28),
                              fontWeight: FontWeight.w400,
                              color: const Color(0xffFFFFFF),
                            ),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              )
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        backgroundColor: const Color(0xffFFFFFF),
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(width(20)),
            icon: Image.asset(
              "images/back.png",
              width: width(16),
            ),
          ),
          title: Text('提取',
              style: TextStyle(
                  color: const Color(0xff000000),
                  fontSize: sp(34),
                  fontWeight: FontWeight.bold)),
          backgroundColor: Colors.white,
        ),
        body: GetX<WithdrawalController>(
            init: controller,
            builder: (_) => SingleChildScrollView(
                child: _buildHome(controller.titleString.value, context))));
  }
}

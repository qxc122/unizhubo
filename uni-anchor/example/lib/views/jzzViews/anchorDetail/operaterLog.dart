import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/anchorDetail/controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OperaterLog extends StatelessWidget {
  var controller = Get.find<AnchorDetailController>();

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomCenter,
      height: width(270),
      padding: EdgeInsets.symmetric(horizontal: width(25)),
      child: Column(
        children: [
          InkWell(
            onTap: () {
              controller.frozenAnchor();
            },
            child: Container(
              height: width(80),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(width(40))),
                  color: Colors.white),
              child: Text(
                "${controller.isFrozen.value ? '解封': '封停'}",
                style: TextStyle(
                  fontSize: sp(32),
                  fontWeight: FontWeight.w400,
                  color: const Color(0xffFF0000),
                ),
              ),
            ),
          ),
          SizedBox(
            height: width(24),
          ),
          InkWell(
            onTap: () {
              Get.back();
            },
            child: Container(
              height: width(80),
              alignment: Alignment.center,
              decoration: const BoxDecoration(
                  color: Color(0xffFFFFFF),
                  borderRadius: BorderRadius.all(Radius.circular(40))),
              child: Text(
                "取消",
                style: TextStyle(
                  fontSize: sp(32),
                  fontWeight: FontWeight.w400,
                  color: const Color(0xff000000),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

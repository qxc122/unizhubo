import 'package:agora_rtc_rawdata_example/models/anchor_report_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pickers/pickers.dart';
import 'package:flutter_pickers/time_picker/model/date_mode.dart';
import 'package:flutter_pickers/time_picker/model/pduration.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';

class AnchorReportController extends GetxController
{
  RxString titleString = "out".obs;
  List<String> tabs = ['全部', '订阅', '礼物', '佣金', '提现', '其他'];
  /// 27订阅, 32礼物, 14佣金, 34提现, 26其他
  RxMap tabMap = {0:'全部',27: "订阅", 32: "礼物", 14: "佣金", 34:"提现", 26:"其他"}.obs;
  RxInt currentIndex = 0.obs;
  RxString startTime = "".obs;
  RxString endTime = "".obs;
  RxString yearMonth = "".obs;
  RxString expend = "".obs; /// 支出金额
  RxString income = "".obs;  /// 收入金额，根据收入类型统计
  RxInt pageNum = 0.obs;
  RxInt totalNum = 0.obs;
  RxInt changeType = 0.obs;


  RxList<AnchorReportModel> anchorReportList = RxList<AnchorReportModel>();
  RefreshController refreshController =
  RefreshController(initialRefresh: false);
  RxBool isLoading = false.obs;



  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
    DateTime today = new DateTime.now();
    String dateSlug ="${today.year.toString()}-${today.month.toString().padLeft(2,'0')}-${today.day.toString().padLeft(2,'0')}";
    String dateSlug2 ="${today.year.toString()}-${today.month.toString().padLeft(2,'0')}-01";
    print(dateSlug);
    endTime.value = dateSlug.toString();
    startTime.value = dateSlug2.toString();
    getInit(1);
  }

  setStartTime (String year, String month, String day) {
    var mon = "";
    var d = "";
    if (month.toString().length==1){
      mon= "-0" + month;
    } else {
      mon = month.toString();
    }
    if (day.toString().length==1){
      d= "-0" + day;
    } else {
      d = day.toString();
    }
    startTime.value = year + mon + d;
  }

  setEndTime (String year, String month, String day) {
    var mon = "";
    var d = "";
    if (month.toString().length==1){
      mon= "-0" + month;
    } else {
      mon ="-"+ month.toString();
    }
    if (day.toString().length==1){
      d= "-0" + day;
    } else {
      d ="-"+ day.toString();
    }
    endTime.value = year + mon + d;
  }

  getInit(int page) async {
    pageNum.value = page;
    if (pageNum.value == 1) {
      anchorReportList.clear();
    }
    var res = await API.anchorLiveTimeList({
      "startTime": startTime.value,
      "endTime": endTime.value,
      "pageNum": pageNum.value,
      "pageSize":20
    });
    print(res.toString());
    if (res['code'] == 200) {
      totalNum.value = res['data']["total"];
      var list =
      (res['data']['list'] as List).map((e) => AnchorReportModel.fromJson(e)).toList();
      anchorReportList.addAll(list);
      refreshController.refreshCompleted();
      refreshController.loadComplete();
      if (list.length < 20) {
        refreshController.loadNoData();
      }
      isLoading.value = false;
      update();
    }
  }

}

import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:agora_rtc_rawdata_example/models/anchor_report_model.dart';
import 'package:agora_rtc_rawdata_example/models/incom_model.dart';
import 'package:agora_rtc_rawdata_example/utils/util.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/anchorReport/controller.dart';
import 'package:agora_rtc_rawdata_example/views/processImg/ProcessImg.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pickers/pickers.dart';
import 'package:flutter_pickers/time_picker/model/date_mode.dart';
import 'package:flutter_pickers/time_picker/model/pduration.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class AnchorReportScreen extends GetView<AnchorReportController> {
  const AnchorReportScreen({Key? key}) : super(key: key);

  _showStartDatePicker(context) {
    Pickers.showDatePicker(
      context,
      // 模式，详见下方
      mode: DateMode.YMD,
      // 样式  详见下方样式
      // pickerStyle: pickerStyle,
      // 默认选中
      selectDate: PDuration(
          year: int.parse(controller.startTime.value.split("-")[0]),
          month: int.parse(controller.startTime.value.split("-")[1]),
          day: int.parse(controller.startTime.value.split("-")[2])),
      minDate: PDuration(year: 2010),
      maxDate: PDuration(year: 2040),
      onConfirm: (p) {
        print('longer >>> 返回数据：$p');
        controller.setStartTime(
            p.year.toString(), p.month.toString(), p.day.toString());
        var timer;
        timer = Timer.periodic(Duration(milliseconds: 500), (timer) {
          _showEndDatePicker(context);
          timer.cancel(); //取消定时器
          print('业务逻辑');
        });
      },
    );
  }

  _showEndDatePicker(context) {
    Pickers.showDatePicker(
      context,
      // 模式，详见下方
      mode: DateMode.YMD,
      // 样式  详见下方样式
      // pickerStyle: pickerStyle,
      // 默认选中
      selectDate: PDuration(
          year: int.parse(controller.endTime.value.split("-")[0]),
          month: int.parse(controller.endTime.value.split("-")[1]),
          day: int.parse(controller.endTime.value.split("-")[2])),
      minDate: PDuration(year: 2010),
      maxDate: PDuration(year: 2040),
      onConfirm: (p) {
        print('longer >>> 返回数据：$p');
        controller.setEndTime(
            p.year.toString(), p.month.toString(), p.day.toString());
      },
    );
  }

  _buildHome(String title, context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          height: width(82),
          decoration: BoxDecoration(
              border: Border(
                  top: BorderSide(color: Color(0xffF4F4F4), width: width(2)))),
          padding: EdgeInsets.symmetric(horizontal: width(20)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              InkWell(
                onTap: () {
                  _showStartDatePicker(context);
                },
                child: Row(
                  children: [
                    Container(
                      child: Text('${controller.startTime.value}',
                          style: TextStyle(
                              color: const Color(0xff000000),
                              fontSize: sp(24),
                              fontWeight: FontWeight.w400)),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: width(10)),
                      child: Text('至',
                          style: TextStyle(
                              color: const Color(0xff000000),
                              fontSize: sp(24),
                              fontWeight: FontWeight.w400)),
                    ),
                    Container(
                      child: Text('${controller.endTime.value}',
                          style: TextStyle(
                              color: const Color(0xff000000),
                              fontSize: sp(24),
                              fontWeight: FontWeight.w400)),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: width(10)),
                      width: width(24),
                      height: width(20),
                      child: Image.asset(
                        "images/jzz/jbt.png",
                      ),
                    ),
                  ],
                ),
              ),
              InkWell(
                onTap: () {
                  controller.getInit(1);
                },
                child: Container(
                  width: width(136),
                  height: width(50),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      border:
                          Border.all(color: Color(0xff9F44FF), width: width(2)),
                      borderRadius:
                          BorderRadius.all(Radius.circular(width(40)))),
                  child: Text('搜索',
                      style: TextStyle(
                          color: const Color(0xff9F44FF),
                          fontSize: sp(24),
                          fontWeight: FontWeight.w400)),
                ),
              )
            ],
          ),
        ),
        Expanded(
          child: Container(
            alignment: Alignment.centerLeft,
            child: SmartRefresher(
              enablePullDown: true,
              enablePullUp: true,
              header: WaterDropHeader(
                refresh: CircularProgressIndicator(
                    strokeWidth: 2,
                    valueColor: AlwaysStoppedAnimation(Color(0xff999999))),
                complete: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.done,
                      color: Colors.grey,
                      size: sp(30),
                    ),
                    Container(width: 30.0),
                    Text("加载完成",
                        style: TextStyle(
                            fontSize: sp(28), color: Color(0xff999999)))
                  ],
                ),
              ),
              footer: CustomFooter(
                builder: (BuildContext context, LoadStatus? mode) {
                  Widget body;
                  if (mode == LoadStatus.idle) {
                    body = Text(
                      "下拉加载更多",
                      style:
                          TextStyle(color: Color(0xff999999), fontSize: sp(28)),
                    );
                  } else if (mode == LoadStatus.loading) {
                    body = CupertinoActivityIndicator();
                  } else if (mode == LoadStatus.failed) {
                    body = Text(
                      "加载失败，点击重试",
                      style:
                          TextStyle(color: Color(0xff999999), fontSize: sp(28)),
                    );
                  } else if (mode == LoadStatus.canLoading) {
                    body = Text("释放加载更多",
                        style: TextStyle(
                            color: Color(0xff999999), fontSize: sp(28)));
                  } else {
                    body = Text(
                      "暂无更多数据",
                      style:
                          TextStyle(color: Color(0xff999999), fontSize: sp(28)),
                    );
                  }
                  return Container(
                    height: 55.0,
                    child: Center(child: body),
                  );
                },
              ),
              controller: controller.refreshController,
              onRefresh: () {
                controller.getInit(1);
              },
              onLoading: () {
                controller.getInit(controller.pageNum.value + 1);
              },
              child: controller.anchorReportList.isEmpty
                  ? GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () {
                        controller.getInit(1);
                      },
                      child: controller.isLoading.value
                          ? Center(
                              child: CupertinoActivityIndicator(),
                            )
                          : empty(),
                    )
                  : ListView.separated(
                      physics: ScrollPhysics(),
                      itemBuilder: (c, i) =>
                          IncomeItem(controller.anchorReportList[i],i),
                      itemCount: controller.anchorReportList.length,
                      separatorBuilder: (context, i) => Container(),
                    ),
            ),
          ),
        )
      ],
    );
  }

  Widget IncomeItem(AnchorReportModel e, int i) {
    return Container(
      height: width(132),
      padding: EdgeInsets.symmetric(horizontal: width(30)),
      decoration: BoxDecoration(
          border: Border(
              bottom:
                  BorderSide(color: const Color(0xffF2F2F2), width: width(1))),
          color: Colors.white),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Container(
                width: width(90),
                height: width(90),
                margin: EdgeInsets.only(right: width(20)),
                  clipBehavior: Clip.hardEdge,
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(45))
                  ),
                child: MyImage(url: e.avatar.toString(),id: 'anchorReport' + i.toString())
                // Image.asset(
                //   "images/icon_contribute_def_head.webp",
                // ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Text(
                      "${e.nickName}",
                      style: TextStyle(
                        fontSize: sp(28),
                        fontWeight: FontWeight.w400,
                        color: const Color(0xff000000),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: width(4)),
                    child: Text(
                      "${e.accno}",
                      style: TextStyle(
                        fontSize: sp(28),
                        fontWeight: FontWeight.w400,
                        color: const Color(0xffBCBCBC),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
          Container(
            child: Text(
              "${durationTransform(int.parse(e.liveTime.toString()))}",
              style: TextStyle(
                fontSize: sp(28),
                fontWeight: FontWeight.w500,
                color: const Color(0xff5A5A5A),
              ),
            ),
          ),
        ],
      ),
    );
  }

  ///默认空页面
  empty() {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: width(220),
          height: width(226),
          margin: EdgeInsets.only(bottom: width(10)),
          child: Image.asset(
            "images/no_data.png",
            width: width(220),
            height: width(226),
          ),
        ),
        Text("暂无更多数据",
            style: TextStyle(
                fontSize: sp(28),
                color: Color(0xff768A90),
                fontWeight: FontWeight.w400,
                decoration: TextDecoration.none))
      ],
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        backgroundColor: const Color(0xffF5F5F5),
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(width(20)),
            icon: Image.asset(
              "images/back.png",
              width: width(16),
            ),
          ),
          title: Text('主播报表',
              style: TextStyle(
                  color: const Color(0xff000000),
                  fontSize: sp(34),
                  fontWeight: FontWeight.bold)),
          backgroundColor: Colors.white,
        ),
        body: GetX<AnchorReportController>(
            init: controller,
            builder: (_) => _buildHome(controller.titleString.value, context)));
  }
}

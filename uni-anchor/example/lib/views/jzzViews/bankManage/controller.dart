import 'package:agora_rtc_rawdata_example/models/bank_list_model.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/bankManage/makeSureLog.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';

class BankManageController extends GetxController {
  RxString titleString = "out".obs;
  RxString canBindCount = "0".obs;
  RxString bankAccid = "".obs;

  /// 家族长还可以绑定银行卡数量
  RxList<BankListModel> bankList = RxList<BankListModel>();

  RxBool flag = false.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
    getInit();
  }

  getInit() async {
    var res = await API.bankList();
    print(res.toString());
    if (res['code'] == 200) {
      canBindCount.value = res['data']['canBindCount'].toString();
      var list = (res['data']['memBankAccountList'] as List)
          .map((e) => BankListModel.fromJson(e))
          .toList();
      list.forEach((element) {
        element.flag = false;
      });
      bankList.clear();
      bankList.addAll(list);
      update();
    }
  }

  setBank(BankListModel e) {
    if (e.flag != null) {
      if (e.flag == true) {
        e.flag = false;
        bankAccid.value = "";
      } else {
        bankAccid.value = e.bankAccid.toString();
        e.flag = true;
      }
    } else {
      e.flag = true;
    }
    bankList.refresh();
  }

  delFlag() async {
    if (flag.value && bankAccid.value != '') {
      Get.dialog(MakeSureLog());
    } else {
      flag.value = !flag.value;
    }

  }

  makeSure() async {
    Get.back();
    var res = await API.delete({
      "bankAccid":bankAccid.value
    });
    print(res.toString());
    if (res['code'] == 200) {
      BotToast.showText(
          text: "删除成功",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
      getInit();
    } else {
      BotToast.showText(
          text: res["msg"],
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    }
  }

  add() {
    Get.toNamed("/addBank")?.then((value) {
      if (value != null && value) {
        getInit();
      }
    });
  }
}

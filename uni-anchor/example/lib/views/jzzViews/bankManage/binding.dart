
import 'package:agora_rtc_rawdata_example/views/jzzViews/bankManage/controller.dart';
import 'package:get/get.dart';
class BankManagebinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BankManageController>(()=> BankManageController());
  }
}

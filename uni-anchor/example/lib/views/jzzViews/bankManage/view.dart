import 'dart:convert';
import 'dart:math';

import 'package:agora_rtc_rawdata_example/views/jzzViews/bankManage/controller.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class BankManageScreen extends GetView<BankManageController> {
  const BankManageScreen({Key? key}) : super(key: key);

  _buildHome(String title, context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
            top: BorderSide(
                color: Color(0xffF7F7F7), width: width(2))),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ...controller.bankList.value.map((e) {
            return InkWell(
              onTap: () {
                controller.setBank(e);
              },
              child: Container(
                  height: width(120),
                  margin: EdgeInsets.only(bottom: width(20)),
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(horizontal: width(30)),
                  decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            color: Color(0xffF7F7F7), width: width(2))),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          e.bankLogo != null?Container(
                              width: width(90),
                              height: width(90),
                              margin: EdgeInsets.only(right: width(28)),
                              child: Image.network("${e.bankLogo}")
                          ):
                          Container(
                            width: width(90),
                            height: width(90),
                            margin: EdgeInsets.only(right: width(28)),
                            child: Image.asset(
                              "images/icon_contribute_def_head.webp",
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                child: Text(
                                  "${e.bankAccountName}",
                                  style: TextStyle(
                                    fontSize: sp(28),
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xff000000),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: width(4)),
                                child: Text(
                                  "${e.bankAccountNo}",
                                  style: TextStyle(
                                    fontSize: sp(28),
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xff000000),
                                  ),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                      controller.flag.value && e.flag != null ?  e.flag == true ? Container(
                        width: width(40),
                        height: width(40),
                        child: Image.asset(
                          "images/jzz/select.png",
                        ),
                      ): Container(
                        width: width(40),
                        height: width(40),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(width(4))),
                          border: Border.all(color: Color(0xffF4F4F4),width: width(2))
                        ),
                      ):Container(),
                    ],
                  )),
            );
          }),
          SizedBox(
            height: width(32),
          ),
          Container(
            alignment: Alignment.center,
            child: Text(
              "您还可以绑定${controller.canBindCount.value}张银行卡",
              style: TextStyle(
                fontSize: sp(24),
                fontWeight: FontWeight.w400,
                color: const Color(0xffC0C0C0),
              ),
            ),
          ),
          SizedBox(
            height: width(186),
          ),
          controller.canBindCount.value == '0'?Container():InkWell(
            onTap: () {
              controller.add();
            },
            child: Container(
              height: width(70),
              margin: EdgeInsets.symmetric(horizontal: width(96)),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(width(40))),
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [
                      Color(0xff6129FF),
                      Color(0xffD96CFF),
                    ],
                    stops: [
                      0.03,
                      0.97
                    ]),
              ),
              alignment: Alignment.center,
              child: Text(
                "添加银行卡",
                style: TextStyle(
                  fontSize: sp(32),
                  fontWeight: FontWeight.w400,
                  color: const Color(0xffFFFFFF),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        backgroundColor: const Color(0xffFFFFFF),
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(width(20)),
            icon: Image.asset(
              "images/back.png",
              width: width(16),
            ),
          ),
          title: Text('银行卡管理',
              style: TextStyle(
                  color: const Color(0xff000000),
                  fontSize: sp(34),
                  fontWeight: FontWeight.bold)),
          backgroundColor: Colors.white,
          actions: [
            Obx(()=> controller.bankList.length> 0 ? InkWell(
              onTap: () {
                controller.delFlag();
              },
              child: Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(right: width(30)),
                child: Text("${controller.flag.value ? '删除':'编辑'}",
                    style: TextStyle(
                        color: const Color(0xff000000),
                        fontSize: sp(28),
                        fontWeight: FontWeight.w400)),
              ),
            ):Container())
          ],
        ),
        body: GetX<BankManageController>(
            init: controller,
            builder: (_) => SingleChildScrollView(
                child: _buildHome(controller.titleString.value, context))));
  }
}

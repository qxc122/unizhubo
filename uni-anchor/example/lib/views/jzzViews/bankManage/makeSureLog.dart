import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/bankManage/controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class MakeSureLog extends Dialog {
  @override
  Widget build(BuildContext context) {
    var controller = Get.find<BankManageController>();
    //自定义弹框内容
    return WillPopScope(
        onWillPop: () async {
          return Future.value(true);
          // return Future.value(controller.compulsory.value=='1'?false:true);
        },
        child: Material(
            type: MaterialType.transparency,
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: width(80)),
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(15))),
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(
                              top: width(40), bottom: width(36)),
                          child: Text(
                            "是否删除该银行卡？",
                            style: TextStyle(
                              fontSize: sp(32),
                              fontWeight: FontWeight.w400,
                              color: const Color(0xff000000),
                            ),
                          ),
                        ),
                        Divider(
                          height: width(1),
                          color: Color(0xffD6D6D6),
                        ),
                        Container(
                          height: width(100),
                          child: Row(
                            children: [
                              Expanded(
                                child: InkWell(
                                  onTap: () {
                                    Get.back();
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      "取消",
                                      style: TextStyle(
                                        fontSize: sp(32),
                                        fontWeight: FontWeight.w400,
                                        color: const Color(0xff9F9F9F),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: width(1),
                                height: width(48),
                                color: const Color(0xffD6D6D6),
                              ),
                              Expanded(
                                child: InkWell(
                                  onTap: () {
                                    controller.makeSure();
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      "确定",
                                      style: TextStyle(
                                        fontSize: sp(32),
                                        fontWeight: FontWeight.w400,
                                        color: const Color(0xff9F44FF),
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            )));
  }
}

import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/anchorWithdrawal/controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class CashLog extends Dialog {
  @override
  Widget build(BuildContext context) {
    var controller = Get.find<AnchorWithdrawalController>();
    //自定义弹框内容
    return WillPopScope(
        onWillPop: () async {
          return Future.value(true);
          // return Future.value(controller.compulsory.value=='1'?false:true);
        },
        child: Material(
            type: MaterialType.transparency,
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: width(520),
                    margin: EdgeInsets.symmetric(horizontal: width(25)),
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(15))),
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(
                              top: width(40), bottom: width(36)),
                          child: Text(
                            "提取到余额",
                            style: TextStyle(
                              fontSize: sp(32),
                              fontWeight: FontWeight.w400,
                              color: const Color(0xff000000),
                            ),
                          ),
                        ),

                        Container(
                          margin: EdgeInsets.symmetric(horizontal: width(20)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  height: width(70),
                                  padding: EdgeInsets.symmetric(horizontal: width(20)),
                                  decoration: BoxDecoration(
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                      border: Border.all(
                                          color: Color(0xffBCBCBC), width: width(1))),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: TextField(
                                          inputFormatters: [
                                            LengthLimitingTextInputFormatter(15)
                                          ],
                                          keyboardType: TextInputType.number,
                                          cursorColor: const Color(0xff333333),
                                          style: TextStyle(
                                              color: const Color(0xff333333),
                                              fontSize: sp(28)),
                                          decoration: const InputDecoration(
                                            border: InputBorder.none,
                                            hintText: "请输入金额",
                                            hintStyle: TextStyle(
                                                color: Color(0xffC7C7CC), fontSize: 14.0),
                                            floatingLabelBehavior:
                                            FloatingLabelBehavior.never,
                                          ),
                                          focusNode: controller.priceFocus,
                                          controller: controller.priceCtr,
                                          onChanged: (String text) {
                                            controller.setTitle(text);
                                          },
                                        ),
                                      ),
                                    ],
                                  )
                              ),
                              SizedBox(height: width(10),),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        child: Text(
                                          "可提取金额",
                                          style: TextStyle(
                                            fontSize: sp(24),
                                            fontWeight: FontWeight.w400,
                                            color: const Color(0xffBCBCBC),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: width(10)),
                                        child: Text(
                                          "${controller.currentInfo.value.withdrawAccount??'0.0'}",
                                          style: TextStyle(
                                            fontSize: sp(24),
                                            fontWeight: FontWeight.w400,
                                            color: const Color(0xffBCBCBC),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  InkWell(
                                    onTap: () {
                                      controller.setAllPrice();
                                    },
                                    child: Container(
                                      width: width(70),
                                      height: width(42),
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                        border: Border.all(color: Color(0xff3300FF), width: width(2)),
                                        borderRadius: BorderRadius.all(Radius.circular(width(10)))
                                      ),
                                      child: Text(
                                        "全部",
                                        style: TextStyle(
                                          fontSize: sp(24),
                                          fontWeight: FontWeight.w400,
                                          color: const Color(0xff3300FF),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(height: width(26),)
                            ],
                          ),
                        ),
                        Divider(
                          height: width(1),
                          color: Color(0xffD6D6D6),
                        ),
                        Container(
                          height: width(100),
                          child: Row(
                            children: [
                              Expanded(
                                child: InkWell(
                                  onTap: () {
                                    Get.back();
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      "取消",
                                      style: TextStyle(
                                        fontSize: sp(32),
                                        fontWeight: FontWeight.w400,
                                        color: const Color(0xff9F9F9F),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: width(1),
                                height: width(48),
                                color: const Color(0xffD6D6D6),
                              ),
                              Expanded(
                                child: InkWell(
                                  onTap: () {
                                    controller.cash();
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      "确定",
                                      style: TextStyle(
                                        fontSize: sp(32),
                                        fontWeight: FontWeight.w400,
                                        color: const Color(0xff9F44FF),
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            )));
  }
}

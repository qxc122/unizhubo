import 'dart:convert';
import 'dart:math';

import 'package:agora_rtc_rawdata_example/views/jzzViews/anchorWithdrawal/controller.dart';
import 'package:agora_rtc_rawdata_example/views/processImg/ProcessImg.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class AnchorWithdrawalScreen extends GetView<AnchorWithdrawalController> {
  const AnchorWithdrawalScreen({Key? key}) : super(key: key);

  _buildHome(String title, context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: width(30)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              height: width(248),
              padding: EdgeInsets.symmetric(horizontal: width(30))
                  .copyWith(top: width(40), bottom: width(24)),
              alignment: Alignment.centerLeft,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: new AssetImage('images/jzz/cbg2.png'),
                      fit: BoxFit.fitWidth)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      "账户可提现（金币）",
                      style: TextStyle(
                        fontSize: sp(28),
                        fontWeight: FontWeight.w400,
                        color: const Color(0xffFFFFFF),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: width(42),
                  ),
                  Container(
                    child: Text(
                      "${controller.withdrawAccount.value}",
                      style: TextStyle(
                        fontSize: sp(40),
                        fontWeight: FontWeight.w400,
                        color: const Color(0xffFFF500),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      InkWell(
                        onTap: () {
                          controller.allCash();
                        },
                        child: Container(
                          alignment: Alignment.center,
                          width: width(200),
                          height: width(50),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(width(40))),
                              border: Border.all(
                                  color: Color(0xffffffff), width: width(2))),
                          child: Text(
                            "一键领取",
                            style: TextStyle(
                              fontSize: sp(28),
                              fontWeight: FontWeight.w400,
                              color: const Color(0xffFFFFFF),
                            ),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              )),
          // SizedBox(
          //   height: width(32),
          // ),
          ...controller.anchorWalletList.value.map((e) {
            return Container(
                height: width(248),
                margin: EdgeInsets.only(top: width(32)),
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: new AssetImage('images/jzz/cbg1.png'),
                        fit: BoxFit.fitWidth)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: width(30))
                          .copyWith(top: width(30), bottom: width(20)),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: Color(0xffffffff), width: width(2)))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Container(
                                width: width(90),
                                height: width(90),
                                margin: EdgeInsets.only(right: width(12)),
                                  clipBehavior: Clip.hardEdge,
                                  decoration: const BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(45))
                                  ),
                                child: MyImage(url: e.avatar.toString(),id: 'anchorWithdrawal' + e.userId.toString())
                                // Image.asset(
                                //   "images/icon_contribute_def_head.webp",
                                // ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    child: Text(
                                      "${e.userAccount}",
                                      style: TextStyle(
                                        fontSize: sp(28),
                                        fontWeight: FontWeight.w400,
                                        color: const Color(0xffFFFFFF),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: width(4)),
                                    child: Text(
                                      "${e.nickName}",
                                      style: TextStyle(
                                        fontSize: sp(24),
                                        fontWeight: FontWeight.w400,
                                        color: const Color(0xffFFFFFF),
                                      ),
                                    ),
                                  )
                                ],
                              )
                            ],
                          ),
                          InkWell(
                            onTap: () {
                              controller.setCash(e);
                            },
                            child: Container(
                              alignment: Alignment.center,
                              width: width(120),
                              height: width(50),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(width(40))),
                                  border: Border.all(
                                      color: Color(0xffffffff),
                                      width: width(2))),
                              child: Text(
                                "提现",
                                style: TextStyle(
                                  fontSize: sp(28),
                                  fontWeight: FontWeight.w400,
                                  color: const Color(0xffFFFFFF),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: width(30))
                          .copyWith(top: width(20)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            child: Text(
                              "未提取金额（金币）",
                              style: TextStyle(
                                fontSize: sp(24),
                                fontWeight: FontWeight.w400,
                                color: const Color(0xffFFFFFF),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: width(6)),
                            child: Text(
                              "${e.withdrawAccount}",
                              style: TextStyle(
                                fontSize: sp(28),
                                fontWeight: FontWeight.w400,
                                color: const Color(0xffFFFFFF),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ));
          })
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          Get.back(result: true);
          return Future.value(true);
        },
        child: Scaffold(
            extendBodyBehindAppBar: false,
            backgroundColor: const Color(0xffFFFFFF),
            appBar: AppBar(
              centerTitle: true,
              leading: IconButton(
                onPressed: () {
                  Get.back(result: true);
                },
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.all(width(20)),
                icon: Image.asset(
                  "images/back.png",
                  width: width(16),
                ),
              ),
              title: Text('提取',
                  style: TextStyle(
                      color: const Color(0xff000000),
                      fontSize: sp(34),
                      fontWeight: FontWeight.bold)),
              backgroundColor: Colors.white,
            ),
            body: GetX<AnchorWithdrawalController>(
                init: controller,
                builder: (_) => SingleChildScrollView(
                    child:
                        _buildHome(controller.titleString.value, context)))));
  }
}

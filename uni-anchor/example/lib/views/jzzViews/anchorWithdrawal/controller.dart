import 'package:agora_rtc_rawdata_example/models/anchor_wallet_model.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/anchorWithdrawal/allCashLog.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/anchorWithdrawal/cashLog.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';

class AnchorWithdrawalController extends GetxController {
  RxString titleString = "out".obs;
  RxString price = "".obs;
  RxString withdrawAccount = "0.0".obs;

  /// 账户可提现（金币/USD）
  RxList<AnchorWalletModel> anchorWalletList = RxList<AnchorWalletModel>();

  var currentInfo = Rx<AnchorWalletModel>(AnchorWalletModel());

  final priceCtr = TextEditingController();
  final FocusNode priceFocus = FocusNode();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
    getInit();
  }

  getInit() async {
    var res = await API.canWithdrawAnchor();
    print(res.toString());
    anchorWalletList.clear();
    if (res['code'] == 200) {
      withdrawAccount.value = res['data']['withdrawAccount'].toString();
      var list = (res['data']['withdrawAnchorList'] as List)
          .map((e) => AnchorWalletModel.fromJson(e))
          .toList();
      anchorWalletList.addAll(list);
      update();
    }
  }

  setTitle(String text) {
    price.value = text;
  }

  setCash(AnchorWalletModel e) {
    priceCtr.text = '';
    price.value = '';
    currentInfo.value = e;
    Get.dialog(CashLog());
  }

  setAllPrice() {
    var pe = currentInfo.value.withdrawAccount.toString();
    priceCtr.text = pe.toString();
    price.value = pe.toString();
  }

  allCash() {
    Get.dialog(AllCashLog());
  }

  submitAll() async {
    var res =
        await API.withdrawAnchor({"withdrawMoney": withdrawAccount.value});
    print(res.toString());
    if (res['code'] == 200) {
      Get.back();
      BotToast.showText(
          text: "提取成功",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
      getInit();
    } else {
      BotToast.showText(
          text: res["msg"],
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    }
  }

  cash() async {
    var res = await API.withdrawAnchor(
        {"userId": currentInfo.value.userId, "withdrawMoney": price.value});
    print(res.toString());
    if (res['code'] == 200) {
      Get.back();
      priceCtr.text = '';
      price.value = '';
      BotToast.showText(
          text: "提取成功",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
      getInit();
    } else {
      BotToast.showText(
          text: res["msg"],
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    }
  }
}

import 'dart:convert';
import 'dart:math';

import 'package:agora_rtc_rawdata_example/views/jzzViews/jzzWallet/controller.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class JzzWalletScreen extends GetView<JzzWalletController> {
  const JzzWalletScreen({Key? key}) : super(key: key);



  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        backgroundColor: const Color(0xffFFFFFF),
        appBar: AppBar(
          centerTitle: true,
          actions: [
            Row(
              children: [
                InkWell(
                  onTap: () {
                    controller.getOnlineService();
                  },
                  child: Container(
                    margin: EdgeInsets.only(right: width(20)),
                    width: width(48),
                    height: width(48),
                    child: Image.asset(
                      "images/kf.png",
                      width: width(48),
                      height: width(48),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    Get.toNamed("/setting");
                  },
                  child: Container(
                    margin: EdgeInsets.only(right: width(30)),
                    width: width(48),
                    height: width(48),
                    child: Image.asset(
                      "images/jzz/setting.png",
                      width: width(48),
                      height: width(48),
                    ),
                  ),
                )
              ],
            )
          ],
          backgroundColor: Colors.white,
        ),
        body: GetX<JzzWalletController>(
            init: controller,
            builder: (_) =>Column(
              children: [
                Expanded(
                  child: SmartRefresher(
                    enablePullDown: true,
                    enablePullUp: false,
                    header: WaterDropHeader(
                      refresh: CircularProgressIndicator(
                          strokeWidth: 2,
                          valueColor:
                          AlwaysStoppedAnimation(Color(0xff999999))),
                      complete: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.done,
                            color: Colors.grey,
                            size: sp(30),
                          ),
                          Container(width: 30.0),
                          Text("加载完成",
                              style: TextStyle(
                                  fontSize: sp(28),
                                  color: Color(0xff999999)))
                        ],
                      ),
                    ),
                    controller: controller.refreshController,
                    onRefresh: () {
                      controller.getInit();
                    },
                    child: Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.symmetric(horizontal: width(15)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.symmetric(horizontal: width(20))
                                .copyWith(top: width(40), bottom: width(50)),
                            decoration: const BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(8)),
                                image: DecorationImage(
                                    image: AssetImage("images/wallet_bg.png"),
                                    fit: BoxFit.cover)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(left: width(30)),
                                  child: Text(
                                    "总资产（${controller.shortcutOptionsUnit.value}）",
                                    style: TextStyle(
                                      fontSize: sp(28),
                                      fontWeight: FontWeight.w400,
                                      height: 1.2,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(left: width(30)),
                                  child: Text(
                                    "${controller.totalAssets.value}",
                                    style: TextStyle(
                                      fontSize: sp(28),
                                      fontWeight: FontWeight.w400,
                                      height: 1.4,
                                      color: const Color(0xffFAFF00),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: width(24),
                                ),
                                const Divider(color: Colors.white),
                                SizedBox(
                                  height: width(44),
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Column(
                                        children: [
                                          Container(
                                            child: Text(
                                              "今日收入（${controller.shortcutOptionsUnit.value}）",
                                              style: TextStyle(
                                                fontSize: sp(28),
                                                fontWeight: FontWeight.w400,
                                                height: 1.2,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            child: Text(
                                              "${controller.todayIncome.value}",
                                              style: TextStyle(
                                                fontSize: sp(28),
                                                fontWeight: FontWeight.w400,
                                                height: 1.4,
                                                color: const Color(0xffFAFF00),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      width: width(1),
                                      height: width(60),
                                      color: Colors.white,
                                    ),
                                    Expanded(
                                      child: Column(
                                        children: [
                                          Container(
                                            child: Text(
                                              "本月收入（${controller.shortcutOptionsUnit.value}）",
                                              style: TextStyle(
                                                fontSize: sp(28),
                                                fontWeight: FontWeight.w400,
                                                height: 1.2,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            child: Text(
                                              "${controller.monthIncome.value}",
                                              style: TextStyle(
                                                fontSize: sp(28),
                                                fontWeight: FontWeight.w400,
                                                height: 1.4,
                                                color: const Color(0xffFAFF00),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: width(40),
                          ),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      Get.toNamed("/anchorManagement");
                                    },
                                    child: Column(
                                      children: [
                                        Container(
                                          width: width(90),
                                          height: width(90),
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(width(10))),
                                              gradient: LinearGradient(
                                                  begin: Alignment.topCenter,
                                                  end: Alignment.bottomCenter,
                                                  colors: [
                                                    Color(0xFFFF4B1F),
                                                    Color(0xFFFF9068)
                                                  ])),
                                          child: Image.asset(
                                            "images/jzz/zbgl.png",
                                            width: width(66),
                                            height: width(56),
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(top: width(10)),
                                          child: Text(
                                            "主播管理",
                                            style: TextStyle(
                                              fontSize: sp(24),
                                              fontWeight: FontWeight.w400,
                                              color: const Color(0xff000000),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      Get.toNamed("/cwdz");
                                    },
                                    child: Column(
                                      children: [
                                        Container(
                                          width: width(90),
                                          height: width(90),
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(width(10))),
                                              gradient: LinearGradient(
                                                  begin: Alignment.topCenter,
                                                  end: Alignment.bottomCenter,
                                                  colors: [
                                                    Color(0xFFF2994A),
                                                    Color(0xFFF2C94C)
                                                  ])),
                                          child: Image.asset(
                                            "images/jzz/cwdz.png",
                                            width: width(66),
                                            height: width(56),
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(top: width(10)),
                                          child: Text(
                                            "账务对账",
                                            style: TextStyle(
                                              fontSize: sp(24),
                                              fontWeight: FontWeight.w400,
                                              color: const Color(0xff000000),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      Get.toNamed("/withdrawal");
                                    },
                                    child: Column(
                                      children: [
                                        Container(
                                          width: width(90),
                                          height: width(90),
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(width(10))),
                                              gradient: LinearGradient(
                                                  begin: Alignment.topCenter,
                                                  end: Alignment.bottomCenter,
                                                  colors: [
                                                    Color(0xFF4E54C8),
                                                    Color(0xFF8F94FB)
                                                  ])),
                                          child: Image.asset(
                                            "images/jzz/zhtx.png",
                                            width: width(60),
                                            height: width(48),
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(top: width(10)),
                                          child: Text(
                                            "转化提现",
                                            style: TextStyle(
                                              fontSize: sp(24),
                                              fontWeight: FontWeight.w400,
                                              color: const Color(0xff000000),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      Get.toNamed("/anchorReport");
                                    },
                                    child: Column(
                                      children: [
                                        Container(
                                          width: width(90),
                                          height: width(90),
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(width(10))),
                                              gradient: LinearGradient(
                                                  begin: Alignment.topCenter,
                                                  end: Alignment.bottomCenter,
                                                  colors: [
                                                    Color(0xFFFF416C),
                                                    Color(0xFFFF8AA5)
                                                  ])),
                                          child: Image.asset(
                                            "images/jzz/zbbb.png",
                                            width: width(60),
                                            height: width(54),
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(top: width(10)),
                                          child: Text(
                                            "主播报表",
                                            style: TextStyle(
                                              fontSize: sp(24),
                                              fontWeight: FontWeight.w400,
                                              color: const Color(0xff000000),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: width(40),
                          ),
                          Divider(
                            color: Color(0xffFCFCFC),
                            height: width(20),
                          ),
                          Container(
                            color: Colors.white,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  height: width(80),
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.symmetric(horizontal: width(20)),
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(
                                              color: Color(0xffF9F9F9), width: width(1)))),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Container(
                                            width: width(30),
                                            height: width(30),
                                            child: Image.asset(
                                              "images/40.png",
                                              width: width(30),
                                              height: width(30),
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(left: width(14)),
                                            child: Text(
                                              "订阅",
                                              style: TextStyle(
                                                fontSize: sp(24),
                                                fontWeight: FontWeight.w400,
                                                color: const Color(0xff000000),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        child: Text(
                                          "${controller.focusMoney.value}",
                                          style: TextStyle(
                                            fontSize: sp(24),
                                            fontWeight: FontWeight.w400,
                                            color: const Color(0xff000000),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: width(80),
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.symmetric(horizontal: width(20)),
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(
                                              color: Color(0xffF9F9F9), width: width(1)))),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Container(
                                            width: width(30),
                                            height: width(30),
                                            child: Image.asset(
                                              "images/32.png",
                                              width: width(30),
                                              height: width(30),
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(left: width(14)),
                                            child: Text(
                                              "礼物",
                                              style: TextStyle(
                                                fontSize: sp(24),
                                                fontWeight: FontWeight.w400,
                                                color: const Color(0xff000000),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        child: Text(
                                          "${controller.giftMoney.value}",
                                          style: TextStyle(
                                            fontSize: sp(24),
                                            fontWeight: FontWeight.w400,
                                            color: const Color(0xff000000),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: width(80),
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.symmetric(horizontal: width(20)),
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(
                                              color: Color(0xffF9F9F9), width: width(1)))),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Container(
                                            width: width(30),
                                            height: width(30),
                                            child: Image.asset(
                                              "images/14.png",
                                              width: width(30),
                                              height: width(30),
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(left: width(14)),
                                            child: Text(
                                              "佣金",
                                              style: TextStyle(
                                                fontSize: sp(24),
                                                fontWeight: FontWeight.w400,
                                                color: const Color(0xff000000),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        child: Text(
                                          "${controller.rebatesMoney.value}",
                                          style: TextStyle(
                                            fontSize: sp(24),
                                            fontWeight: FontWeight.w400,
                                            color: const Color(0xff000000),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: width(80),
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.symmetric(horizontal: width(20)),
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(
                                              color: Color(0xffF9F9F9), width: width(1)))),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Container(
                                            width: width(30),
                                            height: width(30),
                                            child: Image.asset(
                                              "images/32.png",
                                              width: width(30),
                                              height: width(30),
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(left: width(14)),
                                            child: Text(
                                              "其他",
                                              style: TextStyle(
                                                fontSize: sp(24),
                                                fontWeight: FontWeight.w400,
                                                color: const Color(0xff000000),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        child: Text(
                                          "${controller.otherMoney.value}",
                                          style: TextStyle(
                                            fontSize: sp(24),
                                            fontWeight: FontWeight.w400,
                                            color: const Color(0xff000000),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            )));
  }
}

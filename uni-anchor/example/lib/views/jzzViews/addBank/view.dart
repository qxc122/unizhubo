import 'dart:convert';
import 'dart:math';

import 'package:agora_rtc_rawdata_example/views/jzzViews/addBank/controller.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class AddBankScreen extends GetView<AddBankController> {
  const AddBankScreen({Key? key}) : super(key: key);

  _buildHome(String title, context) {
    return Container(
      decoration: BoxDecoration(
          border: Border(
              top:
              BorderSide(color: Color(0xffF4F4F4), width: width(2)))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: width(100),
            padding: EdgeInsets.symmetric(horizontal: width(20)),
            decoration: BoxDecoration(
                border: Border(
                    bottom:
                        BorderSide(color: Color(0xffF4F4F4), width: width(2)))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Text('开卡地区',
                      style: TextStyle(
                          color: const Color(0xff000000),
                          fontSize: sp(28),
                          fontWeight: FontWeight.w400)),
                ),
                Container(
                  child: Text('${controller.userInfo.value.countryName == null ? "": controller.userInfo.value.countryName}',
                      style: TextStyle(
                          color: const Color(0xffACACAC),
                          fontSize: sp(28),
                          fontWeight: FontWeight.w400)),
                ),
              ],
            ),
          ),
          Container(
            height: width(100),
            padding: EdgeInsets.symmetric(horizontal: width(20)),
            decoration: BoxDecoration(
                border: Border(
                    bottom:
                    BorderSide(color: Color(0xffF4F4F4), width: width(2)))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Text('真实姓名',
                      style: TextStyle(
                          color: const Color(0xff000000),
                          fontSize: sp(28),
                          fontWeight: FontWeight.w400)),
                ),
                Expanded(
                  child: TextField(
                    // inputFormatters: [LengthLimitingTextInputFormatter(15)],
                    keyboardType: TextInputType.text,
                    textAlign: TextAlign.right,
                    cursorColor: const Color(0xff333333),
                    style: TextStyle(
                        color: const Color(0xff333333), fontSize: sp(28)),
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                      hintText: "请输入真实姓名",
                      hintStyle:
                      TextStyle(color: Color(0xffC7C7CC), fontSize: 14.0),
                      floatingLabelBehavior: FloatingLabelBehavior.never,
                    ),
                    focusNode: controller.nameFocus,
                    controller: controller.nameCtr,
                    onChanged: (String text) {
                      controller.setName(text);
                    },
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: width(100),
            padding: EdgeInsets.symmetric(horizontal: width(20)),
            decoration: BoxDecoration(
                border: Border(
                    bottom:
                    BorderSide(color: Color(0xffF4F4F4), width: width(2)))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Text('开户银行',
                      style: TextStyle(
                          color: const Color(0xff000000),
                          fontSize: sp(28),
                          fontWeight: FontWeight.w400)),
                ),
                InkWell(
                  onTap: () {
                    controller.selectBankLog();
                  },
                  child: Row(
                    children: [
                      Container(
                        child: Text('${controller.bankCode.value}',
                            style: TextStyle(
                                color: const Color(0xffACACAC),
                                fontSize: sp(28),
                                fontWeight: FontWeight.w400)),
                      ),
                      Container(
                        width: width(32),
                        height: width(32),
                        child: Image.asset(
                          "images/right_arr.webp",
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            height: width(100),
            padding: EdgeInsets.symmetric(horizontal: width(20)),
            decoration: BoxDecoration(
                border: Border(
                    bottom:
                    BorderSide(color: Color(0xffF4F4F4), width: width(2)))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Text('银行卡号',
                      style: TextStyle(
                          color: const Color(0xff000000),
                          fontSize: sp(28),
                          fontWeight: FontWeight.w400)),
                ),
                Expanded(
                  child: TextField(
                    // inputFormatters: [LengthLimitingTextInputFormatter(15)],
                    keyboardType: TextInputType.number,
                    textAlign: TextAlign.right,
                    cursorColor: const Color(0xff333333),
                    style: TextStyle(
                        color: const Color(0xff333333), fontSize: sp(28)),
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                      hintText: "请输入银行卡号",
                      hintStyle:
                      TextStyle(color: Color(0xffC7C7CC), fontSize: 14.0),
                      floatingLabelBehavior: FloatingLabelBehavior.never,
                    ),
                    focusNode: controller.bankFocus,
                    controller: controller.bankCtr,
                    onChanged: (String text) {
                      controller.setBank(text);
                    },
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: width(100),
            padding: EdgeInsets.symmetric(horizontal: width(20)),
            decoration: BoxDecoration(
                border: Border(
                    bottom:
                    BorderSide(color: Color(0xffF4F4F4), width: width(2)))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Text('开户网点',
                      style: TextStyle(
                          color: const Color(0xff000000),
                          fontSize: sp(28),
                          fontWeight: FontWeight.w400)),
                ),
                Expanded(
                  child: TextField(
                    // inputFormatters: [LengthLimitingTextInputFormatter(15)],
                    keyboardType: TextInputType.text,
                    textAlign: TextAlign.right,
                    cursorColor: const Color(0xff333333),
                    style: TextStyle(
                        color: const Color(0xff333333), fontSize: sp(28)),
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                      hintText: "请输入开户网点",
                      hintStyle:
                      TextStyle(color: Color(0xffC7C7CC), fontSize: 14.0),
                      floatingLabelBehavior: FloatingLabelBehavior.never,
                    ),
                    focusNode: controller.outletFocus,
                    controller: controller.outletCtr,
                    onChanged: (String text) {
                      controller.setOutlet(text);
                    },
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: width(100),
            padding: EdgeInsets.symmetric(horizontal: width(20)),
            decoration: BoxDecoration(
                border: Border(
                    bottom:
                    BorderSide(color: Color(0xffF4F4F4), width: width(2)))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Text('设置为默认卡',
                      style: TextStyle(
                          color: const Color(0xff000000),
                          fontSize: sp(28),
                          fontWeight: FontWeight.w400)),
                ),
                Switch(
                    activeColor:Color(0xffFFFFFF),
                    activeTrackColor:Color(0xff6129FF),
                    inactiveTrackColor:Color(0xffD9D9D9),
                    value: controller.isDefault.value, onChanged: (value){
                      controller.changeDefault(value);
                }),
              ],
            ),
          ),
          Container(
            alignment: Alignment.center,
            margin: EdgeInsets.only(top: width(30),bottom: width(134)),
            child: Text('（请妥善填写银行卡信息，绑定后不可更改）',
                style: TextStyle(
                    color: const Color(0xffFF0000),
                    fontSize: sp(20),
                    fontWeight: FontWeight.w400)),
          ),
          InkWell(
            onTap: () {
              controller.submit();
            },
            child: Container(
              height: width(70),
              margin: EdgeInsets.symmetric(horizontal: width(96)),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(width(40))),
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [
                      Color(0xff6129FF),
                      Color(0xffD96CFF),
                    ],
                    stops:[
                      0.03,0.97
                    ]
                ),
              ),
              alignment: Alignment.center,
              child: Text(
                "保 存",
                style: TextStyle(
                  fontSize: sp(32),
                  fontWeight: FontWeight.w400,
                  color: const Color(0xffFFFFFF),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        backgroundColor: const Color(0xffFFFFFF),
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(width(20)),
            icon: Image.asset(
              "images/back.png",
              width: width(16),
            ),
          ),
          title: Text('添加银行卡',
              style: TextStyle(
                  color: const Color(0xff000000),
                  fontSize: sp(34),
                  fontWeight: FontWeight.bold)),
          backgroundColor: Colors.white,
        ),
        body: GetX<AddBankController>(
            init: controller,
            builder: (_) => SingleChildScrollView(
                child: _buildHome(controller.titleString.value, context))));
  }
}

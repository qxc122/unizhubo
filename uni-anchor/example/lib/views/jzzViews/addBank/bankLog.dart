import 'package:agora_rtc_rawdata_example/views/jzzViews/addBank/controller.dart';
import 'package:agora_rtc_rawdata_example/views/play/liveRoom/medal.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../utils/screen.dart';

class BankLog extends StatelessWidget {
  var controller = Get.find<AddBankController>();

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomCenter,
      height: width(700),
      padding: EdgeInsets.symmetric(horizontal: width(25)),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(topRight: Radius.circular(width(20)), topLeft: Radius.circular(width(20)))
      ),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: width(32),bottom: width(30)),
            alignment:Alignment.center,
            child: Text(
              "选择银行卡",
              style: TextStyle(
                fontSize: sp(28),
                fontWeight: FontWeight.w400,
                height: 1.0,
                color: const Color(0xff000000),
              ),
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ...controller.bankList.map((element){
                    return InkWell(
                      onTap: () {
                        controller.selectBank(element);
                      },
                        child: Container(
                          margin: EdgeInsets.only(bottom: width(20)),
                          decoration: BoxDecoration(
                            border: Border(bottom: BorderSide(color: Color(0xffF4F4F4),width: width(2)))
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    width: width(160),
                                    height: width(100),
                                    margin: EdgeInsets.only(right: width(20)),
                                    child: Image.network(
                                        "${element.logo}",
                                        width: width(160),
                                        height: width(120),
                                        errorBuilder: (ctx, err, stackTrace) =>
                                            Image.asset(
                                              'images/icon_contribute_def_head.webp',
                                              width: width(120),
                                              height: width(120),
                                            )
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      "${element.value}",
                                      style: TextStyle(
                                        fontSize: sp(28),
                                        fontWeight: FontWeight.w400,
                                        height: 1.0,
                                        color: const Color(0xff000000),
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        )
                    );
                  })
                ],
              ),
            )
          )
        ],
      ),
    );
  }
}

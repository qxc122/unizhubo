import 'dart:async';

import 'package:agora_rtc_rawdata_example/models/bank_model.dart';
import 'package:agora_rtc_rawdata_example/models/user_model.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/addBank/bankLog.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/addBank/smsLog.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';

class AddBankController extends GetxController {
  RxString titleString = "".obs;
  RxString title = "".obs;
  RxString bankAccountName = "".obs;

  /// 开户人姓名[必填]
  RxString bankAccountNo = "".obs;

  /// 银行账号[必填]
  RxString bankAddress = "".obs;

  /// 开户银行地址[必填]
  RxString bankCode = "".obs;

  /// 银行名称标识符 如ICBC[必填]
  RxBool isDefault = false.obs;

  /// 是否默认银行卡 [必填]
  RxString smsCode = "".obs;

  /// 手机验证码[必填]

  final addressCtr = TextEditingController();
  final FocusNode addressFocus = FocusNode();

  final nameCtr = TextEditingController();
  final FocusNode nameFocus = FocusNode();

  final outletCtr = TextEditingController();
  final FocusNode outletFocus = FocusNode();

  final bankCtr = TextEditingController();
  final FocusNode bankFocus = FocusNode();

  final smsCtr = TextEditingController();
  final FocusNode smsFocus = FocusNode();

  RxList<BankModel> bankList = RxList<BankModel>();

  RxInt count = 60.obs;
  RxBool canGetVCode = true.obs; //是否可以获取验证码，默认为`false`
  RxString smsStr = '获取验证码'.obs;
  var userInfo = Rx<UserModel>(UserModel());

  /// 倒计时的计时器。
  Timer? timer;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
    getInit();
    getUserInfo();
  }

  @override
  void close() {
    timer?.cancel();
  }

  getUserInfo() async {
    var res = await API.getUserInfo();
    print(res.toString());
    if (res['code'] == 200) {
      var user = UserModel.fromJson(res['data']);
      userInfo.value = user;
      update();
    }
  }

  getInit() async {
    var res = await API.getBankList();
    print(res.toString());
    if (res['code'] == 200) {
      var list =
          (res['data'] as List).map((e) => BankModel.fromJson(e)).toList();
      bankList.addAll(list);
      if (bankList.length > 0) {
        // bankAddress.value = bankList[0].des.toString();
        bankCode.value = bankList[0].value.toString();
      }
    }
  }

  changeDefault(bool val) {
    isDefault.value = val;
  }

  selectBankLog() {
    Get.bottomSheet(BankLog());
  }

  selectBank(BankModel e) {
    // bankAddress.value = e.des.toString();
    bankCode.value = e.value.toString();
    Get.back();
  }

  setName(String text) {
    bankAccountName.value = text;
  }

  setSms(String text) {
    smsCode.value = text;
  }

  setOutlet(String text) {
    bankAddress.value = text;

  }

  setBank(String text) {
    bankAccountNo.value = text;
  }

  startTimer() {
    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      count.value--;
      canGetVCode.value = false;
      smsStr.value = '已发送${count.value}s';
      if (count.value == 0) {
        smsStr.value = '重新获取';
        canGetVCode.value = true;
        count.value = 60;
        timer.cancel();
      }
      update();
    });
  }

  sendMsg() async {
    var res = await API.sendSms({
      "areaCode": "86",
      "captchaKey": "",
      "imgCode": '',
      "phone": userInfo.value.mobilePhone,
      "sendType": 6
    });
    print(res);
    if (res['code'] == 200) {
      BotToast.showText(text: "发送成功", align: Alignment(0, 0));
      startTimer();
    } else {
      BotToast.showText(text: res['msg'], align: Alignment(0, 0));
    }
  }

  submit() async {
    if (bankAccountName.isEmpty) {
      BotToast.showText(
          text: "请输入真实姓名",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    } else if (bankCode.isEmpty) {
      BotToast.showText(
          text: "请选择开户行",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    } else if (bankAccountNo.isEmpty) {
      BotToast.showText(
          text: "请输入银行卡号",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    } else if (bankAddress.isEmpty) {
      BotToast.showText(
          text: "请输入开户网点",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    }else {
      Get.dialog(SmsLog());
    }
  }

  submitAll() async {
    if (bankAccountName.isEmpty) {
      BotToast.showText(
          text: "请输入真实姓名",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    } else if (bankCode.isEmpty) {
      BotToast.showText(
          text: "请选择开户行",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    } else if (bankAccountNo.isEmpty) {
      BotToast.showText(
          text: "请输入银行卡号",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    } else if (bankAddress.isEmpty) {
      BotToast.showText(
          text: "请输入开户网点",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    } else {
      Get.back();
      var res = await API.addBank({
        "bankAccountName": bankAccountName.value,
        "bankAccountNo": bankAccountNo.value,
        "bankAddress": bankAddress.value,
        "bankCode": bankCode.value,
        "isDefault": isDefault.value,
        "smsCode": smsCode.value,
      });
      print(res.toString());
      if (res['code'] == 200) {
        Get.back(result: true);
      } else {
        BotToast.showText(
            text: res["msg"],
            textStyle: const TextStyle(fontSize: 14, color: Colors.white),
            align: const Alignment(0, 0));
      }
    }
  }
}

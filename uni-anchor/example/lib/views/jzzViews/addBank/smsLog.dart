import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/addBank/controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class SmsLog extends Dialog {
  @override
  Widget build(BuildContext context) {
    var controller = Get.find<AddBankController>();
    //自定义弹框内容
    return WillPopScope(
        onWillPop: () async {
          return Future.value(true);
          // return Future.value(controller.compulsory.value=='1'?false:true);
        },
        child: Material(
            type: MaterialType.transparency,
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: width(25)),
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(15))),
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(
                              top: width(40), bottom: width(36)),
                          child: Text(
                            "请输入验证码",
                            style: TextStyle(
                              fontSize: sp(32),
                              fontWeight: FontWeight.w400,
                              color: const Color(0xff000000),
                            ),
                          ),
                        ),
                        Container(
                            margin: EdgeInsets.symmetric(horizontal: width(40)),
                            padding: EdgeInsets.symmetric(horizontal: width(20)),
                            height: width(92),
                            decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                                border: Border.all(
                                    color: Color(0xffBCBCBC), width: width(1))),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: TextField(
                                    inputFormatters: [
                                      LengthLimitingTextInputFormatter(15)
                                    ],
                                    keyboardType: TextInputType.phone,
                                    cursorColor: const Color(0xff333333),
                                    style: TextStyle(
                                        color: const Color(0xff333333),
                                        fontSize: sp(28)),
                                    decoration: const InputDecoration(
                                      border: InputBorder.none,
                                      hintText: "请输入短信验证码",
                                      hintStyle: TextStyle(
                                          color: Color(0xffC7C7CC), fontSize: 14.0),
                                      floatingLabelBehavior:
                                      FloatingLabelBehavior.never,
                                    ),
                                    focusNode: controller.smsFocus,
                                    controller: controller.smsCtr,
                                    onChanged: (String text) {
                                      controller.setSms(text);
                                    },
                                  ),
                                ),
                                InkWell(
                                  onTap: () {
                                    controller.sendMsg();
                                  },
                                  child: Container(
                                    width: width(156),
                                    height: width(50),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(width(40))),
                                      color: Color(0xffD7BFFF)
                                    ),
                                    alignment: Alignment.center,
                                    child: Obx(()=>Text(
                                      "${controller.smsStr.value}",
                                      style: TextStyle(
                                        fontSize: sp(24),
                                        fontWeight: FontWeight.w400,
                                        color: const Color(0xff3300FF),
                                      ),
                                    ),)
                                  ),
                                )
                              ],
                            )
                        ),
                        Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(
                              top: width(20), bottom: width(30)),
                          child: Text(
                            "因涉及资金安全，需要安全验证",
                            style: TextStyle(
                              fontSize: sp(24),
                              fontWeight: FontWeight.w400,
                              color: const Color(0xff5A5A5A),
                            ),
                          ),
                        ),
                        Divider(
                          height: width(1),
                          color: Color(0xffD6D6D6),
                        ),
                        Container(
                          height: width(100),
                          child: Row(
                            children: [
                              Expanded(
                                child: InkWell(
                                  onTap: () {
                                    controller.smsCtr.text = "";
                                    controller.smsCode.value = "";
                                    Get.back();
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      "取消",
                                      style: TextStyle(
                                        fontSize: sp(32),
                                        fontWeight: FontWeight.w400,
                                        color: const Color(0xff9F9F9F),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: width(1),
                                height: width(48),
                                color: const Color(0xffD6D6D6),
                              ),
                              Expanded(
                                child: InkWell(
                                  onTap: () {
                                    controller.submitAll();
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      "确定",
                                      style: TextStyle(
                                        fontSize: sp(32),
                                        fontWeight: FontWeight.w400,
                                        color: const Color(0xff9F44FF),
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            )));
  }
}

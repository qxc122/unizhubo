import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/withdrawalDetail/controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BankLog extends StatelessWidget {
  var controller = Get.find<WithdrawalDetailController>();

  @override
  Widget build(BuildContext context) {
    return Obx(()=>Container(
      alignment: Alignment.bottomCenter,
      height: width(520),
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: width(24)).copyWith(top: width(40)),
      child: Column(
        children: [
          ...controller.bankList.value.map((e){
            return InkWell(
              onTap: () {
                controller.selectBank(e);
              },
              child: Container(
                  height: width(120),
                  margin: EdgeInsets.only(bottom: width(20)),
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(horizontal: width(30)),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(width(10))),
                      border: Border.all(color: Color(0xffC5C5C5),width: width(2)),
                      color: Color(0xffEDEDED)),
                  child: Row(
                    children: [
                      e.bankLogo != null?Container(
                          width: width(90),
                          height: width(90),
                          margin: EdgeInsets.only(right: width(28)),
                          child: Image.network("${e.bankLogo}")
                      ):
                      Container(
                        width: width(90),
                        height: width(90),
                        margin: EdgeInsets.only(right: width(28)),
                        child: Image.asset(
                          "images/icon_contribute_def_head.webp",
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            child: Text(
                              "${e.bankAccountName}",
                              style: TextStyle(
                                fontSize: sp(28),
                                fontWeight: FontWeight.w400,
                                color: const Color(0xff000000),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: width(4)),
                            child: Text(
                              "${e.bankAccountNo}",
                              style: TextStyle(
                                fontSize: sp(28),
                                fontWeight: FontWeight.w400,
                                color: const Color(0xff000000),
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  )
              ),
            );
          })

        ],
      ),
    ));
  }
}

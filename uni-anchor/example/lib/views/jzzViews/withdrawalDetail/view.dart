import 'dart:convert';
import 'dart:math';

import 'package:agora_rtc_rawdata_example/views/jzzViews/withdrawalDetail/controller.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class WithdrawalDetailScreen extends GetView<WithdrawalDetailController> {
  const WithdrawalDetailScreen({Key? key}) : super(key: key);

  _buildHome(String title, context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: width(30)),
            decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(color: Color(0xffEEEEEE), width: width(2)),
                    bottom:
                        BorderSide(color: Color(0xffEEEEEE), width: width(2)))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InkWell(
                  onTap: () {
                    controller.setBankLog();
                  },
                  child: Container(
                    height: width(120),
                    alignment: Alignment.centerLeft,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        controller.bankAccid.value == 'null'
                            ? Container(
                                child: Text(
                                  "添加银行卡",
                                  style: TextStyle(
                                    fontSize: sp(28),
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xff6129FF),
                                  ),
                                ),
                              )
                            : Row(
                                children: [
                                  controller.bankLogo.value != '' &&
                                          controller.bankLogo.value
                                              .contains("http")
                                      ? Container(
                                          width: width(90),
                                          height: width(90),
                                          margin:
                                              EdgeInsets.only(right: width(20)),
                                          child: Image.network(
                                              "${controller.bankLogo.value}"))
                                      : Container(
                                          width: width(90),
                                          height: width(90),
                                          margin:
                                              EdgeInsets.only(right: width(20)),
                                          child: Image.asset(
                                            "images/icon_contribute_def_head.webp",
                                          ),
                                        ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        child: Text(
                                          "${controller.bankName.value}",
                                          style: TextStyle(
                                            fontSize: sp(28),
                                            fontWeight: FontWeight.w400,
                                            color: const Color(0xff000000),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        child: Text(
                                          "${controller.bankAccountNo.value}",
                                          style: TextStyle(
                                            fontSize: sp(24),
                                            fontWeight: FontWeight.w400,
                                            color: const Color(0xffBCBCBC),
                                          ),
                                        ),
                                      )
                                    ],
                                  )
                                ],
                              ),
                        Container(
                          width: width(32),
                          height: width(32),
                          child: Image.asset(
                            "images/right_arr.webp",
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: width(40)),
                  child: Text(
                    "提现金额（金币）",
                    style: TextStyle(
                      fontSize: sp(28),
                      fontWeight: FontWeight.w400,
                      color: const Color(0xff000000),
                    ),
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      child: TextField(
                        inputFormatters: [LengthLimitingTextInputFormatter(15)],
                        keyboardType: TextInputType.number,
                        cursorColor: const Color(0xff333333),
                        style: TextStyle(
                            color: const Color(0xff333333), fontSize: sp(28)),
                        decoration: const InputDecoration(
                          border: InputBorder.none,
                          hintText: "请输入提现金额",
                          hintStyle: TextStyle(
                              color: Color(0xffC7C7CC), fontSize: 14.0),
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                        ),
                        focusNode: controller.titleFocus,
                        controller: controller.titleCtr,
                        onChanged: (String text) {
                          controller.setTitle(text);
                        },
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: width(30)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: width(34),
                ),
                Row(
                  children: [
                    Container(
                      width: width(140),
                      child: Text(
                        "用户名：",
                        style: TextStyle(
                          fontSize: sp(24),
                          fontWeight: FontWeight.w400,
                          color: const Color(0xffBCBCBC),
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        "${controller.account.value}",
                        style: TextStyle(
                          fontSize: sp(24),
                          fontWeight: FontWeight.w400,
                          color: const Color(0xff5A5A5A),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: width(30),
                ),
                Row(
                  children: [
                    Container(
                      width: width(140),
                      child: Text(
                        "余额：",
                        style: TextStyle(
                          fontSize: sp(24),
                          fontWeight: FontWeight.w400,
                          color: const Color(0xffBCBCBC),
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        "${controller.balance.value}",
                        style: TextStyle(
                          fontSize: sp(24),
                          fontWeight: FontWeight.w400,
                          color: const Color(0xff5A5A5A),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: width(30),
                ),
                Row(
                  children: [
                    Container(
                      width: width(140),
                      child: Text(
                        "最多提取：",
                        style: TextStyle(
                          fontSize: sp(24),
                          fontWeight: FontWeight.w400,
                          color: const Color(0xffBCBCBC),
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        "${controller.maxAmount.value}",
                        style: TextStyle(
                          fontSize: sp(24),
                          fontWeight: FontWeight.w400,
                          color: const Color(0xff5A5A5A),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: width(30),
                ),
                Row(
                  children: [
                    Container(
                      child: Text(
                        "根据签约的分成比例${controller.giftRatio.value}%，将到账：${controller.currentRatioBalance.value}",
                        style: TextStyle(
                          fontSize: sp(20),
                          fontWeight: FontWeight.w400,
                          color: const Color(0xff000000),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: width(168),
                ),
                InkWell(
                  onTap: () {
                    controller.submit();
                  },
                  child: Container(
                    height: width(70),
                    margin: EdgeInsets.symmetric(horizontal: width(96)),
                    decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.all(Radius.circular(width(40))),
                      gradient: LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [
                            Color(0xff6129FF),
                            Color(0xffD96CFF),
                          ],
                          stops: [
                            0.03,
                            0.97
                          ]),
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      "提现",
                      style: TextStyle(
                        fontSize: sp(32),
                        fontWeight: FontWeight.w400,
                        color: const Color(0xffFFFFFF),
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        backgroundColor: const Color(0xffFFFFFF),
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              Get.back(result: true);
            },
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(width(20)),
            icon: Image.asset(
              "images/back.png",
              width: width(16),
            ),
          ),
          title: Text('提现',
              style: TextStyle(
                  color: const Color(0xff000000),
                  fontSize: sp(34),
                  fontWeight: FontWeight.bold)),
          backgroundColor: Colors.white,
          actions: [
            InkWell(
              onTap: () {
                Get.toNamed("/bankManage");
              },
              child: Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(right: width(30)),
                child: Text('银行卡管理',
                    style: TextStyle(
                        color: const Color(0xff000000),
                        fontSize: sp(28),
                        fontWeight: FontWeight.w400)),
              ),
            )
          ],
        ),
        body: WillPopScope(
            onWillPop: () async {
              Get.back(result: true);
              return true;
            },
            child: GetX<WithdrawalDetailController>(
                init: controller,
                builder: (_) => SingleChildScrollView(
                    child:
                        _buildHome(controller.titleString.value, context)))));
  }
}

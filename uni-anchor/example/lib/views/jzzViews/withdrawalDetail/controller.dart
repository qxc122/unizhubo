import 'package:agora_rtc_rawdata_example/models/bank_list_model.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/withdrawalDetail/bankLog.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/withdrawalDetail/payPaswordLog.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';

class WithdrawalDetailController extends GetxController
{
  RxString titleString = "".obs;
  RxString title = "".obs;
  RxString account = "".obs; /// 账户名
  RxString balance = "".obs; /// 当前可提现的余额
  RxString bankAccountName = "".obs; /// 开户人姓名
  RxString bankAccountNo = "".obs; /// 银行账号
  RxString bankLogo = "".obs; /// 银行卡logo地址
  RxString bankName = "".obs; /// 银行卡
  RxString bankCode = "".obs; /// 银行卡
  RxString ratioBalance = "".obs; /// 实际到账的金额
  RxString bankAccid = ''.obs; /// 会员银行卡id
  RxString maxAmount = '0'.obs; /// 最大提现到账金额, 0为不限制
  RxString minAmount = '0'.obs; /// 最小提现到账金额, 0为不限制
  RxDouble giftRatio = 0.0.obs; /// 礼物抽成比例
  RxString currentRatioBalance = '0.0'.obs; ///

  final titleCtr = TextEditingController();
  final FocusNode titleFocus = FocusNode();

  RxList<BankListModel> bankList = RxList<BankListModel>();

  TextEditingController codeController = TextEditingController(text: "");



  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
    getInit();
  }

  getInit() async {
    var res = await API.withdrawIndex();
    print(res.toString());
    if (res['code'] == 200) {
      bankAccid.value = res['data']['bankAccid'].toString();
      account.value = res['data']['account'].toString();
      bankAccountNo.value = res['data']['bankAccountNo'].toString();
      bankCode.value = res['data']['bankCode'].toString();
      bankName.value = res['data']['bankName'].toString();
      bankLogo.value = res['data']['bankLogo'].toString();
      balance.value = res['data']['balance'].toString();
      minAmount.value = res['data']['minAmount'].toString();
      maxAmount.value = res['data']['maxAmount'].toString();
      giftRatio.value = double.parse(res['data']['giftRatio'].toString());
      ratioBalance.value = res['data']['ratioBalance'].toString();
      getBankList();
      update();
    }
  }

  getBankList() async {
    bankList.clear();
    var res = await API.bankList();
    print(res.toString());
    if (res['code'] == 200) {
      var list = (res['data']['memBankAccountList'] as List)
          .map((e) => BankListModel.fromJson(e))
          .toList();
      bankList.addAll(list);
      list.forEach((element) {
        if (element.bankLogo.toString().contains(bankLogo.value.toString())) {
          bankLogo.value = element.bankLogo.toString();
          update();
        }
      });

      update();
    }
  }

  setBankLog() {
    if(bankAccid.value == "null") {
      Get.toNamed("/addBank")?.then((value){
        if (value != null && value) {
          getInit();
        }
      });
    } else {
      getBankList();
      Get.bottomSheet(BankLog());
    }

  }
  selectBank(BankListModel e) {
    bankAccid.value = e.bankAccid.toString();
    bankName.value = e.bankName.toString();
    bankCode.value = e.bankCode.toString();
    bankAccountNo.value = e.bankAccountNo.toString();
    bankLogo.value = e.bankLogo.toString();
    Get.back();
  }

  setTitle(String text) {
    if (text.trim().isNotEmpty) {
      title.value = text;
      currentRatioBalance.value = (double.parse(text)*double.parse(giftRatio.value.toString())/ 100).toString();
    } else {
      title.value = "";
      currentRatioBalance.value = '0.0';
    }

  }
  submit() {
    if (title.value.isEmpty) {
      BotToast.showText(
          text: "请输入提现金额",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    } else {
      codeController = TextEditingController(text: "");
      Get.dialog(PayPasswordLog());
    }

  }

  setPassword(String pwd) async {
    Get.back();
    var res = await API.withdrawCash({
      "bankAccid":bankAccid.value,
      "payPassword": pwd,
      "price":title.value
    });
    print(res.toString());
    if (res['code'] == 200) {
      BotToast.showText(
          text: "提交成功",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
      titleCtr.text = "";
      title.value = "";
      getInit();
    } else {
      BotToast.showText(
          text: res["msg"],
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    }
  }


}

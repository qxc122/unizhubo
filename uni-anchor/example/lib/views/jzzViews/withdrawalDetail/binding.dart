import 'package:agora_rtc_rawdata_example/views/jzzViews/withdrawalDetail/controller.dart';
import 'package:get/get.dart';
class WithdrawalDetailbinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<WithdrawalDetailController>(()=> WithdrawalDetailController());
  }
}

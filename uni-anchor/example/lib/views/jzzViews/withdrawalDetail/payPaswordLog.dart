import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/withdrawalDetail/controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';

class PayPasswordLog extends Dialog {

  var controller = Get.find<WithdrawalDetailController>();

  @override
  Widget build(BuildContext context) {
    // timer(context);
    TextEditingController _controller = TextEditingController();
    FocusNode _node = FocusNode();

    //自定义弹框内容
    return Material(
      type: MaterialType.transparency,
      child: Center(
          child: Container(
              width: width(608),
              height: width(320),
              // padding: EdgeInsets.symmetric(horizontal: width(75))
              //     .copyWith(bottom: width(31)),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(width(14)))),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(
                          top: width(50), left: width(30), right: width(30),bottom: width(64)),
                      child: Column(
                        children: [
                          Align(
                            child: Text(
                              '请输入支付密码',
                              style: TextStyle(
                                  color: Color(0xff333333),
                                  fontSize: sp(32),
                                  fontWeight: FontWeight.w400),
                            ),
                            alignment: Alignment.center,
                          ),
                          SizedBox(
                            height: width(26),
                          ),
                          Divider(
                            height: width(2),
                            color: Color(0xffF2F2F2),
                          ),
                          SizedBox(
                            height: width(26),
                          ),
                          PinCodeTextField(
                            keyboardType: TextInputType.numberWithOptions(decimal: false),
                            pinBoxWidth: width(72),
                            pinBoxHeight: width(72),
                            pinBoxBorderWidth: 0.0,
                            pinBoxRadius: width(12),
                            pinBoxOuterPadding:
                            EdgeInsets.symmetric(horizontal: width(10)),
                            autofocus: true,
                            controller: controller.codeController,
                            hideCharacter: true, // 隐藏字符
                            maskCharacter: '●',
                            highlight: true,
                            highlightColor: Color(0Xff707070),
                            defaultBorderColor: Colors.transparent,
                            hasTextBorderColor: Colors.transparent,
                            maxLength: 6,
                            onDone: (text) {
                              print("DONE $text");
                              controller.setPassword(text);
                            },
                            wrapAlignment: WrapAlignment.spaceEvenly,
                            pinBoxDecoration:
                            ProvidedPinBoxDecoration.defaultPinBoxDecoration,
                            pinTextStyle:
                            TextStyle(fontSize: sp(30), color: Color(0xff000000)),
                            pinTextAnimatedSwitcherTransition:
                            ProvidedPinBoxTextAnimation.scalingTransition,
                            pinBoxColor: Color(0xffEDEDED),
                            pinTextAnimatedSwitcherDuration: Duration(milliseconds: 30),
                            highlightAnimation: true,
                            highlightPinBoxColor: Color(0xffEDEDED),
                            highlightAnimationBeginColor: Colors.black,
                            highlightAnimationEndColor: Colors.white12,
                          ),
                        ],
                      )),
                ],
              ))),
    );
  }
}


import 'package:agora_rtc_rawdata_example/views/jzzViews/anchorManagement/controller.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/anchorPassword/controller.dart';
import 'package:get/get.dart';
class AnchorPasswordbinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AnchorPasswordController>(()=> AnchorPasswordController());
  }
}

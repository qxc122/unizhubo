import 'dart:convert';
import 'dart:math';

import 'package:agora_rtc_rawdata_example/views/jzzViews/anchorPassword/controller.dart';
import 'package:agora_rtc_rawdata_example/views/processImg/ProcessImg.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class AnchorPasswordScreen extends GetView<AnchorPasswordController> {
  const AnchorPasswordScreen({Key? key}) : super(key: key);

  _buildHome(String title, context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            Container(
              padding: EdgeInsets.only(left: width(30),right: width(10)),
              decoration: BoxDecoration(
                  border: Border(bottom: BorderSide(color: Color(0xffEEEEEE),width: width(2))),
                  color: Color(0xffF5F5F5)
              ),
              alignment: Alignment.centerLeft,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: width(24),),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Container(
                            width: width(90),
                            height: width(90),
                            margin: EdgeInsets.only(right: width(20)),
                              clipBehavior: Clip.hardEdge,
                              decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(45))
                              ),
                            child: MyImage(url: controller.avatar.value.toString(),id: 'anchorPassword' + controller.userId.value.toString())
                            // Image.asset(
                            //   "images/icon_contribute_def_head.webp",
                            // ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                child: Text(
                                  "${controller.userId.value}",
                                  style: TextStyle(
                                    fontSize: sp(28),
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xff000000),
                                  ),
                                ),
                              ),
                              Container(
                                child: Text(
                                  "${controller.nickName.value}",
                                  style: TextStyle(
                                    fontSize: sp(24),
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xffBCBCBC),
                                  ),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                      Row(
                        children: [],
                      )
                    ],
                  ),
                  SizedBox(height: width(30),),
                  Row(
                    children: [
                      Container(
                        width: width(36),
                        height: width(36),
                        margin: EdgeInsets.only(right: width(12)),
                        child: Image.asset(
                          "images/jzz/note.png",
                        ),
                      ),
                      Container(
                        child: Text(
                          "正在修改主播的密码，会导致正在直播的主播重新登录！",
                          style: TextStyle(
                            fontSize: sp(24),
                            fontWeight: FontWeight.w400,
                            color: const Color(0xffFF0000),
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: width(20),),
                ],
              )
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: width(20)),
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: width(100),
                    padding: EdgeInsets.symmetric(horizontal: width(24)),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: const Color(0xffEEEEEE),
                                width: width(1)))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: width(40)),
                                width: width(24),
                                height: width(28),
                                child: Image.asset(
                                  "images/aq.png",
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: TextField(
                                  obscureText: true,
                                  keyboardType: TextInputType.visiblePassword,
                                  cursorColor: const Color(0xff333333),
                                  style: TextStyle(
                                      color: const Color(0xff333333),
                                      fontSize: sp(28)),
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "新登录密码",
                                    hintStyle: TextStyle(
                                        color: Color(0xffC7C7CC),
                                        fontSize: 14.0),
                                    floatingLabelBehavior:
                                    FloatingLabelBehavior.never,
                                  ),
                                  focusNode: controller.titleFocus,
                                  controller: controller.titleCtr,
                                  onChanged: (String text) {
                                    controller.setTitle(text);
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            controller.clear();
                          },
                          child: Container(
                            width: width(30),
                            height: width(30),
                            child: Image.asset(
                              "images/c2.png",
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: width(100),
                    padding: EdgeInsets.symmetric(horizontal: width(24)),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: const Color(0xffEEEEEE),
                                width: width(1)))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: width(40)),
                                width: width(24),
                                height: width(28),
                                child: Image.asset(
                                  "images/password.png",
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: TextField(
                                  obscureText: true,
                                  keyboardType: TextInputType.visiblePassword,
                                  cursorColor: const Color(0xff333333),
                                  style: TextStyle(
                                      color: const Color(0xff333333),
                                      fontSize: sp(28)),
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "请再次输入密码",
                                    hintStyle: TextStyle(
                                        color: Color(0xffC7C7CC),
                                        fontSize: 14.0),
                                    floatingLabelBehavior:
                                    FloatingLabelBehavior.never,
                                  ),
                                  focusNode: controller.passwordFocus,
                                  controller: controller.passwordCtr,
                                  onChanged: (String text) {
                                    controller.setTitle1(text);
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            controller.clear1();
                          },
                          child: Container(
                            width: width(30),
                            height: width(30),
                            child: Image.asset(
                              "images/c2.png",
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: width(100),
                    padding: EdgeInsets.symmetric(horizontal: width(24)),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: const Color(0xffEEEEEE),
                                width: width(1)))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: width(40)),
                                child: Text(
                                  "家族登录密码",
                                  style: TextStyle(
                                    fontSize: sp(28),
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xff000000),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: TextField(
                                  obscureText: true,
                                  keyboardType: TextInputType.visiblePassword,
                                  cursorColor: const Color(0xff333333),
                                  style: TextStyle(
                                      color: const Color(0xff333333),
                                      fontSize: sp(28)),
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "请输入家族登录密码",
                                    hintStyle: TextStyle(
                                        color: Color(0xffC7C7CC),
                                        fontSize: 14.0),
                                    floatingLabelBehavior:
                                    FloatingLabelBehavior.never,
                                  ),
                                  focusNode: controller.newPFocus,
                                  controller: controller.newPCtr,
                                  onChanged: (String text) {
                                    controller.setTitle2(text);
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            controller.clear2();
                          },
                          child: Container(
                            width: width(30),
                            height: width(30),
                            child: Image.asset(
                              "images/c2.png",
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: width(400),
            ),
            InkWell(
              onTap: () {
                controller.save();
              },
              child: Container(
                height: width(70),
                margin: EdgeInsets.symmetric(horizontal: width(75)),
                alignment: Alignment.center,
                decoration: controller.flag.value
                    ? const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(35)),
                    color: Color(0xff9F44FF))
                    : const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(35)),
                    color: Color(0xffCCCCCC)),
                child: Text(
                  "确定",
                  style: TextStyle(
                    fontSize: sp(28),
                    fontWeight: FontWeight.w400,
                    color: const Color(0xffFFFFFF),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        backgroundColor: const Color(0xffFFFFFF),
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(width(20)),
            icon: Image.asset(
              "images/back.png",
              width: width(16),
            ),
          ),
          title: Text('修改主播密码',
              style: TextStyle(
                  color: const Color(0xff000000),
                  fontSize: sp(34),
                  fontWeight: FontWeight.bold)),
          backgroundColor: Colors.white,
        ),
        body: GetX<AnchorPasswordController>(
            init: controller,
            builder: (_) => SingleChildScrollView(
                child: _buildHome(controller.titleString.value, context))));
  }
}


import 'package:agora_rtc_rawdata_example/views/jzzViews/anchorManagement/controller.dart';
import 'package:get/get.dart';
class AnchorManagementbinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AnchorManagementController>(()=> AnchorManagementController());
  }
}

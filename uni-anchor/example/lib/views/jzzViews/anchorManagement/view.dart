import 'dart:convert';
import 'dart:math';

import 'package:agora_rtc_rawdata_example/models/anchor_model.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/anchorManagement/controller.dart';
import 'package:agora_rtc_rawdata_example/views/processImg/ProcessImg.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class AnchorManagementScreen extends GetView<AnchorManagementController> {
  const AnchorManagementScreen({Key? key}) : super(key: key);

  _buildHome(String title, context) {
    return Column(
      children: [
        Container(
          padding:EdgeInsets.symmetric(horizontal: width(16)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(child:
              Container(
                padding: EdgeInsets.symmetric(horizontal: width(20)),
                height: width(60),
                decoration: BoxDecoration(
                    color: Color(0xffEEEEEE),
                    borderRadius: BorderRadius.all(Radius.circular(width(40)))
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: width(40),
                      height: width(40),
                      margin: EdgeInsets.only(right: width(20)),
                      alignment: Alignment.center,
                      child: Image.asset(
                        "images/jzz/find.png",
                        width: width(40),
                        height: width(40),
                      ),
                    ),
                    Expanded(
                      child: TextField(
                        // inputFormatters: [
                        //   LengthLimitingTextInputFormatter(15)
                        // ],
                        keyboardType: TextInputType.text,
                        cursorColor: const Color(0xff333333),
                        style: TextStyle(
                            color: const Color(0xff333333),
                            fontSize: sp(28)),
                        decoration: const InputDecoration(
                            border: InputBorder.none,
                            hintText: "请输入主播昵称/主播账号",
                            hintStyle: TextStyle(
                                color: Color(0xffC7C7CC), fontSize: 14.0),
                            floatingLabelBehavior:
                            FloatingLabelBehavior.never,
                            contentPadding:EdgeInsets.only(bottom: 14)
                        ),
                        focusNode: controller.titleFocus,
                        controller: controller.titleCtr,
                        onChanged: (String text) {
                          controller.setTitle(text);
                        },
                      ),
                    ),
                    controller.nickName.value != '' ? InkWell(
                      onTap: () {
                        controller.clearName();
                      },
                      child: Container(
                        width: width(40),
                        height: width(40),
                        alignment: Alignment.center,
                        child: Image.asset(
                          "images/jzz/close.png",
                          width: width(40),
                          height: width(40),
                        ),
                      ),
                    ):Container()
                  ],
                ),
              )
              ),
              InkWell(
                onTap: () {
                  controller.getInit(1);
                },
                child: Container(
                  padding: EdgeInsets.only(left: width(14)),
                  child: Text(
                    "搜索",
                    style: TextStyle(
                      fontSize: sp(24),
                      fontWeight: FontWeight.w400,
                      color: const Color(0xff000000),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        SizedBox(height: width(24),),
        Expanded(
          child: Container(
            alignment: Alignment.centerLeft,
            child: SmartRefresher(
              enablePullDown: true,
              enablePullUp: true,
              header: WaterDropHeader(
                refresh: CircularProgressIndicator(
                    strokeWidth: 2,
                    valueColor:
                    AlwaysStoppedAnimation(Color(0xff999999))),
                complete: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.done,
                      color: Colors.grey,
                      size: sp(30),
                    ),
                    Container(width: 30.0),
                    Text("加载完成",
                        style: TextStyle(
                            fontSize: sp(28),
                            color: Color(0xff999999)))
                  ],
                ),
              ),
              footer: CustomFooter(
                builder: (BuildContext context, LoadStatus? mode) {
                  Widget body;
                  if (mode == LoadStatus.idle) {
                    body = Text(
                      "下拉加载更多",
                      style: TextStyle(
                          color: Color(0xff999999),
                          fontSize: sp(28)),
                    );
                  } else if (mode == LoadStatus.loading) {
                    body = CupertinoActivityIndicator();
                  } else if (mode == LoadStatus.failed) {
                    body = Text(
                      "加载失败，点击重试",
                      style: TextStyle(
                          color: Color(0xff999999),
                          fontSize: sp(28)),
                    );
                  } else if (mode == LoadStatus.canLoading) {
                    body = Text("释放加载更多",
                        style: TextStyle(
                            color: Color(0xff999999),
                            fontSize: sp(28)));
                  } else {
                    body = Text(
                      "暂无更多数据",
                      style: TextStyle(
                          color: Color(0xff999999),
                          fontSize: sp(28)),
                    );
                  }
                  return Container(
                    height: 55.0,
                    child: Center(child: body),
                  );
                },
              ),
              controller: controller.refreshController,
              onRefresh: () {
                controller.getInit(1);
              },
              onLoading: () {
                controller.getInit(controller.pageNum.value + 1);
              },
              child: controller.anchorList.isEmpty
                  ? GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  controller.getInit(1);
                },
                child: controller.isLoading.value
                    ? Center(
                  child: CupertinoActivityIndicator(),
                )
                    : empty(),
              )
                  : ListView.separated(
                physics: ScrollPhysics(),
                itemBuilder: (c, i) => AnchorItem(
                    controller.anchorList[i]),
                itemCount: controller.anchorList.length,
                separatorBuilder: (context, i) => Container(),
              ),
            ),
          ),)
      ],
    );
  }


  Widget AnchorItem(AnchorModel e) {
    return InkWell(
      onTap: () {
        Get.toNamed("/anchorDetail", arguments: {"userId": e.userId,"isFrozen": e.isFrozen, "nickName": e.nickName, "avatar":e.avatar});
      },
      child: Container(
        height: width(120),
        padding: EdgeInsets.symmetric(horizontal: width(30)),
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Color(0xffEEEEEE),width: width(2)))
        ),
        alignment: Alignment.centerLeft,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Container(
                  width: width(90),
                  height: width(90),
                  margin: EdgeInsets.only(right: width(20)),
                    clipBehavior: Clip.hardEdge,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(45))
                    ),
                  child: MyImage(url: e.avatar.toString(),id: 'anchorManange' + e.userId.toString())
                  //
                  //   "images/icon_contribute_def_head.webp",
                  // ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: Text(
                        "${e.nickName}",
                        style: TextStyle(
                          fontSize: sp(28),
                          fontWeight: FontWeight.w400,
                          color: const Color(0xff000000),
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        "${e.userAccount}",
                        style: TextStyle(
                          fontSize: sp(24),
                          fontWeight: FontWeight.w400,
                          color: const Color(0xffBCBCBC),
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
            Container(
              width: width(32),
              height: width(32),
              child: Image.asset(
                "images/right_arr.webp",
              ),
            ),
          ],
        ),
      ),
    );
  }


  ///默认空页面
  empty() {
    return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: width(220),
              height: width(226),
              margin: EdgeInsets.only(bottom: width(10)),
              child: Image.asset(
                "images/no_data.png",
                width: width(220),
                height: width(226),
              ),
            ),
            Text("暂无数据",
                style: TextStyle(
                    fontSize: sp(28),
                    color: Color(0xff768A90),
                    fontWeight: FontWeight.w400,
                    decoration: TextDecoration.none))
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        backgroundColor: const Color(0xffFFFFFF),
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(width(20)),
            icon: Image.asset(
              "images/back.png",
              width: width(16),
            ),
          ),
          title: Text('主播管理',
              style: TextStyle(
                  color: const Color(0xff000000),
                  fontSize: sp(34),
                  fontWeight: FontWeight.bold)),
          backgroundColor: Colors.white,
        ),
        body: GetX<AnchorManagementController>(
            init: controller,
            builder: (_) => _buildHome(controller.titleString.value, context)));
  }
}

import 'package:agora_rtc_rawdata_example/models/anchor_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class AnchorManagementController extends GetxController
{
  RxString titleString = "out".obs;
  RxString nickName = "".obs;
  RxInt pageNum = 0.obs;
  RxInt totalNum = 0.obs;

  final titleCtr = TextEditingController();
  final FocusNode titleFocus = FocusNode();


  RxList<AnchorModel> anchorList = RxList<AnchorModel>();
  RefreshController refreshController =
  RefreshController(initialRefresh: false);
  RxBool isLoading = false.obs;



  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
    getInit(1);
  }

  setTitle(String text) {
    nickName.value = text;
  }
  clearName() {
    nickName.value = "";
    titleCtr.text = "";
  }

  getInit(int page) async {
    pageNum.value = page;
    if (pageNum.value == 1) {
      anchorList.clear();
    }
    var res = await API.anchorList({
      "nickName": nickName.value,
      "pageNum": pageNum.value,
      "pageSize":20
    });
    print(res.toString());
    if (res['code'] == 200) {
      totalNum.value = res['data']["total"];
      var list =
      (res['data']['list'] as List).map((e) => AnchorModel.fromJson(e)).toList();
      anchorList.addAll(list);
      refreshController.refreshCompleted();
      refreshController.loadComplete();
      if (list.length < 20) {
        refreshController.loadNoData();
      }
      isLoading.value = false;
      update();
    }
  }

}

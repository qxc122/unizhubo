import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/login/controller.dart';
class Loginbinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LoginController>(()=> LoginController());
  }
}

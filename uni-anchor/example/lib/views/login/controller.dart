import 'dart:convert';
import 'dart:typed_data';

import 'package:bot_toast/bot_toast.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';

class LoginController extends GetxController {
  RxString titleString = "out".obs;

  final idCtr = TextEditingController();
  final FocusNode idFocus = FocusNode();
  final accountCtr = TextEditingController();
  final FocusNode accountFocus = FocusNode();
  final passwordCtr = TextEditingController();
  final FocusNode passwordFocus = FocusNode();

  RxString id = "".obs;
  RxString account = "".obs;
  RxString password = "".obs;
  RxBool flag = false.obs;
  RxBool isShow = true.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
  }

  changeShow() {
    isShow.value = !isShow.value;
    update();
  }

  setId(String text) {
    id.value = text;
    verify();
  }

  setAccount(String text) {
    account.value = text;
    verify();
  }

  setPassword(String text) {
    password.value = text;
    verify();
  }

  verify() {
    if (id.isNotEmpty && account.isNotEmpty && password.isNotEmpty) {
      flag.value = true;
    } else {
      flag.value = false;
    }
    update();
  }

  String generateMD5(String data) {
    Uint8List content = new Utf8Encoder().convert(data);
    Digest digest = md5.convert(content);
    return digest.toString();
  }

  login() async {
    if (id.isEmpty) {
      BotToast.showText(
          text: "请输入平台ID",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    } else if (account.isEmpty) {
      BotToast.showText(
          text: "请输入账号",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    } else if (password.isEmpty) {
      BotToast.showText(
          text: "请输入密码",
          textStyle: const TextStyle(fontSize: 14, color: Colors.white),
          align: const Alignment(0, 0));
    } else {
      var res = await API.login({
        "merchantCode": id.value,
        "userAccount": account.value,
        "password": password.value
      });
      print(res.toString());
      if (res['code'] == 200) {
        SpUtils.setToken(res['data']['acctoken']);
        SpUtils.setIsFamily(res['data']['isFamily']);
        if (res['data']['isFamily']) {
          Get.offAllNamed("/jzzWallet");
        } else {
          Get.offAllNamed("/layout");
        }

      } else {
        BotToast.showText(
            text: res["msg"],
            textStyle: const TextStyle(fontSize: 14, color: Colors.white),
            align: const Alignment(0, 0));
      }
    }
  }
}

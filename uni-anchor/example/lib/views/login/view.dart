import 'dart:convert';
import 'dart:math';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/views/login/controller.dart';

import 'package:agora_rtc_rawdata_example/views/mine/controller.dart';

UnderlineInputBorder border = UnderlineInputBorder(borderSide: BorderSide.none);

class LoginScreen extends GetView<LoginController> {
  const LoginScreen({Key? key}) : super(key: key);

  _buildHome(String title, context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: width(55)).copyWith(top:width(112)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: width(12)),
              child: Text(
                "Welcome",
                style: TextStyle(
                  fontSize: sp(32),
                  fontWeight: FontWeight.w400,
                  color: const Color(0xff000000),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: width(48)),
              child: Text(
                "开始直播生活",
                style: TextStyle(
                  fontSize: sp(32),
                  fontWeight: FontWeight.w400,
                  color: const Color(0xff000000),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: width(34)),
              height: width(80),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(5)),
                border: Border.all(color: Color(0xff000000),width: width(2))
              ),
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      // inputFormatters: [
                      //   LengthLimitingTextInputFormatter(11)
                      // ],
                      keyboardType: TextInputType.number,
                      cursorColor: const Color(0xff333333),
                      style: TextStyle(
                          color: const Color(0xff333333), fontSize: sp(28)),
                      decoration: const InputDecoration(
                        border: InputBorder.none,
                        hintText: "平台ID",
                        hintStyle:
                        TextStyle(color: Color(0xffC7C7CC), fontSize: 14.0),
                        floatingLabelBehavior: FloatingLabelBehavior.never,
                      ),
                      focusNode: controller.idFocus,
                      controller: controller.idCtr,
                      onChanged: (String text) {
                        controller.setId(text);
                      },
                    ),
                  ),
                ],
              )
            ),
            SizedBox(height: width(30),),
            Container(
              padding: EdgeInsets.symmetric(horizontal: width(34)),
              height: width(80),
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(5)),
                  border: Border.all(color: Color(0xff000000),width: width(2))
              ),
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      // inputFormatters: [
                      //   LengthLimitingTextInputFormatter(11)
                      // ],
                      keyboardType: TextInputType.text,
                      cursorColor: const Color(0xff333333),
                      style: TextStyle(
                          color: const Color(0xff333333), fontSize: sp(28)),
                      decoration: const InputDecoration(
                        border: InputBorder.none,
                        hintText: "账号",
                        hintStyle:
                        TextStyle(color: Color(0xffC7C7CC), fontSize: 14.0),
                        floatingLabelBehavior: FloatingLabelBehavior.never,
                      ),
                      focusNode: controller.accountFocus,
                      controller: controller.accountCtr,
                      onChanged: (String text) {
                        controller.setAccount(text);
                      },
                    ),
                  ),
                ],
              )
            ),
            SizedBox(height: width(30),),
            Container(
              padding: EdgeInsets.symmetric(horizontal: width(34)),
              height: width(80),
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(5)),
                  border: Border.all(color: Color(0xff000000),width: width(2))
              ),
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      // inputFormatters: [
                      //   LengthLimitingTextInputFormatter(11)
                      // ],
                      obscureText: controller.isShow.value,
                      keyboardType: controller.isShow.value ? TextInputType.visiblePassword : TextInputType.text,
                      cursorColor: const Color(0xff333333),
                      style: TextStyle(
                          color: const Color(0xff333333), fontSize: sp(28)),
                      decoration: const InputDecoration(
                        border: InputBorder.none,
                        hintText: "密码",
                        hintStyle:
                        TextStyle(color: Color(0xffC7C7CC), fontSize: 14.0),
                        floatingLabelBehavior: FloatingLabelBehavior.never,
                      ),
                      focusNode: controller.passwordFocus,
                      controller: controller.passwordCtr,
                      onChanged: (String text) {
                        controller.setPassword(text);
                      },
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      controller.changeShow();
                    },
                    child: Container(
                      width: width(32),
                      alignment: Alignment.center,
                      child: Image.asset(
                        controller.isShow.value ?"images/e1.png" : "images/e2.png",
                        width: width(32),
                        height: width(20),
                      ),
                    ),
                  )
                ],
              )
            ),
            SizedBox(height: width(234),),
            InkWell(
              onTap: () {
                controller.login();
              },
              child: Container(
                height: width(80),
                margin: EdgeInsets.symmetric(horizontal: width(124)),
                alignment: Alignment.center,
                decoration: controller.flag.value ? const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                    gradient: LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                        colors:[Color(0xff6129FF), Color(0xffD96CFF
                        )]
                    )
                ): const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                    color: Color(0xffD9D9D9)
                ),
                child: Text(
                  "登入",
                  style: TextStyle(
                    fontSize: sp(32),
                    fontWeight: FontWeight.w400,
                    color: const Color(0xffFFFFFF),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        backgroundColor: Colors.white,
        body: GetX<LoginController>(
            init: controller,
            builder: (_) => SingleChildScrollView(
                child: _buildHome(controller.titleString.value, context))));
  }
}

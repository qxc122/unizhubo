import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'controller.dart';
import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewScreen extends GetView<WebViewsController> {
  const WebViewScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(width(20)),
            icon: Image.asset(
              "images/back.png",
              width: width(16),
            ),
          ),
          backgroundColor: Colors.white,
          title: Obx(() => Text('${controller.titleStr.value}',
              style: TextStyle(
                  color: Color(0xff000000),
                  fontSize: sp(32),
                  fontWeight: FontWeight.w600))),
          centerTitle: true,
        ),
        body: Obx(() => WebView(
              initialUrl: "${controller.urlStr.value}",
              javascriptMode: JavascriptMode.unrestricted,
            )));
  }
}

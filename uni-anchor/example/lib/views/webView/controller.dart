import 'dart:async';
import 'package:get/get.dart';

class WebViewsController extends GetxController {
  RxString urlStr = "".obs;
  RxString titleStr = "".obs;

  @override
  void onInit() {
    super.onInit();
    final details = Get.arguments as Map;
    print(33333);
    urlStr.value = details['url'].toString();
    titleStr.value = details['title'].toString();
    print(details['url'].toString());
    print(33333);
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}

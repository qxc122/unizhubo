import 'package:get/get.dart';
import '../../binding/local_repository_impl.dart';
import '../../services/local_storage_repository.dart';
import 'controller.dart';
class SplashBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LocalRepositoryInterface>(()=> LocalRepositoryImpl());
    Get.lazyPut<SplashController>(() => SplashController(
        localRepositoryInterface: Get.find()
    ));
  }
}

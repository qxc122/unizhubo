import 'dart:async';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../services/local_storage_repository.dart';

class SplashController extends GetxController {
  SplashController({required this.localRepositoryInterface});

  final LocalRepositoryInterface localRepositoryInterface;

  RxInt countdownTime = 3.obs;
  var _timer;
  // 切换splash方式
  RxBool showGuide = true.obs;
  RxBool isFirst = false.obs;
  RxBool isLogin = false.obs;

  /* ------ guide------- */
  List<String> images = <String>[
    'images/splash/splash.png',
    'images/splash/splash.png',
    'images/splash/splash.png',
  ];
  RxInt curIndex = 0.obs;

  @override
  void onReady() {
    super.onReady();
    validateSession();
  }

  @override
  void onClose() {
    super.onClose();
    _timer?.cancel();
    _timer = null;
  }

  getInit() async {
    // SharedPreferences preferences = await SharedPreferences.getInstance();
    // String? first = preferences.getString('first');
    // if (first.toString().isNotEmpty && first != null) {
    //   Get.offAllNamed(AppRoutes.Layout);
    // } else {
    //   validateSession();
    // }
    validateSession();
  }

  void validateSession() async {
    final token = await SpUtils.getToken();
    isLogin.value = token != null && token != "" ? true: false;
    // if (token.toString().isNotEmpty) {
    //   SharedPreferences preferences = await SharedPreferences.getInstance();
    //   bool isFamily = preferences.getBool('isFamily')?? false;
    //   if (isFamily) {
    //     Get.offAllNamed("/jzzWallet");
    //   } else {
    //     Get.offAllNamed("/layout");
    //   }
    // } else {
    //   Get.offAllNamed("/login");
    // }
    if (!showGuide.value) {
      isFirst.value = true;
    } else {
      isFirst.value = false;
      startCountdownTimer();
    }
  }

  void nextPage() async {
    _timer?.cancel();
    if (isLogin.value) {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      bool isFamily = preferences.getBool('isFamily')?? false;
      if (isFamily) {
        Get.offAllNamed("/jzzWallet");
      } else {
        Get.offAllNamed("/layout");
      }
    } else {
      Get.offAllNamed(AppRoutes.Login);
    }
    // SharedPreferences preferences = await SharedPreferences.getInstance();
    // preferences.setString('first', "1");
    // Get.offAllNamed(AppRoutes.Layout);
  }

  void startCountdownTimer() {
    const oneSec = const Duration(seconds: 1);
    var callback = (timer) {
      if (countdownTime < 1) {
        _timer.cancel();
        nextPage();
      } else {
        countdownTime = countdownTime - 1;
      }
    };
    _timer = Timer.periodic(oneSec, callback);
  }
}

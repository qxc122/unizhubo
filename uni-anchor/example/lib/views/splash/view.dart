import 'dart:async';
import 'dart:ui';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/splash/controller.dart';
import '../../utils/screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper_tv/flutter_swiper.dart';

class SplashPage extends GetView<SplashController> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Obx(() => WillPopScope(
        onWillPop: () => Future.value(false),
        child: Stack(fit: StackFit.expand, children: <Widget>[
          controller.isFirst.value
              ? GuidePage()
              : Image.asset('images/splash/splash.png', fit: BoxFit.cover),
          Visibility(
            visible: !controller.isFirst.value,
            child: Align(
              alignment: Alignment.bottomRight,
              child: SafeArea(
                  child: InkWell(
                      onTap: controller.nextPage,
                      child: GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onTap: controller.nextPage,
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 5),
                          margin: EdgeInsets.only(right: 20, bottom: 20),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(40),
                            color: Colors.black.withAlpha(100),
                          ),
                          child: Obx(() => Text(
                            (controller.countdownTime.value == 0
                                ? '跳过'
                                : '${controller.countdownTime.value} | ' +
                                '跳过'),
                            style: TextStyle(color: Colors.white),
                          )),
                        ),
                      ))),
            ),
          ),
        ]),
      )),
    );
  }
}

class GuidePage extends GetView<SplashController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          color: Theme.of(context).primaryColor,
          child: Stack(
            alignment: Alignment(0, 0.87),
            children: <Widget>[
              Swiper(
                  itemBuilder: (ctx, index) =>
                      Image.asset(controller.images[index], fit: BoxFit.cover),
                  itemCount: controller.images.length,
                  loop: false,
                  onIndexChanged: (index) => controller.curIndex.value = index?? 0),
              Obx(() => Offstage(
                offstage:
                controller.curIndex.value != controller.images.length - 1,
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: SafeArea(
                    child: InkWell(
                      onTap: controller.nextPage,
                      child: Container(
                        width: width(160),
                        height: height(60),
                        alignment: Alignment.center,
                        margin: EdgeInsets.symmetric(
                            horizontal: width(50), vertical: height(80)),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                          color: Colors.black.withAlpha(50),
                        ),
                        child: Text('点击进入',
                            style: TextStyle(
                                color: Colors.white, fontSize: sp(28))),
                      ),
                    ),
                  ),
                ),
              )),
            ],
          ),
        ));
  }
}

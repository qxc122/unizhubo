import 'dart:typed_data';

import 'package:agora_rtc_rawdata_example/utils/CacheFileImage.dart';
import 'package:agora_rtc_rawdata_example/utils/aes2.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';



class MyImage extends StatelessWidget {
  final String url;
  final String id;
  MyImage({required this.url, required this.id});
  @override
  Widget build(BuildContext context) {
    final controller = Get.put(MyImageController(), tag: id);
    if (controller != null) controller.loadImage(url,id);
    return GetBuilder<MyImageController>(
      id: url.toString().split(".w.")[0].split("/").last,
      key: UniqueKey(),
      init: MyImageController(),
      tag: id,
      builder: (_) {
        if (url.toString().contains(".w.")) {
          if (_.isLoading == true) {
            // return Center(child: CircularProgressIndicator());
            return Container(
              child: Image.asset('images/icon_contribute_def_head.webp',
                  fit: BoxFit.cover),
            );
          }
          if (_.image == null) {
            return Container(
              child: Image.asset('images/icon_contribute_def_head.webp',
                  fit: BoxFit.cover),
            );
          }
          return Image.memory(_.image, fit: BoxFit.cover);
        } else {
          return Container(
            child: Image.network('${url}',
                fit: BoxFit.cover,
                errorBuilder: (ctx, err, stackTrace) => Image.asset(
                  'images/icon_contribute_def_head.webp', //默认显示图片
                  fit: BoxFit.cover,
                )),
          );
        }
      },
    );
  }
}

class MyImageController extends GetxController {
  bool _isLoading = true;
  bool get isLoading => _isLoading;
  late Uint8List _image;
  Uint8List get image => _image;
  void loadImage(String imgUrl, String id) async {
    if (imgUrl.toString().contains(".w.")) {
      _isLoading = true;
      update(["${imgUrl.toString().split(".w.")[0].split("/").last}"]);
      final CacheFileImage _cacheFileImage = CacheFileImage();
      Uint8List? cacheImg = await _cacheFileImage.getFileBytes(imgUrl);
      if (cacheImg !=  null){
        _image = cacheImg;
        _isLoading = false;
        update(["${imgUrl.toString().split(".w.")[0].split("/").last}",""]);
      } else {
        var response = await Dio().get(imgUrl.toString(),
            options: Options(responseType: ResponseType.bytes));
        final bytes = response.data;
        _image = EncryptUtil2.encodeImg(bytes);
        if (_image != null) {
          _cacheFileImage.saveBytesToFile(imgUrl, _image);
          _isLoading = false;
          update(["${imgUrl.toString().split(".w.")[0].split("/").last}",""]);
        }
      }
    }
  }
}



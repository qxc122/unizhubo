import 'package:agora_rtc_rawdata_example/api/index.dart';
import 'package:agora_rtc_rawdata_example/models/studio_model.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/views/layout/isPlay.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:agora_rtc_rawdata_example/utils/screen.dart';
import 'package:agora_rtc_rawdata_example/views/mine/controller.dart';
import 'package:agora_rtc_rawdata_example/views/mine/view.dart';

import '../home/view.dart';
import '../play/view.dart';

class LayoutController extends GetxController {
  final MineController mineController = Get.find<MineController>();

  RxInt currentIndex = 0.obs;
  RxString studioNum = "".obs;

  final List<Widget> pageList = [
    HomeScreen(), // 首页
    PlayScreen(), // 直播
    MineScreen(), // 我的
  ];

  final list = [
    BottomNavigationBarItem(
      icon: Image.asset('images/tabbar/home.png',
          width: width(60), height: width(60)),
      activeIcon: Image.asset('images/tabbar/home_active.png',
          width: width(60), height: width(60)),
      label: '首页',
    ),
    BottomNavigationBarItem(
      icon: Image.asset('images/tabbar/play.png',
          width: width(60), height: width(60)),
      activeIcon: Image.asset('images/tabbar/play_active.png',
          width: width(60), height: width(60)),
      label: '开播',
    ),
    BottomNavigationBarItem(
      icon: Image.asset('images/tabbar/mine.png',
          width: width(60), height: width(60)),
      activeIcon: Image.asset('images/tabbar/mine_active.png',
          width: width(60), height: width(60)),
      label: '我的',
    ),
  ];

  var studioInfo = Rx<StudioModel>(StudioModel());

  @override
  void onReady() {
    super.onReady();
    getStudioStatus();
    if (Get.arguments != null) {
      var detail = Get.arguments as Map;
      ;
      setCurrent(int.parse(detail["index"]));
    }
  }

  /// 获取主播当前开播状态
  getStudioStatus() async {
    studioNum.value = await SpUtils.getStudioNum() ?? '';
    var res = await API.getStudioStatus();
    print(res.toString());
    if (res['code'] == 200) {
      studioInfo.value = StudioModel.fromJson(res['data']);
      if (res['data']['studioStatus'] == 1) {
        Get.dialog(IsPlayLog(), barrierDismissible: false);
      }
    }
  }

  closeLive() {
    Get.offAndToNamed("/layout");
  }

  toLive() async {
    Get.back();
    Get.toNamed("/liveRoom", arguments: {
      "studioNum": studioNum.value,
      "studioThumbImage": studioInfo.value.studioThumbImage,
      "channelGameId": "t_${studioInfo.value.gameId}",
      "chargeType": studioInfo.value.colour.toString()
    });
  }

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onClose() {
    // welfareController.clearTimer();
    super.onClose();
  }

  setCurrent(index) {
    currentIndex.value = index;
    getStudioStatus();
    update();
    // if (currentIndex == index &&  index != 3) {
    //   return;
    // } else {
    //   _currentIndex.value = index;
    //   _appBarKey.currentState?.animateTo(index);
    // }
  }
}

import 'package:agora_rtc_rawdata_example/views/processImg/ProcessImg.dart';
import 'package:get/get.dart';

import 'package:agora_rtc_rawdata_example/binding/local_repository_impl.dart';
import 'package:agora_rtc_rawdata_example/services/local_storage_repository.dart';
import 'package:agora_rtc_rawdata_example/views/mine/controller.dart';
import 'package:agora_rtc_rawdata_example/views/layout/controller.dart';

import 'package:agora_rtc_rawdata_example/views/home/controller.dart';
import 'package:agora_rtc_rawdata_example/views/play/controller.dart';

class LayoutBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<LocalRepositoryInterface>(LocalRepositoryImpl());
    Get.lazyPut<LayoutController>(() => LayoutController());
    Get.lazyPut<HomeController>(() => HomeController());
    Get.lazyPut<MineController>(() => MineController());
    Get.lazyPut<PlayController>(() => PlayController());
    // Get.lazyPut<MyImageController>(() => MyImageController());
  }
}

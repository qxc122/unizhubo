import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:agora_rtc_rawdata_example/views/layout/controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../../utils/screen.dart';

class IsPlayLog extends Dialog {
  @override
  Widget build(BuildContext context) {
    var controller = Get.find<LayoutController>();
    //自定义弹框内容
    return WillPopScope(
        onWillPop: () async {
          return Future.value(false);
        },
        child: Material(
            type: MaterialType.transparency,
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: width(100)),
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(15))),
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(
                              top: width(40), bottom: width(36)),
                          child: Text(
                            "提示",
                            style: TextStyle(
                              fontSize: sp(32),
                              fontWeight: FontWeight.w400,
                              color: const Color(0xff000000),
                            ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(
                              top: width(0), bottom: width(36)),
                          child: Text(
                            "当前账号正在直播中，是否继续直播？",
                            style: TextStyle(
                              fontSize: sp(26),
                              fontWeight: FontWeight.w400,
                              color: const Color(0xff000000),
                            ),
                          ),
                        ),
                        Divider(
                          height: width(1),
                          color: Color(0xffD6D6D6),
                        ),
                        Container(
                          height: width(100),
                          child: Row(
                            children: [
                              Expanded(
                                child: InkWell(
                                  onTap: () {
                                   controller.closeLive();
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      "关闭直播",
                                      style: TextStyle(
                                        fontSize: sp(32),
                                        fontWeight: FontWeight.w400,
                                        color: const Color(0xff9F9F9F),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: width(1),
                                height: width(48),
                                color: const Color(0xffD6D6D6),
                              ),
                              Expanded(
                                child: InkWell(
                                  onTap: () {
                                    controller.toLive();
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      "继续直播",
                                      style: TextStyle(
                                        fontSize: sp(32),
                                        fontWeight: FontWeight.w400,
                                        color: const Color(0xff9F44FF),
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            )));
  }
}

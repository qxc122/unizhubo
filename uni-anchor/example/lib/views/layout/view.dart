import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:agora_rtc_rawdata_example/views/layout/controller.dart';

import '../../utils/screen.dart';
import '../mine/view.dart';

class LayoutView extends GetView<LayoutController> {
  const LayoutView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return true;
      },
      child:Obx(()=>Scaffold(
        body: controller.pageList[controller.currentIndex.value],
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: const Color(0xfff9f9f9),
          type: BottomNavigationBarType.fixed,
          currentIndex: controller.currentIndex.value,
          selectedItemColor: const Color(0xff000000),
          unselectedItemColor: const Color(0xff999999),
          selectedFontSize: sp(24),
          unselectedFontSize: sp(24),
          onTap: (int index) {
            controller.setCurrent(index);
          },
          items: controller.list,
        ),

      ))
    );
  }
}

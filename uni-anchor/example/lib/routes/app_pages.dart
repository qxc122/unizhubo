import 'package:agora_rtc_rawdata_example/views/jzzViews/addBank/binding.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/addBank/view.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/anchorDetail/binding.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/anchorDetail/view.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/anchorManagement/binding.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/anchorManagement/view.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/anchorPassword/binding.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/anchorPassword/view.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/anchorReport/binding.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/anchorReport/view.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/anchorWithdrawal/binding.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/anchorWithdrawal/view.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/bankManage/binding.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/bankManage/view.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/cwdz/binding.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/cwdz/view.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/jzzWallet/binding.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/jzzWallet/view.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/withdrawal/binding.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/withdrawal/view.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/withdrawalDetail/binding.dart';
import 'package:agora_rtc_rawdata_example/views/jzzViews/withdrawalDetail/view.dart';
import 'package:agora_rtc_rawdata_example/views/mine/forgetPassword/binding.dart';
import 'package:agora_rtc_rawdata_example/views/mine/forgetPassword/view.dart';
import 'package:agora_rtc_rawdata_example/views/mine/payPassword2/binding.dart';
import 'package:agora_rtc_rawdata_example/views/mine/payPassword2/view.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/home/income/binding.dart';
import 'package:agora_rtc_rawdata_example/views/home/income/view.dart';
import 'package:agora_rtc_rawdata_example/views/home/ranking/binding.dart';
import 'package:agora_rtc_rawdata_example/views/home/ranking/view.dart';
import 'package:agora_rtc_rawdata_example/views/home/wallet/binding.dart';
import 'package:agora_rtc_rawdata_example/views/home/wallet/view.dart';
import 'package:agora_rtc_rawdata_example/views/layout/binding.dart';
import 'package:agora_rtc_rawdata_example/views/layout/view.dart';
import 'package:agora_rtc_rawdata_example/views/login/binding.dart';
import 'package:agora_rtc_rawdata_example/views/login/view.dart';
import 'package:agora_rtc_rawdata_example/views/mine/history/binding.dart';
import 'package:agora_rtc_rawdata_example/views/mine/history/view.dart';
import 'package:agora_rtc_rawdata_example/views/mine/information/binding.dart';
import 'package:agora_rtc_rawdata_example/views/mine/information/view.dart';
import 'package:agora_rtc_rawdata_example/views/mine/loginPassword/binding.dart';
import 'package:agora_rtc_rawdata_example/views/mine/loginPassword/view.dart';
import 'package:agora_rtc_rawdata_example/views/mine/nickname/binding.dart';
import 'package:agora_rtc_rawdata_example/views/mine/nickname/view.dart';
import 'package:agora_rtc_rawdata_example/views/mine/payPassword/binding.dart';
import 'package:agora_rtc_rawdata_example/views/mine/payPassword/view.dart';
import 'package:agora_rtc_rawdata_example/views/mine/setting/binding.dart';
import 'package:agora_rtc_rawdata_example/views/mine/setting/view.dart';
import 'package:agora_rtc_rawdata_example/views/notfound/view.dart';
import 'package:agora_rtc_rawdata_example/views/play/endLive/binding.dart';
import 'package:agora_rtc_rawdata_example/views/play/endLive/view.dart';
import 'package:agora_rtc_rawdata_example/views/play/liveRoom/binding.dart';
import 'package:agora_rtc_rawdata_example/views/play/liveRoom/view.dart';
import 'package:agora_rtc_rawdata_example/views/play/preview/binding.dart';
import 'package:agora_rtc_rawdata_example/views/play/preview/view.dart';
import 'package:agora_rtc_rawdata_example/views/splash/binding.dart';
import 'package:agora_rtc_rawdata_example/views/splash/view.dart';
import 'package:agora_rtc_rawdata_example/views/webView/binding.dart';
import 'package:agora_rtc_rawdata_example/views/webView/view.dart';

part 'app_routes.dart';

class AppPages {
  static const INITIAL = AppRoutes.Layout;

  static final routes = [
    GetPage(
      name: AppRoutes.Splash,
      page: () => SplashPage(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: AppRoutes.Layout,
      page: () => LayoutView(),
      binding: LayoutBinding(),
    ),
    GetPage(
      name: AppRoutes.Login,
      page: () => LoginScreen(),
      binding: Loginbinding(),
    ),
    GetPage(
      name: AppRoutes.Income,
      page: () => IncomeScreen(),
      binding: Incomebinding(),
    ),
    GetPage(
      name: AppRoutes.Wallet,
      page: () => WalletScreen(),
      binding: Walletbinding(),
    ),
    GetPage(
      name: AppRoutes.Ranking,
      page: () => RankingScreen(),
      binding: Rankingbinding(),
    ),
    GetPage(
      name: AppRoutes.LiveRoom,
      page: () => LiveRoomScreen(),
      binding: LiveRoombinding(),
    ),
    GetPage(
      name: AppRoutes.Preview,
      page: () => PreviewScreen(),
      binding: Previewbinding(),
    ),
    GetPage(
      name: AppRoutes.Nickname,
      page: () => NicknameScreen(),
      binding: Nicknamebinding(),
    ),
    GetPage(
      name: AppRoutes.History,
      page: () => HistoryScreen(),
      binding: Historybinding(),
    ),
    GetPage(
      name: AppRoutes.Setting,
      page: () => SettingScreen(),
      binding: Settingbinding(),
    ),
    GetPage(
      name: AppRoutes.LoginPassword,
      page: () => LoginPasswordScreen(),
      binding: LoginPasswordbinding(),
    ),
    GetPage(
      name: AppRoutes.Information,
      page: () => InformationScreen(),
      binding: Informationbinding(),
    ),
    GetPage(
        name: AppRoutes.WebView,
        page: () => WebViewScreen(),
        binding: WebViewBinding()
    ),
    GetPage(
        name: AppRoutes.EndLive,
        page: () => EndLiveScreen(),
        binding: EndLiveinding()
    ),
    GetPage(
        name: AppRoutes.PayPassword,
        page: () => PayPasswordScreen(),
        binding: PayPasswordbinding()
    ),
    GetPage(
        name: AppRoutes.PayPassword2,
        page: () => PayPassword2Screen(),
        binding: PayPassword2binding()
    ),
    GetPage(
        name: AppRoutes.ForgetPassword,
        page: () => ForgetPasswordScreen(),
        binding: ForgetPasswordbinding()
    ),
    GetPage(
        name: AppRoutes.JzzWallet,
        page: () => JzzWalletScreen(),
        binding: JzzWalletbinding()
    ),
    GetPage(
        name: AppRoutes.AnchorManagement,
        page: () => AnchorManagementScreen(),
        binding: AnchorManagementbinding()
    ),
    GetPage(
        name: AppRoutes.AnchorDetail,
        page: () => AnchorDetailScreen(),
        binding: AnchorDetailbinding()
    ),
    GetPage(
        name: AppRoutes.AnchorPassword,
        page: () => AnchorPasswordScreen(),
        binding: AnchorPasswordbinding()
    ),
    GetPage(
        name: AppRoutes.Cwdz,
        page: () => CwdzScreen(),
        binding: Cwdzbinding()
    ),
    GetPage(
        name: AppRoutes.Withdrawal,
        page: () => WithdrawalScreen(),
        binding: Withdrawalbinding()
    ),
    GetPage(
        name: AppRoutes.AnchorWithdrawal,
        page: () => AnchorWithdrawalScreen(),
        binding: AnchorWithdrawalbinding()
    ),
    GetPage(
        name: AppRoutes.WithdrawalDetail,
        page: () => WithdrawalDetailScreen(),
        binding: WithdrawalDetailbinding()
    ),
    GetPage(
        name: AppRoutes.BankManage,
        page: () => BankManageScreen(),
        binding: BankManagebinding()
    ),
    GetPage(
        name: AppRoutes.AddBank,
        page: () => AddBankScreen(),
        binding: AddBankbinding()
    ),
    GetPage(
        name: AppRoutes.AnchorReport,
        page: () => AnchorReportScreen(),
        binding: AnchorReportbinding()
    ),
  ];

  static final unknownRoute = GetPage(
    name: AppRoutes.NotFound,
    page: () => NotfoundView(),
  );
}

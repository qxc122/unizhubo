part of 'app_pages.dart';

abstract class AppRoutes {
  static const Splash = '/splash'; // 启动页
  static const Layout = '/layout'; // 布局
  static const NotFound = '/notFound'; // 未找到的页面
  static const Income = '/income'; // 收入
  static const Wallet = '/wallet'; // 我的钱包
  static const Ranking = '/ranking'; // 排行榜
  static const Login = '/login'; // 登录
  static const Preview = '/preview'; // 预览
  static const LiveRoom = '/liveRoom'; // 直播间
  static const Information = '/information'; // 个人中心
  static const Setting = '/setting'; // 设置
  static const LoginPassword = '/loginPassword'; // 修改登录密码
  static const History = '/history'; // 开播历史
  static const Nickname = '/nickname'; // 修改昵称
  static const WebView = '/webView'; // webView
  static const PayPassword = '/payPassword'; // 支付密码
  static const PayPassword2 = '/payPassword2'; // 设置支付密码
  static const ForgetPassword = '/forgetPassword'; // 忘记密码
  static const EndLive = '/endLive'; // 直播结束
  static const JzzWallet = '/jzzWallet'; // 家族长首页
  static const AnchorManagement = '/anchorManagement'; // 主播管理
  static const AnchorDetail = '/anchorDetail'; // 主播详情
  static const AnchorPassword = '/anchorPassword'; // 修改主播登录密码
  static const Cwdz = '/cwdz'; // 财务对账
  static const Withdrawal = '/withdrawal'; // 提现
  static const AnchorWithdrawal = '/anchorWithdrawal'; // 提现
  static const WithdrawalDetail = '/withdrawalDetail'; // 提现详情
  static const BankManage = '/bankManage'; // 银行卡管理
  static const AddBank = '/addBank'; // 添加银行卡
  static const AnchorReport = '/anchorReport'; // 主播报表
}

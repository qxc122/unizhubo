class AnchorModel {
  String? avatar;
  bool? isFrozen;
  String? liveTime;
  String? nickName;
  String? userAccount;
  String? userId;

  AnchorModel(
      {this.avatar,
        this.isFrozen,
        this.liveTime,
        this.nickName,
        this.userAccount,
        this.userId});

  AnchorModel.fromJson(Map<String, dynamic> json) {
    avatar = json['avatar'];
    isFrozen = json['isFrozen'];
    liveTime = json['liveTime'].toString();
    nickName = json['nickName'];
    userAccount = json['userAccount'];
    userId = json['userId'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['avatar'] = this.avatar;
    data['isFrozen'] = this.isFrozen;
    data['liveTime'] = this.liveTime;
    data['nickName'] = this.nickName;
    data['userAccount'] = this.userAccount;
    data['userId'] = this.userId;
    return data;
  }
}

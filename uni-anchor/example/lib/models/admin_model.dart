class AdminModel {
  String? accno;
  String? avatar;
  String? createTime;
  int? id;
  String? nickName;
  int? sex;
  String? userAccount;
  int? userId;
  int? userLevel;

  AdminModel(
      {this.accno,
        this.avatar,
        this.createTime,
        this.id,
        this.nickName,
        this.sex,
        this.userAccount,
        this.userId,
        this.userLevel});

  AdminModel.fromJson(Map<String, dynamic> json) {
    accno = json['accno'];
    avatar = json['avatar'];
    createTime = json['createTime'];
    id = json['id'];
    nickName = json['nickName'];
    sex = json['sex'];
    userAccount = json['userAccount'];
    userId = json['userId'];
    userLevel = json['userLevel'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['accno'] = this.accno;
    data['avatar'] = this.avatar;
    data['createTime'] = this.createTime;
    data['id'] = this.id;
    data['nickName'] = this.nickName;
    data['sex'] = this.sex;
    data['userAccount'] = this.userAccount;
    data['userId'] = this.userId;
    data['userLevel'] = this.userLevel;
    return data;
  }
}

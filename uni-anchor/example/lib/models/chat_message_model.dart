
import 'dart:convert' as covert;

class ChatMessageModel {
  Body? body;
  int? code;
  bool? isReceive;
  String? sendTime;

  ChatMessageModel({this.body, this.code, this.isReceive, this.sendTime});

  ChatMessageModel.fromJson(Map<String, dynamic> json) {
    body = json['body'] != null ? new Body.fromJson(json['body']) : null;
    code = json['code'];
    isReceive = json['isReceive'];
    sendTime = json['sendTime'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.body != null) {
      data['body'] = this.body!.toJson();
    }
    data['code'] = this.code;
    data['isReceive'] = this.isReceive;
    data['sendTime'] = this.sendTime;
    return data;
  }
}

class Body {
  int? chatType;
  Content? content;
  int? operatorType;
  String? sendTime;

  Body({this.chatType, this.content, this.operatorType, this.sendTime});

  Body.fromJson(Map<String, dynamic> json) {
    chatType = json['chatType'];
    content =
    json['content'].toString().indexOf("userAccount") > -1 || json['content'].toString().indexOf("liveGift") > -1 || json['content'].toString().indexOf("hotnessNum") > -1 || json['content'].toString().indexOf("barrage") > -1|| json['content'].toString().indexOf("ticketName") > -1|| json['content'].toString().indexOf("zjMoney") > -1 ? new Content.fromJson(covert.json.decode(json['content'])):json['content'] != null ? new Content.fromJson(json['content']) : null;
    operatorType = json['operatorType'];
    sendTime = json['sendTime'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['chatType'] = this.chatType;
    if (this.content != null) {
      data['content'] = this.content!.toJson();
    }
    data['operatorType'] = this.operatorType;
    data['sendTime'] = this.sendTime;
    return data;
  }
}

class Content {
  String? avatar;
  String? level;
  String? msgContent;
  String? nickName;
  String? userId;
  String? hotnessNum;
  String? onlineUsersCount;
  String? barrage;
  String? amount;
  String? ticketName;
  String? zjMoney;
  LiveGift? liveGift;

  Content(
      {this.avatar,
        this.level,
        this.msgContent,
        this.nickName,
        this.hotnessNum,
        this.userId,
        this.amount,
        this.ticketName,
        this.barrage,
        this.zjMoney,
        this.onlineUsersCount,
        this.liveGift});

  Content.fromJson(Map<String, dynamic> json) {
    avatar = json['avatar'];
    level = json['level'].toString();
    msgContent = json['msgContent'];
    nickName = json['nickName'];
    userId = json['userId'].toString();
    barrage= json['barrage'].toString();
    amount= json['amount'].toString();
    ticketName= json['ticketName'].toString();
    onlineUsersCount= json['onlineUsersCount'].toString();
    hotnessNum = json['hotnessNum'].toString();
    zjMoney = json['zjMoney'].toString();
    liveGift = json['liveGift'] != null
        ? new LiveGift.fromJson(json['liveGift'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['avatar'] = this.avatar;
    data['level'] = this.level;
    data['msgContent'] = this.msgContent;
    data['nickName'] = this.nickName;
    data['userId'] = this.userId;
    data['barrage'] = this.barrage;
    data['hotnessNum'] = this.hotnessNum;
    data['amount'] = this.amount;
    data['ticketName'] = this.ticketName;
    data['zjMoney'] = this.zjMoney;
    data['onlineUsersCount'] = this.onlineUsersCount;
    if (this.liveGift != null) {
      data['liveGift'] = this.liveGift!.toJson();
    }
    return data;
  }
}

class LiveGift {
  String? dynamicImage;
  String? giftName;
  String? giftNumber;
  String? imageUrl;

  LiveGift({this.dynamicImage, this.giftName, this.giftNumber, this.imageUrl});

  LiveGift.fromJson(Map<String, dynamic> json) {
    dynamicImage = json['dynamicImage'];
    giftName = json['giftName'];
    giftNumber = json['giftNumber'].toString();
    imageUrl = json['imageUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dynamicImage'] = this.dynamicImage;
    data['giftName'] = this.giftName;
    data['giftNumber'] = this.giftNumber;
    data['imageUrl'] = this.imageUrl;
    return data;
  }
}

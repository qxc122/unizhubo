class FinanceModel {
  int? type;
  String? createTime;
  String? amount;

  FinanceModel({this.type, this.createTime, this.amount});

  FinanceModel.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    createTime = json['createTime'];
    amount = json['amount'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['createTime'] = this.createTime;
    data['amount'] = this.amount;
    return data;
  }
}

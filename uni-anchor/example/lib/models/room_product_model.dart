class roomProductModel {
  int? giftId;
  String? price;

  roomProductModel({this.giftId, this.price});

  roomProductModel.fromJson(Map<String, dynamic> json) {
    giftId = json['giftId'];
    price = json['price'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['giftId'] = this.giftId;
    data['price'] = this.price;
    return data;
  }
}

class StudioModel {
  String? studioTitle;
  String? studioThumbImage;
  String? colour;
  String? sharpness;
  String? productId;
  String? trySeeTime;
  String? gameId;
  bool? isRelateToy;
  String? countryCode;
  String? provinceCode;
  String? studioNum;
  int? studioStatus;

  StudioModel(
      {this.studioTitle,
        this.studioThumbImage,
        this.colour,
        this.sharpness,
        this.studioNum,
        this.productId,
        this.trySeeTime,
        this.gameId,
        this.isRelateToy,
        this.countryCode,
        this.provinceCode,
        this.studioStatus});

  StudioModel.fromJson(Map<String, dynamic> json) {
    studioTitle = json['studioTitle'];
    studioThumbImage = json['studioThumbImage'];
    colour = json['colour'].toString();
    studioNum = json['studioNum'].toString();
    sharpness = json['sharpness'];
    productId = json['productId'].toString();
    trySeeTime = json['trySeeTime'].toString();
    gameId = json['gameId'].toString();
    isRelateToy = json['isRelateToy'];
    countryCode = json['countryCode'];
    provinceCode = json['provinceCode'];
    studioStatus = json['studioStatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['studioTitle'] = this.studioTitle;
    data['studioThumbImage'] = this.studioThumbImage;
    data['colour'] = this.colour;
    data['studioNum'] = this.studioNum;
    data['sharpness'] = this.sharpness;
    data['productId'] = this.productId;
    data['trySeeTime'] = this.trySeeTime;
    data['gameId'] = this.gameId;
    data['isRelateToy'] = this.isRelateToy;
    data['countryCode'] = this.countryCode;
    data['provinceCode'] = this.provinceCode;
    data['studioStatus'] = this.studioStatus;
    return data;
  }
}

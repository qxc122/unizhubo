class AnchorReportModel {
  String? accno;
  String? avatar;
  String? liveTime;
  String? nickName;

  AnchorReportModel({this.accno, this.avatar, this.liveTime, this.nickName});

  AnchorReportModel.fromJson(Map<String, dynamic> json) {
    accno = json['accno'];
    avatar = json['avatar'];
    liveTime = json['liveTime'].toString();
    nickName = json['nickName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['accno'] = this.accno;
    data['avatar'] = this.avatar;
    data['liveTime'] = this.liveTime;
    data['nickName'] = this.nickName;
    return data;
  }
}

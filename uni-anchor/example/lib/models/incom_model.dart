class IncomeModel {
  int? changeType;
  String? createTime;
  String? changeName;
  String? changeMoney;

  IncomeModel(
      {this.changeType, this.createTime, this.changeName, this.changeMoney});

  IncomeModel.fromJson(Map<String, dynamic> json) {
    changeType = json['changeType'];
    createTime = json['createTime'];
    changeName = json['changeName'];
    changeMoney = json['changeMoney'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['changeType'] = this.changeType;
    data['createTime'] = this.createTime;
    data['changeName'] = this.changeName;
    data['changeMoney'] = this.changeMoney;
    return data;
  }
}

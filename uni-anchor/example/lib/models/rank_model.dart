class RankModel {
  int? position;
  int? userId;
  String? nickName;
  int? distance;
  String? fansCount;
  String? avatar;
  String? decodeAvatar;
  String? firepower;
  int? userType;

  RankModel(
      {this.position,
        this.userId,
        this.nickName,
        this.distance,
        this.fansCount,
        this.avatar,
        this.decodeAvatar,
        this.firepower,
        this.userType});

  RankModel.fromJson(Map<String, dynamic> json) {
    position = json['position'];
    userId = json['userId'];
    nickName = json['nickName'];
    distance = json['distance'];
    fansCount = json['fansCount'].toString();
    avatar = json['avatar'];
    decodeAvatar = json['avatar'];
    firepower = json['firepower'].toString();
    userType = json['userType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['position'] = this.position;
    data['userId'] = this.userId;
    data['nickName'] = this.nickName;
    data['distance'] = this.distance;
    data['fansCount'] = this.fansCount;
    data['avatar'] = this.avatar;
    data['decodeAvatar'] = this.decodeAvatar;
    data['firepower'] = this.firepower;
    data['userType'] = this.userType;
    return data;
  }
}

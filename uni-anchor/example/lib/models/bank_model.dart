class BankModel {
  String? des;
  String? logo;
  String? value;

  BankModel({this.des, this.logo, this.value});

  BankModel.fromJson(Map<String, dynamic> json) {
    des = json['des'];
    logo = json['logo'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['des'] = this.des;
    data['logo'] = this.logo;
    data['value'] = this.value;
    return data;
  }
}

import 'dart:convert' as covert;

import 'dart:convert';

class WinningLotteryModel {
  int? code;
  Body? body;
  String? sendTime;
  String? receiptTime;
  String? msgId;
  String? errorMsg;
  String? receive;

  WinningLotteryModel(
      {this.code,
        this.body,
        this.sendTime,
        this.receiptTime,
        this.msgId,
        this.errorMsg,
        this.receive});

  WinningLotteryModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    body = json['body'] != null ? new Body.fromJson(json['body']) : null;
    sendTime = json['sendTime'].toString();
    receiptTime = json['receiptTime'].toString();
    msgId = json['msgId'].toString();
    errorMsg = json['errorMsg'].toString();
    receive = json['receive'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    if (this.body != null) {
      data['body'] = this.body!.toJson();
    }
    data['sendTime'] = this.sendTime;
    data['receiptTime'] = this.receiptTime;
    data['msgId'] = this.msgId;
    data['errorMsg'] = this.errorMsg;
    data['receive'] = this.receive;
    return data;
  }
}

class Body {
  String? id;
  int? chatType;
  String? targetId;
  int? operatorType;
  String? sendTime;
  List<Content>? content;
  String? bannedEndTime;

  Body(
      {this.id,
        this.chatType,
        this.targetId,
        this.operatorType,
        this.sendTime,
        this.content,
        this.bannedEndTime});

  Body.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    chatType = json['chatType'];
    targetId = json['targetId'];
    operatorType = json['operatorType'];
    sendTime = json['sendTime'];
    if (json['content'] != null) {
      content = <Content>[];
      covert.json.decode(json['content']).forEach((v) {
        content!.add(new Content.fromJson(v));
      });
    }
    bannedEndTime = json['bannedEndTime'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['chatType'] = this.chatType;
    data['targetId'] = this.targetId;
    data['operatorType'] = this.operatorType;
    data['sendTime'] = this.sendTime;
    if (this.content != null) {
      data['content'] = this.content!.map((v) => v.toJson()).toList();
    }
    data['bannedEndTime'] = this.bannedEndTime;
    return data;
  }
}

class Content {
  String? nickName;
  String? studioNum;
  String? uid;
  String? zjMoney;

  Content({this.nickName, this.studioNum, this.uid, this.zjMoney});

  Content.fromJson(Map<String, dynamic> json) {
    nickName = json['nickName'];
    studioNum = json['studioName'].toString();
    uid = json['uid'].toString();
    zjMoney = json['zjMoney'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nickName'] = this.nickName;
    data['studioNum'] = this.studioNum;
    data['uid'] = this.uid;
    data['zjMoney'] = this.zjMoney;
    return data;
  }
}

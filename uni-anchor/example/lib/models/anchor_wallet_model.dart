class AnchorWalletModel {
  String? avatar;
  String? nickName;
  String? userAccount;
  String? userId;
  String? withdrawAccount;

  AnchorWalletModel(
      {this.avatar,
        this.nickName,
        this.userAccount,
        this.userId,
        this.withdrawAccount});

  AnchorWalletModel.fromJson(Map<String, dynamic> json) {
    avatar = json['avatar'];
    nickName = json['nickName'];
    userAccount = json['userAccount'];
    userId = json['userId'].toString();
    withdrawAccount = json['withdrawAccount'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['avatar'] = this.avatar;
    data['nickName'] = this.nickName;
    data['userAccount'] = this.userAccount;
    data['userId'] = this.userId;
    data['withdrawAccount'] = this.withdrawAccount;
    return data;
  }
}

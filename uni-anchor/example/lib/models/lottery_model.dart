class LotteryModel {
  String? icon;
  int? lotteryId;
  String? name;
  String? baseUrl;

  LotteryModel({this.icon, this.lotteryId, this.name, this.baseUrl});

  LotteryModel.fromJson(Map<String, dynamic> json) {
    icon = json['icon'];
    lotteryId = json['lotteryId'];
    name = json['name'];
    baseUrl = json['baseUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['icon'] = this.icon;
    data['lotteryId'] = this.lotteryId;
    data['name'] = this.name;
    data['baseUrl'] = this.baseUrl;
    return data;
  }
}

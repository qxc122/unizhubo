class GameModel {
  String? userName;
  int? userId;
  String? betTime;
  String? betAmount;

  GameModel({this.userName, this.userId, this.betTime, this.betAmount});

  GameModel.fromJson(Map<String, dynamic> json) {
    userName = json['userName'];
    userId = json['userId'];
    betTime = json['betTime'];
    betAmount = json['betAmount'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userName'] = this.userName;
    data['userId'] = this.userId;
    data['betTime'] = this.betTime;
    data['betAmount'] = this.betAmount;
    return data;
  }
}

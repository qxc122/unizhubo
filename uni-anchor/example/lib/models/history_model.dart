class HistoryModel {
  String? liveDate;
  String? dayHour;
  String? dayMinute;
  bool? flag;
  List<HistoryList>? historyList;

  HistoryModel({this.liveDate, this.dayHour, this.dayMinute,this.flag, this.historyList});

  HistoryModel.fromJson(Map<String, dynamic> json) {
    liveDate = json['liveDate'];
    dayHour = json['dayHour'].toString();
    dayMinute = json['dayMinute'].toString();
    flag= json['flag'];
    if (json['historyList'] != null) {
      historyList = <HistoryList>[];
      json['historyList'].forEach((v) {
        historyList!.add(new HistoryList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['liveDate'] = this.liveDate;
    data['dayHour'] = this.dayHour;
    data['dayMinute'] = this.dayMinute;
    data['flag'] = this.flag;
    if (this.historyList != null) {
      data['historyList'] = this.historyList!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class HistoryList {
  String? startTime;
  String? endTime;
  String? hour;
  String? minute;

  HistoryList({this.startTime, this.endTime, this.hour, this.minute});

  HistoryList.fromJson(Map<String, dynamic> json) {
    startTime = json['startTime'];
    endTime = json['endTime'];
    hour = json['hour'].toString();
    minute = json['minute'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['startTime'] = this.startTime;
    data['endTime'] = this.endTime;
    data['hour'] = this.hour;
    data['minute'] = this.minute;
    return data;
  }
}

class GiftModel {
  String? userAccountAnchor;
  String? userAccount;
  String? studioNum;
  int? giftType;
  String? giftLogNo;
  String? giftName;
  String? giftNumber;
  String? givingTime;
  String? price;
  String? avatar;
  String? userName;
  String? userLevel;

  GiftModel(
      {this.userAccountAnchor,
        this.userAccount,
        this.studioNum,
        this.giftType,
        this.giftLogNo,
        this.giftName,
        this.giftNumber,
        this.givingTime,
        this.price,
        this.avatar,
        this.userName,
        this.userLevel});

  GiftModel.fromJson(Map<String, dynamic> json) {
    userAccountAnchor = json['userAccountAnchor'];
    userAccount = json['userAccount'];
    studioNum = json['studioNum'].toString();
    giftType = json['giftType'];
    giftLogNo = json['giftLogNo'];
    giftName = json['giftName'];
    giftNumber = json['giftNumber'].toString();
    givingTime = json['givingTime'];
    price = json['price'].toString();
    avatar = json['avatar'];
    userName = json['userName'];
    userLevel = json['userLevel'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userAccountAnchor'] = this.userAccountAnchor;
    data['userAccount'] = this.userAccount;
    data['studioNum'] = this.studioNum;
    data['giftType'] = this.giftType;
    data['giftLogNo'] = this.giftLogNo;
    data['giftName'] = this.giftName;
    data['giftNumber'] = this.giftNumber;
    data['givingTime'] = this.givingTime;
    data['price'] = this.price;
    data['avatar'] = this.avatar;
    data['userName'] = this.userName;
    data['userLevel'] = this.userLevel;
    return data;
  }
}

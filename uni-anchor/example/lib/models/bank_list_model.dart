class BankListModel {
  int? bankAccid;
  String? bankAccountName;
  String? bankAccountNo;
  String? bankAddress;
  String? bankCode;
  String? bankLogo;
  String? bankName;
  String? createTime;
  int? isDefault;
  bool? flag;

  BankListModel(
      {this.bankAccid,
        this.bankAccountName,
        this.bankAccountNo,
        this.bankAddress,
        this.bankCode,
        this.bankLogo,
        this.bankName,
        this.createTime,
        this.flag,
        this.isDefault});

  BankListModel.fromJson(Map<String, dynamic> json) {
    bankAccid = json['bankAccid'];
    bankAccountName = json['bankAccountName'];
    bankAccountNo = json['bankAccountNo'];
    bankAddress = json['bankAddress'];
    bankCode = json['bankCode'];
    bankLogo = json['bankLogo'];
    bankName = json['bankName'];
    createTime = json['createTime'];
    isDefault = json['isDefault'];
    flag = json['flag'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bankAccid'] = this.bankAccid;
    data['bankAccountName'] = this.bankAccountName;
    data['bankAccountNo'] = this.bankAccountNo;
    data['bankAddress'] = this.bankAddress;
    data['bankCode'] = this.bankCode;
    data['bankLogo'] = this.bankLogo;
    data['bankName'] = this.bankName;
    data['createTime'] = this.createTime;
    data['isDefault'] = this.isDefault;
    data['flag'] = this.flag;
    return data;
  }
}

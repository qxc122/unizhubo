class UserModel {
  int? userId;
  int? anchorId;
  String? userAccount;
  String? nickName;
  String? familyName;
  bool? nickNameStatus;
  String? avatar;
  int? userType;
  String? registerAreaCode;
  String? mobilePhone;
  String? registerCountryCode;
  String? countryName;
  String? lastLoginTime;
  String? registerTime;
  bool? isRelateToy;

  UserModel(
      {this.userId,
        this.anchorId,
        this.userAccount,
        this.nickName,
        this.familyName,
        this.nickNameStatus,
        this.avatar,
        this.userType,
        this.registerAreaCode,
        this.mobilePhone,
        this.registerCountryCode,
        this.countryName,
        this.lastLoginTime,
        this.registerTime,
        this.isRelateToy});

  UserModel.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    anchorId = json['anchorId'];
    userAccount = json['userAccount'];
    nickName = json['nickName'];
    familyName = json['familyName'];
    nickNameStatus = json['nickNameStatus'];
    avatar = json['avatar'];
    userType = json['userType'];
    registerAreaCode = json['registerAreaCode'];
    mobilePhone = json['mobilePhone'];
    registerCountryCode = json['registerCountryCode'];
    countryName = json['countryName'];
    lastLoginTime = json['lastLoginTime'];
    registerTime = json['registerTime'];
    isRelateToy = json['isRelateToy'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userId'] = this.userId;
    data['anchorId'] = this.anchorId;
    data['userAccount'] = this.userAccount;
    data['nickName'] = this.nickName;
    data['familyName'] = this.familyName;
    data['nickNameStatus'] = this.nickNameStatus;
    data['avatar'] = this.avatar;
    data['userType'] = this.userType;
    data['registerAreaCode'] = this.registerAreaCode;
    data['mobilePhone'] = this.mobilePhone;
    data['registerCountryCode'] = this.registerCountryCode;
    data['countryName'] = this.countryName;
    data['lastLoginTime'] = this.lastLoginTime;
    data['registerTime'] = this.registerTime;
    data['isRelateToy'] = this.isRelateToy;
    return data;
  }
}

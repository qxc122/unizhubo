class RoomMemberModel {
  int? adminType;
  String? avatar;
  String? countryCode;
  String? level;
  String? nickName;
  String? personalSignature;
  String? silver;
  int? sex;
  int? userType;
  String? userId;

  RoomMemberModel(
      {this.adminType,
        this.userId,
        this.avatar,
        this.countryCode,
        this.level,
        this.nickName,
        this.personalSignature,
        this.silver,
        this.sex,
        this.userType});

  RoomMemberModel.fromJson(Map<String, dynamic> json) {
    adminType = json['adminType'];
    userId = json['userId'].toString();
    avatar = json['avatar'];
    countryCode = json['countryCode'];
    level = json['level'].toString();
    nickName = json['nickName'];
    personalSignature = json['personalSignature'];
    silver = json['silver'].toString();
    sex = json['sex'];
    userType = json['userType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['adminType'] = this.adminType;
    data['userId'] = this.userId;
    data['avatar'] = this.avatar;
    data['countryCode'] = this.countryCode;
    data['level'] = this.level;
    data['nickName'] = this.nickName;
    data['personalSignature'] = this.personalSignature;
    data['silver'] = this.silver;
    data['sex'] = this.sex;
    data['userType'] = this.userType;
    return data;
  }
}

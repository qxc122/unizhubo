class ContributionModel {
  String? avatar;
  String? fireDifference;
  String? fireNum;
  String? userId;
  String? userLevel;
  String? userName;

  ContributionModel(
      {this.avatar,
        this.fireDifference,
        this.fireNum,
        this.userId,
        this.userLevel,
        this.userName});

  ContributionModel.fromJson(Map<String, dynamic> json) {
    avatar = json['avatar'];
    fireDifference = json['fireDifference'].toString();
    fireNum = json['fireNum'].toString();
    userId = json['userId'].toString();
    userLevel = json['userLevel'].toString();
    userName = json['userName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['avatar'] = this.avatar;
    data['fireDifference'] = this.fireDifference;
    data['fireNum'] = this.fireNum;
    data['userId'] = this.userId;
    data['userLevel'] = this.userLevel;
    data['userName'] = this.userName;
    return data;
  }
}

class BarrageModel {
  String? avatar;
  String? barrageContent;
  String? giftName;
  String? giftNumber;
  int? giftType;
  String? givingTime;
  String? price;
  String? userAccount;
  String? userAccountAnchor;
  String? userLevel;
  String? userName;

  BarrageModel(
      {this.avatar,
        this.barrageContent,
        this.giftName,
        this.giftNumber,
        this.giftType,
        this.givingTime,
        this.price,
        this.userAccount,
        this.userAccountAnchor,
        this.userLevel,
        this.userName});

  BarrageModel.fromJson(Map<String, dynamic> json) {
    avatar = json['avatar'];
    barrageContent = json['barrageContent'];
    giftName = json['giftName'];
    giftNumber = json['giftNumber'].toString();
    giftType = json['giftType'];
    givingTime = json['givingTime'];
    price = json['price'].toString();
    userAccount = json['userAccount'];
    userAccountAnchor = json['userAccountAnchor'];
    userLevel = json['userLevel'].toString();
    userName = json['userName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['avatar'] = this.avatar;
    data['barrageContent'] = this.barrageContent;
    data['giftName'] = this.giftName;
    data['giftNumber'] = this.giftNumber;
    data['giftType'] = this.giftType;
    data['givingTime'] = this.givingTime;
    data['price'] = this.price;
    data['userAccount'] = this.userAccount;
    data['userAccountAnchor'] = this.userAccountAnchor;
    data['userLevel'] = this.userLevel;
    data['userName'] = this.userName;
    return data;
  }
}

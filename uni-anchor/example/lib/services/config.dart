import 'dart:io';

class GlobalConfig {
  //static String production = "http://api.uat.radiotimucin.com"; /// uat
  // static String production =  "https://api.pre.radiotimucin.com"; /// pre
  // static String production = "http://api.91momo50.vip"; /// 开发
   static String production = "https://api.shopkarterj.com"; /// 正式
  ///当前版本号和版本名称，由于ios打tf包只能用1.0版本，所以升级判断要用自己的配置文件
  static int andVersionCode = 100;
  static String andVersionName = '1.0.0';
  static int iosVersionCode = 100;
  static String iosVersionName = '1.0.0';
  //aes加密得key
  static String aesKey = '==';
  // keys
  static String parameterKey = "";
  static String deviceType = Platform.isIOS ? 'ios' : 'android';
  static String isTest = "1"; /// 是否调试
}

abstract class LocalRepositoryInterface {
  Future<String> saveToken(String token);
  Future<String> getToken();
  Future<bool> saveIsFirst(bool isFirst);
  Future<bool> getIsFirst();
  Future<void> clearAllData();
}

import 'package:get/get.dart';

import '../services/local_storage_repository.dart';
import '../views/layout/binding.dart';
import '../views/splash/binding.dart';
import 'local_repository_impl.dart';

class MainBinding extends Bindings {
  @override
  void dependencies() async {
    Get.lazyPut<LocalRepositoryInterface>(() => LocalRepositoryImpl(),
        tag: 'local', fenix: true);
    Get.put(SplashBinding());
    Get.put(LayoutBinding());
  }
}

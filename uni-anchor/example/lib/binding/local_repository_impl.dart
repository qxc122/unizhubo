import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../services/local_storage_repository.dart';

const SURTOKEN = 'SUR_TOKEN';
const SURUSER = 'SUR_USER';
const ISFIRST = 'ISFIRST';

class LocalRepositoryImpl extends LocalRepositoryInterface {
  @override
  Future<void> clearAllData() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.clear();
  }

  @override
  Future<String> saveToken(String token) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setString(SURTOKEN, token);
    return token;
  }

  @override
  Future<String> getToken() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String token = sp.getString(SURTOKEN).toString();
    return token;
  }

  @override
  Future<bool> getIsFirst() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    bool isFirst = sp.getBool(ISFIRST) ?? false;
    return Future.value(isFirst);
  }
  @override

  Future<bool> saveIsFirst(bool isFirst) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setBool(ISFIRST, isFirst);
    return Future.value(isFirst);
  }
}

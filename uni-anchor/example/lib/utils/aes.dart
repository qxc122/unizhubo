import 'dart:convert';
import 'dart:developer';
import 'dart:typed_data';

import 'package:encrypt/encrypt.dart';

/// 通用key
/// const commonKey = "ytojWVpX|to_kl5k";

 const commonKey = "c1kgVioySoUVimtw";

class EncryptUtil {
  /// 图片key
  static String imageKey = "qs0fIlSFMMZxOuzD";

  /// 视频key
  static String videoKey = "elZNmenx4XPxah3n";

  static List<int> bytes = utf8.encode(imageKey);

  //aes图片加密
  String aesEncode(String content) {
    try {
      final key = Key.fromBase64(base64Encode(bytes));
      final encrypter = Encrypter(AES(key, mode: AESMode.cbc));
      final encrypted =
      encrypter.encrypt(content, iv: IV.fromBase64(base64Encode(bytes)));
      return encrypted.base64;
    } catch (err) {
      print("aes encode error:$err");
      return content;
    }
  }

  //aes解密
  static dynamic aesDecode(dynamic base64) {
    try {
      final key = Key.fromBase64(base64Encode(bytes));
      final encrypter = Encrypter(AES(key, mode: AESMode.ecb));
      return encrypter.decrypt64(base64,
          iv: IV.fromBase64(base64Encode(bytes)));
    } catch (err) {
      print("aes decode error:$err");
      return base64;
    }
  }

  ///AES通用加密
  static dynamic aesCommonEncode(dynamic data, {key = commonKey}) {
    try {
      final encrypter = Encrypter(AES(Key.fromUtf8(key)));
      final iv = IV.fromLength(16);
      final encrypted = encrypter.encrypt(data, iv: iv);
      return encrypted.base64;
    } catch (err) {
      print("aes encode error:$err");
    }
  }

  ///AES通用解密
  static dynamic aesCommonDecode(dynamic base64) {
    try {
      final key = Key.fromUtf8(commonKey);
      final encrypter = Encrypter(AES(key, mode: AESMode.ecb));
      return encrypter.decrypt64(base64, iv: IV.fromLength(16));
    } catch (err) {
      print("aes decode error:$err");
      return base64;
    }
  }


  static dynamic aesDecode3(dynamic plainText) async {
    try {
      final key = Key.fromUtf8(imageKey);
      final encrypter = Encrypter(AES(key, mode: AESMode.ecb));
      var i = encrypter.decrypt64(plainText, iv: IV.fromLength(128));
      // 把解密后的对象再转为base64编码,这步是关键,跟解密文字不同 utf8.encode(i)
      // i = i.substring(0,i.lastIndexOf("}") + 1);
      log(i);

      var d64 = base64Encode(utf8.encode(i));
      var url = 'data:image/png;base64,' + d64;
      log(url);
      // new String.fromCharCode(i);
      return url;
    } catch (err) {
      print("aes decode error:$err");
      return base64;
    }
  }

  static Uint8List base64ToUint8Array(String base64Str) {
    // Decode the base64 string to a list of bytes
    final bytes = base64.decode(base64Str);

    // Convert the list of bytes to a Uint8List
    return Uint8List.fromList(bytes);
  }
}

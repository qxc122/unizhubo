String durationTransform(int seconds) {
  var d = Duration(seconds:seconds);
  List<String> ds = d.toString().split('.');
  List<String> parts = ds[0].toString().split(':');
  return '${parts[0]}:${parts[1]}:${parts[2]}';
}

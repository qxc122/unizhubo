import 'dart:convert';
import 'package:convert/convert.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert' as convert;
//import 'package:common_utils/common_utils.dart';

import 'package:agora_rtc_rawdata_example/services/config.dart';

// md5 加密
String generateMD5(String data) {
    var content = new Utf8Encoder().convert(data);
    var digest = md5.convert(content);
    // 这里其实就是 digest.toString()
    return hex.encode(digest.bytes);
}

class ParameterUtil {
    static transformParameter(json) {
        json['sign'] = '';
        List<String> keys = json.keys.toList();
        // key排序
        keys.sort((a, b) {
            List<int> al = a.codeUnits;
            List<int> bl = b.codeUnits;
            for (int i = 0; i < al.length; i++) {
            if (bl.length <= i) return 1;
            if (al[i] > bl[i]) {
                return 1;
            } else if (al[i] < bl[i]) return -1;
        }
            return 0;
        });
        var treeMap = Map();
        print(keys);
        keys.forEach((element) {
            treeMap[element] = json[element].toString().isNotEmpty? json[element]: "";
        });
        print(treeMap);
        String jsonString = convert.jsonEncode(treeMap);
        print(jsonString);
        print(5555);
        var sign = generateMD5(jsonString + GlobalConfig.parameterKey);
        json["sign"] = sign;
        print(json);
        return json;
    }


    static regPhoneExp(String phone) {
        RegExp exp = RegExp(
            r'^((13[0-9])|(14[0-9])|(15[0-9])|(16[0-9])|(17[0-9])|(18[0-9])|(19[0-9]))\d{8}$');
        bool matched = exp.hasMatch(phone);
        print(phone);
        print(matched);
        return matched;
    }


//    static conversionNum(dynamic number) {
//        double n;
//        if (number is double)
//            n = number;
//        else
//            n = NumUtil.getDoubleByValueStr(number);
//        if (n >= 10000) {
//            return formatNum(n / 10000, pos: 2).toString() + 'W';
//        } else if (n < 1000) {
//            return cutZero(n);
//        } else {
//            return formatNum(n, pos: 2).toString();
//        }
//    }

    //保留几位小数
    static formatNum(var num, {int pos = 8}) {
//        return NumUtil.getNumByValueStr(num.toString(), fractionDigits: pos).toString();

        /*if ((num.toString().length - num.toString().lastIndexOf(".") - 1) <
            postion) {
          //小数点后有几位小数
          return cutZero(num.toStringAsFixed(postion)
              .substring(0, num.toString().lastIndexOf(".") + postion + 1)
              .toString());
        } else {
          return cutZero(num.toString()
              .substring(0, num.toString().lastIndexOf(".") + postion + 1)
              .toString());
        }*/
    }

    // 去掉小数末尾的0
    static cutZero(old) {
        // 拷贝一份 返回去掉零的新串
        return double.parse(old.toString()).toString();
    }
}

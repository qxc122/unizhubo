import 'package:platform_device_id/platform_device_id.dart';

class DeviceUtils {
  static DeviceUtils of() => DeviceUtils();

  /// 待优化，需缓存到本地，并且不存在的时候，需要自动生成一个
  Future<String?> getDeviceId() async{
    var deviceId = PlatformDeviceId.getDeviceId;
    return deviceId;
  }
}

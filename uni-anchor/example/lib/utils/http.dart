import 'dart:async';
import 'dart:math';

import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:get/get.dart';

import '../binding/local_repository_impl.dart';
import '../services/config.dart';
import 'device_utils.dart';

class HttpUtil {
  final LocalRepositoryImpl localRepositoryImpl = LocalRepositoryImpl();

  // 单例模式
  static final HttpUtil _instance = HttpUtil._internal();

  factory HttpUtil() => _instance;

  HttpUtil._internal() {
    init();
  }

  Dio? dio;

  init() {
    // BaseOptions、Options、RequestOptions 都可以配置参数，优先级别依次递增，且可以根据优先级别覆盖参数
    print("baseUrl：${GlobalConfig.production}");
    BaseOptions options = new BaseOptions(
      // 请求基地址,可以包含子路径
      baseUrl: GlobalConfig.production,

      // baseUrl: storage.read(key: STORAGE_KEY_APIURL) ?? SERVICE_API_BASEURL,
      //连接服务器超时时间，单位是毫秒.
      connectTimeout: 50000,

      // 响应流上前后两次接受到数据的间隔，单位为毫秒。
      receiveTimeout: 5000,

      // Http请求头.
      headers: {},
      // "Authorization": "Bearer " + localRepositoryImpl.getToken().toString(),
      contentType: 'application/json; charset=utf-8',
      responseType: ResponseType.json,
    );

    dio = Dio(options);

    //begin为了解决测试抓包问题。。打正式包的时候。需要注释掉
    // (dio?.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
    //     (client) {
    //   //这一段是解决安卓https抓包的问题
    //   client.badCertificateCallback =
    //       (X509Certificate cert, String host, int port) {
    //     return Platform.isAndroid;
    //   };
    //   client.findProxy = (uri) {
    //     return "PROXY 192.168.1.6:8888";
    //   };
    // };
    //end

    num timeNum = 0;

    // Cookie管理
    CookieJar cookieJar = CookieJar();
    dio?.interceptors.add(CookieManager(cookieJar));
    dio?.interceptors
        .add(InterceptorsWrapper(onRequest: (options, handler) async {
      String token = await SpUtils.getToken() ?? '';
      String timep = DateTime.now().millisecondsSinceEpoch.toString();
      String randomNum = Random().nextInt(100).toString();
      String url = options.path.split("/").last;
      options.headers["lang"] = "zh_CN";
      options.headers["unilive-appleType"] = "1";
      options.headers["unilive-deviceId"] =
          await DeviceUtils.of().getDeviceId();
      options.headers["unilive-devices"] = "HONORRNA-AN00";
      options.headers["unilive-random"] = randomNum;

      /// options.headers["unilive-signature"] = EncryptUtil.aesCommonEncode("$timep||$randomNum||$token||$url");
      options.headers["unilive-signature"] = "1";
      options.headers["unilive-source"] = GlobalConfig.deviceType;
      options.headers["unilive-timestamp"] = timep;
      options.headers["unilive-url"] = url;
      options.headers["isTest"] = GlobalConfig.isTest;
      if (token != '') {
        options.headers["Authorization"] = token;
      }
      if (options.uri.toString().contains("upload")) {
        options.headers['content-type'] = "multipart/form-data";
      }
      print("【请求参数headers:${options.headers.toString()}】");
      // Do something before request is sent
      return handler.next(options); //continue
      // 如果你想完成请求并返回一些自定义数据，你可以resolve一个Response对象 `handler.resolve(response)`。
      // 这样请求将会被终止，上层then会被调用，then中返回的数据将是你的自定义response.
      //
      // 如果你想终止请求并触发一个错误,你可以返回一个`DioError`对象,如`handler.reject(error)`，
      // 这样请求将被中止并触发异常，上层catchError会被调用。
    }, onResponse: (response, handler) {
      // print('响应：${response}');
//      BotToast.closeAllLoading();
      if (response.data["code"] == 401) {
        Get.offAllNamed("/login");
      } else {
        return handler.next(response);
      }

//       if (response.data["code"] == -1) {
//
//         // var controller = Get.find<LayoutController>();
//         // controller.resErrorMsg.value = response.data["msg"];
//         // Get.dialog(MaintainLog(), barrierDismissible: false);
//       } else {
//
//
//       }

      // Do something with response data
//      return handler.next(response); // continue
      // 如果你想终止请求并触发一个错误,你可以 reject 一个`DioError`对象,如`handler.reject(error)`，
      // 这样请求将被中止并触发异常，上层catchError会被调用。
    }, onError: (DioError e, handler) {
      BotToast.closeAllLoading();
      // Do something with response error
      return handler.next(e); //continue
      // 如果你想完成请求并返回一些自定义数据，可以resolve 一个`Response`,如`handler.resolve(response)`。
      // 这样请求将会被终止，上层then会被调用，then中返回的数据将是你的自定义response.
    }));
  }

  /// restful get 操作
  Future get(
    String path, {
    dynamic queryParameters,
    Options? options,
  }) async {
    // print(GlobalConfig.deviceType);
    var response = await dio?.get(
      path,
      queryParameters: queryParameters,
      options: options,
    );
    return response?.data;
  }

  Future post(
    String path, {
    dynamic data,
    Options? options,
  }) async {
    print("请求参数：【\r\n path：$path \r\ndata：$data\r\n options：$options\r\n】");
    var response = await dio?.post(
      path,
      data: data,
      options: options,
    );
    return response?.data;
  }

  Future delete(
    String path, {
    dynamic data,
    required Options options,
  }) async {
    var response = await dio?.delete(
      path,
      data: data,
      options: options,
    );
    return response?.data;
  }

  Future put(
    String path, {
    dynamic data,
    required Options options,
  }) async {
    var response = await dio?.put(
      path,
      data: data,
      options: options,
    );
    return response?.data;
  }

  ///清除授权
  clearAuthorization() async {
    await localRepositoryImpl.clearAllData();
  }

  ///获取授权token
  getAuthorization() async {
    return await localRepositoryImpl.getToken();
  }
}

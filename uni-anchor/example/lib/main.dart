import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_rawdata_example/views/layout/binding.dart';
import 'package:agora_rtc_rawdata_example/routes/app_pages.dart';
import 'package:agora_rtc_rawdata_example/themes/app_theme.dart';
import 'package:agora_rtc_rawdata_example/utils/logger.dart';
import 'package:agora_rtc_rawdata_example/utils/sp_utils.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'services/global_config.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SpUtils.init();
  await initServices();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  runApp(MyApp());
}

Future<void> initServices() async {
  print('starting services ...');
  // LogUtil.init(true);
//  LogUtil.init(isDebug: true);
  await Get.putAsync(() => GlobalConfigService().init());
  // await Get.putAsync(SettingsService()).init();
  print('All services started...');
}


class MyApp extends StatelessWidget {
  final easyload = EasyLoading.init();
  final botToastBuilder = BotToastInit();

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(750, 1334),
        builder: (context, child) {
          return GetMaterialApp(
          title: 'B球直播(F)',
          theme: AppTheme.light,
          darkTheme: AppTheme.light,
          themeMode: ThemeMode.system,
          debugShowCheckedModeBanner: false,
          initialRoute: AppRoutes.Splash,
          getPages: AppPages.routes,
          builder: (context, child) {
            child = easyload(context, child);
            child = botToastBuilder(context,child);
            child = Scaffold(
              // Global GestureDetector that will dismiss the keyboard
                body: GestureDetector(
                  onTap: () => hideKeyboard(context),
                  child: child,
                ));
            // return child;
            return MediaQuery(
              //设置文字大小不随系统设置改变
              data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
              child: child,
            );
          },
          navigatorObservers: [BotToastNavigatorObserver()],
          unknownRoute: AppPages.unknownRoute,
          enableLog: true,
          logWriterCallback: Logger.write,
          initialBinding: LayoutBinding(),
        );});
  }
}

void hideKeyboard(BuildContext context) {
  FocusScopeNode currentFocus = FocusScope.of(context);
  if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
    FocusManager.instance.primaryFocus?.unfocus();
  }
}

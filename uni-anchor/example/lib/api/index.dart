import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:agora_rtc_rawdata_example/utils/http.dart';

/// api
class API {
  /// 家长/主播登录
  static login(data) async {
    var response = await HttpUtil().post('/anchor/account/app/v1/login', data: data);
    return response;
  }

  /// 主播/家族长退出登录
  static logout() async {
    var response = await HttpUtil().post('/anchor/account/app/v1/logout');
    return response;
  }

  /// 主播/家族长基本信息
  static getUserInfo() async {
    var response = await HttpUtil().post('/anchor/account/app/v1/getUserInfo');
    return response;
  }



  /// 主播收入详情
  static anchorIncomeDetails(data) async {
    var response = await HttpUtil().post('/anchor/account/app/v1/anchorIncomeDetails', data: data);
    return response;
  }

  /// 主播月收入/支出统计
  static anchorIncomeMonth(data) async {
    var response = await HttpUtil().post('/anchor/account/app/v1/anchorIncomeMonth', data: data);
    return response;
  }

  /// 家长或主播查询自己的资产
  static assets() async {
    var response = await HttpUtil().post('/anchor/account/app/v1/assets');
    return response;
  }

  /// 主播查看自己的直播记录，最近三十天
  static liveHistory() async {
    var response = await HttpUtil().post('/anchor/account/app/v1/liveHistory');
    return response;
  }

  /// 获取当前语言的客服
  static getOnlineService() async {
    var response = await HttpUtil().post('/anchor/account/app/v1/getOnlineService');
    return response;
  }

  /// 主播查询当日直播时长(直播时长)
  static liveTime() async {
    var response = await HttpUtil().post('/anchor/account/app/v1/liveTime');
    return response;
  }

  /// 主播修改自己的昵称
  static modifNickName(data) async {
    var response = await HttpUtil().post('/anchor/account/app/v1/modifNickName', data: data);
    return response;
  }

  /// 平台主播排行榜
  static rankingAnchor(data) async {
    var response = await HttpUtil().post('/anchor/account/app/v1/rankingAnchor', data: data);
    return response;
  }

  /// 家长/主播修改自己的密码
  static resetPassword(data) async {
    var response = await HttpUtil().post('/api/login/app/v1/resetPassword', data: data);
    return response;
  }

  /// 主播修改自己的头像
  static updateAvatar(data) async {
    var response = await HttpUtil().post('/anchor/account/app/v1/updateAvatar', data: data);
    return response;
  }

  /// 上传单个图片，form提交方式
  static uploadSingleImg(data) async {
    // /anchor/upload/app/v1/uploadSingleImg
    // /upload/v1/uploadPicture
    var response = await HttpUtil().post('/upload/v1/uploadPicture', data: data);
    return response;
  }

  /// 获取APP版本信息
  static queryAppVersion() async {
    var response = await HttpUtil().get('/anchor/sys/queryAppVersion?proName=bball');
    return response;
  }

  /// 获取国家列表
  static getCountryList(data) async {
    var response = await HttpUtil().post('/anchor/sys/v1/getCountryList', data: data);
    return response;
  }

  /// 通过md5获取Url地址
  static getFileUrl(data) async {
    var response = await HttpUtil().post('/anchor/upload/pc/v1/getFileUrl', data: data);
    return response;
  }

  /// 获取推流和拉流的token
  static getFlowToken(data) async {
    var response = await HttpUtil().post('/anchor/account/app/v1/getFlowToken',data: data);
    return response;
  }

  /// 获取IM的token
  static getImToken() async {
    var response = await HttpUtil().post('/anchor/account/app/v1/getImToken');
    return response;
  }

  /// 设置支付密码
  static setPayPassword(data) async {
    var response = await HttpUtil().post('/api/userCenter/app/v1/setPayPassword', data: data);
    return response;
  }

  /// 修改支付密码
  static modifyPayPassword(data) async {
    var response = await HttpUtil().post('/api/userCenter/app/v1/modifyPayPassword', data: data);
    return response;
  }

  // /// 修改支付密码
  // static resetPassword(data) async {
  //   var response = await HttpUtil().post('/api/login/app/v1/resetPassword', data: data);
  //   return response;
  // }

  /// 设置支付密码(短信验证)
  static setPayPasswordByMessage(data) async {
    var response = await HttpUtil().post('/api/userCenter/app/v1/setPayPasswordByMessage', data: data);
    return response;
  }

  /// 设置支付密码(短信验证)
  static findPassword(data) async {
    var response = await HttpUtil().post('/api/login/app/v1/findPassword', data: data);
    return response;
  }

  /// 获取用户基本信息
  static getUserInfo2() async {
    var response = await HttpUtil().post('/api/userCenter/app/v1/getUserInfo');
    return response;
  }


  /// 主播开播
  static liveBegin(data) async {
    var response = await HttpUtil().post('/anchor/live/app/v1/begin',data:data);
    return response;
  }

  /// 获取openssl生成直播密钥
  static getOpenSSLKey() async {
    var response = await HttpUtil().get('/anchor/live/v1/getOpenSSLKey');
    return response;
  }


  /// 主播下播
  static liveClose() async {
    var response = await HttpUtil().post('/anchor/live/app/v1/close');
    return response;
  }

  /// 获取主播当前直播间信息
  static getStudioInfo() async {
    var response = await HttpUtil().post('/anchor/live/getStudioInfo');
    return response;
  }

  /// 获取主播当前开播状态
  static getStudioStatus() async {
    var response = await HttpUtil().post('/anchor/live/getStudioStatus');
    return response;
  }

  /// 直播间用户扩展信息
  static getUserExpandInfo() async {
    var response = await HttpUtil().post('/anchor/live/app/v1/getUserExpandInfo');
    return response;
  }

  /// 主播修改直播间封面
  static updateStudioThumbImage(data) async {
    var response = await HttpUtil().post('/anchor/live/app/v1/updateStudioThumbImage',data:data);
    return response;
  }


  /// 获取直播间弹幕信息
  static getRoomBarrageList(data) async {
    var response = await HttpUtil().post('/anchor/live/app/v1/getRoomBarrageList',data:data);
    return response;
  }


  /// 获取直播间礼物信息
  static getRoomGiftList(data) async {
    var response = await HttpUtil().post('/anchor/live/app/v1/getRoomGiftList',data:data);
    return response;
  }

  /// 获取直播间游戏信息
  static getRoomGameList(data) async {
    var response = await HttpUtil().post('/anchor/live/app/v1/getRoomGameList',data:data);
    return response;
  }


  /// 管理员列表
  static getAdminList(data) async {
    var response = await HttpUtil().post('/anchor/live/app/v1/getAdminList',data:data);
    return response;
  }

  /// 被禁言用户列表
  static getBannedList(data) async {
    var response = await HttpUtil().post('/anchor/live/app/v1/getBannedList',data:data);
    return response;
  }

  /// 被踢用户列表
  static getKickingList(data) async {
    var response = await HttpUtil().post('/anchor/live/app/v1/getKickingList',data:data);
    return response;
  }

  /// 直播间贡献榜
  static getContributionList(data) async {
    var response = await HttpUtil().post('/anchor/live/app/v1/getContributionList',data:data);
    return response;
  }

  /// 主播开房间时候，可以选择的彩票列表
  static getLottery() async {
    var response = await HttpUtil().post('/anchor/live/app/v1/getLottery');
    return response;
  }

  /// 直播间进入的时候请求公告
  static getByType(data) async {
    var response = await HttpUtil().post('/api/advNotice/app/v1/getByType',data: data);
    return response;
  }

  // /// 通过房间号获取直播间详情,进入房间之前请求的接口 studioNum
  // static getRoomDetail(data) async {
  //   var response = await HttpUtil().post('/api/live/app/v1/getRoomDetail',data: data);
  //   return response;
  // }

  /// 查询直播间在线观众50个
  static onlineUsers(data) async {
    var response = await HttpUtil().post('/anchor/live/app/v1/onlineUsers',data: data);
    return response;
  }

  /// 主播开启/关闭直播间状态为收费 productId
  static switchCharge(data) async {
    var response = await HttpUtil().post('/anchor/live/app/v1/switchCharge',data: data);
    return response;
  }


  /// 房间的商品列表
  static roomProducts(data) async {
    var response = await HttpUtil().post('/anchor/live/app/v1/roomProducts',data: data);
    return response;
  }


  /// 当前家族下的主播列表(主播管理-主播列表)
  static anchorList(data) async {
    var response = await HttpUtil().post('/anchor/family/app/v1/anchorList',data: data);
    return response;
  }

  /// 家长冻结/解冻主播账号(主播管理-封停)
  static frozenAnchor(data) async {
    var response = await HttpUtil().post('/anchor/family/app/v1/frozenAnchor',data: data);
    return response;
  }

  /// 家长修改主播密码(主播管理-修改主播密码)
  static resetAnchorPassword(data) async {
    var response = await HttpUtil().post('/anchor/family/app/v1/resetAnchorPassword',data: data);
    return response;
  }

  /// 主播报表(家族长-主播报表)
  static anchorLiveTimeList(data) async {
    var response = await HttpUtil().post('/anchor/family/app/v1/anchorLiveTimeList',data: data);
    return response;
  }

  /// 家族长的财务记录(财务对账)
  static financialRecord(data) async {
    var response = await HttpUtil().post('/anchor/family/app/V1/financialRecord',data: data);
    return response;
  }

  /// 查询可以提现额，主播可提现额(转化提现)
  static canWithdraw() async {
    var response = await HttpUtil().post('/anchor/family/app/v1/canWithdraw');
    return response;
  }


  /// 查询名下主播的可提现列表(转化提现-主播账户)
  static canWithdrawAnchor() async {
    var response = await HttpUtil().post('/anchor/family/app/v1/canWithdrawAnchor');
    return response;
  }

  /// 提现主播金额，不传userid 即全部(转化提现-主播账户-提现)
  static withdrawAnchor(data) async {
    var response = await HttpUtil().post('/anchor/family/app/V1/withdrawAnchor', data: data);
    return response;
  }

  /// 家族长的提现index页面(转化提现-账户提现-页面数据)
  static withdrawIndex() async {
    var response = await HttpUtil().post('/anchor/family/app/V1/withdrawIndex');
    return response;
  }

  /// 家族长提现余额到银行卡(转化提现-账户提现-提现)
  static withdrawCash(data) async {
    var response = await HttpUtil().post('/anchor/family/app/V1/withdrawCash', data: data);
    return response;
  }

  /// 获取银行下拉列表(绑定银行卡-银行选择)
  static getBankList() async {
    var response = await HttpUtil().post('/anchor/family/app/V1/getBankList');
    return response;
  }

  /// 新增银行卡信息(银行卡管理-绑定银行卡)
  static addBank(data) async {
    var response = await HttpUtil().post('/anchor/family/app/V1/addBank', data: data);
    return response;
  }

  /// 发送短信验证码
  static sendSms(data) async {
    var response = await HttpUtil().post('/anchor/sms/v1/sendSms', data: data);
    return response;
  }

  /// 家族长的银行卡列表(账户提现-银行卡选择)
  static bankList() async {
    var response = await HttpUtil().post('/anchor/family/app/V1/bankList');
    return response;
  }

  /// 随机生成图片
  static randCode() async {
    var response = await HttpUtil().post('/api/sys/v1/randCode');
    return response;
  }


  /// 用户删除银行卡信息
  static delete(data) async {
    var response = await HttpUtil().post('/api/memBank/app/V1/delete', data: data);
    return response;
  }

  /// 用户删除银行卡信息
  static getNewestSgInfoByIds(data) async {
    var response = await HttpUtil().post('/anchor/live/getNewestSgInfoByIds.json', data: data);
    return response;
  }















}
